(*
 *
 * Test about AbstractSeven module (test 1)
 * Test of the function "add_place_of_node"
 *
 *)

(* Init system *)
let d = 4;;
let s =
  Parikhproc.make_system_of_word_list
    d
    [
      [0;2;1;3];
      [3;1;2;0];
      [1;3;0;2]
    ];;

(* Configuration *)
let conf =
  {
    Configuration.conf_verbose = 1;
    Configuration.conf_approximation = Configuration.default_apx_config;
    Configuration.conf_pdf_output = None;
    Configuration.conf_graph_output = None;
    Configuration.conf_seven_output = None;
    Configuration.conf_display_net = false;
    Configuration.conf_classifier = "";
    Configuration.conf_table = None;
    Configuration.conf_input = ""
  };;

(* Synthesis *)
let t = Parikhproc.quotient s;;
let r = Synthesis.make_region_spaces t;;
let w = Separation.init t;;
Synthesis.synthesize conf r w;;
Separation.elim_redundent w;;    
  
(* Abstract seven initial *)  
let a7 = (AbstractSeven.make (Separation.dimension w));;
A7Work.print stdout a7;;

(* test of the function "add_place_of_node" *)
let m = (AbstractSeven.get_node_mapping a7);;
let m_dim = Parikh.dimension m;;
let c = AbstractSeven.urn_count;;
let q = (Separation.get_initial_vertex w);;
let v = (Separation.parikh_of_state q);;
let v_dim = (Vector.length v);;
let u = (AbstractSeven.get_urn_of_node m c q);;
let p = (AbstractSeven.define_place u);;
let a7' = (AbstractSeven.add_place a7 p);;
A7Work.print stdout a7';;

