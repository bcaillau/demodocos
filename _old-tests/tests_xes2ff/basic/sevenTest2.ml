(*
 *
 * Test about AbstractSeven module (test 2)
 * Abstract seven making
 * Simple example
 *
 *)

(* Init system *)
let d = 2;;
let s =
  Parikhproc.make_system_of_word_list
    d
    [
      [0;0;1];
      [0];
      [1;1;1;0]
    ];;

(* Configuration *)
let conf =
  {
    Configuration.conf_verbose = 1;
    Configuration.conf_approximation = Configuration.default_apx_config;
    Configuration.conf_pdf_output = None;
    Configuration.conf_graph_output = None;
    Configuration.conf_seven_output = None;
    Configuration.conf_display_net = false;
    Configuration.conf_classifier = "";
    Configuration.conf_table = None;
    Configuration.conf_input = ""
  };;

(* Synthesis *)
let t = Parikhproc.quotient s;;
let r = Synthesis.make_region_spaces t;;
let w = Separation.init t;;
Synthesis.synthesize conf r w;;
Separation.elim_redundent w;;    
  
(* Initial abstract seven *)  
let a7 = (AbstractSeven.make (Separation.dimension w));;
A7Work.print stdout a7;;

(* Abstract seven making with main function *)   
let a7' = (A7Work.abstract_seven_of_system w);;
A7Work.print stdout a7';;

