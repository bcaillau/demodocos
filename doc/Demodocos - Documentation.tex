\documentclass[a4paper, french, 10pt, halfparskip+, parindent]{article}
% essayer parskip, parskip+, parskip*, parskip- halfparskip, halfparskip+, halfparskip*, halfparskip-
% essayer sans parindent
%essayer 9pt, 8pt  ou plus petit
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{babel}
\usepackage{fancybox}
\usepackage[pdftex]{graphicx}

\usepackage{ulem} % avec: \unline{} \uuline{} \uwave{} \sout{} \xsout{} \dashuline{} dotuline{}

\usepackage{draftcopy}

\setlength{\parskip}{1.5ex plus 1ex minus 0.5ex}

\setlength{\parindent}{0cm}

\newcommand{\tnftool}{DEMODOCOS }

\title{
DEMODOCOS
\\
Examples to Generic Scenario Models Generator
}
%\author{ }

\date{Novembre 2017}

\author{Aurélien Lamercerie}
\begin{document}

\maketitle

\section{Présentation}
\label{section:presentation}

L'outil \tnftool permet de construire un modèle générique pour une procédure donnée à partir de quelques exemples d'instances de cette procédure. Le modèle généré peut prendre la forme d'un graphe, d'un réseau Test 'n Flip ou d'un scénario SEVEN (prévu pour une intégration dans un environnement en réalité virtuelle).

L'utilisation classique de l'outil consiste à appliquer l'opération de synthèse sur un ensemble de fichiers décrivant les instances de la procédure cible. Plusieurs formats de fichiers sont supportés, dont le format standard XES pour les journaux d'évènements (logs). En sortie, plusieurs fichiers sont générés. Ces fichiers représentent la procédure générique sous différentes formes, répondant à des usages variés.

Cette application présente un intérêt limité dans le cas d'une utilisation isolée, hors contexte et sans objectif précis quand à l'usage du modèle généré. Elle a été développée dans le cadre d'un projet de recherche s'intéressant en particulier aux procédures chirurgicales, et nécessitant la génération de modèle générique pour une intégration dans un environnement de formation en réalité virtuelle. Il est également tout à fait envisageable d'appliquer la même méthode dans un autre contexte.

Ce document décrit la procédure d'installation du programme et les fonctionnalités proposées. L'outil \tnftool s'exécute en ligne de commande. Une commande principale est définie pour lancer la synthèse. Plusieurs commandes complémentaires permettent de configurer l'application et d'exécuter d'autres opérations utiles.

\normalem

\newcommand{\KILL}[1]{}
\newcommand{\AVANT}[1]{} % annexe

\renewcommand{\sout}[1]{}

\section{Installation}
\label{section:installation}

L'application est potentiellement multi-plateforme. Néanmoins, elle a été développée et testée sur un environnement LINUX. Il est probable que des ajustements soient nécessaires pour les environnements WINDOWS et MAC.

\subsection{Installation simple}
\label{section:installSimple}

Copiez les sources dans un répertoire quelconque. Si nécessaire, spécifiez la configuration locale en modifiant le fichier "makefile". Par défaut,
le répertoire d'installation est "\$(HOME)/.demodocos" (il est possible de modifier ce repertoire dans le makefile).
Ouvrez un terminal et tapez la commande "make install".

Le programme nécessite xsltproc et sed.


\subsection{Installation optimale}
\label{section:installOpt}

Copiez les sources dans un répertoire quelconque. Allez dans le répertoire "src".
Si nécessaire, spécifiez la configuration locale en modifiant le fichier "Makefile-config".

Ouvrez un terminal. Tapez la commande "make", puis "make install".

Le programme nécessite le package ocaml "Xml-Light" (qui peut être récupéré avec OPAM), le processeur xsltproc et la commande sed.


\subsection{Désinstallation}
\label{section:desinstallation}

Allez dans le répertoire des sources. Ouvrez un terminal. Tapez la commande "make uninstall". 



\section{Commandes}
\label{section:commandes}

L'outil propose une commande principale et plusieurs opérations associées. La commande principale est "dem". Elle peut être associée aux opérations suivantes :
\begin{itemize}
\item classifier (attributs permettant de classer les évènements)
\item config (configuration de l'outil)
\item synth (synthèse de procédures)
\item ontology (évaluation de la base ontologique)
\item convert (conversion au format xes à partir d'un fichier source donné)
\item quotient (calcul du graphe quotient) \\
\end{itemize}

L'utilisation classique de l'outil implique les étapes suivantes :
\begin{enumerate}
\item mise à jour des classifieurs (si nécessaire)
\item vérification et mise à jour de la configuration
\item exécution de la synthèse \\
\end{enumerate}

Un classifieur regroupe les attributs permettant de classer les événements du modèle, c'est à dire les attributs identifiant un événement et permettant de le différencier d'autres éléments.
        
L'outil doit être configuré avant de lancer la synthèse. Plusieurs variables de configuration sont proposées pour préciser le contexte d'excécution de la synthèse, spécifier les fichiers attendus en sortie et définir quelques traitements complémentaires (calcul de localisation, analyse du résultat, calcul de chemins). Pour configurer l'outil, il suffit d'utiliser la commande "dem config edit" avec la variable à configurer et la nouvelle valeur de cette variable. Il est possible de visualiser la configuration avec la commande "dem config view".

La synthèse d'un réseau TnF peut être réalisée à partir d'un fichier xes regroupant les instances à synthétiser. La synthèse est aussi possible avec les formats de fichier "iccas", "ispm2" et "ispm3". Les fichiers de scénarios SEVEN sont également gérés.

Certaines commandes supplémentaires sans synthèse sont également proposées, dont la conversion des fichiers source (commande "dem convert") et le calcul du graphe quotient (commande "dem quotient").


\subsection{Classifiers}
\label{section:classifier}

Un classifieur regroupe les attributs permettant de classer les événements du modèle, c'est à dire les attributs identifiant un événement et permettant de le différencier d'autres éléments. Les attributs possibles dépendent du format du fichier source (ou des fichiers sources) ; par conséquent, les classifiers sont définis en précisant le groupe des fichiers traités (c'est à dire le format de la source). La commande suivante permet de visualiser les configuration des classifiers. 

\begin{verbatim}
$ dem classifiers view
\end{verbatim}

On peut observer avec cette commande les différents éléments utilisés par l'outil pour prendre en compte les classifiers :
\begin{itemize}
\item separator (séparateur utilisé pour la liste des attributs)
\item prefix (préfixe utilisé en fonction du groupe)
\item keys (attributs possibles pour chaque groupe)
\item def (définition des classifiers, par groupe) \\
\end{itemize}

Pour gérer la définition des classifiers, la commande suivante est utilisée en précisant l'opération à réaliser : 

\begin{verbatim}
$ dem classifiers def
Usage: dem classifiers [def] [view|add|remove|edit] ...
\end{verbatim}

Les opérations suivantes sont proposées :
\begin{itemize}
\item view (vue des classifiers définis)
\item add (ajout d'un classifier)
\item remove (suppression d'un classifier)
\item edit (édition d'un classifier) \\
\end{itemize}

L'ajout, la suppression ou la mise à jour d'un classifier est réalisée en précisant le nom du classifier, le groupe concerné et la liste des attributs définissant le classifier. Par exemple, la commande suivante permet d'ajouter un classifier dont le nom "test", pour le groupe "ispm2", avec les attributs "verb" et "instrument" :
\begin{verbatim}
$ dem classifiers def add test ispm2 verb instrument

Add classifier: test (format group: ispm2) 
Classifier definitions: 
 (x) test (group: ispm2) 
       => verb ; instrument 
 (x) default_xes (group: xes) 
       => name
 (x) default_iccas (group: iccas) 
       => actuator ; verb  ; affected  ; instrument  ; treatedStructure 
 (x) default_ispm2 (group: ispm2) 
       => actuator ; verb  ; affected  ; instrument  ; treatedStructure 
 (x) default_ispm3 (group: ispm3) 
       => verb ; affected  ; instrument 
 (x) default_seven (group: seven) 
       => name
\end{verbatim}

L'outil propose une définition par défaut pour chaque groupe. Il est possible de modifier cette définition, ou d'ajouter de nouveaux classifiers.  Il est aussi possible de définir des classifiers dans les fichiers sources (uniquemet pour le groupe xes). Le choix du classifier utilisé par la synthèse se fait ensuite dans la configuration de l'outil.


\subsection{Configuration}
\label{section:configuration}

L'outil étant conçu pour s'adapter à différents usages. Plusieurs options sont donc proposées pour configurer la synthèse. La commande suivante permet de visualiser la configuration courante : 

\begin{verbatim}
$ dem config view
\end{verbatim}

Elle présente toutes les variables de configuration, chaque ligne indiquant le nom de la variable et sa valeur courante. Nous pouvons considérer les options proposées selon quatre ensembles :
\begin{itemize}
\item adpatation des fichiers d'entrée (input)
\item configuration de la synthèse (classifier choice, ontology...)
\item définition des sorties (output)
\item ajustement des fichiers générés (tnfgraph, seven) \\
\end{itemize}

Il est possible de configurer la prise en charge des fichiers en entrée (input) avec les variables suivantes :

\begin{verbatim}
 input_working_file_postfix
 input_adapting_script (starting_action, russiandolls or none)
\end{verbatim}


La première étape de la synthèse consiste à construire le graphe quotient à partir des instances en entrée. Ce graphe est un ensemble de places et d'évènements. Les évènements sont définis en considérant un ensemble d'attributs (cet ensemble est nommé "classifier", voir section \ref{section:classifier}). Le choix du classifier se fait avec la variable suivante :

\begin{verbatim}
 classifier_choice
\end{verbatim}

Pour construire certains fichiers, en particulier les scénarios SEVEN, il est nécessaire de s'appuyer sur un ensemble structuré des termes utilisés dans les attributs, et leurs représentations techniques. Les fichiers d'ontologie permettent de construire cette base. L'outil permet de définir le fichier de base (correspondant à l'ontologie de référence), les fichiers globaux (correspondant à des ontologies plus générales) et le type de label utilisé dans les attributs. Les variables suivantes permettent de configurer la base ontologique de l'outil :

\begin{verbatim}
 ontology_base_file
 ontology_global_files
 ontology_label_type (iri_segment or label)
\end{verbatim}

Quelques options complémentaires sont proposées pour définir le nombre d'instances de la source à prendre en compte (par défaut, toutes les instances sont prises en compte), le niveau d'affichage dans le terminal ou provoquer une approximation de la synthèse (en limitant le niveau de généralisation). Les variables suivantes permettent de configurer ces options complémentaires :

\begin{verbatim}
 instances_number
 verbose
 approximation_requested (apx_config)
 approximation_limit (apx_config)
\end{verbatim}

L'éxécution de la synthèse produit en sortie plusieurs fichiers. Il est possible de définir le nom du repertoire des fichiers sortants, et le postfixe des fichiers à produire. La mention "none" ou une chaine de caractère vide indique que le fichier ne doit pas être produit. La sortie est configurée avec les variables suivantes :

\begin{verbatim}
 output_directory
 output_tnfnet
 output_tnfgraph
 output_seven
 output_a7xspm
 output_events_table
\end{verbatim}

Enfin, quelques options sont proposées pour adapter certains fichiers en sortie. Les variables suivantes sont proposées :

\begin{verbatim}
 tnfgraph_label_type (letter, number or label)
 seven_sensor_label_type (letter, number, onto_label or five_ref)
 seven_five_value_type (onto_label or five_ref)
 seven_location_computing
 seven_analysis
 seven_event_number_limit
\end{verbatim}

L'outil est proposée avec une configuration par défaut, à mettre à jour selon les besoins.

\subsection{Synthèse}
\label{section:synthese}

La commande suivante permet d'exécuter l'algorithme de synthèse Test 'n Flip sur un fichier (ou un ensemble de fichiers) décrivant les instances de la procédures cibles :
\begin{verbatim}
$ dem synth
Usage: dem synth <source> [<other sources>]
\end{verbatim}

Cette commande prend en compte la configuration définie, et produit en sortie les fichiers attendus et un rapport d'exécution.


\subsection{Opérations complémentaires}
\label{section:operationsComplementaires}

Plusieurs opérations complémentaires sont proposées pour répondre à des besoins spécifiques. 

Il est possible d'évaluer la base ontologique produite à partir des fichiers d'ontologie (définis dans la configuration), en précisant éventuellement des fichiers sources (à synthétiser). L'évaluation simple produit le fichier "default\_ontology\_basis.txt" et l'évaluation sur un ou plusieurs fichiers sources produit la table des évènements correspondant aux sources. La commandes suivante est proposée :

\begin{verbatim}
$ dem ontology
Usage: dem ontology value [<source>] [<other sources>]
\end{verbatim}

La conversion de fichiers sources vers le format XES, sans synthèse, peut être obtenue avec la commande suivante :

\begin{verbatim}
$ dem convert
Usage: dem convert <source> [<target>]
\end{verbatim}

Enfin, l'outil permet de construire le quotient de plusieurs instances (sans appliquer la synthèse, et donc sans généralisation) avec la commande suivante :

\begin{verbatim}
$ dem quotient
Usage: dem quotient <source>
\end{verbatim}


\section{Exemples d'utilisation}
\label{section:exemples}

Le repertoire "basic\_examples" contient quelques fichiers sources pour prendre la main l'outil avec quelques exemples :

\begin{itemize}
\item ABCD (fichier XES "abcd.xes")
\item Procédure d'une simple réparation (fichier XES "simplerepair.xes")
\item Montage de poupée russe (repertoire "russiandolls" avec quelques fichiers iSPM3) \\
\end{itemize}

Un fichier "makefile" est proposé pour lancer directement la synthèse avec la commande "make" (taper "make" dans un terminal). 

Dans la suite de cette section, l'utilisation de l'outil est détaillée sur l'exemple abcd. 


\subsection*{ABCD croisés}
\label{section:exempleABCD}

Pour cette exemple, nous utilisons le classifier par défaut pour les fichiers XES.

Nous visualisons d'abord la liste des classifiers pour contrôle. Nous observons ainsi que le classifier "default\_xes" est définie avec un unique attribut, "name".

\begin{verbatim}
$ dem classifiers def view                                                               
Classifier definitions: 
 (x) default_xes (group: xes) 
       => name
 (x) default_iccas (group: iccas) 
       => actuator ; verb  ; affected  ; instrument  ; treatedStructure 
 (x) default_ispm2 (group: ispm2) 
       => actuator ; verb  ; affected  ; instrument  ; treatedStructure 
 (x) default_ispm3 (group: ispm3) 
       => verb ; affected  ; instrument 
 (x) default_seven (group: seven) 
       => name
\end{verbatim}

Les commandes d'éditon de la configuration nous permettent ensuite de choisir le classifier, préciser le repertoire de sortie et le prefix du fchier représentant le réseau TnF (qui sera donc produit en sortie) et quelques options concernant le fichier SEVEN produit (calcul des locations et analyse du fichier).

\begin{verbatim}
$ dem config edit classifier_choice 'default_xes'
[...]
 classifier_choice: default_xes
[...]
\end{verbatim}


\begin{verbatim}
$ dem config edit output_directory synth_results
$ dem config edit output_tnfnet '_tnfnet'
[...]
 output_directory: synth_results
 output_tnfnet: _tnfnet
[...]
\end{verbatim}

\begin{verbatim}
$ dem config edit seven_location_computing true
$ dem config edit seven_analysis true
[...]
 seven_location_computing: true
 seven_analysis: true
[...]
\end{verbatim}

Nous pouvons vérifier la configuration obtenue :

\begin{verbatim}
$ dem config view
 input_working_file_postfix: _input
 input_adapting_script (starting_action, russiandolls or none): -
 classifier_choice: default_xes
 ontology_base_file: -
 ontology_global_files: OntoSPM_for_Sunset.owl OntoS3PM_for_Sunset.owl FMA_for_Sunset.owl
 ontology_label_type (iri_segment or label): iri_segment                                                                           
 instances_number: -                                                                                                               
 verbose: 0                                                                                                                        
 approximation_requested (apx_config): false                                                                                       
 approximation_limit (apx_config): 1000                                                                                            
 output_directory: synth_results                                                                                                   
 output_tnfnet: _tnfnet                                                                                                            
 output_tnfgraph: _graph                                                                                                           
 output_seven: _seven                                                                                                              
 output_a7xspm: -                                                                                                                  
 output_events_table: _events_table                                                                                                
 tnfgraph_label_type (letter, number or label): letter                                                                             
 seven_sensor_label_type (letter, number, onto_label or five_ref): onto_label                                                      
 seven_five_value_type (onto_label or five_ref): onto_label                                                                          
 seven_location_computing: true
 seven_analysis: true
 seven_event_number_limit: 50
\end{verbatim}

Sur cette configuration, nous observons les points suivants :
\begin{itemize}
\item le fichier de travail correspondant aux sources portera le suffixe "\_input"
\item il n'y a pas de script adaptant le fichier source
\item le classifier choisi est "default\_xes"
\item il n'y a pas d'ontologie (les options ontology\_global\_files et ontology\_label\_type sont ignorées)
\item aucun nombre d'instance n'est définie (toutes les instances seront donc prise en compte)
\item aucune approximation n'est requise
\item le repertoire de sortie est "synth\_results"
\item l'outil produira en sortie les fichiers représentant le réseau TnF (tnfnet), le graphe de marquage correspondant (tnfgraph), le scénario SEVEN et la table des évènements (events table)
\item les labels du graphe seront des lettres
\item le scénario SEVEN utilisera les références ontologiques (références par défaut issues des sources) pour les labels des sensors et les valeurs du code FIVE
\item le scénario SEVEN sera produit avec des emplacements calculés
\item le code SEVEN produit sera analysé
\item les calculs et l'analyse du code ne seront réalisés que si le nombre d'évènements ne dépasse pas 50 \\
\end{itemize}

La commande suivante permet de lancer la synthèse sur les instances du fichier "abcd.xes" :

\begin{verbatim}
$ dem synth synth_input/abcd.xes > abcd.txt
\end{verbatim}

Le fichier "abcd.txt" contient le rapport d'exécution, avec le temps de calcul des différents étapes de la synthèse, et les résultats d'analyse du code SEVEN. Les fichiers en sortie sont générés dans le repertoire "synth\_results". La figure montre le résultat sous la forme d'un graphe.


\begin{figure}[ht!]
  \centering
\includegraphics[width=0.5\textwidth]{abcd_graph.jpg}%%
\caption{Graphe résultant de la synthèse pour l'exemple ABCD}
\label{fig:abcd_result}
\end{figure}


%\AVANT{
%\newpage
%
%\section*{Annexe}
%
%}

\end{document}