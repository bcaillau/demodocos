=======================================================================================================================
== 
== Demodocos - Changes (by version) 
== 
=======================================================================================================================


version 0.8, October 2017 (todo)
=================================
Done:
In progress:
To do:
  o Improve the execution report by adding useful information messages
  o Add command to update separator, prefix and keys of classifiers config
  o Analysis and operations on graph using module ocamlgraph
  o Improvement of the generated #7 code
  o Support of external #7 code
  o Add synthesis from VR logs
  o Add module DemControl with function to manage the executions of step
  o Improve functions to manage the classifiers (add some controls)
  o Management of the transient files via the configuration module (+ test mode to not remove transient files)
  o Revision and configuration the use of command graphviz (dot/neato)
  o Add a config parameter to save the names (tags) of config saving files, and a function to view all tags
  o Revision of TnF net generating (add generation of dot and jpg)
  o Removing of unused module(s) (xes_utilities)
  o Removing of code traces in comment (test code)
  o Revision and use of the module StringUtilities
  o Revision and use of the module TempFile
  o Simplifying a7 five values updating (direct update on module A7Work and removing of module A7Attributes)
  o Revision of seven five value type (ontology_label, five_ref) 
  o Language managing for ontology label (impacts on configuration, ontology basis input, ontology...)
  o New configuration parameter to define the set of attribute for five objects (seven output)
  o New configuration parameter to define the set of decorations (seven output)
  o Add actuators to five objects
  o Five objects depending on configuration
  o Support for five elements definition (file from VR)
  

version 0.7.6, October 2017
=================================
  o Script to add a starting action on five scenario
  o Coding revision (module Tnf)
  o Documentation
  o Change the name of the tool (Demodocos instead of TnF Synthesis Tool)
  o Change the main module name (Dem instead of Tnf)
  o Change names of modules tnfXXX (tnfAnalysisProcess, tnfBasis...)
  
  

version 0.7.5, October 2017
=================================
  o New command to test the tool (and new module : TnfTestingProcess)
  o Add origin and destination to five objects
  o Taking into account "ufo_related_match" on DataOntologyBasis
  o Fix bug about copy of input file
  o Add support for multiple iccas files (scripts for grouping files)
  o Add script to adapt input file (if necessary)
  o Renaming of some modules (XmlSevenXxx to A7XmlXxx, XxxIn to XxxInput and XxxOut to XxxOutput, tnfProcessBase to tnfBasis)
  

version 0.7.4, September 2017
=================================
  o Coding revision (module Tnf)
  o Fix bug about synthesize of several files
  o Revision of Ontology Basis Structure
  o Taking into account annotations of the ontology base file
  o New configuration parameter for global ontology files
  o Support for global ontology files
  o New configuration parameter for ontological label type (iri_segment, label)
  o Taking into account iri_segment on DataOntologyBasis
  o Adaptation of sensor object text
  o New config param to sensor label type
  

version 0.7.3, September 2017
=================================
  o Simplifying the synthesis command by removing the category of input source (category of source directly computed)
  o Fix bug about labels updating
  o Command to compute the ontology structure from an instance file and the ontology base file
  

version 0.7.2, September 2017
=================================
  o Taking account an ontology base file to update the structure defining the ontology
  o Configuration parameters revision (add tnfgraph_label_type, seven_five_value_type and remove output_tnf_display)
  o Update five values as ontology or ref_vr (according to the config parameter) by codes barbarian update (module A7Attributes)
  o Update TNF graph labels as number, letter, ontology_en or ref_vr (according to the config parameter)
  o Taking account the UFR_exact_match on ontology / VR mapping
  

version 0.7.1, September 2017
=================================
  o Revision of configuration parameters naming
  o Remove the full view of configuration (command "tnf config view full")
  o Add configuration parameter "input_working_file_postfix"
  o Fix bug on function to group some files
  o Copy the input file on working_directory (with input working file postfix)
  o Add function to remove xmlns attribute of input work file


version 0.7, September 2017
=================================
  o Remove directory old-tests
  o Add modules TnfXxxProcess to group process by stage (synthesis, output generation, ...)
  o Revision of configuration parameters
  o Addition of a parameter to define the ontology base file
  o Add module to define an ontology basis (and load this from an ontology base file)
  o Add command to value the ontology basis (from an ontology base file)
  o Taking account prefix of classifier keys
  o Revision of some commands syntax
  

version 0.6.1, August 2017
=================================
  o Minor revision of module Xes 
  o Add specific data structure for ontology (DataOntology and DataEvent)
  o Specific module to define and manage the classifiers
  o Remove commands xes2ff, xes2quotient and xes2seven
  o Development adjustment and cleanup
  o Add configuration variable for event number limit to A7 working (evaluation of event paths)
  

version 0.6, August 2017
=================================
  o Command to save (and load) a given configuration
  o Command to group several iSPM files into a single file
  o TnF synthesis from several files
  o Automatic recognition of source file formats
  o Definition and load of global classifiers (with global classifiers config file)
  o Specific command to list the global classifiers
  o Specific command to add, remove and modify a global classifier
  o Add classifiers for xes, iccas, ispm2, ispm3 and seven files
  o Move test directory about removing commands on _old-tests


version 0.5.1, July 2017
=================================
  o Fix bugs about paths computing
  o Revision of tests directory (directory by command)
  o Cutting module A7Analysis into some sub-modules
  o Generation of A7 xSPM file (iSPM as xes file corresponding to an abstract seven)
  o Improve synthesis from #7 (indirect method using a transient xes file)
  

version 0.5, June 2017
=================================
  o Cutting module XmlSeven into two modules (XmlSevenIn and XmlSevenOut)
  o Module XmlSevenIn to work on #7 xml files (and translate #7 doc to A7)
  o Command seven2ff to construct a TnF net from #7 scenario (basic proposal)
  o Add test directory with basic tests for seven2ff developement (basic_seven2ff)
  o Function to compute the simple paths of an abstract seven
  o Command tnf which should become the main command of the tool
  o Revision of the module Configuration
  o Modules ConfigurationIn and ConfigurationOut to load/save the configuration
  o Command tnf config to configure the tool
  o Generation of TnF marking graph
  o Add directory examples
  

version 0.4, May 2017
=================================
  o Revision of the modules working on #7 code with a new representation (a7Element, a7Attributes, a7UrnMapping)
  o Functions to analyse the #7 code resulting from the synthesis
  o Xslt script to convert ICCAS format observations to XES format
  o Update of the xes2ff command
  o Command xes2quotient to construct a quotient system without synthesis
  o Redistribution of the test directory with regrouping of the basic tests and creation of a directory by case study

  
version 0.3, December 2016
=================================
  o Xslt script to convert ISPM2 format observations to XES format
  o Modules to represent the graph in the form of an abstract #7 code
  o Functions to modify the abstract #7 code (module A7Work)
  o Modules to generate an XML file with #7 code
  o Update of the xes2ff command


version 0.2, March 2013
=================================
  o Release with native code commandline
  
  
version 0.1, March 2013
=================================
  o first release
 
