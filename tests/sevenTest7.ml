(*
 *
 * Test about AbstractSeven & XmlSeven modules (test 7)
 * XML of #SEVEN code with sensors labels from classify  
 * ABCD cross example
 *
 *)

(* Files to process *)
let xes_dir = "_xes/";;
let xml_dir = "_seven/";;
let file_name = "abcd";;
let classifier_name = "Event Name";;
let xes_file = xes_dir ^ file_name ^ ".xes";;
let xml_file = xml_dir ^ "sevenTest7.xml";;

(* XES file processing *)
let x = Xes.log_of_file xes_file;;
(*x.Xes.log_classifiers;;*)
let c = classifier_name;;
let v = Xes.ontology_of_classifier x c;;
let (a,y) = Xes.encode_log x c;;

(* Classifier array *)
let event_attributes e = e.Xes.event_attributes;;
let event_name e =
  let a = event_attributes e in
  let (s, va) = List.hd a in
  let value_scalar = va.Xes.value_scalar in
  Xes.string_of_scalar value_scalar;;
let e1 = event_name (Array.get v 0);;
let e2 = event_name (Array.get v 1);;
let e3 = event_name (Array.get v 2);;
let e4 = event_name (Array.get v 3);;
let c_list = e1 :: e2 :: e3 :: e4 :: [];;
let c_array = Array.of_list c_list;;

(* Init system *)
let d = Array.length v;;
let s = Parikhproc.make_system_of_word_list d y;;

(* Synthesis *)
let t = Parikhproc.quotient s;;
let r = Synthesis.make_region_spaces t;;
let w = Separation.init t;;
Synthesis.synthesize r w;;
Separation.elim_redundent w;;  

(* Abstract seven making *) 
let a7 = (A7Work.abstract_seven_of_system w);;
A7Work.print stdout a7;;

(* Update sensors labels *) 
let a7' = (A7Work.update_sensors_labels a7 c_array);;
A7Work.print stdout a7';;
  
(* XML #SEVEN code generation *)               
XmlSeven.abstract_seven_to_seven_xml_file xml_file a7';;
