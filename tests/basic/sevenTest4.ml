(*
 *
 * Test about AbstractSeven & XmlSeven modules (test 4)
 * XML file with #SEVEN code generated from system of word
 * Simple system and views about place, sensor/event and transition
 *
 *)

(* Files to process *)
let file_name = "sevenTest4";;
let xml_file = file_name ^ ".xml";;

(* Init system *)
let d = 2;;
let s =
  Parikhproc.make_system_of_word_list
    d
    [
      [0;1;1]
    ];;

(* Configuration *)
let conf =
  {
    Configuration.conf_verbose = 1;
    Configuration.conf_approximation = Configuration.default_apx_config;
    Configuration.conf_pdf_output = None;
    Configuration.conf_graph_output = None;
    Configuration.conf_seven_output = None;
    Configuration.conf_display_net = false;
    Configuration.conf_classifier = "";
    Configuration.conf_table = None;
    Configuration.conf_input = ""
  };;

(* Synthesis *)
let t = Parikhproc.quotient s;;
let r = Synthesis.make_region_spaces t;;
let w = Separation.init t;;
Synthesis.synthesize conf r w;;
Separation.elim_redundent w;;    
  
(* Abstract seven making *) 
let a7 = (A7Work.abstract_seven_of_system w);;
A7Work.print stdout a7;;

(* View about place *)  
let pl = (AbstractSeven.get_place_list a7);;
let p = List.hd pl;;
let p_id = (AbstractSeven.get_place_id p);;
let p_label = (AbstractSeven.get_place_label p);;

(* View about sensor/event *)  
let sel = (AbstractSeven.get_sensor_event_list a7);;
let se = List.hd sel;;
let e_id = (AbstractSeven.get_event_id se);;
let s_label = (AbstractSeven.get_sensor_label se);;

(* View about transition *)  
let tl = (AbstractSeven.get_transition_list a7);;
let transition = List.hd tl;;
let t_id = (AbstractSeven.get_transition_id transition);;
let t_label = (AbstractSeven.get_transition_label transition);;
let t_event_idref = (AbstractSeven.get_event_idref transition);;
let t_up_place_idref = (AbstractSeven.get_up_place_idref transition);;
let t_down_place_idref = (AbstractSeven.get_down_place_idref transition);;
  
(* XML #SEVEN code generation *)               
XmlSeven.abstract_seven_to_seven_xml_file xml_file a7;;
