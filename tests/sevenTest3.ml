(*
 *
 * Test about AbstractSeven module (test 3)
 * Abstract seven making
 * Abcd cross example
 *
 *)

(* Init system *)
let d = 4;;
let s =
  Parikhproc.make_system_of_word_list
    d
    [
      [0;2;1;3];
      [3;1;2;0];
      [1;3;0;2]
    ];;

(* Synthesis *)
let t = Parikhproc.quotient s;;
let r = Synthesis.make_region_spaces t;;
let w = Separation.init t;;
Synthesis.synthesize r w;;
Separation.elim_redundent w;;    
  
(* Initial abstract seven *)  
let a7 = (AbstractSeven.make (Separation.dimension w));;
A7Work.print stdout a7;;

(* Abstract seven making with main function *)  
let a7' = (A7Work.abstract_seven_of_system w);;
A7Work.print stdout a7';;

