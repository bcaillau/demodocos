(*
 *
 * Test about AbstractSeven & XmlSeven modules (test 5)
 * XML file with #SEVEN code generated from system of word
 * Abcd cross example
 *
 *)

(* Files to process *)
let xml_dir = "_seven/";;
let file_name = "sevenTest5";;
let xml_file = xml_dir ^ file_name ^ ".xml";;

(* Init system *)
let d = 4;;
let s =
  Parikhproc.make_system_of_word_list
    d
    [
      [0;2;1;3];
      [3;1;2;0];
      [1;3;0;2]
    ];;

(* Synthesis *)
let t = Parikhproc.quotient s;;
let r = Synthesis.make_region_spaces t;;
let w = Separation.init t;;
Synthesis.synthesize r w;;
Separation.elim_redundent w;;    

(* Abstract seven making *) 
let a7 = (A7Work.abstract_seven_of_system w);;
A7Work.print stdout a7;;
  
(* XML #SEVEN code generation *)               
XmlSeven.abstract_seven_to_seven_xml_file xml_file a7;;

