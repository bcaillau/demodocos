[
  0 (a) : 
     { 
     action:affected-1 : XES = "bottom of size 2 russian doll"  |  VR = "UFO_DollBottom5"
     action:actuator : XES = "Surgeon_left-hand"  |  VR = -
     action:affected : XES = "#affected2#bottom of size 2 russian doll"  |  VR = -
     action:affected-2 : XES = "affected2"  |  VR = -
     action:affected-number : XES = "2"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 0  |  VR = -
     action:instrument : XES = "#grasper_left#grasper_right"  |  VR = -
     action:instrument-1 : XES = "grasper_right"  |  VR = -
     action:instrument-2 : XES = "grasper_left"  |  VR = -
     action:instrument-number : XES = "2"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 0  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "Reaching for needle with left hand"  |  VR = - 
     } 
  1 (b) : 
     { 
     action:instrument-1 : XES = "paper card"  |  VR = "UFO_PaperCard"
     action:actuator : XES = "Surgeon_left-hand"  |  VR = -
     action:affected : XES = ""  |  VR = -
     action:affected-number : XES = "0"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 1  |  VR = -
     action:instrument : XES = "#grasper_left#paper card"  |  VR = -
     action:instrument-2 : XES = "grasper_left"  |  VR = -
     action:instrument-number : XES = "2"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 0  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "Making a C Loop around the right hand"  |  VR = - 
     } 
  2 (c) : 
     { 
     action:actuator : XES = "Surgeon_left-hand"  |  VR = -
     action:affected : XES = ""  |  VR = -
     action:affected-number : XES = "0"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 2  |  VR = -
     action:instrument : XES = "#grasper_left#grasper_right"  |  VR = -
     action:instrument-1 : XES = "grasper_right"  |  VR = -
     action:instrument-2 : XES = "grasper_left"  |  VR = -
     action:instrument-number : XES = "2"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 0  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "Making a C Loop around the right hand"  |  VR = - 
     } 
  3 (d) : 
     { 
     action:actuator : XES = "Surgeon_left-hand"  |  VR = -
     action:affected : XES = ""  |  VR = -
     action:affected-number : XES = "0"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 3  |  VR = -
     action:instrument : XES = "#grasper_left#grasper_right"  |  VR = -
     action:instrument-1 : XES = "grasper_right"  |  VR = -
     action:instrument-2 : XES = "grasper_left"  |  VR = -
     action:instrument-number : XES = "2"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 0  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "Reaching for suture with right hand"  |  VR = - 
     } 
  4 (e) : 
     { 
     action:actuator : XES = "Surgeon_both-hands"  |  VR = -
     action:affected : XES = ""  |  VR = -
     action:affected-number : XES = "0"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 4  |  VR = -
     action:instrument : XES = "#grasper_left#grasper_right"  |  VR = -
     action:instrument-1 : XES = "grasper_right"  |  VR = -
     action:instrument-2 : XES = "grasper_left"  |  VR = -
     action:instrument-number : XES = "2"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 0  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "Pulling suture with both hands"  |  VR = - 
     } 
  5 (f) : 
     { 
     action:actuator : XES = "Surgeon_left-hand"  |  VR = -
     action:affected : XES = ""  |  VR = -
     action:affected-number : XES = "0"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 5  |  VR = -
     action:instrument : XES = "#grasper_left#grasper_right"  |  VR = -
     action:instrument-1 : XES = "grasper_right"  |  VR = -
     action:instrument-2 : XES = "grasper_left"  |  VR = -
     action:instrument-number : XES = "2"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 0  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "Reaching for needle with left hand"  |  VR = - 
     } 
  6 (g) : 
     { 
     action:actuator : XES = "Surgeon_left-hand"  |  VR = -
     action:affected : XES = ""  |  VR = -
     action:affected-number : XES = "0"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 9  |  VR = -
     action:instrument : XES = "#grasper_left#grasper_right"  |  VR = -
     action:instrument-1 : XES = "grasper_right"  |  VR = -
     action:instrument-2 : XES = "grasper_left"  |  VR = -
     action:instrument-number : XES = "2"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 0  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "Dropping suture at end and moving to end points"  |  VR = - 
     } 
]
