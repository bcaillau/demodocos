<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text"/>
    <xsl:template match="/">
digraph seven {
node [shape=circle]

<xsl:apply-templates select="log/trace/event"/>
}
    </xsl:template>

    <xsl:template match="log/trace/event">
      <xsl:variable name="eventID">
        <xsl:value-of select="int/@idref"/>
      </xsl:variable>
      <xsl:value-of select="upstreamPlace/@idref"/>
      <xsl:text> -> </xsl:text>
      <xsl:value-of select="downstreamPlace/@idref"/>
      <xsl:text> [label="</xsl:text>
      <xsl:value-of select="../../event[@id=$eventID]/sensorLabel"/>
      <xsl:text>"];&#10;</xsl:text>
    </xsl:template>
</xsl:stylesheet>
