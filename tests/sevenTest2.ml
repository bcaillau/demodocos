(*
 *
 * Test about AbstractSeven module (test 2)
 * Abstract seven making
 * Simple example
 *
 *)

(* Init system *)
let d = 2;;
let s =
  Parikhproc.make_system_of_word_list
    d
    [
      [0;0;1];
      [0];
      [1;1;1;0]
    ];;

(* Synthesis *)
let t = Parikhproc.quotient s;;
let r = Synthesis.make_region_spaces t;;
let w = Separation.init t;;
Synthesis.synthesize r w;;
Separation.elim_redundent w;;    
  
(* Initial abstract seven *)  
let a7 = (AbstractSeven.make (Separation.dimension w));;
A7Work.print stdout a7;;

(* Abstract seven making with main function *)   
let a7' = (A7Work.abstract_seven_of_system w);;
A7Work.print stdout a7';;

