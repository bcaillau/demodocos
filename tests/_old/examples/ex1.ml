(*
 *
 * Example 1
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA Rennes, France
 *
 *)

let d = 3;;

let s =
  Parikhproc.make_system_of_word_list
    d
    [
      [0;1;2];
      [1;2];
      [1;1;2];
      [0;2]
    ];;

Confusion.print_confusion_system stdout s;;

let t = Parikhproc.quotient s;;

Quotient.print stdout t;;

let r = Synthesis.make_region_spaces t;;

let w = Separation.init t;;

Synthesis.synthesize r w;;

Separation.elim_redundent w;;

Separation.print stdout w;;

let n = Separation.get_admissible_regions w;;

List.iter (Region.print stdout) n;;

let n' = Region.superimpose_list n;;

List.iter (Region.print stdout) n';;

Display.net_to_pdf n' "ex1" "ex1.pdf";;
