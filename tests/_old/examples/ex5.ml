(*
 *
 * Example 3: cylinder head studs torquing process with 6 studs
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA Rennes, France
 *
 *)

let d = 6;;

let s =
  Parikhproc.make_system_of_word_list
    d
    [
      [0;3;4;1;2;5];
      [0;3;1;4;5;2];
      [3;0;4;1;2;5];
      [3;0;1;4;5;2];
      [1;4;3;0;2;5];
      [4;1;0;3;2;5];
      [2;5;3;0;1;4];
      [5;2;4;1;0;3]
    ];;

Confusion.print_confusion_system stdout s;;

let t = Parikhproc.quotient s;;

Quotient.print stdout t;;

let r = Synthesis.make_region_spaces t;;

let w = Separation.init t;;

Synthesis.synthesize r w;;

Separation.elim_redundent w;;

Separation.print stdout w;;

let n = Separation.get_admissible_regions w;;

List.iter (Region.print stdout) n;;

let n' = Region.superimpose_list n;;

List.iter (Region.print stdout) n';;

Display.net_to_pdf n' "ex5" "ex5.pdf";;
