(*
 *
 * Test about AbstractSeven & XmlSeven modules (test 8)
 * XML of #SEVEN code with #FIVE code
 * ABCD cross example
 *
 *)

(* Utilities *)
let print_a_step c s =
  begin
    Printf.fprintf c "\n\n  =============================================================== \n";
    Printf.fprintf c "   %s" s;
    Printf.fprintf c "\n  =============================================================== \n\n"
  end;;

let time_ref = ref 0.0;;
  
let print_chrono c =
  let this_time = Sys.time() in
  let chrono = this_time -. !time_ref in
  Printf.fprintf c "\n\n // Process time = %f\n\n" chrono;;
  
(* Files to process *)
let xes_dir = "_xes/";;
let xml_dir = "_seven/";;
let file_name = "abcd_five_xspm2";;
let classifier_name = "V Classifier";;
let xes_file = xes_dir ^ file_name ^ ".xes";;
let xml_file = xml_dir ^ "sevenTest8.xml";;

(* XES file processing *)
print_a_step stdout "XES file processing";;
time_ref := Sys.time();; 
let x = Xes.log_of_file xes_file;;
(*x.Xes.log_classifiers;;*)
let c = classifier_name;;
let v = Xes.ontology_of_classifier x c;;
let (a,y) = Xes.encode_log x c;;
print_chrono stdout;;
  
(* Init system *)
print_a_step stdout "Init system";;
time_ref := Sys.time();;  
let d = Array.length v;;
let s = Parikhproc.make_system_of_word_list d y;;
print_chrono stdout;;

(* Configuration *)
print_a_step stdout "Configuration";;
time_ref := Sys.time();;
let conf =
  {
    Configuration.conf_verbose = 1;
    Configuration.conf_approximation = Configuration.default_apx_config;
    Configuration.conf_pdf_output = None;
    Configuration.conf_graph_output = None;
    Configuration.conf_seven_output = None;
    Configuration.conf_display_net = false;
    Configuration.conf_classifier = "";
    Configuration.conf_table = None;
    Configuration.conf_input = ""
  };;
print_chrono stdout;;

(* Synthesis *)
print_a_step stdout "Synthesis";;
time_ref := Sys.time();;    
let t = Parikhproc.quotient s;;
let r = Synthesis.make_region_spaces t;;
let w = Separation.init t;;
Synthesis.synthesize conf r w;;
Separation.elim_redundent w;; 
print_chrono stdout;; 

(* Abstract seven making *) 
print_a_step stdout "Abstract seven making";;
time_ref := Sys.time();; 
let a7 = (A7Work.abstract_seven_of_system w);;
A7Work.print stdout a7;;
print_chrono stdout;;

(* Update sensors labels *) 
print_a_step stdout "Update sensors labels";;
time_ref := Sys.time();;
let a7' = (A7Work.update_sensors_labels_from_ontology a7 v);;
A7Work.print stdout a7';;
print_chrono stdout;;

(* Evaluate five code for sensor/event *)   
print_a_step stdout "Evaluate five code for sensor/event";;
time_ref := Sys.time();;
let a7'' = (A7Work.evaluate_five_code_from_ontology a7' v);;
A7Work.print stdout a7'';;
print_chrono stdout;;
  
(* XML #SEVEN code generation *)     
print_a_step stdout "XML #SEVEN code generation";;
time_ref := Sys.time();;           
XmlSeven.abstract_seven_to_seven_xml_file xml_file a7'';;
print_chrono stdout;;
