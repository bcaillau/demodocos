(*
 *
 * Test about AbstractSeven & XmlSeven modules (test 6)
 * Classifier study of XES file  
 * ABCD cross example
 *
 *)

(* Files to process *)
let xes_dir = "_xes/";;
let xml_dir = "_seven/";;
let file_name = "abcd";;
let classifier_name = "Event Name";;
let xes_file = xes_dir ^ file_name ^ ".xes";;
let xml_file = xml_dir ^ "sevenTest6.xml";;

(* XES file processing *)
let x = Xes.log_of_file xes_file;;
(*x.Xes.log_classifiers;;*)
let c = classifier_name;;
let v = Xes.ontology_of_classifier x c;;
let (a,y) = Xes.encode_log x c;;

(* Classifier study *)
let event_attributes e = e.Xes.event_attributes;;
let event_name e =
  let a = event_attributes e in
  let (s, va) = List.hd a in
  let value_scalar = va.Xes.value_scalar in
  Xes.string_of_scalar value_scalar;;
let e1 = event_name (Array.get v 0);;
let e2 = event_name (Array.get v 1);;
let e3 = event_name (Array.get v 2);;
let e4 = event_name (Array.get v 3);;
let c_list = e1 :: e2 :: e3 :: e4 :: [];;
let c_array = Array.of_list c_list;;
let get_sensor_label i = Array.get c_array i;;
get_sensor_label 0;;
