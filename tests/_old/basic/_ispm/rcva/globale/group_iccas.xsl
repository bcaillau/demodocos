<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" 
            version="1.0" 
            encoding="UTF-8" 
            indent="yes"/>

<xsl:template match="/">
    <iccas>
        <xsl:apply-templates select="/iccas/iccasRef"/>
    </iccas>
</xsl:template>

<xsl:template match="iccas/iccasRef">
    <xsl:copy-of select="document(.)//iccas/rec_workflow"/>
</xsl:template>

</xsl:stylesheet> 
