[
  0 (a) : 
     { 
     action:actuator : XES = "Other"  |  VR = -
     action:affected : XES = "#handle_1B"  |  VR = -
     action:affected-1 : XES = "handle_1B"  |  VR = -
     action:affected-number : XES = "1"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 1  |  VR = -
     action:instrument : XES = "#"  |  VR = -
     action:instrument-1 : XES = ""  |  VR = -
     action:instrument-number : XES = "1"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 1  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "take"  |  VR = - 
     } 
  1 (b) : 
     { 
     action:actuator : XES = "Other"  |  VR = -
     action:affected : XES = "#blade_1B"  |  VR = -
     action:affected-1 : XES = "blade_1B"  |  VR = -
     action:affected-number : XES = "1"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 2  |  VR = -
     action:instrument : XES = "#"  |  VR = -
     action:instrument-1 : XES = ""  |  VR = -
     action:instrument-number : XES = "1"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 1  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "take"  |  VR = - 
     } 
  2 (c) : 
     { 
     action:actuator : XES = "Other"  |  VR = -
     action:affected : XES = "#blade_1B#handle_1B"  |  VR = -
     action:affected-1 : XES = "handle_1B"  |  VR = -
     action:affected-2 : XES = "blade_1B"  |  VR = -
     action:affected-number : XES = "2"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 3  |  VR = -
     action:instrument : XES = "#"  |  VR = -
     action:instrument-1 : XES = ""  |  VR = -
     action:instrument-number : XES = "1"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 1  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "mount"  |  VR = - 
     } 
  3 (d) : 
     { 
     action:actuator : XES = "Other"  |  VR = -
     action:affected : XES = "#handle_1B"  |  VR = -
     action:affected-1 : XES = "handle_1B"  |  VR = -
     action:affected-number : XES = "1"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 4  |  VR = -
     action:instrument : XES = "#"  |  VR = -
     action:instrument-1 : XES = ""  |  VR = -
     action:instrument-number : XES = "1"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 1  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "pose"  |  VR = - 
     } 
  4 (e) : 
     { 
     action:actuator : XES = "Other"  |  VR = -
     action:affected : XES = "#handle_1A"  |  VR = -
     action:affected-1 : XES = "handle_1A"  |  VR = -
     action:affected-number : XES = "1"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 1  |  VR = -
     action:instrument : XES = "#"  |  VR = -
     action:instrument-1 : XES = ""  |  VR = -
     action:instrument-number : XES = "1"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 1  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "take"  |  VR = - 
     } 
  5 (f) : 
     { 
     action:actuator : XES = "Other"  |  VR = -
     action:affected : XES = "#blade_1A"  |  VR = -
     action:affected-1 : XES = "blade_1A"  |  VR = -
     action:affected-number : XES = "1"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 2  |  VR = -
     action:instrument : XES = "#"  |  VR = -
     action:instrument-1 : XES = ""  |  VR = -
     action:instrument-number : XES = "1"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 1  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "take"  |  VR = - 
     } 
  6 (g) : 
     { 
     action:actuator : XES = "Other"  |  VR = -
     action:affected : XES = "#blade_1A#handle_1A"  |  VR = -
     action:affected-1 : XES = "handle_1A"  |  VR = -
     action:affected-2 : XES = "blade_1A"  |  VR = -
     action:affected-number : XES = "2"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 3  |  VR = -
     action:instrument : XES = "#"  |  VR = -
     action:instrument-1 : XES = ""  |  VR = -
     action:instrument-number : XES = "1"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 1  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "mount"  |  VR = - 
     } 
  7 (h) : 
     { 
     action:actuator : XES = "Other"  |  VR = -
     action:affected : XES = "#handle_1A"  |  VR = -
     action:affected-1 : XES = "handle_1A"  |  VR = -
     action:affected-number : XES = "1"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 4  |  VR = -
     action:instrument : XES = "#"  |  VR = -
     action:instrument-1 : XES = ""  |  VR = -
     action:instrument-number : XES = "1"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 1  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "pose"  |  VR = - 
     } 
  8 (i) : 
     { 
     action:actuator : XES = "Other"  |  VR = -
     action:affected : XES = "#handle_2A"  |  VR = -
     action:affected-1 : XES = "handle_2A"  |  VR = -
     action:affected-number : XES = "1"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 1  |  VR = -
     action:instrument : XES = "#"  |  VR = -
     action:instrument-1 : XES = ""  |  VR = -
     action:instrument-number : XES = "1"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 1  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "take"  |  VR = - 
     } 
  9 (j) : 
     { 
     action:actuator : XES = "Other"  |  VR = -
     action:affected : XES = "#blade_2A"  |  VR = -
     action:affected-1 : XES = "blade_2A"  |  VR = -
     action:affected-number : XES = "1"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 2  |  VR = -
     action:instrument : XES = "#"  |  VR = -
     action:instrument-1 : XES = ""  |  VR = -
     action:instrument-number : XES = "1"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 1  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "take"  |  VR = - 
     } 
  10 (k) : 
     { 
     action:actuator : XES = "Other"  |  VR = -
     action:affected : XES = "#blade_2A#handle_2A"  |  VR = -
     action:affected-1 : XES = "handle_2A"  |  VR = -
     action:affected-2 : XES = "blade_2A"  |  VR = -
     action:affected-number : XES = "2"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 3  |  VR = -
     action:instrument : XES = "#"  |  VR = -
     action:instrument-1 : XES = ""  |  VR = -
     action:instrument-number : XES = "1"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 1  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "mount"  |  VR = - 
     } 
  11 (l) : 
     { 
     action:actuator : XES = "Other"  |  VR = -
     action:affected : XES = "#handle_2A"  |  VR = -
     action:affected-1 : XES = "handle_2A"  |  VR = -
     action:affected-number : XES = "1"  |  VR = -
     action:description : XES = ""  |  VR = -
     action:destination : XES = ""  |  VR = -
     action:id : XES = 4  |  VR = -
     action:instrument : XES = "#"  |  VR = -
     action:instrument-1 : XES = ""  |  VR = -
     action:instrument-number : XES = "1"  |  VR = -
     action:origin : XES = ""  |  VR = -
     action:recipient : XES = ""  |  VR = -
     action:state : XES = 1  |  VR = -
     action:type : XES = ""  |  VR = -
     action:verb : XES = "pose"  |  VR = - 
     } 
]
