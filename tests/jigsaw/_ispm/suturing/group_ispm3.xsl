<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" 
            version="1.0" 
            encoding="UTF-8" 
            indent="yes"/>

<xsl:template match="/">
    <iSPM>
        <xsl:apply-templates select="/iSPM/dataRef"/>
    </iSPM>
</xsl:template>

<xsl:template match="iSPM/dataRef">
    <xsl:copy-of select="document(.)//iSPM/data"/>
</xsl:template>

</xsl:stylesheet> 
