(*
 *
 * Process for testing
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

open Configuration
open TnfBasis


       
(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)
  
  
(*-----------------------------------------------------------------
 * Testing functions
 *-----------------------------------------------------------------*)

let test_parikh i =
  let parikh = Parikh.make i in
  begin
    Parikh.dump parikh;
  end

let test_quotient i =
  begin
    let d = 4 in
    let s =
      Parikhproc.make_system_of_word_list
        d
        [
          [0;2;1;3];
          [3;1;2;0];
          [1;3;0;2]
        ]
    in
    let t = Parikhproc.quotient s in
    Confusion.print_confusion_system stdout s;
    Quotient.print stdout t
  end

let test_parikhproc config traces =
  let d = log_dim traces in
  let system_of_word_list = Parikhproc.make_system_of_word_list d traces in
  begin
    Confusion.print_confusion_system stdout system_of_word_list;
  end
