(*
 *
 * Vector: boolean vectors
 *
 * (C) Benoit Caillaud <benoit.caillaud@inria.fr>, INRIA Rennes, France, 2012
 *
 *)

(* Datatypes *)

type t

(* Factory *)

val make : int -> bool -> t

val set : t -> int -> bool -> unit

val get : t -> int -> bool

val init : (int -> bool) -> int -> t

val clone : t -> t

val extend : t -> int -> bool -> t

val restrict : t -> int -> t

(* to_string and print *)

val to_string : t -> string

val print : t -> unit

(* Length of other functions *)

val length : t -> int

val is_zero : t -> bool

val leading_zeroes : t -> int

val arity : t -> int

val compare : t -> t -> int

(* Mappings *)

(* Operators *)

(* Binary operators raise [Invalid_argument _] whenever the parameters have different lengths *)

val vxor : t -> t -> t

val vand : t -> t -> t

(* val lneg : t -> t -> t *)

val prod : t -> t -> bool

val partial_prod : t -> t -> int -> int -> bool

(* In place binary operators *)

val ixor : t -> t -> unit (* [ixor a b] performs [a] := [a] xor [b] *)

val iand : t -> t -> unit (* [iand a b] performs [a] := [a] and [b] *)

val ior : t -> t -> unit (* [ior a b] performs [a] := [a] or [b] *)

(* In place unary operators *)

val iflip : t -> int -> unit (* [iflip a i] flips bit of index [i] in vector [a] *)

(* Vector blit *)

val blit : src:t -> src_idx:int -> dst:t -> dst_idx:int -> len:int -> unit

(* Byte read and write *)

val get_byte : t -> idx:int -> len:int -> int
val set_byte : t -> idx:int -> len:int -> byte:int -> unit

