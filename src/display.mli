(*
 *
 ***********************************************************************
 *                                                                     *
 *                   Flipflop : A flipflop synthesis tool              *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2013.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Display : display FF-nets using the dot command (part of graphViz)
 *
 * $Id$
 *
 *)

(* [net_to_doc a n] generates dot code in doc format from FF-net [a]. The graph is named [n] *)

val net_to_doc : Region.n -> string -> Pp.doc

(* [net_to_file a n m] generates dot code in file [m] from FF-net [a]. The graph is named [n] *)

val net_to_file : Region.n -> string -> string -> unit

(* [net_display a n] displays FF-net [a] using the GraphViz dot command. The graph is named [n] *)

val net_display : Region.n -> string -> unit

(* [net_to_pdf a n m] generates a PDF file named [m] from FF-net [a] using the GraphViz dot command. The graph is named [n] *)

val net_to_pdf : Region.n -> string -> string -> unit

