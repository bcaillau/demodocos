(*
 *
 * Flipflop net synthesis algorithm
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA Rennes, France
 *
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

(* None *)


                                    

(*-----------------------------------------------------------------
 * Region spaces
 *-----------------------------------------------------------------*)

(* Region spaces *)

type e =
    {
      flip : Region.t;
      test : Region.t
    }

type t = e array

(* Compute region spaces *)

(* val make_region_spaces : Quotient.t -> t *)

let make_region_spaces s =
  let d = Parikh.dimension s in
    Array.init
      d
      (fun e ->
	let r = Region.make d e in
	  begin
	    Quotient.iter_event_trans
	      (fun q -> Region.enable r ~state:q ~event:e)
	      s e;
	    Region.cook r;
	    let rf = Region.clone r
	    and rt = r in
	      begin
		Region.force_flip rf;
		Region.force_test rt;
		{ flip = rf; test = rt }
	      end
	  end)


(*-----------------------------------------------------------------
 * Synthesis
 *-----------------------------------------------------------------*)    

(* Solves all ESSPs until saturation of the graph. 
 * The graph is expected to be virgin. Traversal starts at initial state *)

let synthesize conf w s =
  let rec dfs q =
    (* is the vertex virgin *)
    if (Separation.get_vertex_status q) = Separation.Virgin
    then (* Yes *)
      begin
	Verbose.cond 2 (fun _ ->
                       Printf.fprintf stderr
                                      "*** Visiting state %s\n"
                                      (Vector.to_string (Separation.parikh_of_state q)));
	flush stderr;
	(* Mark vertex visited *)
	Separation.set_vertex_status q Separation.Visited;
	(* Iterates on ESSPs in vertex [q] *)
	Separation.iter_state_essp
	  (fun e ->
	    Verbose.cond 2 (fun _ -> Printf.fprintf stderr "**** Event %d\n" e);
	    flush stderr;
	    (* Is ESSP solved by a region in the heap? *)
	    try
	      let r = Separation.find_heap_region s (Separation.parikh_of_state q) e in
		(* Mark ESSP solved *)
		Separation.mark_solved q e r
	    with
		(* No region in the heap solves the ESSP *)
		Not_found ->
		  (* Is there a region solving the ESSP? *)
                  (* Flip regions are given priority because they decompose easily into minimal regions *)
		  try
		    Verbose.cond 2 (fun _ -> Printf.fprintf stderr "***** Trying flip region\n");
		    flush stderr;
		    let z =
		      try
			(* Is there a flip region solving the ESSP? *)
			Region.solve_essp w.(e).flip (Separation.parikh_of_state q) e
		      with
			  (* No flip region solves the ESSP *)
			  Region.Separation_failure (_,_) ->
			    begin
			      Verbose.cond 2 (fun _ -> Printf.fprintf stderr "***** Trying test region\n");
			      flush stderr;
			      (* Is there a test region solving the ESSP? *)
			      Region.solve_essp w.(e).test (Separation.parikh_of_state q) e
			    end
		    in
		    (*let r = 
		      begin
			Verbose.cond 2 (fun _ -> Printf.fprintf stderr "***** Forming new region\n");
			flush stderr;
			Separation.make_region s z
		      end
                    in*)
		    begin
		      Verbose.cond 2 (fun _ -> Printf.fprintf stderr "***** Adding new region\n");
		      flush stderr;
                      (* Add the region to the heap *)
                      Separation.add_region s e z;
                      (* Mark ESSP solved *) 
                      Separation.mark_solved q e z
		    end
		  with
		      Region.Separation_failure (_,_) ->
			begin
			  Verbose.cond 2 (fun _ -> Printf.fprintf stderr "***** Failed\n");
			  flush stderr;
                          if (Configuration.is_approximation_limit conf)
                          then (* approximation cut for optimization *)
                            Separation.mark_cut q e
                          else (* Mark ESSP failed *)
			    Separation.mark_failed q e
			end)
	  q;
	Verbose.cond 2 (fun _ -> Printf.fprintf stderr "**** Iterate on exiting transitions\n");
	flush stderr;
	(* Iterate enabled transitions and failed ESSPs *)
	Separation.iter_enabled_or_failed
          conf
	  (fun _ q' -> dfs q')
	  s q;
	(* Mark vertex complete *)
	Separation.set_vertex_status q Separation.Complete;
	Verbose.cond 2 (fun _ -> Printf.fprintf stderr "*** Done visiting state %s\n" (Vector.to_string (Separation.parikh_of_state q)));
	flush stderr
      end
  in
  dfs (Separation.get_initial_vertex s)
      
    
