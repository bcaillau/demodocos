(*
 *
 * Xes datastructure
 *
 * Compatible with the XES 1.4 Standard Definition
 *
 *)

(*-----------------------------------------------------------------
 * Datastructures
 *-----------------------------------------------------------------*)

(* Datatypes *)

type log =
    {
      log_version : float;
      log_features : string list;
      log_content : trace list;
      log_attributes : attributes;
      log_global_trace_attributes : attributes;
      log_global_event_attributes : attributes;
      log_classifiers : classifiers
    }

and classifiers = (string * (string list)) list (* List of (name, key list) *)

and trace =
    {
      trace_content : event list;
      trace_attribute : attributes
    }

and event =
    {
      event_attributes : attributes
    }

and attributes = (string * value) list

and value =
    {
      value_scalar : scalar;
      value_nested : attributes
    }

and scalar =
      String of string
    | Date of string
    | Int of int
    | Float of float
    | Boolean of bool
    | ID of string

and filter = string list

           

(*-----------------------------------------------------------------
 * Comparison functions
 *-----------------------------------------------------------------*)
           
(* Comparison functions *)

(* val compare_scalar : scalar -> scalar -> int *)

let compare_scalar =
  function
      String s1 ->
	begin
	  function
	      String s2 -> compare s1 s2
	    | Date _
	    | Int _
	    | Float _
	    | Boolean _
	    | ID _ -> -1
	end
    | Date d1 ->
	begin
	  function
	      String _ -> 1
	    | Date d2 -> compare d1 d2
	    | Int _
	    | Float _
	    | Boolean _
	    | ID _ -> -1
	end
    | Int n1 ->
	begin
	  function
	      String _
	    | Date _ -> 1
	    | Int n2 -> compare n1 n2
	    | Float _
	    | Boolean _
	    | ID _ -> -1
	end
    | Float x1 ->
	begin
	  function
	      String _
	    | Date _
	    | Int _ -> -1
	    | Float x2 -> compare x1 x2
	    | Boolean _
	    | ID _ -> 1
	end
    | Boolean b1 ->
	begin
	  function
	      String _
	    | Date _
	    | Int _
	    | Float _ -> -1
	    | Boolean b2 -> compare b1 b2
	    | ID _ -> 1
	end
    | ID j1 ->
	begin
	  function
	      String _
	    | Date _
	    | Int _
	    | Float _
	    | Boolean _ -> -1
	    | ID j2 -> compare j1 j2
	end

(* strip filter *)

let strip_filter l k =
  let k' = k ^ "." in
  let n = String.length k' in
  List.fold_right
    (fun a r ->
      if a=k
      then "" :: r
      else
	let m = String.length a in
	  if m >= n
	  then
	    let b = String.sub a 0 n
	    and c = String.sub a n (m-n) in
	      if b=k'
	      then c :: r
	      else r
	  else r)
    l
    []

(* match filter *)

let match_filter l k = List.mem k l

(* val compare_value : filter -> value -> value -> int *)

let rec compare_value f v1 v2 = compare_value_option f (Some v1) (Some v2)

(* val compare_value_option : filter -> value option -> value option -> int *)

and compare_value_option f =
  function
      None ->
	begin
	  function
	      None -> 0
	    | Some {value_scalar=x2; value_nested=l2} ->
		if match_filter f ""
		then -1
		else compare_attributes f [] l2
	end
    | Some {value_scalar=x1; value_nested=l1} ->
	begin
	  function
	      None ->
		if match_filter f ""
		then 1
		else compare_attributes f l1 []
	    | Some {value_scalar=x2; value_nested=l2} ->
		let c = compare_scalar x1 x2 in
		  if (match_filter f "") && (c <> 0)
		  then c
		  else compare_attributes f l1 l2
	end

(* val compare_attributes : attributes -> attributes -> int *)

and compare_attributes f =
  function
      [] as l1 ->
	begin
	  function
	      [] -> 0
	    | (i2,y2) :: k2 ->
		let c = compare_value_option (strip_filter f i2) None (Some y2) in
		  if c <> 0
		  then c
		  else compare_attributes f l1 k2
	end
    | ((i1,y1) :: k1) as l1 ->
	begin
	  function
	      [] as l2 ->
		let c = compare_value_option (strip_filter f i1) (Some y1) None in
		  if c <> 0
		  then c
		  else compare_attributes f k1 l2
	    | ((i2,y2) :: k2) as l2 ->
		if i1=i2
		then
		  let c = compare_value_option (strip_filter f i1) (Some y1) (Some y2) in
		    if c <> 0
		    then c
		    else compare_attributes f k1 k2
		else
		  if i1<i2
		  then
		    let c = compare_value_option (strip_filter f i1) (Some y1) None in
		      if c <> 0
		      then c
		      else compare_attributes f k1 l2
		else
		    let c = compare_value_option (strip_filter f i2) None (Some y2) in
		      if c <> 0
		      then c
		      else compare_attributes f l1 k2
	end


      
(*-----------------------------------------------------------------
 * Accessors (for attributes and values)
 *-----------------------------------------------------------------*)

(* string of attributes and values *)

(* val string_of_attributes : attribute -> string *)
(* val string_of_value : value -> string *)

let rec string_of_attributes =
  function
      [] -> "{}"
    | [(x,y)] -> "{"^x^":"^(string_of_value y)^"}"
    | (x,y)::l ->
	"{"^x^":"^(string_of_value y)^";"^(string_of_attr_contd l)

and string_of_attr_contd =
  function
      [] -> "}"
    | [(x,y)] -> x^":"^(string_of_value y)^"}"
    | (x,y)::l ->
	x^":"^(string_of_value y)^";"^(string_of_attr_contd l)

and string_of_value =
  function
      { value_scalar=x; value_nested=[] } ->
	string_of_scalar x
    | { value_scalar=x; value_nested=l } ->
	(string_of_scalar x)^(string_of_attributes l)

and string_of_scalar =
  function
      String s -> "\""^(String.escaped s)^"\""
    | Date s -> "["^(String.escaped s)^"]"
    | Int n -> string_of_int n
    | Float x -> string_of_float x
    | Boolean b -> string_of_bool b
    | ID s -> s

and string_of_event { event_attributes=l } = string_of_attributes l

                                           
      
(* accessors for events *)

let get_event_attributes event = event.event_attributes

let string_formatting_of_event event =
  let attributes = get_event_attributes event in
  let formatting (key, value) = (key, (string_of_value value)) in
  List.map formatting attributes
  

(* accessors for attributes *)

let rec set_attribute k x =
  function
      [] -> [(k,x)]
    | (((k',_) as p') :: l') as l ->
	if k < k'
	then (k,x) :: l
	else
	  if k = k'
	  then (k,x) :: l'
	else p' :: (set_attribute k x l')
                                           
           

(*-----------------------------------------------------------------
 * Parsers for log
 *-----------------------------------------------------------------*)

(* utilities for the parser *)

let rec get_float_attr k x0 =
  function
      [] -> x0
    | (h,w) :: l ->
	if h=k
	then float_of_string w
	else get_float_attr k x0 l

let rec get_string_attr k s0 =
  function
      [] -> s0
    | (h,s) :: l ->
	if h=k
	then s
	else get_string_attr k s0 l

let rec tokenize s =
  let n = String.length s in
  let i =
    try
      String.index s ' '
    with
	Not_found -> n in
  let m = n-(i+1) in
    if i > 0
    then
      let t = String.sub s 0 i in
	if m > 0
	then
	  let r = String.sub s (i+1) m in
	    t :: (tokenize r)
	else [t]
    else
      if m > 0
      then
	let r = String.sub s 1 (n-1) in
	  tokenize r
    else []

let filter_elements h l =
  List.filter
    (function
	Xml.Element (t,_,_) -> List.mem t h
      | Xml.PCData _ -> false)
    l

let retrieve_global t =
  function
      Xml.Element ("global",[("scope",t')],g) ->
	if t=t' then g else []
    | _ -> []

let rec attribute_of_xml =
  function
      Xml.Element ("string",a,b) ->
	let k = get_string_attr "key" "" a
	and s = get_string_attr "value" "" a in
	  if k = ""
	  then invalid_arg "Xes.attribute_of_xml: missing key"
	  else (k,{value_scalar=String s; value_nested=attributes_of_xml_list (filter_elements ["string";"date";"int";"float";"boolean";"id"] b)})
    | Xml.Element ("date",a,b) ->
	let k = get_string_attr "key" "" a
	and s = get_string_attr "value" "" a in
	  if k = ""
	  then invalid_arg "Xes.attribute_of_xml: missing key"
	  else (k,{value_scalar=Date s; value_nested=attributes_of_xml_list (filter_elements ["string";"date";"int";"float";"boolean";"id"] b)})
    | Xml.Element ("int",a,b) ->
	let k = get_string_attr "key" "" a
	and s = get_string_attr "value" "" a in
	  if k = ""
	  then invalid_arg "Xes.attribute_of_xml: missing key"
	  else (k,{value_scalar=Int (int_of_string s); value_nested=attributes_of_xml_list (filter_elements ["string";"date";"int";"float";"boolean";"id"] b)})
    | Xml.Element ("float",a,b) ->
	let k = get_string_attr "key" "" a
	and s = get_string_attr "value" "" a in
	  if k = ""
	  then invalid_arg "Xes.attribute_of_xml: missing key"
	  else (k,{value_scalar=Float (float_of_string s); value_nested=attributes_of_xml_list (filter_elements ["string";"date";"int";"float";"boolean";"id"] b)})
    | Xml.Element ("boolean",a,b) ->
	let k = get_string_attr "key" "" a
	and s = get_string_attr "value" "" a in
	  if k = ""
	  then invalid_arg "Xes.attribute_of_xml: missing key"
	  else (k,{value_scalar=Boolean (bool_of_string (String.lowercase s)); value_nested=attributes_of_xml_list (filter_elements ["string";"date";"int";"float";"boolean";"id"] b)})
    | Xml.Element ("id",a,b) ->
	let k = get_string_attr "key" "" a
	and s = get_string_attr "value" "" a in
	  if k = ""
	  then invalid_arg "Xes.attribute_of_xml: missing key"
	  else
            (k,
             {value_scalar = ID s;
              value_nested =
                attributes_of_xml_list
                  (filter_elements ["string";"date";"int";"float";"boolean";"id"] b)
             }
            )
    | _ -> invalid_arg "Xes.attribute_of_xml: syntax error"

and attributes_of_xml_list l =
  List.fold_right
    (fun y h ->
      let (k,x) = attribute_of_xml y in
	set_attribute k x h)
    l
    []

let event_of_xml =
  function
      Xml.Element ("event",[],b) ->
	    {
	      event_attributes =
                attributes_of_xml_list
                  (filter_elements ["string";"date";"int";"float";"boolean";"id"] b)
	    }
    | _ -> invalid_arg "xes.event_of_xml: syntax error"

let trace_of_xml =
  function
      Xml.Element ("trace",[],b) ->
	{
	  trace_content = List.map event_of_xml (filter_elements ["event"] b);
	  trace_attribute =
            attributes_of_xml_list
              (filter_elements ["string";"date";"int";"float";"boolean";"id"] b)
	}
    | _ -> invalid_arg "Xes.trace_of_xml: syntax error"

let classifier_of_xml =
  function
      Xml.Element ("classifier",a,[]) ->
	let n = get_string_attr "name" "" a
	and k = List.sort compare (tokenize (get_string_attr "keys" "" a)) in
	  if n=""
	  then invalid_arg "Xes.classifier_of_xml: missing name"
	  else (n,k)
    | _ -> invalid_arg "Xes.classifier_of_xml: syntax error"

let rec set_classifier n k =
  function
      [] -> [(n,k)]
    | (((n',_) as p') :: l') as l ->
	if n < n'
	then (n,k) :: l
	else
	  if n=n'
	  then (n,k) :: l'
	else p' :: (set_classifier n k l')

let classifiers_of_xml_list l =
  List.fold_right
    (fun y h ->
      let (n,k) = classifier_of_xml y in
	set_classifier n k h)
    l
    []

(* val log_of_xml : Xml.xml -> log *)

let log_of_xml =
  function
      Xml.Element ("log",a,b) ->
	let glt = List.flatten (List.map (retrieve_global "trace") b)
	and gle = List.flatten (List.map (retrieve_global "event") b) in
	  {
	    log_version = get_float_attr "xes.version" 1. a;
	    log_features = tokenize (get_string_attr "xes.features" "" a);
	    log_content = List.map trace_of_xml (filter_elements ["trace"] b);
	    log_attributes =
              attributes_of_xml_list
                (filter_elements ["string";"date";"int";"float";"boolean";"id"] b);
	    log_global_trace_attributes =
              attributes_of_xml_list
                (filter_elements ["string";"date";"int";"float";"boolean";"id"] glt);
	    log_global_event_attributes =
              attributes_of_xml_list
                (filter_elements ["string";"date";"int";"float";"boolean";"id"] gle);
	    log_classifiers = classifiers_of_xml_list (filter_elements ["classifier"] b)
	  }
    | _ -> invalid_arg "Xes.log_of_xml: syntax error"


(* function to add a list of classifiers into a log structure *)
         
let add_classifiers_to_log log classifiers_to_add =
  {
    log_version = log.log_version;
    log_features = log.log_features;
    log_content = log.log_content;
    log_attributes = log.log_attributes;
    log_global_trace_attributes = log.log_global_trace_attributes;
    log_global_event_attributes = log.log_global_event_attributes;
    log_classifiers = List.rev_append log.log_classifiers classifiers_to_add
  }

         
(* parse a XES file and return a log *)

(* val log_of_file : string -> log *)

let log_of_file n =
  let y = Xml.parse_file n in
    log_of_xml y

           

(*-----------------------------------------------------------------
 * Ontology computing function(s)
 *-----------------------------------------------------------------*)

(* returns the filter associated to a classifier *)

(* val filter_of_classifier : log -> string -> filter *)

let filter_of_classifier x c = List.assoc c x.log_classifiers

(* Utilities for the computation of ontologies *)

let is_event_in_list f e l =
  List.exists (fun e' -> (compare_attributes f e.event_attributes e'.event_attributes)=0) l

(* computes an ontology, given a classifier. The ontology is a sorted array of events *)

(* val ontology_of_classifier : log -> string -> event array *)

let ontology_of_classifier x c =
  let f = filter_of_classifier x c
  and m = ref [] in
    begin
      List.iter
	(fun t ->
	  List.iter
	    (fun e ->
	      if not (is_event_in_list f e (!m))
	      then m := e :: (!m))
	    t.trace_content)
	x.log_content;
      let n = List.length (!m) in
      let a = Array.make n { event_attributes=[] } in
	begin
	  ignore
	    (List.fold_right
		(fun e i ->
		  begin
		    a.(i) <- e;
		    i+1
		  end)
		(!m)
		0);
	  a
	end
    end

           

(*-----------------------------------------------------------------
 * Log encoding function(s)
 *-----------------------------------------------------------------*)

(* Utilities for log encoding *)

let index_of_event f a e =
  let i = ref 0
  and n = Array.length a in
    begin
      while ((!i) < n) && ((compare_attributes f e.event_attributes a.(!i).event_attributes) <> 0) do
	i := (!i) + 1
      done;
      if (!i) < n
      then !i
      else raise Not_found
    end
      
(* encodes a log according to a given ontology *)

let encode_log x c =
  let f = filter_of_classifier x c
  and a = ontology_of_classifier x c in
  let y = 
    List.map
      (fun t -> List.map (index_of_event f a) t.trace_content)
      x.log_content in
    (a,y)
