(*
 *
 * Module to analyse an abstract structure of #SEVEN code
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* 
 * This module provides functions to analyse an abstract structure
 * of #SEVEN code (a7), and to obtain information about the code. 
 *)


(*-----------------------------------------------------------------
 * Main function
 *-----------------------------------------------------------------*)

(* function to compute the number of inclusion starting 
 * from a list of traces *)

val evaluate : A7.t -> int list list -> int
  
      

