(*
 *
 ***********************************************************************
 *                                                                     *
 *                   Flipflop : A flipflop synthesis tool              *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2013.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Display : display FF-nets using the dot command (part of graphViz)
 *
 * $Id$
 *
 *)

open Pp

(* Color constants *)

let sienna = "\"#a0522d\""
and bisque = "\"#ffe4c4\""
and firebrick = "\"#92000e\""
and darkmagenta = "\"#8b008b\""
and navy = "\"#000080\""

let global_attr =
  (text "rankdir=LR;") ^^ break ^^ (text "size=\"10.5,6.5\";") ^^ break ^^
    (text "ratio=\"auto\";") ^^ break ^^ (text "center=true;") ^^ break ^^
    (text "margin=\"0.0\";") ^^ break ^^
    (text ("node [style=\"filled\", shape=box, color="^sienna^", fillcolor="^bisque^"];"))

(* aspect ratio *)

let ratio = 8.0

(* exception to break out of a loop *)

exception Break

(* Utilities *)

let place i = "p"^(string_of_int i)

let transition i = "t"^(string_of_int i)

let place_label r =
  if Region.get_m0 r then "1" else "0"

let transition_label i = transition i

let edge_label r i =
  match Region.symbol_of_event r i with
      Region.Ortho -> None
    | Region.Exit -> Some "-"
    | Region.Enter -> Some "+"
    | Region.Compl -> Some "!"
    | Region.True -> Some "1"
    | Region.False -> Some "0"

let doc_of_edge r i j =
  match edge_label r j with
      None -> None
    | Some s ->
	let d =
	  ((text (place i)) ^^ break ^^
	      (text "--") ^^ break ^^
	      (text (transition j)) ^^ break ^^
	      (text "[") ^^
	      (text ("label=\"" ^ s ^ "\"")) ^^
	      (text "]") ^^ (text ";")) in
	  Some (hgrp d)

let doc_of_interval_option foo n =
  let p = ref empty
  and b = ref false in
    begin
      for i=0 to n-1 do
	match foo i with
	    None -> ()
	  | Some d ->
		if !b
		then p := (!p) ^^ break ^^ d
		else
		  begin
		    b := true;
		    p := d
		  end
      done;
      !p
    end

let doc_of_interval foo n =
  doc_of_interval_option (fun i -> Some (foo i)) n

let doc_of_region r i =
  let n = Region.dimension r in
  let p = doc_of_interval_option (doc_of_edge r i) n in
    begin
      nest 2 (vgrp p)
    end

let doc_of_transition i =
  hgrp ((text (transition i)) ^^ break ^^ (text "[") ^^ (text ("label=\"" ^ (transition_label i) ^ "\",")) ^^ (text "shape=box") ^^ (text "];"))

let doc_of_place r i =
  hgrp ((text (place i)) ^^ break ^^ (text "[") ^^ (text ("label=\"" ^ (place_label r) ^ "\",")) ^^ (text "shape=circle") ^^ (text "];"))

(* [net_to_doc a n] generates dot code in doc format from FF-net [a]. The graph is named [n] *)

(* val net_to_doc : Region.n -> string -> Pp.doc *)

let net_to_doc l w =
  let a = Array.of_list l in
  let m = Array.length a in (* number of places *)
    if m <= 0
    then empty
    else
      let n = Region.dimension a.(0) in (* number of transitions *)
	vgrp
	  ((hgrp ((text "graph") ^^ break ^^ (text w) ^^ break ^^ (text "{"))) ^^ break ^^
	      (nest 2 (fgrp (
		global_attr ^^ break ^^
		  (* iterates on transitions *)
		  (doc_of_interval doc_of_transition n) ^^ break ^^
		  (* iterates on places *)
		  (doc_of_interval (fun i -> doc_of_place a.(i) i) m) ^^ break ^^
		  (* iterates on regions *)
		  (doc_of_interval (fun i -> doc_of_region a.(i) i) m)))) ^^
		  (* end *)
		  break ^^ (text "}"))

(* [net_to_file a n m] generates dot code in file [m] from FF-net [a]. The graph is named [n] *)

(* val net_to_file : Region.n -> string -> string -> unit *)

let net_to_file a n m = Ppwrap.to_file (net_to_doc a) m n

(* [net_display a n] displays FF-net [a] using the GraphViz dot command. The graph is named [n] *)

(* val net_display : Region.n -> string -> unit *)

let net_display a n = Ppwrap.display_dot (net_to_doc a) n

(* [net_to_pdf a n m] generates a PDF file named [m] from FF-net [a] using the GraphViz dot command. The graph is named [n] *)

(* val net_to_pdf : Region.n -> string -> string -> unit *)

let net_to_pdf a n m = Ppwrap.dot_to_pdf (net_to_doc a) m n

