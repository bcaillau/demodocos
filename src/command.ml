(***********************************************************************)
(*                                                                     *)
(*                  BDL: a Behaviour Description Language              *)
(*                                                                     *)
(*            Benoit Caillaud, projet Pampa, IRISA/INRIA-Rennes        *)
(*                                                                     *)
(*  Copyright 1999 Institut National de Recherche en Informatique et   *)
(*  Automatique.  Distributed only by permission.                      *)
(*                                                                     *)
(***********************************************************************)

(*
 *                                                                     
 * Command line parsing library
 *
 * $Header: /udd/reutel/bdl/referentiel/bdl/src/mylib/command.ml,v 1.7 2000/08/04 16:08:22 bcaillau Exp $
 * 
 * $Log: command.ml,v $
 * Revision 1.7  2000/08/04 16:08:22  bcaillau
 * ajout des options multiples
 *
 * Revision 1.6  2000/01/10 18:39:56  bcaillau
 * 1) correction Makefile dans mylib et btd
 * 2) correction bug dans btd_command.ml
 *
 * Revision 1.5  2000/01/10 14:03:19  bcaillau
 * Interim (c'est boggue)
 *
 * Revision 1.4  2000/01/06 16:12:00  bcaillau
 * 1) Suite du parser de lignes de commandes
 * 2) Ajout du module proncipal de la commande BTD
 *
 * Revision 1.3  2000/01/04 17:33:59  bcaillau
 * ajout des methodes test_... et models
 *
 * Revision 1.2  2000/01/04 13:02:58  bcaillau
 * Modification dues parametres de la classe generic command
 * Ligne de commande de btd
 *
 * Revision 1.1  1999/12/27 16:22:18  bcaillau
 * Ajout du module mylib::Command
 *
 *
 *)

open Pp

let rec pp_list f =
  function
      [] -> empty
    | [x] -> f x
    | x::l -> (f x) ^^ break ^^ (pp_list f l)

exception Syntax_error of string

class type token_type =
  object
    method add_exclude : token_type -> unit
    method add_exclude_list : token_type list -> unit
    method add_implies : token_type list -> unit
    method add_implies_list : token_type list list -> unit
    method clear : unit
    method tag : string
    method schema : string
    method comment : string
    method select : string Stream.t -> unit
    method evaluate : unit
    method excludes : token_type list
    method implies : token_type list list
    method is_excluded : bool
    method is_enabled : bool
    method is_complete : bool
    method is_selected : bool
    method test_is_excluded : token_type list -> bool
    method test_is_enabled : token_type list -> bool
    method test_is_complete : token_type list -> bool
    method test_is_selected : token_type list -> bool
  end

class type command_type =
  object
    method add_token : token_type -> unit
    method add_token_list : token_type list -> unit
    method add_conjunction : token_type list -> unit
    method add_conjunction_list : token_type list list -> unit
    method clear : unit
    method parse : string Stream.t -> unit
    method evaluate : unit
    method print_usage : out_channel -> unit
    method models : token_type list list
    method is_valid : bool (* every selected token is not excluded and enabled *)
    method is_complete : bool (* one conjunction is satisfied *)
    method is_correct : bool (* both valid and complete *)
    method print_diagnosis : out_channel -> unit
    method test_is_excluded : token_type list -> bool
    method test_is_enabled : token_type list -> bool
    method test_is_complete : token_type list -> bool
    method parameters : string list
  end

class token_generic
    (tag0 : string) (* tag *)
    (schema0 : string) (* schema *)
    (comment0 :  string) (* comment *)
    (arity0 : int) (* arity *)
    (evaluation0 : string array -> unit) (* evaluation function *) =
  object (self)
    val _tag = tag0
    val _schema = schema0
    val _comment = comment0
    val _arity = arity0
    val _evaluation = evaluation0
    val mutable _excludes = ([] : token_type list)
    val mutable _implies = ([] : token_type list list)
    val parameters = Array.make arity0 ""
    val mutable selected = false
    method add_exclude t = _excludes <- t::_excludes
    method add_exclude_list l = _excludes <- l@_excludes 
    method add_implies l = _implies <- l::_implies
    method add_implies_list l = _implies <- l@_implies
    method clear = selected <- false
    method tag = _tag
    method schema = _schema
    method comment = _comment
    method select s =
      if selected
      then raise (Syntax_error ("Option "^_tag^" selected several times"))
      else
	begin
	  begin
	    try
	      for i = 0 to _arity-1 do
		parameters.(i) <- Stream.next s
	      done
	    with
	      Stream.Failure ->
		raise 
		  (Syntax_error
		     ("Option "^_tag^" expects "^(string_of_int _arity)^
		      " parameters : "^_schema))
	  end;
	  selected <- true
	end
    method evaluate = if selected then _evaluation parameters
    method excludes = _excludes
    method implies = _implies
    method is_excluded = List.exists (fun t -> t#is_selected) _excludes
    method is_enabled =
      List.exists
	(fun l ->
	  List.for_all (fun t -> t#is_selected) l)
	_implies
    method is_complete =
      (not selected) || ((not self#is_excluded) && self#is_enabled) 
    method is_selected = selected
    method test_is_excluded l =
      List.exists (fun t -> t#test_is_selected l) _excludes
    method test_is_enabled l =
      List.exists
	(fun c ->
	  List.for_all (fun t -> t#test_is_selected l) c)
	_implies
    method test_is_complete l =
      (not (self#test_is_selected l)) ||
      ((not (self#test_is_excluded l)) &&
       (self#test_is_enabled l))
    method test_is_selected l = List.memq (self :> token_type) l
  end

class token_generic_multiple
    (tag0 : string) (* tag *)
    (schema0 : string) (* schema *)
    (comment0 :  string) (* comment *)
    (arity0 : int) (* arity *)
    (evaluation0 : string array list -> unit) (* evaluation function *) =
  object (self)
    val _tag = tag0
    val _schema = schema0
    val _comment = comment0
    val _arity = arity0
    val _evaluation = evaluation0
    val mutable _excludes = ([] : token_type list)
    val mutable _implies = ([] : token_type list list)
    val mutable parameters = []
    val mutable selected = false
    method add_exclude t = _excludes <- t::_excludes
    method add_exclude_list l = _excludes <- l@_excludes 
    method add_implies l = _implies <- l::_implies
    method add_implies_list l = _implies <- l@_implies
    method clear = selected <- false
    method tag = _tag
    method schema = _schema
    method comment = _comment
    method select s =
      let p = Array.make _arity "" in
      begin
	begin
	  try
	    for i = 0 to _arity-1 do
	      p.(i) <- Stream.next s
	    done
	  with
	    Stream.Failure ->
	      raise 
		(Syntax_error
		   ("Option "^_tag^" expects "^(string_of_int _arity)^
		    " parameters : "^_schema))
	end;
	selected <- true;
	parameters <- parameters @ [p]
      end
    method evaluate = if selected then _evaluation parameters
    method excludes = _excludes
    method implies = _implies
    method is_excluded = List.exists (fun t -> t#is_selected) _excludes
    method is_enabled =
      List.exists
	(fun l ->
	  List.for_all (fun t -> t#is_selected) l)
	_implies
    method is_complete =
      (not selected) || ((not self#is_excluded) && self#is_enabled) 
    method is_selected = selected
    method test_is_excluded l =
      List.exists (fun t -> t#test_is_selected l) _excludes
    method test_is_enabled l =
      List.exists
	(fun c ->
	  List.for_all (fun t -> t#test_is_selected l) c)
	_implies
    method test_is_complete l =
      (not (self#test_is_selected l)) ||
      ((not (self#test_is_excluded l)) &&
       (self#test_is_enabled l))
    method test_is_selected l = List.memq (self :> token_type) l
  end

let rec stream_to_list s =
  match Stream.peek s with
    None -> []
  | Some x ->
      begin
	Stream.junk s;
	x::(stream_to_list s)
      end

class command_generic
    (header0 : string) (* header for usage *)
    (tokens0 : token_type list) (* tokens *)
    (conjunction0 : token_type list list) (* disjunction of conjunctive clauses *) =
  object (self)
    val header = header0
    val mutable contains = tokens0
    val mutable conjunction = conjunction0
    val mutable _parameters = ([] : string list)
    method add_token t = contains <- t::contains
    method add_token_list l = contains <- l@contains
    method add_conjunction l = conjunction <- l::conjunction
    method add_conjunction_list l = conjunction <- l@conjunction
    method clear =
      begin
	List.iter (fun t -> t#clear) contains;
	_parameters <- []
      end
    method parse s =
      let x = ref (Stream.peek s) in
      begin
	while !x <> None do
	  match !x with
	    None -> failwith "Command.command_generic#parse: internal error"
	  | Some y ->
	      begin
		if ((String.length y) >= 2) && (y.[0] = '-') && (y.[1] <> '-') 
		then
		  begin
		    Stream.junk s;
		    (if not (List.fold_left
			       (fun r t ->
				 if r
				 then
				   true
				 else
				   if t#tag = y
				   then
				     begin
				       t#select s;
				       true
				     end
				   else
				     false)
			       false
			       contains)
		    then raise (Syntax_error ("Unknown option: "^y)));
		    x := Stream.peek s
		  end
		else
		  begin
		    (if y  = "--" then Stream.junk s);
		    x := None
		  end
	      end
	done;
	_parameters <- stream_to_list s
      end
    method evaluate = List.iter (fun t -> t#evaluate) contains
    method print_usage ch =
      let mdls = self#models in
      ppToFile
	ch
	80 (* 80 columns *)
	(vgrp
	   (
	    (text "Command name:") ^^
	    (nest 1 (break ^^ (text header))) ^^ break ^^
	    (text "Options:") ^^ break ^^
	    (nest 1
	       (List.fold_left
		  (fun w t -> w ^^ break ^^ (text t#comment))
		  empty
		  contains
	       )
	    ) ^^ break ^^
	    (text "Examples:") ^^ break ^^
	    (nest 1
	       (
		vgrp
		  (
		   pp_list
		     (fun mdl ->
		       hgrp
			 ((text header) ^^ break ^^
			  (pp_list (fun t -> (text t#schema)) mdl)))
		     mdls
		  )
	       )
	    )
	   )
	)
    method models =
      let m = ref [] in
      let rec mdls sel rem =
	let x = self#test_is_excluded sel
	and e = self#test_is_enabled sel
	and c = self#test_is_complete sel in
	begin
	  if (not x) then
	    begin
	      if e && c then m := sel :: !m;
	      extd sel rem
	    end
	end
      and extd sel = function
	  [] -> ()
	| h::t ->
	    begin
	      mdls (h::sel) t;
	      extd sel t
	    end
      in
      begin
	mdls [] contains;
	!m
      end
    method is_valid =
      List.for_all
	(fun t -> (not t#is_selected) || (t#is_enabled && (not t#is_excluded)))
	contains
    method is_complete =
      List.exists
	(fun l ->
	  List.for_all
	    (fun t -> t#is_selected)
	    l)
	conjunction
    method is_correct = self#is_valid && self#is_complete
    method print_diagnosis ch =
      ppToFile
	ch
	80 (* 80 columns *)
	(vgrp
	   ((hgrp
	       ((text "Parsed options:") ^^ 
		(List.fold_left
		   (fun p t ->
		     if t#is_selected
		     then p ^^ break ^^ (text t#schema)
		     else p)
		   empty
		   contains))) ^^ break ^^
	    (hgrp
	       ((text "Parameters:") ^^
		(List.fold_left
		   (fun p x ->
		     p ^^ break ^^ (text x))
		   empty
		   _parameters))) ^^ break ^^
	    (List.fold_left
	       (fun p t ->
		 if t#is_selected
		 then
		   let x =
		     List.filter (fun t' -> t'#is_selected) t#excludes in
		   if x = []
		   then p
		   else
		     p ^^
		     (hgrp
			(
			 (text ("Option "^t#tag^" excludes option(s):")) ^^
			 break ^^
			 (pp_list (fun t' -> (text t'#tag)) x)
			)
		     ) ^^ break
		 else p)
	       empty
	       contains
	    ) ^^ break ^^
	    (List.fold_left
	       (fun p t ->
		 if t#is_selected && (not t#is_enabled)
		 then
		   let x =
		     List.fold_left
		       (fun q l ->
			 List.fold_left
			   (fun q' t' ->
			     if t'#is_selected || (List.memq t' q')
			     then q' else t'::q')
			   q
			   l)
		       []
		       t#implies in
		   if x = []
		   then p
		   else
		     p ^^ break ^^
		     (hgrp
			(
			 (text ("Option "^t#tag^" may require option(s):")) ^^
			 break ^^
			 (pp_list (fun t' -> (text t'#tag)) x)
			)
		     )
		 else p)
	       empty
	       contains
	    ) ^^ break ^^
	    (List.fold_left
	       (fun q l ->
		 List.fold_left
		   (fun p t ->
		     if t#is_selected || t#is_excluded
		     then p
		     else
		       p ^^ break ^^
		       (text ("Option "^t#tag^" might be required"))
		   )
		   q
		   l)
	       empty
	       conjunction
	    )
	   )
	)
    method test_is_excluded l =
      List.exists
	(fun t ->
	  (t#test_is_selected l) &&
	  (t#test_is_excluded l))
	contains
    method test_is_enabled l =
      List.for_all
	(fun t ->
	  (not (t#test_is_selected l)) ||
	  (t#test_is_enabled l))
	contains
    method test_is_complete l =
      List.exists
	(fun c ->
	  List.for_all
	    (fun t -> (t#test_is_selected l))
	    c)
	conjunction
    method parameters = _parameters
  end

let pairwise_exclude t =
  let n = Array.length t in
  for i = 0 to n-1 do
    for j = 0 to n-1 do
      if i <> j then t.(i)#add_exclude t.(j)
    done
  done

