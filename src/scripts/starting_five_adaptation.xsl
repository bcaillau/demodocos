<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" encoding="ISO-8859-1" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template >

  <xsl:template match="iSPM" >
    <iSPM>
      <xsl:apply-templates select="header/info" />
      <xsl:apply-templates select="data" />
    </iSPM>
  </xsl:template >
  
  <xsl:template match="header/info" >    
    <header>
        <info>
	   <type><xsl:value-of select="type"></xsl:value-of></type>
	   <version><xsl:value-of select="version"></xsl:value-of></version>
        </info>
    </header>
  </xsl:template >
  
  <xsl:template match="data" >
    <data>
        <phase_list/>
        <step_list/>
      	<xsl:apply-templates select="action_list" />
    </data>
  </xsl:template >
  
  <xsl:template match="action_list" >
     <action_list>
        <action>
          <id>0</id>
          <temporal_region>
            <start_time/>
            <stop_time/>
          </temporal_region>
          <parent_id>0</parent_id>
          <affected_list>
            <affected>starting_system</affected>
          </affected_list>
          <instrument_list/>
          <state>0</state>
          <actuator></actuator>
          <verb>starting_action</verb>
          <origin/>
          <destination/>
        </action>
        <xsl:apply-templates select="action" />
     </action_list>
  </xsl:template >

  <xsl:template match="action" >
    <action>
       <id><xsl:value-of select="id"></xsl:value-of></id>
       <temporal_region>
          <start_time><xsl:value-of select="start_time"></xsl:value-of></start_time>
          <stop_time><xsl:value-of select="stop_time"></xsl:value-of></stop_time>
       </temporal_region>
       <parent_id><xsl:value-of select="parent_id"></xsl:value-of></parent_id>
       <xsl:apply-templates select="affected_list" />
       <xsl:apply-templates select="instrument_list" />
       <state><xsl:value-of select="state"></xsl:value-of></state>
       <actuator><xsl:value-of select="actuator"></xsl:value-of></actuator>
       <verb><xsl:value-of select="verb"></xsl:value-of></verb>
       <!-- origin -->
       <origin><xsl:value-of select="origin"></xsl:value-of></origin>
       <!-- destination -->
       <destination><xsl:value-of select="destination"></xsl:value-of></destination>
    </action>
  </xsl:template >

  <xsl:template match="affected_list" >
    <affected_list>
       <xsl:apply-templates select="affected" />
    </affected_list>
  </xsl:template >

  <xsl:template match="affected" >
    <affected><xsl:value-of select="."></xsl:value-of></affected>
  </xsl:template >

  <xsl:template match="instrument_list" >
    <instrument_list>
       <xsl:apply-templates select="instrument" />
    </instrument_list>
  </xsl:template >

  <xsl:template match="instrument" >
    <instrument><xsl:value-of select="."></xsl:value-of></instrument>
  </xsl:template >

</xsl:stylesheet>
