<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" encoding="ISO-8859-1" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template >

  <xsl:template match="iSPM" >
    <iSPM>
      <xsl:apply-templates select="header/info" />
      <xsl:apply-templates select="data" />
    </iSPM>
  </xsl:template >
  
  <xsl:template match="header/info" >    
    <header>
        <info>
	   <type><xsl:value-of select="type"></xsl:value-of></type>
	   <version><xsl:value-of select="version"></xsl:value-of></version>
        </info>
    </header>
  </xsl:template >
  
  <xsl:template match="data" >
    <data>
        <phase_list/>
        <step_list/>
      	<xsl:apply-templates select="action_list" />
    </data>
  </xsl:template >
  
  <xsl:template match="action_list" >
     <action_list>
        <action>
          <id>0</id>
          <temporal_region>
            <start_time/>
            <stop_time/>
          </temporal_region>
          <parent_id>0</parent_id>
          <affected_list>
            <affected>starting_system</affected>
          </affected_list>
          <instrument_list/>
          <state>0</state>
          <actuator></actuator>
          <verb>starting_action</verb>
          <origin/>
          <destination/>
        </action>
        <xsl:apply-templates select="action" />
     </action_list>
  </xsl:template >

  <xsl:template match="action" >
    <action>
       <id><xsl:value-of select="id"></xsl:value-of></id>
       <temporal_region>
          <start_time><xsl:value-of select="start_time"></xsl:value-of></start_time>
          <stop_time><xsl:value-of select="stop_time"></xsl:value-of></stop_time>
       </temporal_region>
       <parent_id><xsl:value-of select="parent_id"></xsl:value-of></parent_id>
       <xsl:apply-templates select="affected_list" />
       <xsl:apply-templates select="instrument_list" />
       <state><xsl:value-of select="state"></xsl:value-of></state>
       <actuator><xsl:value-of select="actuator"></xsl:value-of></actuator>
       <verb><xsl:value-of select="verb"></xsl:value-of></verb>
       <!-- origin -->
       <xsl:choose>
          <xsl:when test="verb = 'grabbing_an_object'">
             <origin></origin>
          </xsl:when>
          <xsl:when test="verb = 'grabbing_an_object_with_an_instrument'">
             <origin></origin>
          </xsl:when>
          <xsl:when test="verb = 'grabbing_an_object_with_both_hands'">
             <origin></origin>
          </xsl:when>
          <xsl:when test="origin = 'site_on_table'">
             <origin>table</origin>
          </xsl:when>
          <xsl:otherwise>
             <origin><xsl:value-of select="origin"></xsl:value-of></origin>
          </xsl:otherwise>
        </xsl:choose>
       <!-- destination -->
       <xsl:choose>
          <xsl:when test="verb = 'cutting_an_object'">
             <destination></destination>
          </xsl:when>
          <xsl:when test="destination = 'towel'">
             <destination>towel_location</destination>
          </xsl:when>
          <xsl:when test="destination = 'tray'">
             <destination>tray_location</destination>
          </xsl:when>
          <xsl:when test="destination = 'site_on_table' and verb = 'positioning_an_object_in_some_place_with_both_hands' and affected_list/affected = 'tray'">
             <destination>table_left_corner</destination>
          </xsl:when>
          <xsl:when test="destination = 'site_on_table'">
             <destination>table</destination>
          </xsl:when>
          <xsl:otherwise>
             <destination><xsl:value-of select="destination"></xsl:value-of></destination>
          </xsl:otherwise>
        </xsl:choose>
       
    </action>
  </xsl:template >

  <xsl:template match="affected_list" >
    <affected_list>
       <xsl:apply-templates select="affected" />
    </affected_list>
  </xsl:template >

  <xsl:template match="affected" >
    <xsl:choose>
       <xsl:when test=". = 'Size_2_russian_doll'">
          <affected>size_2_russian_doll</affected>
       </xsl:when>
       <xsl:when test=". = 'Size_3_russian_doll'">
          <affected>size_3_russian_doll</affected>
       </xsl:when>
       <xsl:otherwise>
          <affected><xsl:value-of select="."></xsl:value-of></affected>
       </xsl:otherwise>
    </xsl:choose>
  </xsl:template >

  <xsl:template match="instrument_list" >
    <instrument_list>
       <xsl:apply-templates select="instrument" />
    </instrument_list>
  </xsl:template >

  <xsl:template match="instrument" >
    <instrument><xsl:value-of select="."></xsl:value-of></instrument>
  </xsl:template >

  <xsl:template match="destination" >
    <xsl:choose>
       <xsl:when test=". = 'tea_cup'">
          <location>tea_cup_location</location>
       </xsl:when>
       <xsl:otherwise>
          <location><xsl:value-of select="."></xsl:value-of></location>
       </xsl:otherwise>
    </xsl:choose>
  </xsl:template >

</xsl:stylesheet>
