(*
 *
 *  Doc Format of A7 xSPM (iSPM as xes file)
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* This module provides standard text corresponding to doc format
 * of A7 xSPM. It's use to generate an iSPM as xes file corresponding
 * to an abstract seven.
 *)

  
(*-----------------------------------------------------------------
 * Functions to generates doc containing mutual xSPM-1 and xSPM-2 text
 * (iSPM as xes file)
 *-----------------------------------------------------------------*)

val xml_prologue_text : Pp.doc
                      
val log_text : Pp.doc -> Pp.doc

val usual_heading_text : Pp.doc
  
val trace_text : Pp.doc -> Pp.doc
  
val event_text : Pp.doc -> Pp.doc
  
  
  
(*-----------------------------------------------------------------
 * Functions to generates doc containing specific xSPM-1 text
 * (iSPM as xes file)
 *-----------------------------------------------------------------*)

val xspm1_heading_text : Pp.doc

val xspm1_trace_text : string -> string -> Pp.doc -> Pp.doc   
  
val xspm1_event_text : string -> Pp.doc


  
(*-----------------------------------------------------------------
 * Functions to generates doc containing specific xSPM-2 text
 * (iSPM as xes file)
 *-----------------------------------------------------------------*)

val xspm2_heading_text : Pp.doc
  
val xspm2_trace_text : Pp.doc -> Pp.doc
   
val xspm2_event_text : string array -> string list -> string list -> Pp.doc
