(*
 *
 * Module to load a configuration 
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * The main function of this module is to load a configuration 
 * (from a config file).
 *) 

(*-----------------------------------------------------------------
 * Module openning, parameter & utilities
 *-----------------------------------------------------------------*)

(* functions to obtain a given option type from a string *)

let string_option_of_string s = match s with "" -> None | x -> Some x

let int_option_of_string s = match s with "" -> None | x -> Some (int_of_string x)

                                    
(* function to obtain a string list from a string, wherein each substring
 *  are delimited by a separator character (sep) *)

let rec string_list_of_string sep s_source result = 
  let filter s = not (s = "") in 
  try
    match (s_source, (String.contains s_source sep)) with
    | (_, true) ->
       let start_index = (String.rindex s_source sep) + 1 in
       let sub_length = (String.length s_source) - start_index in
       let rest_length = (start_index - 1) in
       let (sub_source, rest_source) =
         (String.sub s_source (start_index) sub_length,
          String.sub s_source 0 rest_length)
       in
       let result = sub_source::result in
       string_list_of_string sep rest_source result
    | _ -> List.filter filter (s_source::result)
  with _ -> List.filter filter result


                                                          
(*-----------------------------------------------------------------
 * Generic functions to analyze the elements of a xml document
 *-----------------------------------------------------------------*)

(* function to filter the elements of a XML document for a list of given tags *)
                                                          
let filter_elements target_tags_list xml_doc =
  List.filter
    (function
     | Xml.Element (tag,_,_) -> List.mem tag target_tags_list
     | Xml.PCData _ -> false)
    xml_doc

  
(* function to filter the elements of a sequence for a list of given tags *)
  
let filter_elements_of_sequence target_tags_list sequence =
  let f target_list seq =
    match seq with
    | Xml.Element (_, _, children) ->
       List.rev_append target_list (filter_elements target_tags_list children)
    | _ -> target_list
  in
  List.fold_left f [] sequence
  
  
(* function to obtain a value corresponding to an attribute key *)
  
let rec get_string_attr key s0 att_list =
  match att_list with
  | [] -> s0
  | (h,s) :: l ->
     if h = key
     then s
     else get_string_attr key s0 l
    

(* function to map the attributes associated to a list of
 * xml element instance (element appearing several times), 
 * from a root xml element *)
    
let map_attributes_of_xml_element src_xml att_mapping e_name =
  let elements = filter_elements [e_name] src_xml in
  let e_mapping e =
    match e with
    | Xml.Element (_, attributes, _) -> att_mapping attributes
    | _ -> att_mapping []
  in
  List.map e_mapping elements
    

(* function to obtain a values list of an attribute associated to 
 * a xml elements list (element appearing several times), 
 * from a root xml element *)
    
let get_values_of_attributes_from_xml src_xml e_name att_name default_value =
  let att_mapping attr =
    get_string_attr att_name default_value attr
  in
  map_attributes_of_xml_element src_xml att_mapping e_name
  

(* function to obtain a single value of an attribute associated to 
 * a xml element, from a root xml element *)
  
let get_value_of_attribute_from_xml xml e_name att_name default_value =
  List.hd (get_values_of_attributes_from_xml xml e_name att_name default_value) 

  
                  
(*-----------------------------------------------------------------
 * Load Main Configuration
 *-----------------------------------------------------------------*)  

(* function to obtain a config value from a xml element (xconfig) *)
  
let get_config_value_from_xconfig xconfig e_name default_value =
  let config_element = List.hd (filter_elements [e_name] xconfig) in
  match config_element with
  | Xml.Element (_, attributes, _) ->
     get_string_attr "value" default_value attributes
  | _ -> default_value 

       
(* function to load a config value from a xml element (xconfig), 
 * using the given set function and the given convert function *)
       
let load_value xconfig f_set f_convert e_name =
  try
    let default_value = "" in
    let value = (get_value_of_attribute_from_xml xconfig e_name "value" default_value) in
    match value with
    | "" | "None" | "none" -> ()
    | v -> f_set (f_convert v)
  with _ -> ()
          

(* function to load all config values from a xml element (xconfig) *)
          
let load_all_config_values t xconfig =

  (* functions for loading *)
  let f_identite x = x in
  let f_string_list sep x = string_list_of_string sep x [] in 
  let f_load_value f_set f_data_from_string s_data =
    load_value xconfig (f_set t) f_data_from_string s_data;
  in
  let f_load_string f_set s_data = f_load_value f_set f_identite s_data in
  let f_load_string_opt f_set s_data =
    f_load_value f_set string_option_of_string s_data in
  let f_load_int f_set s_data = f_load_value f_set int_of_string s_data in
  let f_load_int_opt f_set s_data = f_load_value f_set int_option_of_string s_data in
  let f_load_bool f_set s_data = f_load_value f_set bool_of_string s_data in
  let f_load_string_list f_set s_data = f_load_value f_set (f_string_list ' ') s_data in
  
  begin
    
    (* synthesis configuration *)
    f_load_string Configuration.set_input_working_file_postfix "input_working_file_postfix";
    f_load_string Configuration.set_input_adapting_script "input_adapting_script";
    f_load_string Configuration.set_classifier_choice "classifier_choice";
    f_load_string_opt Configuration.set_ontology_base_file "ontology_base_file";
    f_load_string_list Configuration.set_ontology_global_files_list "ontology_global_files";
    f_load_string Configuration.set_ontology_label_type "ontology_label_type";
    f_load_int Configuration.set_verbose "verbose";
    f_load_int_opt Configuration.set_instances_number "instances_number";
    
    (* approximation configuration*)
    f_load_bool Configuration.set_approximation_requested "approximation_requested"; 
    f_load_int Configuration.set_approximation_limit "approximation_limit";
    
    (* output configuration *)
    f_load_string_opt Configuration.set_output_directory "output_directory";
    f_load_string_opt Configuration.set_output_tnfnet "output_tnfnet";
    f_load_string_opt Configuration.set_output_tnfgraph "output_tnfgraph";
    f_load_string_opt Configuration.set_output_seven "output_seven";
    f_load_string_opt Configuration.set_output_a7xspm "output_a7xspm";
    f_load_string_opt Configuration.set_output_events_table "output_events_table";
    (* f_load_bool Configuration.set_output_tnfnet_display "output_tnfnet_display"; *)
    
    (* options for output computing *)
    f_load_string Configuration.set_tnfgraph_label_type "tnfgraph_label_type";
    f_load_string Configuration.set_seven_sensor_label_type "seven_sensor_label_type";
    f_load_string Configuration.set_seven_five_value_type "seven_five_value_type";
    f_load_bool Configuration.set_seven_location_computing "seven_location_computing";
    f_load_bool Configuration.set_seven_analysis "seven_analysis";
    f_load_int Configuration.set_seven_event_number_limit "seven_event_number_limit";
    
  end

  
(* function to load the configuration from a config file *)
  
let load_config t config_file =
  let xconfig = Xml.parse_file config_file in
  match xconfig with
  | Xml.Element (tag, attributes, children) ->
     if tag = ConfigurationDocFormat.mainconfig_root_param
     then load_all_config_values t children
     else invalid_arg "ConfigurationIn.load: syntax error"
  | _ -> invalid_arg "ConfigurationIn.load: syntax error"

       
  
(*-----------------------------------------------------------------
 * Load Global Classifiers Configuration
 *-----------------------------------------------------------------*)  

(* function to obtain the separator from a xml element *)

let get_separator_from_xml xconfig =
  let e_name = "separator"
    and att_name = "value"
    and default_sep = ";"
  in
  let sep =
    get_value_of_attribute_from_xml xconfig e_name att_name default_sep
  in
  let sep =
    match (String.length sep) > 0 with
    | true -> sep
    | _ -> default_sep
  in
  String.get sep 0


(* function to split a classifier keys string to obtain a list of keys *)

let rec split_classifier_keys sep keys result =
  try
    match (String.contains keys sep) with
    | true ->
       let start_index = (String.rindex keys sep) + 1 in
       let sub_length = (String.length keys) - start_index in
       let rest_length = (start_index - 1) in
       let (sub_keys, rest_keys) =
         (String.sub keys (start_index) sub_length,
          String.sub keys 0 rest_length)
       in
       let result = sub_keys::result in
       split_classifier_keys sep rest_keys result
    | _ -> keys::result
  with _ -> result
  
       
(* function to obtain the list of prefix (group, prefix) from a xml element *)
  
let get_prefix_list_from_xml xconfig e_name =
  try
    let att_mapping attr =
      let default_string = "" in
      let f_get_string a_name = (get_string_attr a_name default_string attr) in
      let group = f_get_string "group" in
      let prefix = f_get_string "value" in
      (group, prefix)
    in
    (map_attributes_of_xml_element xconfig att_mapping e_name)
  with _ -> []
  
       
(* function to obtain the list of keys (group, keys) from a xml element *)
  
let get_keys_list_from_xml xconfig e_name =
  try
    let sep = get_separator_from_xml xconfig in
    let att_mapping attr =
      let default_string = "" in
      let f_get_string a_name = (get_string_attr a_name default_string attr) in
      let group = f_get_string "group" in
      let keys = f_get_string "value" in
      let keys = split_classifier_keys sep keys [] in
      (group, keys)
    in
    (map_attributes_of_xml_element xconfig att_mapping e_name)
  with _ -> []


(* function to obtain the classifier def (name, group, keys) from a xml element *)  
       
let get_classifiers_list_from_xml xconfig e_name =
  try
    let sep = get_separator_from_xml xconfig in
    let att_mapping attr =
      let default_string = "" in
      let f_get_string a_name = (get_string_attr a_name default_string attr) in
      let name = f_get_string "name" in
      let group = f_get_string "group" in
      let keys = f_get_string "keys" in
      let keys = split_classifier_keys sep keys [] in
      (name, group, keys)
    in
    (map_attributes_of_xml_element xconfig att_mapping e_name)
  with _ -> []

          
(* function to update the classifiers configuration from a xml document (xconfig) *)

let update_classifiers_config_from_xconfig t xconfig =
  let f_set_separator s = Configuration.set_classifier_separator t s in
  let f_add_classifier_prefix (g, p) = Configuration.add_classifier_prefix t g p in
  let f_add_classifier_keys (g, k) = Configuration.add_classifier_keys t g k in
  let f_add_classifier_def c = Configuration.add_classifier_def t c in
  let classifier_separator = String.make 1 (get_separator_from_xml xconfig) in
  let classifier_prefix_list = get_prefix_list_from_xml xconfig "prefix" in
  let classifier_keys_list = get_keys_list_from_xml xconfig "keys" in
  let classifier_def_list = get_classifiers_list_from_xml xconfig "def" in
  begin
    Configuration.init_classifier_prefix_list t;
    Configuration.init_classifier_keys_list t;
    Configuration.init_classifier_def_list t;
    f_set_separator classifier_separator;
    List.iter f_add_classifier_prefix (List.rev classifier_prefix_list);
    List.iter f_add_classifier_keys (List.rev classifier_keys_list);
    List.iter f_add_classifier_def (List.rev classifier_def_list)
  end


(* function to load the global classifiers from a classifiers file *)
  
let load_classifiers t classifiers_config_file =
  let xconfig = Xml.parse_file classifiers_config_file in
  let c_root_tag = ConfigurationDocFormat.classifiers_root_param in
  match xconfig with
  | Xml.Element (tag, _, children) when tag = c_root_tag ->
     (update_classifiers_config_from_xconfig t children)
  | _ -> invalid_arg "ConfigurationIn.load_classifiers: error on classifiers file"
