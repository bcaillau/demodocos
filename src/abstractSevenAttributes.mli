(*
 *
 * Attributes of sensor/event for #SEVEN abstract structure
 *
 * (c) -- <--@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* This module provides functions to obtain the attributes of
 * sensor/event.
 * The functions works from ontology of classifiers to extract
 * events informations and process on them.
 * The result is a string array with the attributes to be associated
 * with the sensors.
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities
 *-----------------------------------------------------------------*)

(* function to remove double quote from a string, if present *)

val disstringify : string -> string


  (* types for five entities *)
type f_relation = string
type f_objects = (string * string) list
               

(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)

(* function to obtain a sorted array of event labels *)
     
val get_event_label : Xes.event -> string
val get_event_labels_of_ontology : Xes.event array -> string array

  
(* function to obtain a sorted array of event five relation *)

val get_event_five_relation : Xes.event -> f_relation
val get_event_five_relations_of_ontology : Xes.event array -> f_relation array

  
(* function to obtain a sorted array of event five objects list *)

val get_event_five_objects_list : Xes.event -> f_objects
val get_event_five_objects_lists_of_ontology : Xes.event array -> f_objects array
  
