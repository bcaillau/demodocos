(*
 *
 * Event from data
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* This module provides a data structure to define an event from data,
 * and functions related to this structure. 
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities
 *-----------------------------------------------------------------*)

(* functions to compute a letter tag corresponding to an int *)

let encode_int_to_letter i =
  match i with
  |  0 -> "a" |  1 -> "b" |  2 -> "c" |  3 -> "d" |  4 -> "e"
  |  5 -> "f" |  6 -> "g" |  7 -> "h" |  8 -> "i" |  9 -> "j"
  | 10 -> "k" | 11 -> "l" | 12 -> "m" | 13 -> "n" | 14 -> "o"
  | 15 -> "p" | 16 -> "q" | 17 -> "r" | 18 -> "s" | 19 -> "t"
  | 20 -> "u" | 21 -> "v" | 22 -> "w" | 23 -> "x" | 24 -> "y" | 25 -> "z"
  | _ -> ""

let rec compute_letter_tag_of_int i result =
  match i > 0 with
  | true ->
     let (i1, i2) = (i / 26, i mod 26) in
     let new_result = (encode_int_to_letter i2) ^ result in
     compute_letter_tag_of_int i1 new_result
  | false -> result

let compute_letter_tag i =
  match i <  26 with
  | true -> (encode_int_to_letter i)
  | false -> (compute_letter_tag_of_int i "")
           
   
(* function to add the double quotes around a string *)
   
let stringify s = "\"" ^ (String.escaped s) ^ "\""
                

(* function to remove double quote from a string, if present *)
(* val disstringify : string -> string *)
                                         
let disstringify s =
     let s_length = String.length s in
     let s1_length = min 1 s_length in
     let s2_start = min 1 s_length in
     let s2_length = max (s_length - 2) 0 in
     let s3_start = max ((String.length s) - 1) 0 in
     let s3_length = min 1 s3_start in
     let s1 = String.sub s 0 s1_length
     and s2 = String.sub s s2_start s2_length
     and s3 = String.sub s s3_start s3_length in
     let s1 = if ((String.compare s1 "\"") == 0) then "" else s1 in
     let s3 = if ((String.compare s3 "\"") == 0) then "" else s3 in
     s1 ^ s2 ^ s3
           
  

(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

(* type(s) to specify the data event *)

type attribute =
  { name : string ;
    xes_value : string option ;
    vr_value : string option }


(* default(s) value(s) to specify the data event *)


(* Structure of data event *)
                     
type t = 
  { urn : int ;
    mutable attributes_list : (string * attribute) list }


(*-----------------------------------------------------------------
 * Constructor(s)
 *-----------------------------------------------------------------*)

(* function to make a data event with an urn and no atribute *)
  
(* val make : int -> t *)

let make i =
  { urn = i ;
    attributes_list = [] }


(* function to make a data event from xes attributes list
 * (xes attribute associating a name with a xes value) *)

let make_from_xes i xes_att_list =
  let new_att_list =
    List.map
      (fun (n, v) ->
        (n, { name = n ; xes_value = Some v ; vr_value = None }))
      xes_att_list
  in
  { urn = i ;
    attributes_list = new_att_list }
  

(* function to make an attribute with a name, a xes value and a vr value *)
  
let make_attribute n xes vr =
  { name = n ; xes_value = xes ; vr_value = vr } 

  

(*-----------------------------------------------------------------
 * Accessors
 *-----------------------------------------------------------------*)    

(* Base accessors *)
  
let get_urn t = t.urn
let get_attributes_list t = t.attributes_list 
let get_letter_tag t = compute_letter_tag t.urn
                     
let set_attributes_list t v = t.attributes_list <- v 


(* Accessors for attribute *)

let get_attribute t a_name =
  let a_name = disstringify a_name in
  List.assoc a_name (get_attributes_list t)

let get_attribute_xes_value t a_name =
  let att = get_attribute t a_name in
  att.xes_value

let get_attribute_vr_value t a_name =
  let att = get_attribute t a_name in
  att.vr_value
                     
let set_attribute t a_name a_xes_value a_vr_value =
  let new_att =
    { name = a_name ;
      xes_value = a_xes_value ;
      vr_value = a_vr_value }
  in
  let attributes = t.attributes_list in
  let attributes =
    match List.mem_assoc a_name attributes with
    | true -> List.remove_assoc a_name attributes
    | false -> attributes
  in
  let attributes = (a_name, new_att)::attributes in
  set_attributes_list t attributes
                     
let set_attribute_xes_value t a_name a_xes_value =
  let a_vr_value =
  let attributes = t.attributes_list in
    match List.mem_assoc a_name attributes with
    | true ->
       let origin_att = (get_attribute t a_name) in
       origin_att.vr_value
    | false -> None
  in
  set_attribute t a_name a_xes_value a_vr_value
                     
let set_attribute_vr_value t a_name a_vr_value =
  let a_xes_value =
    let attributes = t.attributes_list in
    match List.mem_assoc a_name attributes with
    | true ->
       let origin_att = (get_attribute t a_name) in
       origin_att.xes_value
    | false -> None
  in
  set_attribute t a_name a_xes_value a_vr_value


  
(*-----------------------------------------------------------------
 * Updating with ontology basis
 *-----------------------------------------------------------------*)

(* function to compute the xes value depending on the configuration *)

let compute_xes_value onto_vr_mapping config =
  try
    match (Configuration.get_ontology_label_type config) with
    | "iri_segment" -> DataOntologyBasis.get_onto_iri_segment onto_vr_mapping
    | "label" -> DataOntologyBasis.get_onto_label onto_vr_mapping
    | _ -> DataOntologyBasis.get_onto_label onto_vr_mapping
  with _ -> ""

          
(* functions to update the event structure with the mapping 
 * corresponding to the ontology basis structure *)

let update_an_attribute_with_a_given_mapping t att onto_vr_mapping config =
  let m_xes_value = compute_xes_value onto_vr_mapping config in
  let m_vr_value = DataOntologyBasis.get_five_ref onto_vr_mapping in
  match att.xes_value with
  | Some xv ->
     if xv = (stringify m_xes_value)
     then set_attribute_vr_value t att.name (Some (stringify m_vr_value))
     else ()
  | _ -> () 
  
let update_an_attribute_with_onto_basis t att onto_basis config =
  List.iter
    (fun mapping ->
      (update_an_attribute_with_a_given_mapping t att mapping config))
    onto_basis
  
let update_with_mapping_from_ontology_basis t onto_basis config =
  let att_list = List.map (fun (_, a) -> a) (get_attributes_list t) in
  let f_update att =
    update_an_attribute_with_onto_basis t att onto_basis config
  in
  List.iter f_update att_list
  

(*-----------------------------------------------------------------
 * Utilities
 *-----------------------------------------------------------------*)

let string_of_attribute (_,a) =
  let xes_value = match a.xes_value with Some x -> x | None -> "-" in
  let vr_value = match a.vr_value with Some x -> x | None -> "-" in
  "     " ^ a.name ^ " : "
  ^ "ontology label = " ^ xes_value
  ^ "  |  five reference = " ^ vr_value

let rec string_of_attributes_list att_list result =
  match att_list with
  | [] -> result
  | [a] -> result ^ (string_of_attribute a)
  | a::l ->
     string_of_attributes_list
       l
       (result ^ (string_of_attribute a) ^ "\n")  

let string_of_event_attributes t =
  let attributes = t.attributes_list in
  let result = "    { \n" in
  let result = string_of_attributes_list attributes result in
  result ^ " \n     }"
