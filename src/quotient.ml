(*
 *
 * Basic algorithms on quotient of transition systems modulo Parikh vectors
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA, Rennes, France, 2013
 *
 *)


type t = Vector.t Parikh.t

(* Iterates on reachable parikh vectors, enabled and disabled transitions of a quotient system *)

(* val iter : state:(Vector.t -> unit) -> enabled:(Vector.t -> int -> unit) -> disabled:(Vector.t -> int -> unit) -> t -> unit *)

let iter ~state:foo_state ~enabled:foo_enabled ~disabled:foo_disabled s =
  let d = Parikh.dimension s in
  let v = Parikh.make d in
  let rec dfs q =
    if not (Parikh.is_def v q)
    then
      begin
	Parikh.set v q ();
	foo_state q;
	let r = Parikh.get s q in
	  for e=0 to d-1 do
	    if Vector.get r e
	    then
	      let q' = Vector.clone q in
		begin
		  Vector.iflip q' e;
		  foo_enabled q e;
		  dfs q'
		end
	    else foo_disabled q e
	  done
      end
  in
    dfs (Vector.make d false)

(* Iterates on the reachable enabled transitions with a given label of a quotient system *)

(* val iter_event_trans : (Vector.t -> unit) -> t -> int -> unit *)

let iter_event_trans foo s f =
  let d = Parikh.dimension s in
  let v = Parikh.make d in
  let rec dfs q =
    if not (Parikh.is_def v q)
    then
      begin
	Parikh.set v q ();
	let r = Parikh.get s q in
	  begin
	    if Vector.get r f then foo q;
	    for e=0 to d-1 do
	      if Vector.get r e
	      then
		let q' = Vector.clone q in
		  begin
		    Vector.iflip q' e;
		    dfs q'
		  end
	    done
	  end
      end
  in
    dfs (Vector.make d false)

(* Prints a quotient system *)

(* val print : out_channel -> t -> unit *)

let print c m =
  begin
    Printf.fprintf c "[\n";
    Parikh.iter
      (fun u r ->
	Printf.fprintf c "  %s: %s\n" (Vector.to_string u) (Vector.to_string r))
      m;
    Printf.fprintf c "]\n"
  end


