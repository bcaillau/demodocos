(*
 *
 * Xorsat: satisfiability of systems of XOR forumlas
 *
 * (C) Benoit Caillaud <benoit.caillaud@inria.fr>, INRIA Rennes, France, 2012
 *
 *)

(* Constraints *)

type c

val cmake : int -> c

val cclone : c -> c

val dimension : c -> int

val get_polarity : c -> bool

val get_coeff : c -> int -> bool

val set_polarity : c -> bool -> unit

val set_coeff : c -> int -> bool -> unit

val ixor : c -> c -> unit

val cxor : c -> c -> c

val vc_blit : src:Vector.t -> src_idx:int -> dst:c -> dst_idx:int -> len:int -> unit

val leading_zeroes : c -> int

val arity : c -> int

val cprint : c -> unit

(* Systems *)

exception Inconsistent

type m =
    Unsorted
    | Sorted
    | Triangular

type t

val make : int -> t

val clone : t -> t

val print : t -> unit

val set_mode : t -> m -> unit

val add : t -> c -> unit

val solve : t -> Vector.t -> Vector.t
