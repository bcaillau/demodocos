(*
 *
 * Flipflop regions
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2012
 *
 *)

(* Region vector spaces *)

(*
 *
 * Simple regions sensitive to event i are of the form
 * [ c_0 ... c_n-1  b_i m_0 ] where :
 *
 * c_j is the effect of event j on the region
 * b_i is the condition on event i (m + b_i = 0 mod 2)
 * m_0 is the initial marking
 *
 * Composite regions are of the form
 * [c_0 ... c_n-1 a_0 ... a_n-1 b0 ... b_n-1 m_0 ] where:
 * c_i is the effect of event i on the region
 * a_i is true iff the region is sensitive to i
 * b_i is the condition on event i (a_i . m + b_i = 0 mod 2, for all i)
 * 
 *)

(*** Region vector spaces ***)

type t =
    {
      dimension : int;
      event : int;
      constr : Xorsat.t
    }

(* [make dim:n event:i] initializes a region space of dimension [n] and sensitive to event [i] *)

(* val make : dim:int -> event:int -> t *)

let make ~dim:n ~event:e =
  {
    dimension = n;
    event = e;
    constr = Xorsat.make (n+2)
  }

(* [enable s state:q event:j] adds a constraint enabling event [j] in state [q] *)

(* val enable : t -> state:Vector.t -> event:int -> unit *)

let enable { dimension=n; event=f; constr=w } ~state:q ~event:e =
  if (Vector.length q) <> n
  then invalid_arg "Region.enable: invalid dimension"
  else if e<>f
  then () (* no constraint is added if e is not the sensitive event *)
  else
    let c = Xorsat.cmake (n+2) in
      begin
	Xorsat.vc_blit ~dst:c ~src:q ~src_idx:0 ~dst_idx:0 ~len:n;
	Xorsat.set_coeff c n true;
	Xorsat.set_coeff c (n+1) true;
	Xorsat.add w c
      end

(* [clone s] returns a clone of [s] *)

(* val clone : t -> t *)

let clone { dimension=n; event=f; constr=w } = { dimension=n; event=f; constr=Xorsat.clone w }

(* cooks and optimizes the set of constraints. Should be called after the last call to [enable]. *)

(* val cook : t -> unit *)

let cook { constr=w } = Xorsat.set_mode w Xorsat.Triangular

(* add a constraint forcing the sensitive event to test the regions *)

(* val force_test : t -> unit *)

let force_test { dimension=n; event=e; constr=w } =
  let c = Xorsat.cmake (n+2) in
    begin
      Xorsat.set_coeff c e true;
      Xorsat.set_polarity c false;
      Xorsat.add w c
    end

(* add a constraint forcing the sensitive event to flip the regions *)

(* val force_flip : t -> unit *)

let force_flip { dimension=n; event=e; constr=w } =
  let c = Xorsat.cmake (n+2) in
    begin
      Xorsat.set_coeff c e true;
      Xorsat.set_polarity c true;
      Xorsat.add w c
    end

(*** Regions ***)

type r_simple =
    
    {
      dim_s : int;
      evt_s : int;
      vec_s : Vector.t
    }

type r_composite =
    {
      dim_c : int;
      vec_c : Vector.t
    }

type r = Simple of r_simple | Composite of r_composite

(* create a null region, orthogonal to every transition an initialized to 0 *)

(* val null : int -> r *)

let null d = Composite { dim_c = d; vec_c = Vector.make (3*d+1) false }

(*
 *
 * Get a (multiplicative condition), b (additive condition), c (effect) and m0 (initial marking) coefficients such that:
 *
 * m |e> m' iff (i) m' = m + c(e) mod 2 and (ii) a(e)*m + b(e) = 0 mod 2
 *
 *)

(* val get_a : r -> int -> bool *)

let get_a =
  function
      Simple { dim_s=n; evt_s=e } ->
	begin
	  fun f ->
	    if (f < 0) || (f >= n)
	    then invalid_arg "Region.get_a: invalid event"
	    else e=f
	end
    | Composite { dim_c=n; vec_c=v } ->
	begin
	  fun f ->
	    if (f < 0) || (f >= n)
	    then invalid_arg "Region.get_a: invalid event"
	    else Vector.get v (n+f)
	end

(* val get_b : r -> int -> bool *)

let get_b =
  function
      Simple { dim_s=n; evt_s=e; vec_s=v } ->
	begin
	  fun f ->
	    if (f < 0) || (f >= n)
	    then invalid_arg "Region.get_b: invalid event"
	    else if e=f
	    then Vector.get v n
	    else false
	end
    | Composite { dim_c=n; vec_c=v } ->
	begin
	  fun f ->
	    if (f < 0) || (f >= n)
	    then invalid_arg "Region.get_b: invalid event"
	    else Vector.get v (2*n+f)
	end

(* val get_c : r -> int -> bool *)

let get_c =
  function
      Simple { dim_s=n; evt_s=e; vec_s=v } ->
	begin
	  fun f ->
	    if (f < 0) || (f >= n)
	    then invalid_arg "Region.get_c: invalid event"
	    else Vector.get v f
	end
    | Composite { dim_c=n; vec_c=v } ->
	begin
	  fun f ->
	    if (f < 0) || (f >= n)
	    then invalid_arg "Region.get_b: invalid event"
	    else Vector.get v f
	end

(* val get_m0 : r -> bool *)

let get_m0 =
  function
      Simple { dim_s=n; vec_s=v } ->
	Vector.get v (n+1)
    | Composite { dim_c=n; vec_c=v } ->
	Vector.get v (3*n)

(* val set_a : r -> int -> bool -> unit *)

let set_a r e b =
  match r with
      Composite { dim_c=n; vec_c=v } ->
	if (e < 0) || (e >= n)
	then invalid_arg "Region.set_a: invalid event"
	else Vector.set v (n+e) b
    | _ -> raise (Invalid_argument "Region.set_a: region is not composite") 

(* val set_b : r -> int -> bool -> unit *)

let set_b r e b =
  match r with
      Composite { dim_c=n; vec_c=v } ->
	if (e < 0) || (e >= n)
	then invalid_arg "Region.set_b: invalid event"
	else Vector.set v (2*n+e) b
    | _ -> raise (Invalid_argument "Region.set_b: region is not composite") 

(* val set_c : r -> int -> bool -> unit *)

let set_c r e b =
  match r with
      Composite { dim_c=n; vec_c=v } ->
	if (e < 0) || (e >= n)
	then invalid_arg "Region.set_c: invalid event"
	else Vector.set v e b
    | _ -> raise (Invalid_argument "Region.set_c: region is not composite") 

(* val set_m0 : r -> bool -> unit *)

let set_m0 r m0 =
  match r with
      Simple { dim_s=n; vec_s=v } ->
	Vector.set v (n+1) m0
    | Composite { dim_c=n; vec_c=v } ->
	Vector.set v (3*n) m0

(* [dimension r] returns the dimension of region [r] *)

let dimension =
  function
      Simple { dim_s=n } -> n
    | Composite { dim_c=n } -> n

(* [get_c_vector r] returns the effect vector [c_0 ... c_n-1] of region [r] *)

let get_c_vector =
  function
      Simple { dim_s=n; vec_s=v } -> Vector.restrict v n
    | Composite { dim_c=n; vec_c=v } -> Vector.restrict v n

(* [get_a_vector r] returns the sensitivity vector [a_0 ... a_n-1] of region [r] *)

let get_a_vector =
  function
      Simple { dim_s=n; evt_s=i } ->
	let u = Vector.make n false in
	  begin
	    Vector.set u i true;
	    u
	  end
    | Composite { dim_c=n; vec_c=v } ->
	let u = Vector.make n false in
	  begin
	    Vector.blit ~src:v ~src_idx:n ~dst:u ~dst_idx:0 ~len:n;
	    u
	  end

(* [get_b_vector r] returns the condition vector [b_0 ... b_n-1] of region [r] *)

let get_b_vector =
  function
      Simple { dim_s=n; evt_s=i; vec_s=v } ->
	let u = Vector.make n false in
	  begin
	    Vector.set u i (Vector.get v n);
	    u
	  end
    | Composite { dim_c=n; vec_c=v } ->
	let u = Vector.make n false in
	  begin
	    Vector.blit ~src:v ~src_idx:(2*n) ~dst:u ~dst_idx:0 ~len:n;
	    u
	  end

(* [marking r u] computes the value of region [r] after behavior [u] *)

(* val marking : r -> int -> bool *)

let marking r u =
  let n = dimension r
  and w = get_c_vector r in
    if n <> (Vector.length u)
    then invalid_arg "Region.get_m: invalid Parikh vector"
    else
      let d = Vector.prod w u in (* effect *)
	d <> (get_m0 r) (* marking = xor of initial marking with effect *)

(* For printing and drawing FF nets *)

(* val print : out_channel -> r -> unit *)

type symbol =
    Ortho
    | Exit
    | Enter
    | Compl
    | True
    | False

(* val symbol_of_event : r -> int -> symbol *)

let symbol_of_event r e =
  match ((get_a r e),(get_b r e),(get_c r e)) with
      (false,false,false) -> Ortho
    | (true,true,true) -> Exit
    | (true,false,true) -> Enter
    | (false,false,true) -> Compl
    | (true,true,false) -> True
    | (true,false,false) -> False
    | _ -> failwith "Region.symbol_of_event: impossible case (please report bug)"


(* val set_symbol : r -> int -> symbol -> unit *)

let set_symbol r e =
  function
      Ortho ->
	begin
	  set_a r e false;
	  set_b r e false;
	  set_c r e false
	end
    | Exit ->
	begin
	  set_a r e true;
	  set_b r e true;
	  set_c r e true
	end
    | Enter ->
	begin
	  set_a r e true;
	  set_b r e false;
	  set_c r e true
	end
    | Compl ->
	begin
	  set_a r e false;
	  set_b r e false;
	  set_c r e true
	end
    | True ->
	begin
	  set_a r e true;
	  set_b r e true;
	  set_c r e false
	end
    | False ->
	begin
	  set_a r e true;
	  set_b r e false;
	  set_c r e false
	end

let rec string_of_evt_list =
  function
      [] -> ""
    | [e] -> "e"^(string_of_int e)
    | e::l -> "e"^(string_of_int e)^","^(string_of_evt_list l)

let print c r =
  let lexit = ref []
  and lenter = ref []
  and lcompl = ref []
  and ltrue = ref []
  and lfalse = ref []
  and n = dimension r in
    begin
      for e=n-1 downto 0 do
	match symbol_of_event r e with
	    Ortho -> ()
	  | Exit -> lexit := e :: (!lexit)
	  | Enter -> lenter := e :: (!lenter)
	  | Compl -> lcompl := e :: (!lcompl)
	  | True -> ltrue := e :: (!ltrue)
	  | False -> lfalse := e :: (!lfalse)
      done;
      Printf.fprintf c "<\n";
      Printf.fprintf c "  m0 = %b\n" (get_m0 r);
      (if (!lexit)  <> [] then Printf.fprintf c "  exited by %s\n" (string_of_evt_list (!lexit)));
      (if (!lenter) <> [] then Printf.fprintf c "  entered by %s\n" (string_of_evt_list (!lenter)));
      (if (!lcompl) <> [] then Printf.fprintf c "  complemented by %s\n" (string_of_evt_list (!lcompl)));
      (if (!ltrue)  <> [] then Printf.fprintf c "  tested true by %s\n" (string_of_evt_list (!ltrue)));
      (if (!lfalse)  <> [] then Printf.fprintf c "  tested false by %s\n" (string_of_evt_list (!lfalse)));
      Printf.fprintf c ">\n"
    end

(* Solves event-state separation problem. Raises [Separation_failure (q,e)] if no solution exists. *)

exception Separation_failure of Vector.t * int

(* val solve_essp : t -> Vector.t -> int -> r *)

let solve_essp { dimension=n; event=e; constr=w } q f =
  if (Vector.length q) <> n
  then invalid_arg "Region.solve_essp: invalid state"
  else if (f < 0) || (f >= n)
  then invalid_arg "Region.solve_essp: invalid event"
  else if e <> f
  then raise (Separation_failure (q,f))
  else
    let w' = Xorsat.clone w
    and c = Xorsat.cmake (n+2) in
      begin
	Xorsat.vc_blit ~dst:c ~src:q ~src_idx:0 ~dst_idx:0 ~len:n;
	Xorsat.set_coeff c n true;
	Xorsat.set_coeff c (n+1) true;
	Xorsat.set_polarity c true;
	Xorsat.add w' c;
	try
	  let u = Xorsat.solve w' (Vector.make (n+2) false) in
	    Simple
	      {
		dim_s=n;
		evt_s=e;
		vec_s=u
	      }
	with
	    Xorsat.Inconsistent -> raise (Separation_failure (q,f))
      end

(* [make_region n c a b m0] makes a composite region of dimension [n], effect [c], sensitivity [a], condition [b] and initial marking [m0] *)

let make_region n c a b m0 =
  if (n<>(Vector.length c)) ||
    (n<>(Vector.length a)) ||
    (n<>(Vector.length b))
  then invalid_arg "Region.make_region: invalid length"
  else
    let u = Vector.make (3*n+1) false in
      begin
	Vector.blit ~src:c ~src_idx:0 ~dst:u ~dst_idx:0 ~len:n;
	Vector.blit ~src:a ~src_idx:0 ~dst:u ~dst_idx:n ~len:n;
	Vector.blit ~src:b ~src_idx:0 ~dst:u ~dst_idx:(2*n) ~len:n;
	Vector.set u (3*n) m0;
	Composite { dim_c=n; vec_c=u }
      end

(* type of TF nets *)

type n = r list
  
(* [net_dimension n] returns the dimension of net [n]. Raises [Invalid_argument _] if [n] is empty *)

(* val net_dimension : n -> int *)

let net_dimension =
  function
      [] -> raise (Invalid_argument "Region.net_dimension: empty net")
    | r::_ -> dimension r

(* Exception [Undefined] is raised by the [superimpose] operator *)

exception Undefined

(* Superimpose two regions with equal effects. Raises [Undefined] whenever two regions can not be superimposed *)

(* val superimpose : r -> r -> r *)

let superimpose r1 r2 =
  let n = dimension r1 in
    if n <> (dimension r2)
    then invalid_arg "Region.superimpose: dimensions differ"
    else
      let c1 = get_c_vector r1
      and c2 = get_c_vector r2 in
	if c1<>c2 (* could be optimize by comparing subvectors directly *)
	then raise Undefined
	else
	  let a1 = get_a_vector r1
	  and a2 = get_a_vector r2
	  and b1 = get_b_vector r1
	  and b2 = get_b_vector r2
	  and m1 = get_m0 r1
	  and m2 = get_m0 r2 in
	    begin
	      (* Normalize [b1] and [b2], so that initial marking is assumed to be zero *)
	      if m1 then Vector.ixor b1 a1;
	      if m2 then Vector.ixor b2 a2;
	      (* Test whether conditions are equal on the intersection of the sensitivities *)
	      let v = Vector.vand a1 a2 (* sensitivity overlap *)
	      and w = Vector.vxor b1 b2 (* conflicting conditions *) 
	      in
		begin
		  Vector.iand w v;
		  (* Raise [Undefined] if one tests zero and the other tests one *)
		  if not (Vector.is_zero w)
		  then raise Undefined
		  else
		    begin
		      (* Sensitivity is the union of the sensitivity sets *)
		      Vector.ior a1 a2;
		      (* Condition is the or of the two conditions *)
		      Vector.ior b1 b2;
		      (* Build composite region *)
	    	      make_region n c1 a1 b1 false
		    end
		end
	    end
	      
(* superimpose a list of regions onto one another *)

(* val superimpose_list : n -> n *)

let rec superimpose_insert_list rr =
  function
      [] -> [rr]
    | r::l ->
	try
	  let r' = superimpose rr r in
	    r'::l
	with
	    Undefined ->
	      r::(superimpose_insert_list rr l)

let superimpose_list l =
  List.fold_right
    superimpose_insert_list
    l
    []

