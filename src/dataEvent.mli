(*
 *
 * Event from data
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* This module provides a data structure to define an event from data,
 * and functions related to this structure. 
 *)
  

(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

(* type(s) to specify the data event *)

type attribute


(* Structure of data event *)
                     
type t
   


(*-----------------------------------------------------------------
 * Constructor(s)
 *-----------------------------------------------------------------*)

(* function to make a data event with an urn and no atribute *)
  
(* val make : int -> t *)

val make : int -> t


(* function to make a data event from xes attributes list
 * (xes attribute associating a name with a xes value) *)

val make_from_xes : int -> (string * string) list -> t

  

(*-----------------------------------------------------------------
 * Accessors
 *-----------------------------------------------------------------*)    

(* Base accessors*)
  
val get_urn : t -> int
val get_attributes_list : t -> (string * attribute) list 
val get_letter_tag : t -> string

val set_attributes_list : t -> (string * attribute) list  -> unit

  
(* Accessors for attribute *)

val get_attribute : t -> string -> attribute
val get_attribute_xes_value : t -> string -> string option
val get_attribute_vr_value : t -> string -> string option
                     
val set_attribute : t -> string -> string option -> string option -> unit                     
val set_attribute_xes_value : t -> string -> string option -> unit                 
val set_attribute_vr_value : t -> string -> string option -> unit


  
(*-----------------------------------------------------------------
 * Updating with ontology basis
 *-----------------------------------------------------------------*)
  
(* functions to update the event structure with the corresponding mapping 
 * from ontology basis *)
  
val update_with_mapping_from_ontology_basis :
  t -> DataOntologyBasis.t -> Configuration.t -> unit

  

(*-----------------------------------------------------------------
 * Utilities
 *-----------------------------------------------------------------*)

val string_of_event_attributes : t -> string
