(*
 *
 * Abstract #7 data (A7) corresponding to xml #7 file (x7f)
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * This module generates the abstract #7 data corresponding to a xml #7 file.
 *)

(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)

(* function to parse a xml #7 file and return an abstract #7 data *)
val a7_of_x7_file : string -> A7.t
