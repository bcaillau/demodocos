(*
 *
 * Processes quotiented modulo parikh image
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA, France, 2012
 *
 *)

(* Type of systems *)

type s

(* [make n] initializes a system with [n] events *)

val make : int -> s

(* [dimension s] returns the dimension of [s] *)

val dimension : s -> int

(* Opaque label type *)

type r

(* Type of processes *)

type t = r Process.t

(* Warning: processes should not be modified with any operator in module Process. Functions declared below should be used *)

(* [root s] returns the root of system [s] *)

val root : s -> t

(* [system p] returns the system of process [p] *)

val system : t -> s

(* [add p e] returns the process reached from process [p] after firing event [e] *)

val add : t -> int -> t

(* [add_word s u] adds word [u] to system [s] *)

val add_word : s -> int list -> unit

(* [make_system_of_word_list d l] makes a system of dimension [d] from word list [l] *)

val make_system_of_word_list : int -> int list list -> s

(* [parikh p] returns the parikh image of process [p] *)

val parikh : t -> Vector.t

(* [class_iter f s v] evaluates [f p] for all elements [p] of class [v] in system [s] *)

val class_iter : (t -> unit) -> s -> Vector.t -> unit

(* [iter f s] evaluates [f v] for all reachable vector [v] in system [s] *)

val iter : (Vector.t -> unit) -> s -> unit

(* May and must vectors of a process at given parikh vector *)

val may : s -> Vector.t -> Vector.t

val must : s -> Vector.t -> Vector.t

(* Confusion vector : may \ must *)

val conf : s -> Vector.t -> Vector.t

(* [quotient s] computes the quotient of process [s] *)

val quotient : s -> Quotient.t

