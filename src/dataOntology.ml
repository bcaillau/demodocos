(*
 *
 * Ontology from data
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* This module provides a data structure to define an ontology from data,
 * namely a list of data events defined from the data sources.
 * Functions related to this structure are also proposed. 
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities
 *-----------------------------------------------------------------*)
  
(* None *)


(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)


(* Structure of ontology *)
                     
type t = (int * DataEvent.t) list

       

(*-----------------------------------------------------------------
 * Constructor(s)
 *-----------------------------------------------------------------*)

(* function to associate an urn to each xes_event, and obtain a list
 * of these associations (from an array of event) *)
  
let rec assoc_urn_to_event xes_event_array i n result_list =
  match i < n with
  | true ->
     (assoc_urn_to_event
       xes_event_array (i+1) n ((i, xes_event_array.(i))::result_list))
  | false -> result_list


(* function to obtain an array of string formatting event, 
 * ie passing the data of array from format (string, value) 
 * to format (string, string) *)

let string_formatting_of_xes_event_array xe_array =
  let xe_formatting xe = Xes.string_formatting_of_event xe in
  Array.map xe_formatting xe_array


(* function to make an ontology (data events list) from an xes event array *)

let make_from_event_array xes_event_array =
  let fxe_array = string_formatting_of_xes_event_array xes_event_array in
  let n = Array.length xes_event_array in
  let event_list = assoc_urn_to_event fxe_array 0 n [] in
  let f_make (urn, xes_event) = (urn, DataEvent.make_from_xes urn xes_event) in
  List.map f_make event_list

                           

(*-----------------------------------------------------------------
 * Accessors
 *-----------------------------------------------------------------*)    

(* Base accessors *)
(* function to obtain an event corresponding to an urn  *)
(* function to obtain a given attribute of a given event *)
  
let get_event t event_urn = List.assoc event_urn t
                          
let get_attribute_of_event t event_urn a_name =
  DataEvent.get_attribute (get_event t event_urn) a_name
                          

(* function to obtain the letter tag of an event *)
                          
let get_letter_tag_of_event t event_urn =
  DataEvent.get_letter_tag (get_event t event_urn)
                          

(* functions to obtain a specific value associated to a given attribute 
 * of a given event (xes value or vr value) *)
  
let get_xes_value_of_event_attribute t event_urn a_name =
  DataEvent.get_attribute_xes_value (get_event t event_urn) a_name
  
let get_vr_value_of_event_attribute t event_urn a_name =
  DataEvent.get_attribute_vr_value (get_event t event_urn) a_name
                          

(* functions to set a specific value associated to a given attribute 
 * of a given event (xes value or vr value) *)
  
let set_xes_value_of_event_attribute t event_urn a_name value =
  DataEvent.set_attribute_xes_value (get_event t event_urn) a_name (Some value)
  
let set_vr_value_of_event_attribute t event_urn a_name value =
  DataEvent.set_attribute_vr_value (get_event t event_urn) a_name (Some value)

                           

(*-----------------------------------------------------------------
 * Updating with ontology basis
 *-----------------------------------------------------------------*)  

(* function to update the ontology structure with the mapping 
 * corresponding to the ontology basis structure *)
  
let update_with_ontology_basis t onto_basis config =
  List.iter
    (fun (_, e) ->
      (DataEvent.update_with_mapping_from_ontology_basis e onto_basis config))
    t
  

  
(*-----------------------------------------------------------------
 * Utilities
 *-----------------------------------------------------------------*)

(* function to print a given mapping on out channel *)

let output_event t urn file =
  let letter_tag = (get_letter_tag_of_event t urn) in
  let event_attributes = (DataEvent.string_of_event_attributes (get_event t urn)) in
  Printf.fprintf
    file
    "  Event %d (%s) : \n %s \n\n"
    urn letter_tag event_attributes
    

(* val output_ontology : out_channel -> DataOntology.t -> unit *)

let output_ontology file t =
  begin
    Printf.fprintf file "/////////////////////////////////////////////////////////////////////////////////////////////////////// \n";
    Printf.fprintf file "//                                                                                                   // \n";
    Printf.fprintf file "// ONTOLOGY : Set of events with associated data from ontology                                       // \n";
    Printf.fprintf file "//            (including mapping with five references)                                               // \n";
    Printf.fprintf file "//                                                                                                   // \n";
    Printf.fprintf file "/////////////////////////////////////////////////////////////////////////////////////////////////////// \n";
    Printf.fprintf file "\n";
    Printf.fprintf file " Number of events: %i\n" (List.length t);
    Printf.fprintf file "\n";
    List.iter (fun (urn, _) -> output_event t urn file) (List.rev t);
    Printf.fprintf file "\n"
  end

(* val print_ontology_to_file : DataOntology.t -> string -> unit *)

let print_ontology_to_file t file_name =
  let file = open_out file_name in
    try
      output_ontology file t;
      close_out file
    with
    | y ->
       begin
	 close_out file;
	 raise y
       end
  
