(*
 *
 * Commnandline command tnf
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

open TnfBasis

       
  
(*-----------------------------------------------------------------
 * Configuration
 *-----------------------------------------------------------------*)

(* definition of configuration *)
       
let configuration = Configuration.default_t


(* function to initialize the configuration *)
                  
let configuration_init () = Configuration.init configuration


(* functions to obtain the config file *)

let get_config_file cf_name =
  let dir = Filename.dirname (Sys.executable_name) in
  Filename.concat dir cf_name  

let get_tag_config_file tag =  get_config_file (Configuration.tag_config_file_name tag)
let get_main_config_file =  get_config_file Configuration.main_config_file_name
let get_classifiers_config_file =  get_config_file Configuration.classifiers_config_file_name
                          


(*-----------------------------------------------------------------
 * Configuration management : view config, config, save/load config
 *-----------------------------------------------------------------*)

(* function to view the configuration *)

let view_config config = Reporting.report_configuration config
                
                          
(* function to execute the configuration process *)
  
let edit_config config_file var data =
  
  let f_identite x = x in
  let f_config f_set f_data_from_string =
    begin
       f_set configuration (f_data_from_string data);
       ConfigurationOutput.save_config configuration config_file
    end
  in
  let f_config_string f_set = f_config f_set f_identite in
  let f_config_string_opt f_set = f_config f_set string_option_of_string in
  let f_config_int f_set = f_config f_set int_of_string in
  let f_config_int_opt f_set = f_config f_set int_option_of_string in
  let f_config_bool f_set = f_config f_set bool_of_string in
  let f_config_string_list f_set =
    f_config f_set (fun x -> string_list_of_string ' ' x [])
  in
  
  match var with
    
    (* synthesis configuration *)
  | "input_working_file_postfix" -> (f_config_string Configuration.set_input_working_file_postfix)
  | "input_adapting_script" -> (f_config_string Configuration.set_input_adapting_script)
  | "classifier_choice" -> (f_config_string Configuration.set_classifier_choice)
  | "ontology_base_file" -> (f_config_string_opt Configuration.set_ontology_base_file)
  | "ontology_global_files" -> (f_config_string_list Configuration.set_ontology_global_files_list)
  | "ontology_label_type" -> (f_config_string Configuration.set_ontology_label_type)
  | "instances_number" -> (f_config_int_opt Configuration.set_instances_number)
  | "verbose" -> (f_config_int Configuration.set_verbose)
                               
    (* approximation configuration*)
  | "approximation_requested" -> (f_config_bool Configuration.set_approximation_requested)
  | "approximation_limit" -> (f_config_int Configuration.set_approximation_limit)
                           
    (* output configuration *)
  | "output_directory" -> (f_config_string_opt Configuration.set_output_directory)
  | "output_tnfnet" -> (f_config_string_opt Configuration.set_output_tnfnet)
  | "output_tnfgraph" -> (f_config_string_opt Configuration.set_output_tnfgraph)
  | "output_seven" -> (f_config_string_opt Configuration.set_output_seven)
  | "output_a7xspm" -> (f_config_string_opt Configuration.set_output_a7xspm)
  | "output_events_table" -> (f_config_string_opt Configuration.set_output_events_table)
  (* | "output_tnfnet_display" -> (f_config_bool Configuration.set_output_tnfnet_display) *)
                   
    (* options for output computing (and analysis) *)
  | "tnfgraph_label_type" -> (f_config_string Configuration.set_tnfgraph_label_type)
  | "seven_sensor_label_type" -> (f_config_string Configuration.set_seven_sensor_label_type)
  | "seven_five_value_type" -> (f_config_string Configuration.set_seven_five_value_type)
  | "seven_location_computing" -> (f_config_bool Configuration.set_seven_location_computing)
  | "seven_analysis" -> (f_config_bool Configuration.set_seven_analysis)
  | "seven_event_number_limit" -> (f_config_int Configuration.set_seven_event_number_limit)
                                 
  | _ -> failwith "config: unknow configuration variable"


(* functions to load/save the current configuration on tag config file *)

let load_config tag =
  try
    let source_config_file = get_tag_config_file tag in
    let target_config_file = get_main_config_file in
    begin
      ConfigurationInput.load_config configuration source_config_file;
      ConfigurationOutput.save_config configuration target_config_file
    end
  with _ ->
    begin
      Printf.fprintf stderr "No configuration file with tag %s\n" tag;
      exit 10
    end

let save_config tag =
  try
    let target_config_file = get_tag_config_file tag in
    ConfigurationOutput.save_config configuration target_config_file
  with _ ->
    begin
      Printf.fprintf stderr "Unable to save with tag %s for unknown reason\n" tag;
      exit 10
    end


  
(*-----------------------------------------------------------------
 * Log computing
 *-----------------------------------------------------------------*)

(* functions to compute the log structure of a given file (xes or seven file),
 * depending on the source format *)

let compute_log_of_xes_file xes_file_name =
  try
    Xes.log_of_file xes_file_name
  with
  | Invalid_argument s -> (err_report_and_exit "Could not parse XES file: " s)
  | Sys_error s -> (err_report_and_exit "Error when parsing XES file: " s)
  | _ -> (err_report_and_exit "Unknown error when parsing XES file" "")

let compute_log_of_seven_file seven_file =
  let a7 =
    try
      A7Xml.seven_xml_file_to_abstract_seven seven_file
    with
    | Invalid_argument s -> (err_report_and_exit "Could not parse #7 XML file: %s\n" s)
    | Sys_error s -> (err_report_and_exit "Error when parsing #7 XML file: %s\n" s)
    | _ -> (err_report_and_exit "Unknown error when parsing #7 XML file\n" "")
  in
  let transient_xes_file_name = "transient_file.xes" in
  let f_remove_file_and_obtain_log xes_log =
    begin (* Sys.remove transient_xes_file_name; *) xes_log end (* DEBUG !!! *)
  in
  begin
    TnfSynthesisProcess.produce_a7xspm configuration a7 transient_xes_file_name;
    let xes_log = compute_log_of_xes_file transient_xes_file_name in
    f_remove_file_and_obtain_log xes_log
  end

let compute_log_of_input_work_file src_format source =
  let log =
    match src_format with
    | "seven" -> compute_log_of_seven_file source
    | _ -> compute_log_of_xes_file source
  in
  let global_classifiers_list =
    Configuration.get_classifiers_list_of_group configuration src_format
  in
  Xes.add_classifiers_to_log log global_classifiers_list


  
(*-----------------------------------------------------------------
 * Synthesis processes
 *-----------------------------------------------------------------*)
  
(* functions to execute process for generating output *)
  
let process_output_tnfnet tnfnet =
  execute_step
    "TnF Net Generating"
    (TnfSynthesisProcess.produce_tnfnet configuration tnfnet)
    (Configuration.get_target_file configuration "tnfnet")   
  
let process_output_tnfgraph a7 ontology =
  execute_step
    "TnF Net Marking Graph Generating"
    (TnfSynthesisProcess.produce_tnfgraph configuration a7 ontology)
    (Configuration.get_target_file configuration "tnfgraph")  
    
let process_output_seven a7 traces =
  execute_step
    "#7 Code Generating"
    (TnfSynthesisProcess.produce_seven_xml_file configuration a7 traces)
    (Configuration.get_target_file configuration "seven")  
  
let process_output_a7xspm a7 =
    execute_step
      "A7 xSPM (iSPM as xes file) Generating"
      (TnfSynthesisProcess.produce_a7xspm configuration a7)
      (Configuration.get_target_file configuration "a7xspm")
  
let process_output_events_table ontology =
  execute_step
    "Events Table Generating"
    (DataOntology.print_ontology_to_file ontology)
    (Configuration.get_target_file configuration "table")

let process_output traces ontology tnfnet a7 =
  begin
    begin
      match Configuration.get_output_tnfnet configuration with
      | None -> ()
      | Some file -> (process_output_tnfnet tnfnet)
    end;
    begin
      match Configuration.get_output_tnfgraph configuration with
      | None -> ()
      | Some file -> (process_output_tnfgraph a7 ontology)
    end;
    begin
      match Configuration.get_output_seven configuration with
      | None -> ()
      | Some file -> (process_output_seven a7 traces)
    end;
    begin
      match Configuration.get_output_a7xspm configuration with
      | None -> ()
      | Some file -> (process_output_a7xspm a7)
    end;
    begin
      match Configuration.get_output_events_table configuration with
      | None -> ()
      | Some t -> (process_output_events_table ontology)
    end
  end


(* functions to execute process for analysis an abstract seven code *)
  
let process_seven_analysis a7 traces synthesis_traces =
  if Configuration.get_seven_analysis configuration
  then execute_step
         "#7 Code Analysis"
         (TnfSynthesisProcess.analyse_seven_code a7 synthesis_traces)
         (TnfSynthesisProcess.evaluate_test_traces configuration traces)


(* function to verify that a given classifier is defined *)

let verify_classifier log classifier =
  let classifiers_list = List.map (fun (c,_) -> c) log.Xes.log_classifiers in
  ((List.mem classifier classifiers_list), classifiers_list)
  
  
(* function to execute the synthesis process from a log and a classifier reference *)   
  
let synthesize_log log classifier =
  let (is_defined_classifier, classifiers_list) = verify_classifier log classifier in
  match (is_defined_classifier) with
  | true -> 
     let (_, traces) = Xes.encode_log log classifier in
     let ontology = TnfOntologyProcess.compute_ontology_from_log configuration log classifier in
     let synthesis_traces = TnfSynthesisProcess.evaluate_synthesis_traces configuration traces in
     let (r, ffnet_structure) =
       execute_step_with_result
         "Initialisation of synthesis"
         TnfSynthesisProcess.initialize_synthesis configuration synthesis_traces
     in
     begin
       TnfSynthesisProcess.execute_main_synthesis configuration r ffnet_structure;
       let tnfnet =
         (TnfSynthesisProcess.finalize_synthesis_for_tnfnet configuration ffnet_structure)
       in
       let a7 =
         (TnfSynthesisProcess.compute_a7_if_necessary
            configuration ffnet_structure ontology)
       in
       begin
         process_output traces ontology tnfnet a7;
         process_seven_analysis a7 traces synthesis_traces
       end;
       exit 0
     end
  | _ ->
     begin
       Printf.fprintf stderr "Classifier %s not defined\n" classifier;
       Printf.fprintf stderr "Defined classifiers are: %s\n" (css_of_list classifiers_list);
       exit 10
     end
  
  
(* function to execute the ontology evaluation process from a log and a classifier reference *)   
  
let value_ontology_from_log log classifier =
  let (is_defined_classifier, classifiers_list) = verify_classifier log classifier in
  match (is_defined_classifier) with
  | true -> 
     let ontology =
       TnfOntologyProcess.compute_ontology_from_log configuration log classifier
     in
     begin
       begin
         match Configuration.get_output_events_table configuration with
         | None -> ()
         | Some t -> (process_output_events_table ontology)
       end;
       exit 0
     end
  | _ ->
     begin
       Printf.fprintf stderr "Classifier %s not defined\n" classifier;
       Printf.fprintf stderr "Defined classifiers are: %s\n" (css_of_list classifiers_list);
       exit 10
     end
  
  
(* function to compare a given instance to a set of reference instances (using the synthesis), 
 * the given instance corresponding to the first trace on log, and the reference instances 
 * corresponding to the other traces *)

(* let compare_log log classifier =
  let (is_defined_classifier, classifiers_list) = verify_classifier log classifier in
  match (is_defined_classifier) with
  | true -> 
     let (_, traces) = Xes.encode_log log classifier in
     let ontology = TnfOntologyProcess.compute_ontology_from_log configuration log classifier in
     (* let synthesis_traces = 
       TnfSynthesisProcess.evaluate_synthesis_traces configuration traces
     in *)
     (* let (r, ffnet_structure) =
       execute_step_with_result
         "Initialisation of analysis"
         TnfAnalysisProcess.initialize_comparison configuration traces
     in *)
     begin
       (* TnfSynthesisProcess.execute_main_synthesis configuration r ffnet_structure;
       begin
         execute_step
           "Comparison Report Generating"
           (TnfAnalysisProcess.print_report r ffnet_structure)
           "comparison_report.txt";
       end; *)
       exit 0
     end
  | _ ->
     begin
       Printf.fprintf stderr "Classifier %s not defined\n" classifier;
       Printf.fprintf stderr "Defined classifiers are: %s\n" (css_of_list classifiers_list);
       exit 10
     end *)
    
    
  
(*-----------------------------------------------------------------
 * starting functions by operation
 *-----------------------------------------------------------------*)

(* function to start the necessary operations to copy the source file
 * on working directory, the copied file becoming the input work file *)
    
let start_copy_single_file src_file =
  begin
    Configuration.set_input_file_name configuration src_file;
    let target_file = Configuration.get_target_file configuration "input_work" in
    begin
      copy_single_file_to_work_file src_file target_file;
      target_file
    end
  end
  

(* function to start the necessary operations to group some files into a single file *)

let start_group_from_dir src_format src_dir =
  let dir_name = match src_dir with Some "" -> "na" | Some x -> x | _ -> "na" in
  let extension = match src_format with "ispm" -> ".xml" | _ -> ".xml" in
  let input_file_name = dir_name ^ extension in
  begin
    Configuration.set_input_file_name configuration input_file_name;
    let target_file = Configuration.get_target_file configuration "input_work" in
    begin
      begin
        match src_format with
        | "iccas" ->
           group_from_dir "iccas" src_dir target_file
        | "ispm" | "ispm2" | "ispm3" ->
           group_from_dir "ispm" src_dir target_file
        | _ ->
           let msg_1 = "Unrecognized format to group files: " ^ src_format ^ " \n" in
           let msg_2 = "Supported formats are: iccas, ispm2, ispm3" in
           (err_report_and_exit (msg_1 ^ msg_2) "")
      end;
      target_file
    end
  end

let start_group_from_files src_format src_argv =
  let src_files_list = Array.to_list src_argv in
  let first_file_name = match src_files_list with f::xxx -> f | _ -> "_nofile" in
  (* let postfix = (Configuration.get_input_working_file_postfix configuration) in *)
  let input_file_name = first_file_name in
  begin
    Configuration.set_input_file_name configuration input_file_name;
    let target_file = Configuration.get_target_file configuration "input_work" in
    begin
      begin
        match src_format with
        | "iccas" ->
           begin
             group_from_files "iccas" src_files_list target_file
           end
        | "ispm" | "ispm2" | "ispm3" ->
           begin
             group_from_files "ispm" src_files_list target_file
           end
        | _ ->
           let msg_1 = "Unrecognized format to group files: " ^ src_format ^ " \n" in
           let msg_2 = "Supported formats are: iccas, ispm2, ispm3" in
           (err_report_and_exit (msg_1 ^ msg_2) "")
      end;
      target_file
    end
  end
                                            

(* function to start the conversion to xes *)

let start_convert src_format source xes_file =
  let xes_file =
    match xes_file with
    | None ->
       begin
         Configuration.set_input_file_name configuration source;
         Configuration.get_target_file configuration "xes"
       end
    | Some x -> x
  in
  convert src_format source xes_file


(* function to adapt the input file if necessary *)

let adapt_if_necessary src_format input_file =
  let f_adapt src_format src_file target_file =
    TnfBasis.adapt src_format src_file target_file
  in
  let adaptation_request = Configuration.get_input_adapting_script configuration in
  match src_format with  
  | "xes" | "seven" -> ()
  | "iccas" | "ispm2" | "ispm3" ->
     begin
       f_adapt src_format input_file adaptation_request
     end
  | _ ->
     let msg_1 = "Unknow source format \n" in
     let msg_2 = "Supported formats are: xes, iccas, ispm2, ispm3, seven" in
     (err_report_and_exit (msg_1 ^ msg_2) "")


(* function to convert the input file if necessary *)

let convert_if_necessary src_format input_file =
  let f_convert src_format src_file xes_target =
    TnfBasis.convert src_format src_file xes_target
  in
  match src_format with  
  | "xes" | "seven" -> input_file
  | "iccas" | "ispm2" | "ispm3" ->
     let xes_file = (Configuration.get_target_file configuration "xes") in
     begin
       f_convert src_format input_file xes_file;
       xes_file
     end
  | _ ->
     let msg_1 = "Unknow source format \n" in
     let msg_2 = "Supported formats are: xes, iccas, ispm2, ispm3, seven" in
     (err_report_and_exit (msg_1 ^ msg_2) "")
  

(* function to make the input work file from a single file, a group of files
 * or a directory (the input work file name is return) *)

let make_input_work_file src_cat src_format (single_file, files_array, dir) =
  let input_work_file =
    match (src_cat, single_file, files_array, dir) with
    | ("file", Some f, _, _) ->
       begin
         start_copy_single_file f
       end
    | ("files", _, Some fl, _) ->
       begin
         start_group_from_files src_format fl
       end
    | ("dir", _, _, d) -> 
       begin
         start_group_from_dir src_format d
       end
    | (_, _, _, _) -> 
       let msg_1 = "Problem to recognize the category of the source \n " in
       let msg_2 = "Supported category are: single file, some files or directory" in
       (err_report_and_exit (msg_1 ^ msg_2) "")
  in
  begin
    remove_xmlns_of_xml_file input_work_file;
    (adapt_if_necessary src_format input_work_file);
    (convert_if_necessary src_format input_work_file)
  end

(* function to start the synthesize process *)
    
let start_synthesize src_cat src_format ((single_file, files_array, dir) as src) =
  let input_work_file = make_input_work_file src_cat src_format src in
  let log = compute_log_of_input_work_file src_format input_work_file in
  let classifier = (Configuration.get_classifier_choice configuration) in
  begin
    Verbose.set (Configuration.get_verbose configuration);
    Reporting.init_report configuration;
    synthesize_log log classifier
  end


(* function to start an evaluation of ontology from a given target source and 
 * an ontology base file (defined on configuration) *)

let start_ontology_evaluation src_cat src_format ((single_file, files_array, dir) as src) =
  let input_work_file = make_input_work_file src_cat src_format src in
  let log = compute_log_of_input_work_file src_format input_work_file in
  let classifier = (Configuration.get_classifier_choice configuration) in
  begin
    Verbose.set (Configuration.get_verbose configuration);
    Reporting.init_report configuration;
    value_ontology_from_log log classifier
  end

(* function to start the analysis process *)
    
(* let start_comparison src_cat src_format ((single_file, files_array, dir) as src) =
  let input_work_file = make_input_work_file src_cat src_format src in
  let log = compute_log_of_input_work_file src_format input_work_file in
  let classifier = (Configuration.get_classifier_choice configuration) in
  begin
    Verbose.set (Configuration.get_verbose configuration);
    Reporting.init_report configuration;
    compare_log log classifier
  end *)
  

(* function to start the quotient process *)
    
let start_quotient src_type source =
  begin
    Configuration.set_apx_config configuration Configuration.quotient_apx_config;
    start_synthesize "file" src_type (Some source, None, None)
  end
    
    
  
(*-----------------------------------------------------------------
 * starting command function
 *-----------------------------------------------------------------*)  
  
(* function to start a command *)
  
let start_command command argv =
  
  let nb_arg = (Array.length argv) - 2 in
  let arg1 = if nb_arg > 0 then argv.(2) else "" in
  let arg2 = if nb_arg > 1 then argv.(3) else "" in
  let arg3 = if nb_arg > 2 then argv.(4) else "" in
  let arg4 = if nb_arg > 3 then argv.(5) else "" in
  let is_classifier_group arg = true in
  
  match (command, arg1, arg2) with

  (* Classifiers management (classifiers): view, add, remove, edit 
   * for classifiers sep, prefix, keys or def *)
    
  | ("classifiers", "view", _) ->
     begin       
       match nb_arg with         
       | 1 -> (TnfClassifierProcess.view_classifiers configuration)   
       | _ -> (err_report_and_exit "Usage: tnf classifier view \n" "")
     end
    
  | ("classifiers", "def", "view") ->
     begin       
       match nb_arg with         
       | 2 -> (TnfClassifierProcess.view_classifier_def configuration)   
       | _ -> (err_report_and_exit "Usage: tnf classifier def view \n" "")
     end

  | ("classifiers", "def", "add") when (is_classifier_group arg2) ->
     begin
       match nb_arg > 3 with
       | true ->
          let config_file = get_classifiers_config_file in
          let keys_list = (Array.to_list (Array.sub argv 6 (nb_arg - 4))) in
          (TnfClassifierProcess.add_classifier_def
            config_file configuration arg3 arg4 keys_list)
       | _ -> (err_report_and_exit "Usage: tnf classifier add <c_name> <c_group> <keys>" "")
     end

  | ("classifiers", "def", "remove") ->
     begin
       match nb_arg with
       | 3 -> 
          let config_file = get_classifiers_config_file in
          (TnfClassifierProcess.remove_classifier_def config_file configuration arg3)
       | _ -> (err_report_and_exit "Usage: tnf classifier def remove <c_name>" "")
     end

  | ("classifiers", "def", "edit") when (is_classifier_group arg1) ->
     begin
       match nb_arg > 3 with
       | true -> 
          let config_file = get_classifiers_config_file in
          let keys_list = (Array.to_list (Array.sub argv 6 (nb_arg - 4))) in
          (TnfClassifierProcess.edit_classifier_def
            config_file configuration arg3 arg4 keys_list)
       | _ -> (err_report_and_exit "Usage: tnf classifier def edit <c_name> <c_group> <keys>" "")
     end

  (* Configuration management (config): view, edit, save/load *)
    
  | ("config", "view", _) ->
     begin       
       match nb_arg with         
       | 1 -> view_config configuration           
       | _ -> (err_report_and_exit "Usage: tnf config view" "")
     end

  | ("config", "edit", _) ->
     begin
       let config_file = get_main_config_file in
       match nb_arg with
       | 3 ->
          begin
            edit_config config_file arg2 arg3;
            view_config configuration
          end
       | _ -> (err_report_and_exit "Usage: tnf config edit <c_name> <c_value>" "")
     end

  | ("config", "save", _) ->
     begin
       match nb_arg with
       | 2 -> save_config arg2
       | _ -> (err_report_and_exit "Usage: tnf config save <tag>" "")
     end

  | ("config", "load", _) ->
     begin
       match nb_arg with
       | 2 -> load_config arg2
       | _ -> (err_report_and_exit "Usage: tnf config load <tag>" "")
     end

  (* Ontology management : value *)
    
  | ("ontology", "value", _) ->
     begin 
       let test_directory =
         if nb_arg = 2 then (Sys.is_directory arg2) else false
       in
       match (nb_arg = 1, nb_arg = 2, nb_arg > 2, test_directory) with
       | (true, _, _, _) -> (TnfOntologyProcess.value_ontology_basis configuration) 
       | (_, true, _, false) ->
          let src_cat = "file" in
          let src_file = arg2 in
          let src_format = (get_file_format src_file) in
          start_ontology_evaluation src_cat src_format (Some src_file, None, None)
       | (_, true, _, true) ->
          let src_cat = "dir" in
          let dir = arg2 in
          let src_format = get_file_format_from_dir dir in
          start_ontology_evaluation  src_cat src_format (None, None, Some dir)
       | (_, _, true, _) ->
          let src_cat = "files" in
          let files_array = (Array.sub argv 3 (nb_arg - 1)) in
          let src_format = (get_file_format_of_some_files files_array) in 
          start_ontology_evaluation src_cat src_format (None, Some files_array, None) 
       | _ -> (err_report_and_exit "Usage: tnf ontology value [<source>] [<other sources>]\n" "")     
     end

  (* TnF net synthesis : synthesize *)
    
  | ("synthesize", _, _) | ("synth", _, _) ->
     begin
       let test_directory =
         if nb_arg = 1 then (Sys.is_directory arg1) else false
       in
       match (nb_arg = 1, nb_arg > 1, test_directory) with
       | (true, _, false) ->
          let src_cat = "file" in
          let src_file = arg1 in
          let src_format = (get_file_format src_file) in
          start_synthesize src_cat src_format (Some src_file, None, None)
       | (true, _, true) ->
          let src_cat = "dir" in
          let dir = arg1 in
          let src_format = get_file_format_from_dir dir in
          start_synthesize src_cat src_format (None, None, Some dir)
       | (_, true, _) ->
          let src_cat = "files" in
          let files_array = (Array.sub argv 2 nb_arg) in
          let src_format = (get_file_format_of_some_files files_array) in 
          start_synthesize src_cat src_format (None, Some files_array, None)
       | _ -> (err_report_and_exit "Usage: tnf synthesize <source> [<other sources>]\n" "")
     end

  (* TnF net analysis : compare *)
  (*  
  | ("compare", _, _) ->
     begin
       let test_directory =
         match nb_arg with
         | 1 -> (Sys.is_directory arg1)
         | 2 -> (Sys.is_directory arg2)
         | _ -> false
       in
       match (nb_arg = 1, nb_arg > 1, test_directory) with
       | (true, _, false) ->
          let src_cat = "file" in
          let src_file = arg1 in
          let src_format = (get_file_format src_file) in
          start_comparison src_cat src_format (Some src_file, None, None)
       | (true, _, true) ->
          let src_cat = "dir" in
          let dir = arg1 in
          let src_format = get_file_format_from_dir dir in
          start_comparison src_cat src_format (None, None, Some dir)
       | (_, true, _) ->
          let src_cat = "files" in
          let files_array = (Array.sub argv 2 nb_arg) in
          let src_format = (get_file_format_of_some_files files_array) in 
          start_comparison src_cat src_format (None, Some files_array, None)
       | _ -> (err_report_and_exit
                 "Usage: tnf compare <instance_source> <reference_source> [<other sources>]\n" "")
     end*)
    

  (* Other operations : group, convert, quotient *)
    
  | ("group", "files", _) ->
     begin
       match nb_arg > 1 with
       | true ->
          let files_array = (Array.sub argv 3 (nb_arg - 1)) in
          let src_format = (get_file_format_of_some_files files_array) in 
          ignore (start_group_from_files src_format files_array)
       | _ ->
          (err_report_and_exit "Usage: tnf group files <sources>" "")
     end
    
  | ("group", "directory", _) | ("group", "dir", _) ->
     begin
       match nb_arg with
       | 1 ->
          let dir = "." in
          let src_format = get_file_format_from_dir dir in 
          ignore (start_group_from_dir src_format None)
       | 2 ->
          let dir = arg2 in
          let src_format = get_file_format_from_dir dir in
          ignore (start_group_from_dir src_format (Some dir))
       | _ ->
          (err_report_and_exit "Usage: tnf group dir <directory>" "")
     end
    
  | ("convert", _, _) ->
     begin
       match nb_arg with
       | 1 ->
          let src_file = arg1 in
          let src_format = (get_file_format src_file) in
          start_convert src_format src_file None
       | 2 ->
          let src_file = arg1 in
          let src_format = (get_file_format src_file) in
          start_convert src_format src_file (Some arg2)
       | _ ->
          (err_report_and_exit "Usage: tnf convert <source> [<target>]" "")
     end
    
  | ("quotient", _, _) ->
     begin
       match nb_arg with
       | 1 ->
          let src_file = arg1 in
          let src_format = (get_file_format src_file) in
          start_quotient src_format src_file
       | _ ->
          (err_report_and_exit "Usage: tnf quotient <source>" "")
     end


  (* Testing command *)

  | ("test", "parikh", _) -> TnfTestingProcess.test_parikh 1
  | ("test", "quotient", _) -> TnfTestingProcess.test_quotient 1
  | ("test", "parikhproc", _) -> (err_report_and_exit "No test" "")
                               

  (* Unknow command *)

  | ("classifier", _, _) | ("classifiers", _, _) ->
     (err_report_and_exit
        "Usage: tnf classifiers [separator|prefix|keys|def] [view|add|remove|edit] ..." "")
  | ("config", _, _) -> (err_report_and_exit "Usage: tnf config [view|edit|save|load] ..." "")
  | ("ontology", _, _) -> (err_report_and_exit "Usage: tnf ontology [value] ..." "")
  | (_, _, _) -> (err_report_and_exit "Usage: tnf <command> <arguments>" "")
          

            
(*-----------------------------------------------------------------
 * Main function
 *-----------------------------------------------------------------*)

let main () =
  try
    let sys_argv = Sys.argv in
    match (Array.length sys_argv) > 1 with
    | true -> 
       let main_command = Sys.argv.(1) in
       let config_file = get_main_config_file in
       let classifiers_file = get_classifiers_config_file in
       begin
         ConfigurationInput.load_config configuration config_file;
         ConfigurationInput.load_classifiers configuration classifiers_file;
         create_work_directory_if_necessary configuration;
         start_command main_command sys_argv
       end
    | _ -> 
       (err_report_and_exit "Too few argument (usage: tnf <command> <arguments>)" "")
  with
  | _ as x ->
     begin
       Printf.fprintf stderr "Exiting on uncaught unknown exception\n";
       raise x
     end 
     
let _ = Printexc.catch main ()
