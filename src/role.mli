(*
 *
 * Controllability/observability roles
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2014
 *
 *)

(* Type of role *)

type t

(* Constructors *)

val make : int -> t

val init : observable:(int -> bool) -> controllable:(int -> bool) -> int -> t

val clone : t -> t

(* [dim t] returns the dimension of role [t] *)

val dim : t -> int

(* [print c r] prints role [r] on channel [c] *)

val print : out_channel -> t -> unit

(* set/get *)

val set_observable : t -> int -> bool -> unit
val set_controllable : t -> int -> bool -> unit
val get_observable : t -> int -> bool
val get_controllable : t -> int -> bool

(* iterators *)

(* [iter_unobservable f r] evaluates [f i] foreach [i] unobservable in [r] *)

val iter_unobservable : (int -> unit) -> t -> unit

