(*
 *
 * XML of #SEVEN code
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* This module generate the XML file of #SEVEN code.
 * The main function works from an abstract SEVEN structure to obtain 
 * a dot structure (using module Pp). 
 * Then, finally, the XML file is generated.
 *)

(*-----------------------------------------------------------------
 * Module openning, parameter & utilities
 *-----------------------------------------------------------------*)

open Pp

open A7XmlDocFormat

(* Parameters about decoration *)
let param_decoration_origin_name = "Origin"
let param_decoration_origin_ispm = "iSPM"
let param_decoration_origin_flipflop = "synthesis"
                

(*-----------------------------------------------------------------
 * Functions to generates doc containing #SEVEN code parts
 *-----------------------------------------------------------------*)

(* Functions to generates doc about place *)
(* val doc_of_place : A7.place -> Pp.doc *)
(* val doc_of_places_list : A7.place list -> Pp.doc *)

let doc_of_place_origin_decoration e_origin =
  match e_origin with
  | Some A7Element.Ispm ->
     place_decoration_text
       param_decoration_origin_name
       param_decoration_origin_ispm
  | Some A7Element.Flipflop ->
     place_decoration_text
       param_decoration_origin_name
       param_decoration_origin_flipflop
  | _ -> empty
                       
let doc_of_place p =
  let p_id = A7Element.get_id p in
  let p_origin = A7Element.get_origin p in
  let p_label = A7Element.get_label p in
  let (x, y) = A7Element.get_location p in
  let p_content =
    vgrp (
        (doc_of_place_origin_decoration p_origin)
        ^^ (place_position_text x y)
      ) in
  vgrp (place_text p_id p_label p_content)
  
let doc_of_places_list p_list =
  let f = fun doc p -> vgrp (doc ^^ (doc_of_place p)) in
  List.fold_left f empty p_list


(* Functions to generates doc about sensor/event *)
(* val doc_of_sensor_event : A7.sensor_event -> Pp.doc *)
(* val doc_of_sensor_events_list : A7.sensor_event list -> Pp.doc *)

let doc_of_event_origin_decoration e_origin =
  match e_origin with
  | Some A7Element.Ispm ->
     event_decoration_text
       param_decoration_origin_name
       param_decoration_origin_ispm
  | Some A7Element.Flipflop ->
     event_decoration_text
       param_decoration_origin_name
       param_decoration_origin_flipflop
  | _ -> empty
  
let doc_of_sensor_event se =
  let e_id = A7Element.get_id se in
  let e_origin = A7Element.get_origin se in
  let s_label = A7Element.get_label se in
  let e_rel = A7Element.get_five_relation se in
  let e_obj = A7Element.get_five_objects_list se in
  let (x, y) = A7Element.get_location se in
  let e_content =
    vgrp (
        event_assignment_eval_text
        ^^ (doc_of_event_origin_decoration e_origin)
        ^^ (sensor_check_text e_rel e_obj)
        ^^ (sensor_position_text x y)
        ^^ (sensor_text s_label)
      ) in
  vgrp (event_text e_id e_content)
  
let doc_of_sensor_events_list se_list =
  let f = fun doc se -> vgrp (doc ^^ (doc_of_sensor_event se)) in
  List.fold_left f empty se_list


(* Functions to generates doc about transition *)
(* *)
(* *)
  
let doc_of_transition t =
  let t_id = A7Element.get_id t in
  let t_label = A7Element.get_label t in
  let t_e_idref = A7Element.get_event_idref t in
  let t_up_idref = A7Element.get_up_place_idref t in
  let t_dp_idref = A7Element.get_down_place_idref t in
  let (x, y) = A7Element.get_location t in
  let e_content =
    vgrp (
        (transition_event_text t_e_idref)
        ^^ (transition_up_place_text t_up_idref)
        ^^ (transition_down_place_text t_dp_idref)
        ^^ (transition_position_text x y)
      ) in
  vgrp (transition_text t_id t_label e_content)

let doc_of_transitions_list t_list =
  let f = fun doc t -> vgrp (doc ^^ (doc_of_transition t)) in
  List.fold_left f empty t_list

let doc_of_initial_places_list p_list =
  let f = fun doc p -> vgrp (doc ^^ (initial_place_text p)) in
  List.fold_left f empty p_list

let doc_of_final_places_list p_list =
  let f = fun doc p -> vgrp (doc ^^ (final_place_text p)) in
  List.fold_left f empty p_list

(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)

(* val abstract_seven_to_seven_xml_doc : A7.abstract_seven -> Pp.doc *)
                   
let abstract_seven_to_seven_xml_doc a7 =
  let p_list = A7.get_place_list a7
  and se_list = A7.get_sensor_event_list a7
  and t_list = A7.get_transition_list a7
  and ip_list = A7.get_initial_place_idref_list a7
  and fp_list = A7.get_final_place_idref_list a7 in
  vgrp
    (
      xml_prologue_text
      ^^ (scenario_text
            ((doc_of_sensor_events_list se_list)
             ^^ (sequence_text
                   ((doc_of_places_list p_list)
                    ^^ (doc_of_transitions_list t_list)
                    ^^ (doc_of_initial_places_list ip_list)
                    ^^ (doc_of_final_places_list fp_list)
                   )
                )
            )
         )
    )

(* val abstract_seven_to_seven_xml_file : string -> A7.abstract_seven -> unit *)

let abstract_seven_to_seven_xml_file file_name a7 =
  let a7_doc = (abstract_seven_to_seven_xml_doc a7) in
  let file_out = open_out file_name in
  begin
    ppToFile file_out 80 a7_doc;
    flush file_out;
    close_out file_out;
  end
