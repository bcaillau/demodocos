(*
 *
 * Module to save a configuration 
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * The main function of this module is to save a configuration 
 * (on a config file).
 *)

(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)

(* function to save a given configuration on config file *)

val save_config : Configuration.t -> string -> unit
  

(* function to save the classifiers (from configuration) on a 
 * global classifiers config file *)

val save_classifiers : Configuration.t -> string -> unit
