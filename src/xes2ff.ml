(*
 *
 * Commnandline command xes2ff
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2013
 *
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

open Configuration
open XesProcess

let rec css_of_list =
  function
      [] -> ""
    | [s] -> s
    | s::l -> s ^ ", " ^ (css_of_list l)

let rec log_dim =
  function
      [] -> 0
    | t::l -> max (trace_dim t) (log_dim l)

and trace_dim =
  function
      [] -> 0
    | i::l -> max (i+1) (trace_dim l)

let execute_step s_name s_function s_param =
  begin
    Reporting.init_step s_name;
    s_function s_param;
    Reporting.end_step s_name
  end

let execute_step_with_result s_name s_function s_param =
  begin
    Reporting.init_step s_name;
    let result = s_function s_param in
    begin
      Reporting.end_step s_name;
      result
    end
  end

(*-----------------------------------------------------------------
 * Configuration
 *-----------------------------------------------------------------*)

let configuration = Configuration.default_t

let configuration_init () = Configuration.init configuration
                          

(*-----------------------------------------------------------------
 * Commandline
 *-----------------------------------------------------------------*)

class _v =
  Command.token_generic
    "-v"
    "-v <n>"
    "-v <n> (verbose <n>)"
    1
    (function [|s|] ->
              Configuration.set_verbose configuration (int_of_string s)
            | _ -> failwith "Xes2ff._v: impossible case")

class _a =
  Command.token_generic
    "-a"
    "-a <n>"
    "-a <n> (optimization limit <n>)"
    1
    (function [|n|] ->
              (set_direct_apx_config configuration true (int_of_string n) 0)
            | _ -> failwith "Xes2ff._a: impossible case")

class _p =
  Command.token_generic
    "-p"
    "-p <f>"
    "-p <f> (pdf filename <f>)"
    1
    (function [|s|] ->
              Configuration.set_tnfnet_output configuration (Some s)
            | _ -> failwith "Xes2ff._p: impossible case")

class _g =
  Command.token_generic
    "-g"
    "-g <f>"
    "-g <f> (graph filename <f>)"
    1
    (function [|s|] ->
              Configuration.set_tnfgraph_output configuration (Some s)
            | _ -> failwith "Xes2ff._g: impossible case")

class _s =
  Command.token_generic
    "-s"
    "-s <f>"
    "-s <f> (#SEVEN file <f>)"
    1
    (function [|s|] ->
              Configuration.set_seven_output configuration (Some s)
            | _ -> failwith "Xes2ff._s: impossible case")

class _l =
  Command.token_generic
    "-l"
    "-l"
    "-l (location)"
    0
    (fun _ -> Configuration.set_a7work_location configuration true)

class _sa =
  Command.token_generic
    "-sa"
    "-sa"
    "-sa (seven code analysis)"
    0
    (fun _ -> Configuration.set_a7work_analysis configuration true)

class _n =
  Command.token_generic
    "-n"
    "-n <n>"
    "-n <n> (synthesis traces number <n>)"
    1
    (function [|s|] ->
              Configuration.set_synthesis_traces_number configuration (Some (int_of_string s))
            | _ -> failwith "Xes2ff._s: impossible case")

class _x =
  Command.token_generic
    "-x"
    "-x"
    "-x (display)"
    0
    (fun _ -> Configuration.set_display_net configuration true)

class _c =
  Command.token_generic
    "-c"
    "-c <c>"
    "-c <c> (classifier <c>)"
    1
    (function [|s|] ->
              Configuration.set_classifier_choice configuration s
            | _ -> failwith "Xes2ff._c: impossible case")

class _t =
  Command.token_generic
    "-t"
    "-t <f>"
    "-t <f> (event table <f>)"
    1
    (function [|s|] ->
              Configuration.set_table_output configuration (Some s)
            | _ -> failwith "Xes2ff._t: impossible case")
	
(* Command line *)

class command_line =
  fun command_name ->
    let o_v = new _v
    and o_a = new _a
    and o_p = new _p
    and o_g = new _g
    and o_s = new _s
    and o_l = new _l
    and o_sa = new _sa
    and o_n = new _n
    and o_x = new _x
    and o_c = new _c
    and o_t = new _t in
    let _ =
      begin
	Command.pairwise_exclude [|o_p;o_x;o_g;o_s|];
	List.iter
	  (fun t -> t#add_implies [])
	  [o_v;o_a;o_p;o_g;o_s;o_l;o_sa;o_n;o_x;o_c;o_t]
      end in
      Command.command_generic
	command_name
	[o_v;o_a;o_p;o_g;o_s;o_l;o_sa;o_n;o_x;o_c;o_t]
	[[o_p];[o_g];[o_s];[o_l];[o_sa];[o_n];[o_x]]

(* Command line parser *)

let parse v =
  if Array.length v <= 0
  then
    begin
      output_string stderr "Too few arguments\n";
      false
    end
  else
    let s =
      Stream.from
	(fun i -> if (i+1) < (Array.length v) then Some v.(i+1) else None)
    and command_name = v.(0) in
    let c = new command_line command_name in
    let error m =
      begin
	output_string stderr m;
	output_char stderr '\n';
	c#print_diagnosis stderr;
	output_char stderr '\n';
	c#print_usage stderr;
	output_char stderr '\n'
      end in
    try
      c#parse s;
      if not c#is_correct
      then
	begin
	  error "Incorrect parameters/options";
	  false
	end
      else
	begin
	  c#evaluate;
	  match c#parameters with
	    [] ->
	      begin
		error "A XES filename is required";
		false
	      end
	  | [x] ->
	      begin
		Configuration.set_input configuration x;
		true
	      end
	  | _ ->
	      begin
		error "Too many parameters";
		false
	      end
	end
    with
      Command.Syntax_error m ->
	begin
	  error m;
	  false
	end


(*-----------------------------------------------------------------
 * Top level command
 *-----------------------------------------------------------------*)

(* function to execute the synthesis process from an xes file and a classifier *)    
  
let synthesize xfn cn =
  let x =
    try
      Xes.log_of_file xfn
    with
	Invalid_argument s ->
	  begin
	    Reporting.error_report "Could not parse XES file: %s\n" s;
	    exit 2
	  end
      | Sys_error s ->
	  begin
	    Reporting.error_report "Error when parsing XES file: %s\n" s;
	    exit 2
	  end
      | _ ->
	  begin
	    Reporting.error_report "Unknown error when parsing XES file\n" "";
	    exit 2
	  end
  in
  let cl = List.map (fun (c,_) -> c) x.Xes.log_classifiers in
  if not (List.mem cn cl)
  then
    begin
      Printf.fprintf stderr "Classifier %s not defined in XES file %s\n" cn  xfn;
      Printf.fprintf stderr "Defined classifiers are: %s\n" (css_of_list cl);
      exit 1
    end
  else
    let (ontology, traces) = Xes.encode_log x cn in (* a is the ontology *)
    let synthesis_traces = evaluate_synthesis_traces configuration traces in
    begin
      let (r, ffnet_structure) =
        execute_step_with_result
          "Initialisation of synthesis"
          initialize_synthesis configuration synthesis_traces
      in
      begin
	execute_main_synthesis configuration r ffnet_structure;
        if (is_tfnet_synthesis configuration) 
        then (finalize_synthesis_for_tf_net configuration ffnet_structure);
	begin
	  begin
	    match Configuration.get_tnfgraph_output configuration with
	      None -> ()
	    | Some f ->
	       begin
		 failwith "Xes2ff.synthesize: graph output not yet implemented"
	       end
	  end;
	  begin
	    match Configuration.get_seven_output configuration with
	      None -> ()
	    | Some file ->
                 let a7 =
                   execute_step_with_result
                     "#7 Code Generating"
                     (produce_seven_code configuration ffnet_structure
                                         ontology synthesis_traces)
                     file
                 in
                 if Configuration.get_a7work_analysis configuration
                 then
                   execute_step
                     "#7 Code Analysis"
                     (analyse_seven_code a7 synthesis_traces)
                     (evaluate_test_traces configuration traces)
	  end;
	  begin
	    match Configuration.get_table_output configuration with
	      None -> ()
	    | Some t ->
	       begin
		 match Configuration.get_seven_output configuration with
		   None -> Xes_utilities.print_ontology_to_file ontology t
		 | Some _ -> ()
	       end
	  end;
	  exit 0
	end
      end
    end


(* Main function *)
    
let main () =
  try
    if parse Sys.argv
    then
      begin
        (* let dir = Filename.dirname (Sys.executable_name) in
        let config_name = "tnf.config" in
        let config_file = Filename.concat dir config_name in
        Configuration.load configuration config_file; *)
	Verbose.set (Configuration.get_verbose configuration);
        Reporting.init_report configuration;
        let input = (Configuration.get_input configuration) in
        let classifier = (Configuration.get_classifier_choice configuration) in
	synthesize input classifier      
      end
    else
      begin
	Printf.fprintf stderr "Usage: %s [-v <n>] [-p <f>] [-x] [-c <c>] <filename>\n" Sys.argv.(0);
	exit 1
      end
  with
      Failure s ->
	begin
	  Printf.fprintf stderr "Exiting on uncaught failure exception: %s\n" s;
	  exit 2
	end
    | Invalid_argument s ->
	begin
	  Printf.fprintf stderr "Exiting on uncaught invalid argument exception: %s\n" s;
	  exit 2
	end
    | _ as x ->
	begin
	  Printf.fprintf stderr "Exiting on uncaught unknown exception\n";
	  raise x
	end

let _ =
  Printexc.catch
    main
    ()
