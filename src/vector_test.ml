Random.self_init ();;

let d = 30
and n = 50
and p = 3;;

let rbit p =
  let n = truncate (ceil (1. /. p)) in
    (Random.int n)=0;;

let rvect n p =
  let c = Vector.make n false in
    begin
      for i=1 to p do
	let j = ref (Random.int n) in
	  begin
	    while (Vector.get c (!j)) do
	      j := Random.int n
	    done;
	    Vector.set c (!j) true
	  end
      done;
      c
    end;;

let v1 = rvect n p;;

let byte = 8;;

let byte_list v =
  let n = Vector.length v in
  let p = n / byte
  and q = n mod byte
  and l = ref [] in
    begin
      (if (q > 0) then l := [ Vector.get_byte v (p * byte) q ]);
      for i=p-1 downto 0 do
	l := (Vector.get_byte v (i * byte) byte) :: (!l)
      done;
      !l
    end;;
      
Vector.length v1;;

Vector.print v1;;

byte_list v1;;

Vector.set v1 35 true;;

Vector.blit ~src:v1 ~src_idx:27 ~dst:v1 ~dst_idx:6 ~len:21;;  

Vector.length v1;;

Vector.print v1;;

byte_list v1;;

Vector.get_byte v1 0 24;;

Vector.get_byte v1 27 23;;

Vector.set_byte v1 26 16 32000;;

Vector.get_byte v1 27 23;;

Vector.print v1;;

let v2 = Vector.make 100 false;;

Vector.set_byte v2 60 8 255;;

Vector.print v2;;

Vector.blit ~src:v2 ~src_idx:60 ~dst:v2 ~dst_idx:0 ~len:8;;

Vector.print v2;;

Vector.blit  ~src:v2 ~src_idx:4 ~dst:v2 ~dst_idx:62 ~len:8;;

Vector.print v2;;

Vector.set v2 1 false;;
Vector.set v2 3 false;;
Vector.set v2 5 false;;
Vector.set v2 7 false;;

Vector.print v2;;

Vector.blit  ~src:v2 ~src_idx:0 ~dst:v2 ~dst_idx:60 ~len:8;;

Vector.print v2;;

let x = Vector.get_byte v2 ~idx:60 ~len:8;;

Vector.set_byte v2 ~idx:32 ~len:8 ~byte:x;;

Vector.get_byte v2 ~idx:32 ~len:8;;

Vector.print v2;;

Vector.set_byte v2 ~idx:80 ~len:12 ~byte:(2048+512+128+32+8+2);;

Vector.print v2;;
