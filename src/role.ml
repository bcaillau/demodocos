(*
 *
 * Controllability/observability roles
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2014
 *
 *)

(* Type of role *)

type t =
    {
      dim : int;
      ctl : Vector.t;
      obs : Vector.t
    }

(* Constructors *)

(* val make : int -> t *)

let make n =
  if n >= 0
  then
    {
      dim=n;
      ctl=Vector.make n false;
      obs=Vector.make n false;
    }
  else
    invalid_arg "Role.make: invalid dimension"
  
(* val clone : t -> t *)

let clone { dim=n; obs=vo; ctl=vc } =
  {
    dim=n;
    obs=Vector.clone vo;
    ctl=Vector.clone vc
  }

(* [dim t] returns the dimension of role [t] *)

(* val dim : t -> int *)

let dim { dim=n } = n

(* set/get *)

(* val set_observable : t -> int -> bool -> unit *)

let set_observable { dim=n; obs=vo } i b =
  if (i >= 0) && (i < n)
  then Vector.set vo i b
  else invalid_arg "Role.set_observable: invalid index"

(* val set_controllable : t -> int -> bool -> unit *)

let set_controllable { dim=n; ctl=vc } i b =
  if (i >= 0) && (i < n)
  then Vector.set vc i b
  else invalid_arg "Role.set_controllable: invalid index"

(* val get_observable : t -> int -> bool *)

let get_observable { dim=n; obs=vo } i =
  if (i >= 0) && (i < n)
  then Vector.get vo i
  else invalid_arg "Role.get_observable: invalid index"

(* val get_controllable : t -> int -> bool *)

let get_controllable { dim=n; obs=vc } i =
  if (i >= 0) && (i < n)
  then Vector.get vc i
  else invalid_arg "Role.get_controllable: invalid index"

(* Constructors *)

(* val init : observable:(int -> bool) -> controllable:(int -> bool) -> int -> t *)

let init ~observable:fobs ~controllable:fctl n =
  let r = make n in
    begin
      for i=0 to n-1 do
	set_controllable r i (fctl i);
	set_observable r i (fobs i)
      done;
      r
    end

(* [print c r] prints role [r] on channel [c] *)

(* val print : out_channel -> t -> unit *)

let print c r =
  let n = dim r in
    begin
      Printf.fprintf c "<";
      for i=0 to n-1 do
	match ((get_observable r i),(get_controllable r i)) with
	    (false,false) -> ()
	  | (false,true) -> Printf.fprintf c " %d:c" i
	  | (true,false) -> Printf.fprintf c " %d:o" i
	  | (true,true) -> Printf.fprintf c " %d:oc" i
      done;
      Printf.fprintf c " >\n"
    end

(* Iterators *)

(* [iter_unobservable f r] evaluates [f i] foreach [i] unobservable in [r] *)

(* val iter_unobservable : (int -> unit) -> t -> unit *)

let iter_unobservable f ({dim=n} as r) =
  for i=0 to n-1 do
    if not (get_observable r i)
    then f i
  done
