(*
 *
 * XML of #SEVEN code
 *
 * (c) -- <--@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* This module generate the XML file of #SEVEN code.
 * The main function works from an abstract SEVEN structure to obtain 
 * a dot structure (using module Pp). 
 * Then, finally, the XML file is generated.
 *)

(*-----------------------------------------------------------------
 * Module openning, parameter & utilities
 *-----------------------------------------------------------------*)

(* None *)


(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)                                       

(* val abstract_seven_to_seven_xml_file : string -> A7.t -> unit *)

let abstract_seven_to_seven_xml_file file_name a7 =
  XmlSevenOut.abstract_seven_to_seven_xml_file file_name a7
                                       

(* val seven_xml_file_to_abstract_seven : string -> A7.t *)

let seven_xml_file_to_abstract_seven file_name =
  XmlSevenIn.a7_of_x7_file file_name
