(*
 *
 * Sub-Module of A7Analysis to compute an simple paths number on #7 code
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * This module provides functions to compute the number of realizations 
 * of a given scenario, ie the number of simple paths starting from 
 * the initial state and ending in a final state
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

open A7Attributes
open A7

(* function to know if a given place is a final place *)
let is_final_place a7 place_urn =
  let fp_urn_list = get_final_place_urn_list a7 in
  (List.mem place_urn fp_urn_list) 

(* function to sum the elements of a given list *)
let rec sum l =
  match l with
  | [] -> 0
  | hd :: tl -> hd + sum tl

(* function to obtain a list without duplicate (result list) 
 * a given list (source list) *)
let rec list_uniq result source =
  match source with
  | [] -> result
  | e::new_source ->
     let new_result = if (List.mem e result) then result else e::result
     in list_uniq new_result new_source 

            
(*-----------------------------------------------------------------
 * Mains functions
 *-----------------------------------------------------------------*)
      
(* function to obtain the number of simple paths on an abstract seven,
 * starting from a given place *) 
let rec get_number_from_given_place a7 fp_urn_list places_of_path place =
  let place_urn = (A7Element.get_urn place) in
  if
    (List.mem place_urn fp_urn_list) (* place is a final place *)
    || (List.mem place_urn places_of_path) (* 1 loop = 1 path *)
  then
    1
  else
    let children_list = get_children_places_of_place a7 place in
    match children_list with
    | [] -> 0
    | _ -> sum (List.map
                  (get_number_from_given_place
                     a7 fp_urn_list (place_urn::places_of_path))
                  children_list)

           
(* function to obtain the number of simple path on an abstract seven,
 * starting from initial places *)       
let get_number_from_initial_places a7 =
  let ip_urn_list = get_init_place_urn_list a7 in
  let ip_list = get_places_filtered_by_urn_list a7 ip_urn_list in
  let fp_urn_list = get_final_place_urn_list a7 in
  let initial_places_of_path = [] in
  match ip_list with
  | [] -> 0
  | _ -> sum (List.map
                (get_number_from_given_place
                   a7 fp_urn_list initial_places_of_path)
                ip_list)

