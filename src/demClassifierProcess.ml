(*
 *
 * Classifier Management Processes
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

open DemBasis
   


(*-----------------------------------------------------------------
 * Classifiers management :  : view classifiers, add/remove, modify
 *-----------------------------------------------------------------*)

(* function to print a list of keys *)
       
let print_keys keys_list =
  match keys_list with
  | [] | ""::[] ->  Printf.fprintf stdout "%s" "<< no value >>"
  | v::[] ->  Printf.fprintf stdout "%s" v
  | v::xxx ->
     begin
       Printf.fprintf stdout "%s" v;
       List.iter (Printf.fprintf stdout " ; %s ") xxx
     end
    

(* function to print a classifier prefix *)
    
let print_a_classifier_prefix c_group c_prefix =
  begin
    (Printf.fprintf
       stdout
       " (x) %s prefix = %s \n"
       c_group c_prefix);
  end
    

(* function to print a classifier keys *)
    
let print_a_classifier_keys c_group c_keys =
  begin
    (Printf.fprintf stdout " (x) %s keys = " c_group);
    print_keys c_keys;
    Printf.fprintf stdout "%s\n" ""
  end
    

(* function to print a classifier def *)
    
let print_a_classifier_def c_name c_group c_keys =
  begin
    (Printf.fprintf
       stdout
       " (x) %s (group: %s) \n       => "
       c_name c_group);
    print_keys c_keys;
    Printf.fprintf stdout "%s\n" ""
  end
  

(* function to view informations about classifiers*)

let view_classifier_sep config =
  begin
    Printf.fprintf stdout "Classifier separator (for keys): %s\n"
                   (Configuration.get_classifier_separator config)
  end

let view_classifier_prefix config =
  begin
    Printf.fprintf stdout "Classifier prefix (by group): %s\n" "";
    List.iter 
      (fun (c_group, c_prefix) -> print_a_classifier_prefix c_group c_prefix)
      (Configuration.get_classifier_prefix_list config)
  end

let view_classifier_keys config =
  begin
    Printf.fprintf stdout "Classifier possibles keys (by group): %s\n" "";
    List.iter 
      (fun (c_group, c_keys) -> print_a_classifier_keys c_group c_keys)
      (Configuration.get_classifier_keys_list config)
  end

let view_classifier_def config =
  begin
    Printf.fprintf stdout "Classifier definitions: %s\n" "";
    List.iter 
      (fun (c_name, c_group, c_keys) -> print_a_classifier_def c_name c_group c_keys)
      (Configuration.get_classifier_def_list config)
  end

let view_classifiers config =
  begin
    view_classifier_sep config;
    Printf.fprintf stdout "%s\n" "";
    view_classifier_prefix config;
    Printf.fprintf stdout "%s\n" "";
    view_classifier_keys config;
    Printf.fprintf stdout "%s\n" "";
    view_classifier_def config
  end


(* function to add a classifier to all configured classifiers *)

let add_classifier_def classifiers_config_file config c_name c_group c_keys =
  try
    begin
      Printf.fprintf stdout "Add classifier: %s (format group: %s) \n" c_name c_group;
      Configuration.add_classifier_def config (c_name, c_group, c_keys);
      ConfigurationOutput.save_classifiers config classifiers_config_file;
      ConfigurationInput.load_classifiers config classifiers_config_file;
      view_classifier_def config
    end
  with _ ->
    begin
      Printf.fprintf
        stderr
        "Unable to add classifier %s of group %s for unknown reason\n"
        c_name c_group;
      exit 10
    end 


(* function to add a classifier to all configured classifiers *)

let remove_classifier_def classifiers_config_file config c_name =
  try
    begin
      Printf.fprintf stdout "Remove classifier: %s \n" c_name;
      Configuration.remove_classifier_def config c_name;
      ConfigurationOutput.save_classifiers config classifiers_config_file;
      ConfigurationInput.load_classifiers config classifiers_config_file;
      view_classifier_def config
    end
  with _ ->
    begin
      Printf.fprintf
        stderr
        "Unable to remove classifier %s for unknown reason\n"
        c_name;
      exit 10
    end 


(* function to add a classifier to all configured classifiers *)

let edit_classifier_def classifiers_config_file config c_name c_group c_keys =
  try
    begin
      Printf.fprintf stdout "Modify classifier: %s (format group: %s) \n" c_name c_group;
      Configuration.remove_classifier_def config c_name;
      Configuration.add_classifier_def config (c_name, c_group, c_keys);
      ConfigurationOutput.save_classifiers config classifiers_config_file;
      ConfigurationInput.load_classifiers config classifiers_config_file;
      view_classifier_def config
    end
  with _ ->
    begin
      Printf.fprintf
        stderr
        "Unable to edit classifier %s of group %s for unknown reason\n"
        c_name c_group;
      exit 10
    end 
