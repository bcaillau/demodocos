(*
 *
 * Configuration of TnF Tool 
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* 
 * This module provides a type configuration used on tool
 * and some functions directly or indirectly called by a command
 *) 

(*-----------------------------------------------------------------
 * Module openning & utilities
 *-----------------------------------------------------------------*)

(* function to obtain the extension of a given file name *)

let get_extension_of_file_name s =
  try
    let extension_index = String.index s '.' in
    let sub_len = (String.length s) - extension_index in
    String.sub s extension_index sub_len
  with _ -> ""
          

(* function to remove the extension of a given file name *)

let remove_extension_of_file_name s =
  try
    let extension_index = String.rindex s '.' in
    String.sub s 0 extension_index
  with _ -> s
          

(* function to substitute the extension of a given file name *)

let substitute_extension_of_file_name s e = (remove_extension_of_file_name s) ^ e

                                          
(* function to add the double quotes around a string *)
                                          
let stringify s = "\""^(String.escaped s)^"\""


                                          
(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

(* config file definition *)

let main_config_file_name = "tnfMain.conf"
let tag_config_file_name tag = "tnfTag_" ^ tag ^ ".conf"
let classifiers_config_file_name = "tnfGlobalClassifiers.conf"


(* list of recognized source formats *)

let valid_src_formats_list = "xes"::"iccas"::"ispm2"::"ispm3"::"seven"::[]
  

(* type classifiers_config *)

type classifiers_config =
  {
    mutable separator : string ;
    mutable prefix : (string * string) list ;
    mutable keys : (string * string list) list ;
    mutable def : (string * string * string list) list
  }

let default_classifiers_config =
  {
    separator = ";";
    prefix = ("xes", "concept:")::("iccas", "action:")::("ispm2", "action:")
             ::("ispm3", "action:")::("seven", "action:")::[];
    keys =
      ("xes", "name"::[])
      ::("iccas", "id"::"type"::"state"::"actuator"
                  ::"description"::"verb"::"affected"::"instrument"
                  ::"recipient"::"origin"::"destination"::"treatedStructure"::[])
      ::("ispm2", "id"::"type"::"state"::"actuator"
                  ::"description"::"verb"::"affected"::"instrument"
                  ::"recipient"::"origin"::"destination"::"treatedStructure"::[])
      ::("ispm3", "id"::"type"::"state"::"actuator"
                  ::"description"::"verb"::"affected"::"instrument"
                  ::"recipient"::"origin"::"destination"::"treatedStructure"::[])
      ::("seven", "name"::[])
      ::[];
    def =
      ("default_xes", "xes", "name"::[])
      ::("default_iccas", "iccas", "verb"::"affected"::"instrument"::[])
      ::("default_ispm2", "ispm2", "verb"::"affected"::"instrument"::[])
      ::("default_ispm3", "ispm3", "verb"::"affected"::"instrument"::[])
      ::("default_seven", "seven", "name"::[])
      ::[]
  }
                           
                                          
(* type apx_config *)

type apx_config =
  {
    mutable requested : bool ;
    mutable limit : int ;
    mutable count : int
  }

let default_apx_config =
  {
    requested = false ;
    limit = 1000 ;
    count = 0
  }

let quotient_apx_config =
  {
    requested = true ;
    limit = 0 ;
    count = 0
  }


(* control variables for configuration parameters *)

let valid_input_adapting_script = "starting_action"::"russiandolls"::"none"::[]
let default_input_adapting_script = "none"

let default_ontology_global_files = []
(* "OntoSPM_for_Sunset.owl"::"OntoS3PM_for_Sunset.owl"::[] *)

let valid_ontology_label_type = "iri_segment"::"label"::[]
let default_ontology_label_type = "label"

let valid_verbose_list = 0::1::2::3::[]
let default_verbose = 0

let valid_tnfgraph_label_type = "letter"::"number"::"label"::[]
let default_tnfgraph_label_type = "letter"

let valid_seven_sensor_label_type = "letter"::"number"::"onto_label"::"five_ref"::[]
let default_seven_sensor_label_type = "onto_label"

let valid_seven_five_value_type = "onto_label"::"five_ref"::[]
let default_seven_five_value_type = "five_ref"
                                  

(* type configuration *)

type t =
  {
    mutable conf_input_file_name : string ;
    mutable conf_input_working_file_postfix : string ;
    mutable conf_input_adapting_script : string ;
    mutable conf_classifier_choice : string ;
    mutable conf_ontology_base_file : string option ;
    mutable conf_ontology_global_files : string list ;
    mutable conf_ontology_label_type : string ;
    mutable conf_instances_number : int option ;
    mutable conf_verbose : int ; (* 0: quiet, 1: normal, 2: verbose, 3: debug *)
    mutable conf_approximation : apx_config ;
    mutable conf_classifiers : classifiers_config ;
    mutable conf_output_directory : string option ;
    mutable conf_output_xes : string option ;
    mutable conf_output_tnfnet : string option ;
    mutable conf_output_tnfgraph : string option ;
    mutable conf_output_seven : string option ;
    mutable conf_output_a7xspm : string option ;
    mutable conf_output_events_table : string option ;
    mutable conf_tnfgraph_label_type : string ;
    mutable conf_seven_sensor_label_type : string ;
    mutable conf_seven_five_value_type : string ;
    mutable conf_seven_location_computing : bool ;
    mutable conf_seven_analysis : bool ;
    mutable conf_seven_event_nb_limit : int
  }

let default_t =
  {
    conf_input_file_name = "default.xes";
    conf_input_working_file_postfix = "_input";
    conf_input_adapting_script = default_input_adapting_script;
    conf_classifier_choice = "default_xes";
    conf_ontology_base_file = None;
    conf_ontology_global_files = default_ontology_global_files;
    conf_ontology_label_type = default_ontology_label_type ;
    conf_instances_number = None;
    conf_verbose = default_verbose;
    conf_approximation = default_apx_config;
    conf_classifiers = default_classifiers_config;
    conf_output_directory = None;
    conf_output_xes = Some "_xes";
    conf_output_tnfnet = None;
    conf_output_tnfgraph = None;
    conf_output_seven = None;
    conf_output_a7xspm = None;
    conf_output_events_table = None;
    conf_tnfgraph_label_type = default_tnfgraph_label_type ;
    conf_seven_sensor_label_type = default_seven_sensor_label_type ;
    conf_seven_five_value_type = default_seven_five_value_type ;
    conf_seven_location_computing = false;
    conf_seven_analysis = false;
    conf_seven_event_nb_limit = 100
  }


(*-----------------------------------------------------------------
 * Constructor(s)
 *-----------------------------------------------------------------*)

(* function to initialize a configuration with default value *)
  
let init t =
  begin
    t.conf_input_file_name <- "default.xes";
    t.conf_input_working_file_postfix <- "_input";
    t.conf_input_adapting_script <- default_input_adapting_script;
    t.conf_classifiers <- default_classifiers_config;
    t.conf_classifier_choice <- "default_xes";
    t.conf_ontology_base_file <- None;
    t.conf_ontology_global_files <- default_ontology_global_files;
    t.conf_ontology_label_type <- default_ontology_label_type ;
    t.conf_instances_number <- None;
    t.conf_approximation <- default_apx_config;
    t.conf_verbose <- 1;
    t.conf_output_directory <- None;
    t.conf_output_xes <- Some "_xes";
    t.conf_output_tnfnet <- None;
    t.conf_output_tnfgraph <- None;
    t.conf_output_seven <- None;
    t.conf_output_a7xspm <- None;
    t.conf_output_events_table <- None;
    t.conf_tnfgraph_label_type <- "letter";
    t.conf_seven_sensor_label_type <- default_seven_sensor_label_type;
    t.conf_seven_five_value_type <- "ontology_en";
    t.conf_seven_location_computing <- false;
    t.conf_seven_analysis <- false;
    t.conf_seven_event_nb_limit <- 100
  end
  

(* functions to initialize the lists of classifier config *)

let init_classifier_prefix_list t = t.conf_classifiers.prefix <- []
let init_classifier_keys_list t = t.conf_classifiers.keys <- []
let init_classifier_def_list t = t.conf_classifiers.def <- []

                            
  
(*-----------------------------------------------------------------
 * Accessors
 *-----------------------------------------------------------------*)

(* Accessors for synthesis configuration *)

let get_input_file_name t = t.conf_input_file_name
let get_input_working_file_postfix t = t.conf_input_working_file_postfix
let get_input_adapting_script t = t.conf_input_adapting_script
let get_classifier_choice t = t.conf_classifier_choice
let get_ontology_base_file t = t.conf_ontology_base_file
let get_ontology_global_files_list t = t.conf_ontology_global_files
let get_ontology_label_type t = t.conf_ontology_label_type
let get_instances_number t = t.conf_instances_number
let get_verbose t = t.conf_verbose

let set_input_file_name t v = t.conf_input_file_name <- v
let set_input_working_file_postfix t v = t.conf_input_working_file_postfix <- v
let set_input_adapting_script t v = t.conf_input_adapting_script <- v
let set_classifier_choice t v = t.conf_classifier_choice <- v
let set_ontology_base_file t v = t.conf_ontology_base_file <- v
let set_ontology_global_files_list t v = t.conf_ontology_global_files <- v
let set_ontology_label_type t v = t.conf_ontology_label_type <- v
let set_instances_number t v = t.conf_instances_number <- v  
let set_verbose t v = t.conf_verbose <- v
                    
let add_ontology_global_file t file =
  let files_list = get_ontology_global_files_list t in
  match (List.mem file files_list) with
  | true -> ()
  | false ->
     begin
       let new_list = file::files_list in
       (set_ontology_global_files_list t new_list)
     end
  
let remove_ontology_global_file t file =
  let files_list = get_ontology_global_files_list t in
  match (List.mem file files_list) with
  | true ->
     begin
       let new_list =
         List.filter (fun f -> not (f = file)) files_list
       in
       (set_ontology_global_files_list t new_list)
     end
  | false -> ()

  
(* Accessors for approximation configuration *)
                    
let is_approximation_requested t = t.conf_approximation.requested
let get_approximation_limit t = t.conf_approximation.limit
let get_approximation_count t = t.conf_approximation.count
                             
let set_approximation_requested t req = t.conf_approximation.requested <- req
let set_approximation_limit t lim = t.conf_approximation.count <- lim
let set_approximation_count t i = t.conf_approximation.count <- i

let set_apx_config t apx_conf = t.conf_approximation <- apx_conf
                              
let set_direct_apx_config t req lim count =
  begin
    set_approximation_requested t req ;
    set_approximation_limit t lim ;
    set_approximation_count t count
  end


(* Accessors for global classifiers *)

let get_classifier_separator t = t.conf_classifiers.separator
let get_classifier_prefix_list t = t.conf_classifiers.prefix
let get_classifier_keys_list t = t.conf_classifiers.keys
let get_classifier_def_list t = t.conf_classifiers.def

let set_classifier_separator t v = t.conf_classifiers.separator <- v
let set_classifier_prefix_list t v = t.conf_classifiers.prefix <- v
let set_classifier_keys_list t v = t.conf_classifiers.keys <- v                             
let set_classifier_def_list t v = t.conf_classifiers.def <- v
                               
let add_classifier_prefix t group prefix =
  set_classifier_prefix_list
    t
    (List.rev_append ((group, prefix)::[]) (get_classifier_prefix_list t))
  
let add_classifier_keys t group keys =
  set_classifier_keys_list
    t
    (List.rev_append ((group, keys)::[]) (get_classifier_keys_list t))
  
let add_classifier_def t (c_name, c_group, c_keys) =
  set_classifier_def_list
    t
    (List.rev_append ((c_name, c_group, c_keys)::[]) (get_classifier_def_list t))                              
let remove_classifier_def t c_name =
  let mapping1 list = List.rev_map (fun (a, b, c) -> (a, (b, c))) list in
  let mapping2 list = List.rev_map (fun (a, (b, c)) -> (a, b, c)) list in     
  set_classifier_def_list
    t
    (mapping2 (List.remove_assoc c_name (mapping1 (get_classifier_def_list t))))


(* Accessors for output configuration *)

let get_output_directory t = t.conf_output_directory
let get_output_xes t = t.conf_output_xes
let get_output_tnfnet t = t.conf_output_tnfnet
let get_output_tnfgraph t = t.conf_output_tnfgraph
let get_output_seven t = t.conf_output_seven
let get_output_a7xspm t = t.conf_output_a7xspm
let get_output_events_table t = t.conf_output_events_table
(* let get_output_tnfnet_display t = t.conf_output_tnfnet_display *)

let set_output_directory t v = t.conf_output_directory <- v
let set_output_xes t v = t.conf_output_xes <- v
let set_output_tnfnet t v = t.conf_output_tnfnet <- v
let set_output_tnfgraph t v = t.conf_output_tnfgraph <- v
let set_output_seven t v = t.conf_output_seven <- v
let set_output_a7xspm t v = t.conf_output_a7xspm <- v
let set_output_events_table t v = t.conf_output_events_table <- v
(* let set_output_tnfnet_display t v = t.conf_output_tnfnet_display <- v *)
                        
  
(* Accessors for options of output computing *)
  
let get_tnfgraph_label_type t = t.conf_tnfgraph_label_type
let get_seven_sensor_label_type t = t.conf_seven_sensor_label_type
let get_seven_five_value_type t = t.conf_seven_five_value_type
let get_seven_location_computing t = t.conf_seven_location_computing
let get_seven_analysis t = t.conf_seven_analysis
let get_seven_event_number_limit t = t.conf_seven_event_nb_limit
                          
let set_tnfgraph_label_type t v = t.conf_tnfgraph_label_type <- v
let set_seven_sensor_label_type t v = t.conf_seven_sensor_label_type <- v
let set_seven_five_value_type t v = t.conf_seven_five_value_type <- v
let set_seven_location_computing t v = t.conf_seven_location_computing <- v
let set_seven_analysis t v = t.conf_seven_analysis <- v
let set_seven_event_number_limit t v = t.conf_seven_event_nb_limit <- v
                                      
  

(*-----------------------------------------------------------------
 * Function to manage the lists of classifiers by group (source format)
 *-----------------------------------------------------------------*)

(* function to obtain the list of classifiers definitions corresponding
 * to a given group (format of source) *)

let get_classifier_def_list_of_group t group =
  let classifier_def_list = get_classifier_def_list t in
  let filter (n, g, k) = (g = group) in
  List.filter filter classifier_def_list

  
(* function to obtain the prefix corresponding to a given group (format of source) *)

let get_prefix_of_group t group =
  try
    let classifier_prefix_list = get_classifier_prefix_list t in
    let filter (g, p) = (g = group) in
    let prefix_list_of_group = List.filter filter classifier_prefix_list in
    match prefix_list_of_group with
    | (g, p)::xxx -> p
    | _ -> ""
  with _ -> ""
  

(* function to obtain a list of classifiers corresponding to
 * a given group (format of source) *)
                  
let get_classifiers_list_of_group t group =
  let classifier_def_list = get_classifier_def_list_of_group t group in
  let prefix = get_prefix_of_group t group in
  let f_add_prefix_to_keys keys_list = List.map (fun k -> prefix ^ k) keys_list in
  let mapping (n, g, keys) = (n, (f_add_prefix_to_keys keys)) in
  (* List.map (fun (n, g, k) -> (n, k)) classifiers_list *)
  List.map mapping classifier_def_list

  
(* function to add a list of classifiers def *)
  
(* let add_classifiers_list_of_group t classifiers_def_list =
  List.iter (add_def t) classifiers_def_list *)
  
  

(*-----------------------------------------------------------------
 * Function to obtain a script
 *-----------------------------------------------------------------*)  

(* function to obtain the script name for a given operation *)
                  
let get_script (op, arg1, arg2) =
  let dir = Filename.dirname (Sys.executable_name) in
  let script_name =
    match (op, arg1, arg2) with
    | ("adapt", "ispm3", "starting_action") -> "starting_five_adaptation.xsl"
    | ("adapt", "ispm3", "russiandolls") -> "russiandolls_five_adaptation.xsl"
    | ("convert", "iccas", "xspm2") -> "convert_iccas_to_xspm2.xsl"
    | ("convert", "ispm2", "xspm2") -> "convert_ispm2_to_xspm2.xsl"
    | ("convert", "ispm3", "xspm2") -> "convert_ispm3_to_xspm2.xsl"
    | ("convert", "seven", "dot") -> "convert_seven_to_dot.xsl"
    | ("group", "iccas", "dir") -> "group_iccas_from_dir"
    | ("group", "iccas", "files") -> "group_iccas_from_files"
    | ("group", "ispm", "dir") -> "group_ispm_from_dir"
    | ("group", "ispm", "files") -> "group_ispm_from_files"
    | (_, _, _) -> ""
  in
  Filename.concat dir script_name

               

(*-----------------------------------------------------------------
 * Function to obtain a target file
 *-----------------------------------------------------------------*)  

(* function to obtain the target file of a given target format *)
                  
let get_target_file t target_format =
  let directory =
    match (get_output_directory t) with None -> "" | Some x -> (x ^ "/")
  in
  let f_subst_ext target ext =
    (substitute_extension_of_file_name 
       (match target with None -> "" | Some x -> x)
       ext)
  in
  let f_dot_ps s = f_subst_ext (get_output_tnfgraph t) s in
  let input_name = Filename.basename (get_input_file_name t) in 
  let input_extension = get_extension_of_file_name input_name in 
  let postfix_and_extension =
    match target_format with
    | "input_work" ->
       Some (f_subst_ext (Some (get_input_working_file_postfix t)) input_extension)
    | "xes" -> Some (f_subst_ext (get_output_xes t) ".xes")
    | "tnfnet" -> Some (f_subst_ext (get_output_tnfnet t) ".pdf")
    | "tnfgraph" | "dot" -> Some (f_dot_ps ".dot")
    | "ps" -> Some (f_dot_ps ".ps")
    | "jpg" -> Some (f_dot_ps ".jpg")
    | "seven" -> Some (f_subst_ext (get_output_seven t) ".xml")
    | "a7xspm" -> Some (f_subst_ext (get_output_a7xspm t) ".xes")
    | "table" -> Some (f_subst_ext (get_output_events_table t) ".txt")
    | "ispm" -> Some "_ispm.xml"
    | "ontology_basis" -> Some "_ontology_basis.txt"
    | _ -> Some ""
  in
  let postfix_and_extension =
    match postfix_and_extension with None -> "" | Some x -> x
  in
   directory
   ^ (substitute_extension_of_file_name input_name (postfix_and_extension))

  
     
(*-----------------------------------------------------------------
 * Function to analyze the context for synthesis
 *-----------------------------------------------------------------*)  

(* val is_tfnet_synthesis : t -> bool *)
  
let is_tfnet_synthesis t =
  not ((get_output_tnfnet t) = None)
(* || (get_output_tnfnet_display t) *)

  
(* val is_seven_synthesis_only : t -> bool *)

let is_graph_synthesis_only t =
  ((get_output_tnfnet t) = None)
  (* && not (get_output_tnfnet_display t) *)
  && (
    not ((get_output_tnfgraph t) = None)
    || not ((get_output_seven t) = None)
  )

  
(* val is_region_adding_requested : t -> bool *)

let is_region_adding_requested t =
  if (is_tfnet_synthesis t) || (not (is_approximation_requested t))
  then true
  else false (* Graph synthesis only and optimization requested *)

  
let is_a7xspm_generating_possible t event_number =
  (get_seven_event_number_limit t) >= event_number
       

                       
(*-----------------------------------------------------------------
 * Function to manage the configuration of approximation/optimization
 *-----------------------------------------------------------------*)  

(* 
 * The approximation config indicates whether the synthesis has to be optimized 
 * and gives the optimization limit.
 * 
 * The synthesis has to be optimized with an approximation algorithm if the
 * approximation is authorized and if the optimization is requested. The approximation 
 * must not be allowed to generate a T/F net, but it can be allowed to generate a graph.
 *
 * The optimization limit is the maximum computation depth before applying 
 * the optimization. 
 *)                           
                                
let is_approximation_limit t =
  if (is_graph_synthesis_only t) && (is_approximation_requested t)
  then (get_approximation_count t) >= (get_approximation_limit t)
  else false
    
let update_approximation_limit_after_computation t =
  if (is_graph_synthesis_only t) && (is_approximation_requested t)
  then set_approximation_count t ((get_approximation_count t) + 1)
  


(*-----------------------------------------------------------------
 * Prints view on configuration
 *-----------------------------------------------------------------*)

(* val print_approximation_config : out_channel -> t -> unit *)
    
let print_approximation_config c t =
  let requested = is_approximation_requested t
  and limit = get_approximation_limit t
  and count = get_approximation_count t in
  if requested
  then 
    Printf.fprintf
      c
      " Approximation requested : YES (%i, %i) \n"
      limit count
  else
    Printf.fprintf c " Approximation requested : NO \n"
