(*
 *
 * Standard text of #SEVEN code in xml format (from #SEVEN xsd)
 *
 * (c) -- <--@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* This module provides standard text corresponding to #SEVEN
 * code in xml format.
 * The definitions are from #SEVEN xsd files.
 *)

(*-----------------------------------------------------------------
 * Functions to generates doc containing standard text of #SEVEN
 *-----------------------------------------------------------------*)

(* Xml prologue and scenario *)

val xml_prologue_text : Pp.doc

val scenario_text : Pp.doc -> Pp.doc


(* Sensor event *)
  
val event_text : string -> Pp.doc -> Pp.doc

val event_assignment_eval_text : Pp.doc
                 
val event_decoration_text : string -> string -> Pp.doc

val sensor_check_text : string -> (string * string) list -> Pp.doc

val sensor_position_text : int -> int -> Pp.doc

val sensor_text : string -> Pp.doc


(* Sequence *)
  
val sequence_text : Pp.doc -> Pp.doc


(* Place *)

val place_text : string -> string -> Pp.doc -> Pp.doc

val place_decoration_text : string -> string -> Pp.doc
  
val place_position_text : int -> int -> Pp.doc


(* Transition *)  
                         
val transition_text : string -> string -> Pp.doc -> Pp.doc

val transition_event_text : string -> Pp.doc

val transition_up_place_text : string -> Pp.doc

val transition_down_place_text : string -> Pp.doc
  
val transition_position_text : int -> int -> Pp.doc


(* Initial & final place *)
  
val initial_place_text : string -> Pp.doc

val final_place_text : string -> Pp.doc
