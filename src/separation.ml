(*
 *
 * Event-state separation axiom for flipflop nets
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA Rennes, France
 *
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

(* None *)


(*-----------------------------------------------------------------
 * Datastructure for flipflop net synthesis.
 *-----------------------------------------------------------------*)
                        
(* It is a mapping Parikh --> (Region + { disabled, fail , enabled }) *)

type essp_set = Vector.t Parikh.t

type s =
    Region of Region.r (* Region [r] solves the ESSP problem *)
    | Disabled  (* ESSP to be solved *)
    | Fail      (* ESSP has no solution *)
    | Enabled   (* No ESSP to solve here *)
    | Cut       (* Approximation cut for optimization *)

type p =
    Virgin
    | Visited
    | Complete

type q =
    {
      parikh : Vector.t;
      mutable status : p;
      edge : s array
    }

type t =
    {
      dim : int;
      gra : q Parikh.t;
      adm : Region.r list array
    }


(*-----------------------------------------------------------------
 * Functions to checks ESSP solving
 *-----------------------------------------------------------------*)

(* [sat_essp r u e] checks whether region [r] solves ESSP ([u],[e]) *)

(* val sat_essp : Region.r -> Vector.t -> int -> bool *)

let sat_essp r u e =
  let m = Region.marking r u
  and a = Region.get_a r e
  and b = Region.get_b r e in
    (a && m) <> b (* a(e)*m + b(e) <> 0 mod 2 *)

(* [sat_essp_list l u e] checks whether a region [r] in [l] solves ESSP ([u],[e]) and returns [r]. Raises Not_found if none solves the ESSP *)

(* val sat_essp_list : Region.r list -> Vector.t -> int -> Region.r *)

let sat_essp_list l u e = List.find (fun r -> sat_essp r u e) l


(*-----------------------------------------------------------------
 * Constructor(s) and accessor(s)
 *-----------------------------------------------------------------*)
  
(* [make n] initializes the data structure for an alphabet of [n] events *)

(* val make : int -> t *)

let make d =
  if d >= 0
  then
    {
      dim = d;
      gra = Parikh.make d;
      adm = Array.make d []
    }
  else
    invalid_arg "Separation.make: invalid dimension"

(* [dimension s] returns the dimension of system [s] *)

(* val dimension : t -> int *)

let dimension { dim=d } = d


(* [get_nb_states s] returns the number of states on system [s] *)

(* val get_nb_states : t -> int *)

let get_nb_states { gra=g } = Parikh.fold (fun x y z -> z + 1) g 0
                                

(* retrieves the vertex corresponding to a given Parikh vector *)

(* val get_vertex : t -> Vector.t -> q *)

let get_vertex { dim=d; gra=g } u =
  if (Vector.length u) <> d
  then invalid_arg "Separation.get_vertex: invalid Parikh vector"
  else
    try
      Parikh.get g u
    with
	Not_found ->
	  let n = { parikh=Vector.clone u; status=Virgin; edge=Array.make d Disabled } in
	    begin
	      Parikh.set g u n;
	      n
	    end

(* retrieves the initial vertex *)

(* val get_initial_vertex : t -> q *)

let get_initial_vertex ({ dim=d } as s) = get_vertex s (Vector.make d false)

(* Adds an edge to a vertex *)

(* val add_edge : q -> int -> unit *)

let add_edge n e = n.edge.(e) <- Enabled

(* Tests whether a vertex is new, meaning that it has not been allocated yet *)

(* val is_new : t -> Vector.t -> bool *)

let is_new { dim=d; gra=g } u =
  if (Vector.length u) <> d
  then invalid_arg "Separation.is_new: invalid Parikh vector"
  else
    try
      ignore (Parikh.get g u);
      false
    with
      Not_found -> true
                 

(* retrieves the Parikh vector of a state *)

(* val parikh_of_state : q -> Vector.t *)

let parikh_of_state { parikh=u } = u
                                 

(* retrieves and sets the status of a vertex *)

(* val get_vertex_status : q -> p *)

let get_vertex_status { status=x } = x

(* val set_vertex_status : q -> p -> unit *)

let set_vertex_status q x = q.status <- x
                                 

(* Initialization of the graph structure from a Parikh transition graph *)

(* val init : Vector.t Parikh.t -> t *)

let init p =
  let d = Parikh.dimension p in
  let s = make d in
    begin
      Parikh.iter
	(fun u r ->
	  let n = get_vertex s u in
	    for e=0 to d-1 do
	      if Vector.get r e
	      then add_edge n e
	    done)
	p;
      s
    end


(*-----------------------------------------------------------------
 * Functions to computes the set of ESSPs solved
 *-----------------------------------------------------------------*)

(* val get_essp_set_solved_by : t -> Region.r -> essp_set *)
  
let get_essp_set_solved_by {dim=d; gra=g} r =
  let essp_set = Parikh.make d in
    begin
      (* Iterates on reachable Parikh vectors *)
      Parikh.iter
	(fun u { edge=t } ->
	  let z = Vector.make d false in
	  begin
            for e=0 to d-1 do
	      match t.(e) with
		Region _
	      | Disabled ->
		 (* the event is/should be disabled. Test whether the region solves this ESSP *)
		 if sat_essp r u e then
		   Vector.set z e true
	      | Fail
                | Cut 
		| Enabled -> ()
	    done;
	    Parikh.set essp_set u z
	  end)
	g;
      essp_set
    end

(* val get_essp_set : t -> essp_set *)
  
let get_essp_set {dim=d; gra=g} =
  let essp_set = Parikh.make d in
    begin
      (* Iterates on reachable Parikh vectors *)
      Parikh.iter
	(fun u { edge=t } ->
	  let z = Vector.make d false in
	  begin
            for e=0 to d-1 do
	      match t.(e) with
		Region _
	      | Disabled ->
		 (* the event is/should be disabled. *)
		   Vector.set z e true
	      | Fail
                | Cut 
		| Enabled -> ()
	    done;
	    Parikh.set essp_set u z
	  end)
	g;
      essp_set
    end


(*-----------------------------------------------------------------
 * Marking of ESSP
 *-----------------------------------------------------------------*)
  
(* [mark_solved q e r] marks ESSP ([q],[e]) solved by region [r] *)

(* val mark_solved : q -> int -> Region.r -> unit *)

let mark_solved q e r =
  match q.edge.(e) with
      Disabled -> q.edge.(e) <- Region r
    | Region _ -> invalid_arg "Separation.marked_solved: ESSP is already solved"
    | Fail -> invalid_arg "Separation.mark_solved: ESSP is marked failed"
    | Enabled -> invalid_arg "Separation.mark_solved: event is enabled"
    | Cut -> invalid_arg "Separation.mark_failed: ESSP is cut for optimization"

(* [mark_failed q e] marks ESSP ([q],[e]) failed *)

(* val mark_failed : q -> int -> unit *)

let mark_failed q e =
  match q.edge.(e) with
      Disabled -> q.edge.(e) <- Fail
    | Fail -> ()
    | Region _ -> invalid_arg "Separation.mark_failed: ESSP already has a solution"
    | Enabled -> invalid_arg "Separation.mark_failed: event is enabled"
    | Cut -> invalid_arg "Separation.mark_failed: ESSP is cut for optimization"

(* [mark_cut q e] marks ESSP ([q],[e]) cut for optimization *)

(* val mark_cut : q -> int -> unit *)

let mark_cut q e =
  match q.edge.(e) with
      Disabled -> q.edge.(e) <- Cut
    | Fail -> q.edge.(e) <- Cut
    | Region _ -> q.edge.(e) <- Cut
    | Enabled -> q.edge.(e) <- Cut
    | Cut -> ()


(*-----------------------------------------------------------------
 * Advanced functions
 *-----------------------------------------------------------------*)

(* add a new sink state and checks whether each region in the heap solves each ESSP in the state. Updates the region heap accordingly *)

(* val new_sink_state : t -> Vector.t -> unit *)

let new_sink_state ({ dim=d; gra=g; adm=h } as s) u =
  if (Vector.length u) <> d
  then invalid_arg "Separation.new_sink_state: invalid Parikh vector"
  else
    if not (is_new s u)
    then invalid_arg "Separation.new_sink_state: not a new state"
  else
    begin
      ignore (get_vertex s u); (* creates the new state *)
      (* Iterates on event [e] *)
      for e=0 to d-1 do
	(* Iterates on region [r] in the heap *)
	List.iter
	  (fun r ->
            (* let w = (get_essp_set_solved_by s r) in *)
	    let z = Vector.make d false in
	      begin
		  (* checks whether [r] solves essp ([u],[e]) *)
		if sat_essp r u e then Vector.set z e true;
		(* Parikh.set w u z *)
	      end)
	  h.(e)
      done
    end
      
(* adds a new region to the heap *)

(* val add_region : t -> int -> Region.r -> unit *)

let add_region s e r = s.adm.(e) <- r :: s.adm.(e)

(* finds a region in the heap solving a given ESSP. Raises [Not_found] otherwise *)

(* val find_heap_region : t -> Vector.t -> int -> Region.r *)

let find_heap_region {dim=d; adm=h} u e =
  if (Vector.length u) <> d
  then invalid_arg "Separation.find_heap_region: invalid Parikh vector"
  else if (e < 0) || (e >= d)
  then invalid_arg "Separation.find_heap_region: invalid event"
  else List.find
         (fun r -> sat_essp r u e)
           (* let w = (get_essp_set_solved_by s r) in
           Vector.get (Parikh.get w u) e) *)
         h.(e)


(*-----------------------------------------------------------------
 * Iterators
 *-----------------------------------------------------------------*)

(* iterator on unsolved ESSP problems in a given state *)

(* val iter_state_essp : (int -> unit) -> q -> unit *)

let iter_state_essp foo { edge=t } =
  let d = Array.length t in
  for e=0 to d-1 do
      match t.(e) with
	  Disabled -> foo e
	| _ -> ()
    done

(* [trans s q e] retrieves state of [s] reached by transition labelled [e] in state [q] *)
(* Raises [Not_found] if transition is/should be disabled or if the reached state has not been initialized  *)

(* val trans : t -> q -> int -> q *)

let trans { dim=d; gra=g } { parikh=u; edge=t } e =
  if (e < 0) || (e >= d)
  then invalid_arg "Separation.trans: invalid event"
  else
    match t.(e) with
	Enabled | Fail ->
	  let u' = Vector.clone u in
	    begin
	      Vector.iflip u' e;
	      Parikh.get g u'
	    end
      | _ -> raise Not_found

                          
(*
 *
 * Iterates on enabled transitions and failed ESSPs transitions.
 * Raises [Failure _] if an unsolved ESSP transition is found.
 * Ignores solved ESSPs.
 * New reachable states are initialized and ESSP sets are extended
 * for all regions in the heap.
 *
 * Configuration is used to evaluate the optimization limit, and stop
 * adding new place if the limit is reached (only if optimization requested)
 * 
 *)

(* val iter_enabled_or_failed : Configuration.t -> (int -> q -> unit) -> t -> q -> unit *)

let iter_enabled_or_failed conf foo ({ dim=d; gra=g } as s) ({ parikh=u; edge=t } as q) =
  for e=0 to d-1 do
    match t.(e) with
	Region _ -> ()
      | Disabled -> failwith "Separation.iter_enabled_or_failed: unsolved ESSP found"
      | Enabled ->
	  let q' = trans s q e in
	    foo e q'
      | Fail -> 
	  let u' = Vector.clone u in
	  Vector.iflip u' e;
          if is_new s u' then new_sink_state s u';
	  foo e (Parikh.get g u');
          (Configuration.update_approximation_limit_after_computation conf)
      | Cut -> () (* approximation cut stops ESSPs solving *)
  done
  
(*
 * [fold_graph phi psi s x0] resp. [iter_graph phi psi s] iterate on the nodes and edges of
 * the graph of reachable markings of the TF-net.
 *
 * [phi q x] resp. [phi q] is evaluated exactly once on each state [q] and
 * [psi q e q' x] resp. [psi q e q'] is evaluated exactly once for each
 * transition ([q],[e],[q']).
 *
 * Raises [Invalid_argument _] if an ESSP has not been solved yet in a reachable state.
 *
 *)

(* val fold_graph : (q -> 'a -> 'a) -> (q -> int -> q -> 'a -> 'a) -> t -> 'a -> 'a *)

(* val iter_graph : (q -> unit) -> (q -> int -> q -> unit) -> t -> unit *)

let fold_graph phi psi ({gra=g} as s) x0 =
    Parikh.fold
      (fun _ q x ->
	let y = ref (phi q x) in
	  begin
	    Array.iteri
	      (fun e m ->
		match m with
		  Region _ -> ()
		| Disabled -> invalid_arg "Separation.fold_graph: unsolved ESSP found"
		| Fail | Enabled -> y := psi q e (trans s q e) (!y)
                | Cut -> ())
	      q.edge;
	    !y
	  end)
      g
      x0

let iter_graph phi psi s =
  fold_graph (fun q _ -> phi q) (fun q e q' _ -> psi q e q') s ()


(*-----------------------------------------------------------------
 * Operations on ESSP sets
 *-----------------------------------------------------------------*)

(* Decides whether two ESSP sets are comparable *)

exception Exit

let is_included_essp_set x y =
  let d = Parikh.dimension x in
  let ones = Vector.make d true in
  try
    Parikh.iter
      (fun u r ->
	if not (Vector.is_zero r)
	then
	  try
	    (* r /\ ~r' = 0 ? *)
	    let r' = Parikh.get y u in
	    let w = Vector.vxor ones r' in
	      begin
		Vector.iand w r;
		if not (Vector.is_zero w)
		then raise Exit
	      end
	  with
	      Not_found -> raise Exit)
      x; 
    true
  with
      Exit -> false

(* inplace union of two ESSP sets *)

let inplace_union_essp_set x y =
  begin
    Parikh.iter
      (fun u r ->
	try
	  let r' = Parikh.get x u in
	    Vector.ior r' r
	with
	    Not_found ->
	      Parikh.set x u (Vector.clone r))
      y;
    x
  end
    
(* make an empty ESSP set of dimension [d] *)

let make_empty_essp_set d = Parikh.make d

                          
(*-----------------------------------------------------------------
 * Eliminates redundent regions
 *-----------------------------------------------------------------*)

(* eliminate ESSP solved by a given region, and indicate if a region
 * solved at least one ESSP *)
let elim_essp_solved_by d essp_set r =
  let has_essp_solved_by = ref true in
  let essp_set_target = Parikh.make d in
  begin
    (* Iterates on reachable Parikh vectors *)
    Parikh.iter
      (fun u t ->
        for e=0 to d-1 do
	  match Vector.get t e with
	    true ->
	    if (sat_essp r u e)
            then has_essp_solved_by := true
	    else Parikh.set essp_set_target u t
	  | false -> ()
	done)
      essp_set;
    (!has_essp_solved_by, essp_set_target)
  end

(* function to eliminates ESSP solved by a given region, 
 * and updates regions set without the given region if redundent *)
let f_elim d (essp_set, h') r =
  let (has_essp_solved_by, essp_set_target) =
    elim_essp_solved_by d essp_set r
  in
  if has_essp_solved_by
  then (essp_set_target, r::h')
  else (essp_set_target, h') (* region is redundent *)


(* val elim_redundent : Separation.t -> unit *)

let elim_redundent ({ dim=d; adm=h } as s) =
  let essp_set = get_essp_set s in
  for e=0 to d-1 do
    let (_, h') =
      List.fold_left
	(f_elim d)
	(essp_set, [])
	h.(e)
    in
    s.adm.(e) <- h'
  done


(*-----------------------------------------------------------------
 * Retrieves admissible regions solving ESSPs
 *-----------------------------------------------------------------*)

(* val get_admissible_regions_event : t -> int -> Region.r list *)

let get_admissible_regions_event { dim=d; adm=h } e =
  if (e < 0) || (e >= d)
  then invalid_arg "Separation.get_admissible_regions: invalid event"
  else
    List.map (fun r -> r) h.(e)

(* Retrieves all admissible regions *)

(* val get_admissible_regions : t -> Region.r list *)

let get_admissible_regions ({ dim=d } as s) =
  let l = ref [] in
    begin
      for e=0 to d-1 do
	l := (!l) @ (get_admissible_regions_event s e)
      done;
      !l
    end


(*-----------------------------------------------------------------
 * Prints graph + ESSPs + admissible regions
 *-----------------------------------------------------------------*)
  
(* val print : out_channel -> t -> unit *)

let print c { dim=d; gra=g; adm=h} =
  begin
    Printf.fprintf c "[\n";
    Printf.fprintf c "  dim=%d\n" d;
    (* Dump graph *)
    Parikh.iter
      (fun _ {parikh=u; status=z; edge=t} ->
	begin
	  Printf.fprintf c "  state %s (%s):\n    [\n"
	    (Vector.to_string u)
	    (match z with Virgin -> "virgin" | Visited -> "visited" | Complete -> "complete");
	  for e=0 to d-1 do
	    Printf.fprintf c "      event %d: %s;\n"
	      e
	      (match t.(e) with
                 Region _ -> "solved"
               | Disabled -> "not solved"
               | Fail -> "fail"
               | Enabled -> "enabled"
               | Cut -> "approximation cut for optimization"
              )
	  done;
	  Printf.fprintf c "    ];\n"
	end)
      g;
    (* Dump region heap *)
    for e=0 to d-1 do
      List.iter
	(fun r -> Region.print c r)
	h.(e)
    done;
    (* Done *)
    Printf.fprintf c "]\n"
  end

