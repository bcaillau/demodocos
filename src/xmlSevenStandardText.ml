(*
 *
 * Standard text of #SEVEN code in xml format (from #SEVEN xsd)
 *
 * (c) -- <--@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* This module generate the XML file of #SEVEN code.
 * The main function works from an abstract SEVEN structure to obtain 
 * a dot structure (using module Pp). 
 * Then, finally, the XML file is generated.
 *)

(*-----------------------------------------------------------------
 * Module openning, parameter & utilities
 *-----------------------------------------------------------------*)

open Pp

(* Parameters for standard string *)
let param_decoration_type =  "FlipFlop"
let param_decoration_override = "false"
let param_position_interval_x = 200
let param_position_init_y = -650
let param_position_interval_y = 150
let param_position_sensor_gap = 50
   
(* function to add the double quotes around a string *)
let stringify s = "\""^(String.escaped s)^"\""
   
(* functions to evaluate the values for the position of an element *)
                
let x_value_for_place_transition i =
  let x =
    match i mod 2 with
    | 0 -> - (i/2) * param_position_interval_x
    | _ -> (i/2) * param_position_interval_x
  in
  stringify (string_of_int x)
                
let y_value_for_place_transition i =
  let y = param_position_init_y + (i * param_position_interval_y) in
  let y = (if i == 0 then 0 else y) in
  stringify (string_of_int y)
                
let x_value_for_sensor i =
  let x =
    match i mod 2 with
    | 0 -> (- (i/2) * param_position_interval_x) + param_position_sensor_gap
    | _ -> ((i/2) * param_position_interval_x) + param_position_sensor_gap
  in
  stringify (string_of_int x)
                
let y_value_for_sensor i =
  let y = param_position_init_y + (i * param_position_interval_y) - param_position_sensor_gap in
  let y = (if i == 0 then (0 - param_position_sensor_gap) else y) in
  stringify (string_of_int y)
                
   
(*-----------------------------------------------------------------
 * Standard string of #SEVEN code in xml format (from #SEVEN xsd)
 *-----------------------------------------------------------------*)

(* XML prologue *)
                
let xml_prologue_string =
  "<?xml "
  ^ " version=" ^ (stringify "1.0")
  ^ " encoding=" ^ (stringify "UTF-8")
  ^ " standalone=" ^ (stringify "yes")
  ^ "?>"


(* Scenario *)
  
let scenario_start_string =
  "<scenario xmlns=" ^ (stringify "http://www.insa-rennes.fr/#SEVEN") ^ ">"

let scenario_end_string = "</scenario>"


(* Decoration *)

let decoration_start_string =
  "<decoration "
  ^ "type=" ^ (stringify param_decoration_type)
  ^ " override=" ^ (stringify param_decoration_override)
  ^ ">"

let decoration_data_string data_name data_value =
  "<data "
  ^ "name=" ^ (stringify data_name)
  ^ " value=" ^ (stringify (data_value))
  ^ "/>"

let decoration_end_string = "</decoration>"


(* Event with a sensor *)
                        
let event_start_string event_id =
  "<event "
  ^ "id=" ^ (stringify event_id)
  ^ "> "

let assignment_eval_string =
  "<assignmentEval "
  ^ "classname=" ^ (stringify "AssignmentOverlap, SEVEN.Extra")
  ^ "/>"

let sensor_check_start_string = 
  "<sensorCheck "
  ^ "classname=" ^ (stringify "RelationSensor, SEVEN.FIVE")
  ^ ">"

let sensor_check_param_relation_string relation_label =
  "<param "
  ^ "name=" ^ (stringify "relation")
  ^ " value=" ^ (stringify relation_label)
  ^ "/>"

let sensor_check_param_object_string object_label =
  "<param "
  ^ "name=" ^ (stringify "object")
  ^ " value=" ^ (stringify object_label)
  ^ "/>"

let sensor_check_end_string = "</sensorCheck>"
                        
let sensor_position x_level y_level =
  "<sensorPosition "
  ^ "x=" ^ (x_value_for_sensor x_level)
  ^ " y=" ^ (y_value_for_sensor y_level)
  ^ "/>"

let sensor_string sensor_label =
  "<sensorLabel>" ^ sensor_label ^ "</sensorLabel>"

let event_end_string = "</event>"


(* Sequence *)
                      
let sequence_start_string =
  "<sequence "
  ^ "xmlns:xsi=" ^ (stringify "http://www.w3.org/2001/XMLSchema-instance")
  ^ " xsi:type=" ^ (stringify "SafePetriNet")
  ^ " id=" ^ (stringify "-1")
  ^ ">"

let sequence_end_string = "</sequence>"


(* Place *)
                        
let place_start_string place_id place_label =
  "<place "
  ^ "xsi:type=" ^ (stringify "Place")
  ^ " id=" ^ (stringify place_id)
  ^ " label=" ^ (stringify place_label)
  ^ ">"
                        
let place_position x_level y_level =
  "<position "
  ^ "x=" ^ (x_value_for_place_transition x_level)
  ^ " y=" ^ (y_value_for_place_transition y_level)
  ^ "/>"

let place_end_string = "</place>"


(* Transition *)
                     
let transition_start_string t_id t_label =
  "<transition "
  ^ "id=" ^ (stringify t_id)
  ^ " label=" ^ (stringify t_label)
  ^ ">"

let transition_event_string e_idref =
  "<event "
  ^ "idref=" ^ (stringify e_idref)
  ^ "/>"

let transition_up_place_string p_idref =
  "<upstreamPlace "
  ^ "idref=" ^ (stringify p_idref)
  ^ "/>"

let transition_down_place_string p_idref =
  "<downstreamPlace "
  ^ "idref=" ^ (stringify p_idref)
  ^ "/>"    
                        
let transition_position x_level y_level =
  "<position "
  ^ "x=" ^ (x_value_for_place_transition x_level)
  ^ " y=" ^ (y_value_for_place_transition y_level)
  ^ "/>"    

let transition_end_string = "</transition>"


(* Initial and final place *)
                          
let initial_place_string p_idref = 
  "<initialPlace "
  ^ "idref=" ^ (stringify p_idref)
  ^ "/>"

let final_place_string p_idref = 
  "<finalPlace "
  ^ "idref=" ^ (stringify p_idref)
  ^ "/>"
                     
  
(*-----------------------------------------------------------------
 * Functions to generates doc containing standard text of #SEVEN
 *-----------------------------------------------------------------*)

(* val xml_prologue_text : Pp.doc *)

let xml_prologue_text = text xml_prologue_string

                      
(* val scenario_text : Pp.doc -> Pp.doc *)

let scenario_text s_content =
  break ^^ (text scenario_start_string)
  ^^ (nest 4 s_content)
  ^^ break ^^ (text scenario_end_string)
  

(* val event_text : string -> Pp.doc -> Pp.doc *)

let event_text e_id e_content =
  break ^^ (text (event_start_string e_id))
  ^^ (nest 4 e_content)
  ^^ break ^^ (text event_end_string)

(* val event_assignment_eval_text : Pp.doc *)

let event_assignment_eval_text =
  break ^^ (text assignment_eval_string)

(* val event_decoration_text : string -> string -> Pp.doc *)

let event_decoration_text data_name data_value =
  break ^^ (text decoration_start_string)
  ^^ (nest 4 (break ^^ (text (decoration_data_string data_name data_value))))
  ^^ break ^^ (text decoration_end_string)

(* val sensor_position_text : int -> int -> Pp.doc *)

let sensor_position_text x_level y_level =
  break ^^ (text (sensor_position x_level y_level))

(* val sensor_check_text : string option -> (string * string) list option -> Pp.doc *)

let sensor_check_relation_text relation =
  match relation with
  | "" -> (nest 4 (break ^^ text (sensor_check_param_relation_string "NO_RELATION")))
  | r_string -> (nest 4 (break ^^ text (sensor_check_param_relation_string r_string)))

let sensor_check_objects_text objects =
  match objects with
  | [] -> Pp.empty
  | obj_list ->
     let f_text = fun obj_text (_, obj_label) ->
       obj_text
       ^^ (nest 4 (break ^^ text (sensor_check_param_object_string obj_label)))
     in
     List.fold_left f_text Pp.empty obj_list                                                    
  
let sensor_check_text rel objects =
  break ^^ (text sensor_check_start_string)
  ^^ (sensor_check_relation_text rel)
  ^^ (sensor_check_objects_text objects)
  ^^ break ^^ (text sensor_check_end_string)

(* val sensor_text : string -> Pp.doc *)

let sensor_text s_label =
  break ^^ (text (sensor_string s_label))

  
(* val sequence_text : Pp.doc -> Pp.doc *)

let sequence_text s_content =
  break ^^ (text sequence_start_string)
  ^^ (nest 4 s_content)
  ^^ break ^^ (text sequence_end_string)
  
                       
(* val place_text : string -> string -> Pp.doc -> Pp.doc *)
                       
let place_text p_id p_label p_content =
  break ^^ (text (place_start_string p_id p_label))
  ^^ (nest 4 p_content)
  ^^ break ^^ (text place_end_string)

(* val place_decoration_text : string -> string -> Pp.doc *)

let place_decoration_text data_name data_value =
  break ^^ (text decoration_start_string)
  ^^ (nest 4 (break ^^ (text (decoration_data_string data_name data_value))))
  ^^ break ^^ (text decoration_end_string)
  
(* val place_position_text : int -> int -> Pp.doc *)

let place_position_text x_level y_level =
  break ^^ (text (place_position x_level y_level))  
  
                       
(* val transition_text : string -> string -> Pp.doc -> Pp.doc *)
                       
let transition_text t_id t_label s_content =
  break ^^ (text (transition_start_string t_id t_label))
  ^^ (nest 4 s_content)
  ^^ break ^^ (text transition_end_string)

(* val transition_event_text : string -> Pp.doc *)

let transition_event_text e_idref =
  break ^^ (text (transition_event_string e_idref))

(* val transition_up_place_text : string -> Pp.doc *)

let transition_up_place_text p_idref =
  break ^^ (text (transition_up_place_string p_idref))

(* val transition_down_place_text : string -> Pp.doc *)

let transition_down_place_text p_idref =
  break ^^ (text (transition_down_place_string p_idref))
  
(* val transition_position_text : int -> int -> Pp.doc *)

let transition_position_text x_level y_level =
  break ^^ (text (transition_position x_level y_level))  
  

(* val initial_place_text : string -> Pp.doc *)

let initial_place_text p_idref =
  break ^^ (text (initial_place_string p_idref))

(* val final_place_text : string -> Pp.doc *)

let final_place_text p_idref =
  break ^^ (text (final_place_string p_idref))
