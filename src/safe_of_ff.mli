(*
 *
 * Safe_of_ff
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2016
 *
 *
 * Transforms a TF net into a safe net. The safe net is represented as a particular
 * class of TF, with complementary places and no complementation flow arc.
 *
 *)

(*
 *
 * [compl_count_event n i] computes the number of complementation flow arcs for event [i] in TF net [n].
 * [i] must be greater or equal to zero and strictly less than the dimension of [n], otherwise an
 * [invalid_argument _] exception is raised.
 *
 *)

val compl_count_event : Region.n -> int -> int

(*
 *
 * [compl_count_net n] computes the number of complementation flow arcs for each event in TF net [n].
 * Returns an array [x] sur that [x.[e]] is the number of complementation flow arcs for event [e].
 * Raises [Invalid_argument _] if [n] has no places.
 *
 *)

val compl_count_net : Region.n -> int array

(*
 *
 * Datastructure for mapping events to safe net transitions and vice-versa
 *
 *)

type 'a t =
    {
      events : int;
      transitions : int;
      places : int; (* number of places, not counting the complementary places *)
      labeling : int array; (* maps transitions to events *)
      ontology : 'a array; (* maps events to instances of the ontology *)
      first : int array; (* first transition labeled by an event *)
      count : int array; (* number of complementations for an event *)
      compl : int list array; (* list of regions complemented by each event *)
      rank : int array array; (* rank matrix of complementation events *)
      (* [t.rank.(i).(e)] is the rank of event [e] wrt region [i] *)
      network : Region.r array (* array of regions *)
    }

(*
 *
 * Initializes the mapping datastructure.
 *
 *)

val init : 'a array -> Region.n -> 'a t

(*
 *
 * Safe (or its complementary) place of TF region
 *
 *)

val safe_place_of_tf : 'a t -> int (* region index *) -> bool (* false = positive, true = complementary *) -> Region.r

(* Safe net of a TF net *)

val safe_net_of_tf : 'a array -> Region.n -> ('a array) * Region.n

