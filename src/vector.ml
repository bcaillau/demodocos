(*
 *
 * Vector: boolean vectors
 *
 * (C) Benoit Caillaud <benoit.caillaud@inria.fr>, INRIA Rennes, France, 2012
 *
 *)

(* Datatypes *)

type t

(* Factory *)

(* val make : int -> bool -> t *)

external make : int -> bool -> t = "vector_ml_make"

(* val set : t -> int -> bool -> unit *)

external set : t -> int -> bool -> unit = "vector_ml_set"

(* val get : t -> int -> bool *)

external get : t -> int -> bool = "vector_ml_get"

(* val init : (int -> bool) -> int -> t *)

let init f n =
  let v = make n false in
    begin
      for i=0 to n-1 do
	set v i (f i)
      done;
      v
    end

(* val clone : t -> t *)

external clone : t -> t = "vector_ml_clone"

(* val extend : t -> int -> bool -> t *)

external extend : t -> int -> bool -> t = "vector_ml_extend"

(* val restrict : t -> int -> t *)

external restrict : t -> int -> t = "vector_ml_restrict"

(* Length of other functions *)

(* val length : t -> int *)

external length : t -> int = "vector_ml_length"

(* val is_zero : t -> bool *)

external is_zero : t -> bool = "vector_ml_is_zero"

(* val leading_zeroes : t -> int *)

external leading_zeroes : t -> int = "vector_ml_leading_zeroes"

(* val arity : t -> int *)

external arity : t -> int = "vector_ml_arity"

(* val compare : t -> t -> int *)

external compare : t -> t -> int = "vector_ml_compare"

(* Mappings *)

(* Operators *)

(* Binary operators raise [Invalid_argument _] whenever the parameters have different lengths *)

(* val lxor : t -> t -> t *)

external vxor : t -> t -> t = "vector_ml_xor"

(* val land : t -> t -> t *)

external vand : t -> t -> t = "vector_ml_and"

(* val lneg : t -> t -> t *)

(* val prod : t -> t -> bool *)

external prod : t -> t -> bool = "vector_ml_prod"

(* val partial_prod : t -> t -> int -> int -> bool *)

external partial_prod : t -> t -> int -> int -> bool = "vector_ml_partial_prod"

(* In place binary operators *)

(* val ixor : t -> t -> unit *) (* [ixor a b] performs [a] := [a] xor [b] *)

external ixor : t -> t -> unit = "vector_ml_ixor"

(* val iand : t -> t -> unit *) (* [iand a b] performs [a] := [a] and [b] *)

external iand : t -> t -> unit = "vector_ml_iand"

(* val ior : t -> t -> unit *) (* [ior a b] performs [a] := [a] or [b] *)

external ior : t -> t -> unit = "vector_ml_ior"

(* In place unary operators *)
(* [iflip a i] flips bit of index [i] in vector [a] *) 

(* val iflip : t -> int -> unit *)

let iflip a i = set a i (not (get a i))

(* to_string and print *)

(* val to_string : t -> string *)

let to_string c =
  let n = length c 
  and l = ref [] in
  let rec expr =
    function
	[] -> "0"
      | [i] -> "x"^(string_of_int i)
      | i::l -> "x"^(string_of_int i)^"+"^(expr l)
  in
    begin
      for i=n-1 downto 0 do
	if get c i
	then l := i :: (!l)
      done;
      expr (!l)
    end

(* val print : t -> unit *)

let print c =
  let s = to_string c in
    print_endline s

(* Vector blit *)

(* val blit : src:t -> src_idx:int -> dst:t -> dst_idx:int -> len:int -> unit *)

external blit : src:t -> src_idx:int -> dst:t -> dst_idx:int -> len:int -> unit = "vector_ml_blit"

(* Byte read and write *)

external get_byte : t -> idx:int -> len:int -> int = "vector_ml_get_byte"
external set_byte : t -> idx:int -> len:int -> byte:int -> unit = "vector_ml_set_byte"
