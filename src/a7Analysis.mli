(*
 *
 * Module to analyse an abstract structure of #7 code
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* 
 * This module provides functions to analyse an abstract structure
 * of #7 code (a7), and to obtain information about the code. 
 *)

(*-----------------------------------------------------------------
 * Basic Analysis
 *-----------------------------------------------------------------*)

val get_places_number : A7.t -> int
val get_events_number : A7.t -> int
val get_transitions_number : A7.t -> int

  

(*-----------------------------------------------------------------
 * Analysis of simple paths number
 *-----------------------------------------------------------------*)

(* function to obtain the number of simple paths on an abstract seven,
 * starting from a given place *)

val get_number_of_simple_paths_from_place :
  A7.t -> int list -> int list -> A7Element.t -> int

(* function to obtain the number of simple path on an abstract seven,
 * starting from initial places *)
  
val get_number_of_simple_paths_from_initial_places : A7.t -> int
  


(*-----------------------------------------------------------------
 * Analysis of paths
 *-----------------------------------------------------------------*)

(* function to obtain event paths starting from initial places,
 * each event representing by his urn *)
  
val get_e_paths_from_initial_places : A7.t -> (int list) list  
  
      
(* function to obtain event paths starting from initial places,
 * each event representing by his label *)

val get_label_e_paths_from_initial_places : A7.t -> (string list) list
  
  
       
(*-----------------------------------------------------------------
 * Analysis of inclused traces
 *-----------------------------------------------------------------*)       

(* function to compute the number of inclusion starting 
 * from a list of traces *)
  
val evaluate_inclusion_number : A7.t -> int list list -> int
  
      

