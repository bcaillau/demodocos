(*
 *
 * Xes_utilities: utility functions related to the XES file format
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2016
 *
 *)

(* val output_ontology : out_channel -> Xes.event array -> unit *)

let output_ontology c a =
  let n = Array.length a in
  begin
    Printf.fprintf c "[\n";
    for i=0 to n-1 do
      Printf.fprintf c "  %d : %s\n" i (Xes.string_of_event a.(i))
    done;
    Printf.fprintf c "]\n"
  end

(* val print_ontology : Xes.event array -> unit *)

let print_ontology a = output_ontology stdout a

(* val print_ontology_to_file : event array -> string -> unit *)

let print_ontology_to_file ontology file_name =
  let file = open_out file_name in
    try
      output_ontology file ontology;
      close_out file
    with
    | y ->
       begin
	 close_out file;
	 raise y
       end
  
