(*
 *
 * Processes
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2012
 *
 *)

type 'a t

(* creates an empty process *)

val make : int -> 'a -> 'a t

(* Deep copy of a process *)

val clone : ('a -> 'b) -> 'a t -> 'b t

(* [dimension n] returns the dimension of process [n] *)

val dimension : 'a t -> int

(* retrieves the label of a process *)

val get : 'a t -> 'a

(* sets the label of a process *)

val set : 'a t -> 'a -> unit

(* adds an edge to a new process if not already there *)

val add : (unit -> 'a) -> 'a t -> int -> 'a t

(* derivates a process, raises [Not_found] if undefined *)

val der : 'a t -> int -> 'a t

(* tests if a process is derivable by a given event *)

val is_der : 'a t -> int -> bool

(* tests if a process is a root *)

val is_root : 'a t -> bool

(* returns the parent process. Raises Not_found if the process is a root *)

val parent : 'a t -> 'a t

(* returns the label of the ingoing edge, raises Not_found if node is root *)

val edge : 'a t -> int

(* returns the root of a process *)

val root : 'a t -> 'a t

(* in place or/and of a vector with the readyset of a process *)

val ior_ready : Vector.t -> 'a t -> unit

val iand_ready : Vector.t -> 'a t -> unit

