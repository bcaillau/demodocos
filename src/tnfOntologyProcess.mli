(*
 *
 * Ontology Management Processes
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *)

(*-----------------------------------------------------------------
 * Ontology structure computing
 *-----------------------------------------------------------------*)

(* function to update the ontology structure with ontology basis (mapping
 * between xes value and vr value) *)

val update_with_ontology_basis :
  Configuration.t -> DataOntology.t -> unit


(* function to compute the ontology structure from log and classifier,
 * and update this struture with ontology basis *)

val compute_ontology_from_log :
  Configuration.t -> Xes.log -> string -> DataOntology.t

(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)  

(* function to value the ontology basis from the ontology base file 
 * (the ontology base file is defined on configuration) *)

val value_ontology_basis : Configuration.t -> unit
