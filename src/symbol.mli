(*
 *
 * Symbol table and mapping to integers
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA Rennes, France
 *
 *)

module type SYMBOL_TYPE =
  sig
    type t
    val equal : t -> t -> bool (* test equality *)
    val hash : t -> int (* hashing function *)
    val compare : t -> t -> int (* comparison function *)
    val to_string : t -> string (* generates a string *)
    val print : out_channel -> t -> unit (* prints to an channel *)
    val to_xml : t -> Xml.xml (* generates a XML document *)
    val of_xml : Xml.xml -> t (* parses a XML document *)
  end

module type TABLE_TYPE =
  sig
    type s (* Symbols *)
    type t (* Tables *)
    val make : unit -> t (* Makes an empty open symbol table *)
    val add : t -> s -> int (* add a symbol and assigns an integer unique id *)
    val retrieve : t -> s -> int (* retrieve the unique id of a symbol. Raises [Not_found] if unknown *)
    val resolve : t -> int -> s (* retrieves a symbol from its unique id. Raises [Invalid_argument _] if out of bounds *)
    val length : t -> int (* Length of the table *)
    val freeze : t -> unit (* Optimizes the table so that resolution is done in constant time *)
    val iter : (int -> s -> unit) -> t -> unit (* imperative iterator *)
    val fold : (int -> s -> 'a -> 'a) -> t -> 'a -> 'a (* functional iterator *)
    val to_xml : t -> Xml.xml (* turns the table into an XML document *)
    val of_xml : Xml.xml -> t (* parses an XML document and generates an open symbol table *)
  end

module Table : functor (S : SYMBOL_TYPE) -> TABLE_TYPE with type s = S.t
