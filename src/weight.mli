(*
 *
 * Weighted processes
 *
 * (c) Benoit Caillaud, INRIA, Rennes, France, 2013, <benoit.caillaud@inria.fr>
 *
 *)

(* process labels = weights *)

type w =
{
  mutable weight : int
}

(* Weighted processes *)

type t = w Process.t

(* creates an empty process of weight 0 *)

val make : int -> t

(* retrieves the label of a process *)

val weight : t -> int

(* adds an edge to a new process if not already there. Weight of a new node is zero *)

val add : t -> int -> t

(* set the weight of a node and propagates weight to parent nodes *)

val set : t -> int -> unit


