(*
 *
 * Event-state separation axiom for flipflop nets and datastructure for flipflop net synthesis
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA Rennes, France
 *
 *)

(*-----------------------------------------------------------------
 * Datastructure for flipflop net synthesis.
 *-----------------------------------------------------------------*)

(* vertex : readyset + wittness region for each ESSP, if exists, in this state *)

type q

(* graph + ESSPs + regions *)

type t

(* Vertex status *)

type p =
    Virgin
    | Visited
    | Complete


(*-----------------------------------------------------------------
 * Functions to checks ESSP solving
 *-----------------------------------------------------------------*)

(* [sat_essp r u e] checks whether region [r] solves ESSP ([u],[e]) *)

val sat_essp : Region.r -> Vector.t -> int -> bool

(* [sat_essp_list l u e] checks whether a region [r] in [l] solves ESSP ([u],[e]) and returns [r]. Raises Not_found if none solves the ESSP *)

val sat_essp_list : Region.r list -> Vector.t -> int -> Region.r


(*-----------------------------------------------------------------
 * Constructor(s) and accessor(s)
 *-----------------------------------------------------------------*)
  
(* [make n] initializes the data structure for an alphabet of [n] events *)

val make : int -> t

(* [dimension s] returns the dimension of system [s] *)

val dimension : t -> int

(* [get_nb_states s] returns the number of states on system [s] *)

val get_nb_states : t -> int

(* retrieves the vertex correspondign to a given Parikh vector *)

val get_vertex : t -> Vector.t -> q

(* retrieves the initial vertex *)

val get_initial_vertex : t -> q

(* Adds an edge to a vertex *)

val add_edge : q -> int -> unit

(* Tests whether a vertex is new, meaning that it has not been allocated yet *)

val is_new : t -> Vector.t -> bool

(* Initialization of the graph structure from a Parikh transition graph *)

val init : Quotient.t -> t


(*-----------------------------------------------------------------
 * Marking of ESSP
 *-----------------------------------------------------------------*)

(* [mark_solved q e r] marks ESSP ([q],[e]) solved by region [r] *)

val mark_solved : q -> int -> Region.r -> unit

(* [mark_failed q e] marks ESSP ([q],[e]) failed *)

val mark_failed : q -> int -> unit

(* [mark_cut q e] marks ESSP ([q],[e]) cut for optimization *)

val mark_cut : q -> int -> unit


(*-----------------------------------------------------------------
 * Advanced functions
 *-----------------------------------------------------------------*)

(* add a new sink state and checks whether each region in the heap solves each ESSP in the state. Updates the region heap accordingly *)

val new_sink_state : t -> Vector.t -> unit

(* adds a new region to the heap *)

val add_region : t -> int -> Region.r -> unit

(* finds a region in the heap solving a given ESSP. Raises [Not_found] otherwise *)

val find_heap_region : t -> Vector.t -> int -> Region.r


(*-----------------------------------------------------------------
 * Iterators
 *-----------------------------------------------------------------*)

(* iterator on unsolved ESSP problems in a given state *)

val iter_state_essp : (int -> unit) -> q -> unit
  
(*
 *
 * Iterates on enabled transitions and failed ESSPs transitions.
 * Raises [Failure _] if an unsolved ESSP transition is found.
 * Ignores solved ESSPs.
 * New reachable states are initialized and ESSP sets are extended
 * for all regions in the heap.
 *
 *)

val iter_enabled_or_failed : Configuration.t -> (int -> q -> unit) -> t -> q -> unit

(*
 * [fold_graph phi psi s x0] resp. [iter_graph phi psi s] iterate on the nodes and edges of
 * the graph of reachable markings of the TF-net.
 *
 * [phi q x] resp. [phi q] is evaluated exactly once on each state [q] and
 * [psi q e q' x] resp. [psi q e q'] is evaluated exactly once for each
 * transition ([q],[e],[q']).
 *
 * Raises [Invalid_argument _] if an ESSP has not been solved yet in a reachable state.
 *
 *)

val fold_graph : (q -> 'a -> 'a) -> (q -> int -> q -> 'a -> 'a) -> t -> 'a -> 'a

val iter_graph : (q -> unit) -> (q -> int -> q -> unit) -> t -> unit


(*-----------------------------------------------------------------
 * Operations on ESSP sets
 *-----------------------------------------------------------------*)

(* retrieves the Parikh vector of a state *)

val parikh_of_state : q -> Vector.t

(* [trans s q e] retrieves state of [s] reached by transition labelled [e] in state [q] *)
(* Raises [Not_found] if transition is/should be disabled or if the reached state has not been initialized  *)

val trans : t -> q -> int -> q

(* retrieves and sets the status of a vertex *)

val get_vertex_status : q -> p

val set_vertex_status : q -> p -> unit
  

(*-----------------------------------------------------------------
 * Eliminates redundent regions
 *-----------------------------------------------------------------*)

val elim_redundent : t -> unit


(*-----------------------------------------------------------------
 * Retrieves admissible regions solving ESSPs
 *-----------------------------------------------------------------*)

(* Retrieves admissible regions solving ESSPs for event [e] *)

val get_admissible_regions_event : t -> int -> Region.r list

(* Retrieves all admissible regions *)

val get_admissible_regions : t -> Region.r list
  

(*-----------------------------------------------------------------
 * Prints graph + ESSPs + admissible regions
 *-----------------------------------------------------------------*)

val print : out_channel -> t -> unit
