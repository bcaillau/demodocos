(*
 *
 * Process for testing
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *)

(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)
                     

(*-----------------------------------------------------------------
 * Main function
 *-----------------------------------------------------------------*)

val test_parikh : int -> unit 

val test_quotient : int -> unit 
