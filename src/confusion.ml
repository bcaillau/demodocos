(*
 *
 * Confusion: iterators on the state confusions leading to an event-state separation problem
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA Rennes, France
 *
 *)

(* Utilities for printers *)

let string_of_vector v =
  let d = Vector.length v
  and w = ref false
  and s = ref "" in
    begin
      for e=0 to d-1 do
	if Vector.get v e
	then
	  if !w
	  then s := (!s)^"+x"^(string_of_int e)
	  else
	    begin
	      s := "x"^(string_of_int e);
	      w := true
	    end
      done;
      if !w
      then !s
      else "0"
    end

let rec string_of_path n =
  if Process.is_root n
  then "epsilon"
  else
    let e = Process.edge n
    and s = string_of_path' (Process.parent n) in
      s^"x"^(string_of_int e)

and string_of_path' n =
  if Process.is_root n
  then ""
  else
    let e = Process.edge n
    and s = string_of_path' (Process.parent n) in
      s^"x"^(string_of_int e)^"."
    
(* [print_confusion_parikh f s v] prints to [f] confusions in process [s] for parikh vector [v], if any *)

(* val print_confusion_parikh : out_channel -> Parikhproc.s -> Vector.t -> unit *)

let print_confusion_parikh f s v =
  let z = Parikhproc.conf s v
  and d = Vector.length v in
    if not (Vector.is_zero z)
    then
      begin
	Printf.fprintf f "Confusion(s) at Parikh vector %s:\n" (string_of_vector v);
	for e=0 to d-1 do
	  if Vector.get z e
	  then
	    begin
	      Printf.fprintf f "  Event x%d:\n" e;
	      Printf.fprintf f "    Enabled after:\n";
	      Parikhproc.class_iter
		(fun n ->
		if Process.is_der n e
		then Printf.fprintf f "      %s\n" (string_of_path n))
		s v;
	      Printf.fprintf f "    Disabled after:\n";
	      Parikhproc.class_iter
		(fun n ->
		if not (Process.is_der n e)
		then Printf.fprintf f "      %s\n" (string_of_path n))
		s v
	    end
	done;
      end

(* [print_confusion_system f s] prints to [f] confusions in process [s], if any *)

(* val print_confusion_system : out_channel -> Parikhproc.s -> unit *)

let print_confusion_system f s =
  Parikhproc.iter
    (fun v -> print_confusion_parikh f s v)
    s
