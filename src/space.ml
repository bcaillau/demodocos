(*
 *
 * Region spaces
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA, Rennes, France
 *
 *)

(* Generates the set of equations ensuring reachability of the graph for a given event *)

(* val reachability : Vector.t Parikh.t -> Region.t *)

let reachability s e =
  let n = Parikh.dimension s in
    if (e >= 0) && (e < n)
    then
      let r = Region.make n e
      in
	begin
	  Parikh.iter
	    (fun u (* Parikh vector *) v (* readyset *) ->
	      if Vector.get v e
	      then Region.enable r u e)
	    s;
	  Region.cook r;
	  r
	end
    else
      invalid_arg "Separation.reachability: invalid event"

(* Type of region space pairs *)

type pair =
    {
      test : Region.t;
      flip : Region.t
    }

(* Generates the pair of region spaces of the graph for a given event *)

(* val pair :  Vector.t Parikh.t -> int -> pair *)

let pair s e =
  let r = reachability s e in
  let r' = Region.clone r in
    begin
      Region.force_test r;
      Region.force_flip r';
      { test=r; flip=r' }
    end

(* Generates the array of region space pairs of the graph for all events *)

(* val pair_array : Vector.t Parikh.t -> pair array *)

let pair_array s =
  let n = Parikh.dimension s in
    Array.init n (pair s)
