(*
 *
 * Base functions for tnf command processes
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, Inria, Rennes, France, 2017
 *
 *)

(* 
 * This module provides the base functions for executing and managing 
 * the main command processes of the tool (named tnf).
 *)

(*-----------------------------------------------------------------
 * Module openning
 *-----------------------------------------------------------------*)

(* None *)



(*-----------------------------------------------------------------
 * Utilities
 *-----------------------------------------------------------------*)

(* functions used for operations about log and trace *)

let rec css_of_list =
  function
      [] -> ""
    | [s] -> s
    | s::l -> s ^ ", " ^ (css_of_list l)

let rec trace_dim =
  function
      [] -> 0
    | i::l -> max (i+1) (trace_dim l)

let rec log_dim =
  function
      [] -> 0
    | t::l -> max (trace_dim t) (log_dim l)
            

(* function to remove the extension of a given file name *)

let remove_extension_of_file_name s =
  try
    let extension_index = String.rindex s '.' in
    String.sub s 0 extension_index
  with _ -> s
          

(* function to substitute the extension of a given file name *)

let substitute_extension_of_file_name s e = (remove_extension_of_file_name s) ^ e

            
(* functions to obtain a given option type from a string *)

let string_option_of_string s =
  try
    match s with "" -> None | x -> Some x
  with _ -> None

let int_option_of_string s =
  try
    match s with "" -> None | x -> Some (int_of_string x)
  with _ -> None

                                    
(* function to obtain a string list from a string, wherein each substring
 *  are delimited by a separator character (sep) *)

let rec string_list_of_string sep s_source result = 
  let filter s = not (s = "") in 
  try
    match (s_source, (String.contains s_source sep)) with
    | (_, true) ->
       let start_index = (String.rindex s_source sep) + 1 in
       let sub_length = (String.length s_source) - start_index in
       let rest_length = (start_index - 1) in
       let (sub_source, rest_source) =
         (String.sub s_source (start_index) sub_length,
          String.sub s_source 0 rest_length)
       in
       let result = sub_source::result in
       string_list_of_string sep rest_source result
    | _ -> List.filter filter (s_source::result)
  with _ -> List.filter filter result


(* function to add the simple or double quotes around a string *)
   
let stringify nq s =
  let quote = match nq with 1 -> "\'" | _ -> "\"" in
  quote ^ (String.escaped s) ^ quote

  

(*-----------------------------------------------------------------
 * Execution and error manager
 *-----------------------------------------------------------------*)

(* function to report an error and exit *)
  
let err_report_and_exit err_msg arg_msg =
  let default_error_code = 10 in
  begin
    Reporting.error_report err_msg arg_msg;
    exit default_error_code
  end

  
(* function to execute a step and report informations about the process *)

let execute_step s_name s_function s_param =
  try
    begin
      Reporting.init_step s_name;
      s_function s_param;
      Reporting.end_step s_name
    end
  with _ ->
    (err_report_and_exit "Problem during the execution of the following step: " s_name)

  
(* function to execute a step and report informations about the process *)
            
let execute_step_without_end_reporting s_name s_function s_param =
  try
    begin
      Reporting.init_step s_name;
      s_function s_param
    end
  with _ ->
    (err_report_and_exit "Problem during the execution of the following step: " s_name)


(* function to execute a step with result and report informations about the process *)

let execute_step_with_result s_name s_function s_param =
  try
    begin
      Reporting.init_step s_name;
      let result = s_function s_param in
      begin
        Reporting.end_step s_name;
        result
      end
    end
  with _ ->
    (err_report_and_exit "Problem during the execution of the following step: " s_name)
       

                                                          
(*-----------------------------------------------------------------
 * Utilities for working directory and working file
 *-----------------------------------------------------------------*)

(* functions to automatically recognize the format of a file (or several files) *)
                          
let rec get_file_format_of_ispm ispm =
  try
    let get_format_from_version_data xml =
      match xml with
      | Xml.PCData n ->
         begin
           match (int_of_string n) with
           | 2 -> "ispm2"
           | 3 -> "ispm3"
           | _ -> invalid_arg ("unknown iSPM version: " ^ n)
         end
      | _ -> invalid_arg "unrecognized iSPM data"
    in
    match ispm with
    | Xml.Element ("iSPM", _, header::xxx) -> get_file_format_of_ispm header
    | Xml.Element ("header", _, info::xxx) -> get_file_format_of_ispm info
    | Xml.Element ("info", _, typ::version::xxx) -> get_file_format_of_ispm version
    | Xml.Element ("version", _, data::[]) -> get_format_from_version_data data
    | _ -> invalid_arg "unrecognized iSPM header"
  with
  | Invalid_argument m -> invalid_arg m
  | _ -> failwith "unrecognized iSPM document" 

let get_file_format_of_xml file =
  try
    let xml = Xml.parse_file file in
    match xml with
    | Xml.Element ("iccas", _, _) -> "iccas"
    | Xml.Element ("iSPM", _, _) -> get_file_format_of_ispm xml
    | Xml.Element ("scenario", _, _) -> "seven"
    | _ -> invalid_arg "unknow xml document"
  with
  | Invalid_argument m -> invalid_arg m
  | _ -> failwith "unrecognized xml document" 

let get_file_format file =
  try
    let ext_index_start = (String.rindex file '.') + 1 in
    let ext_length = (String.length file) - ext_index_start in
    let extension = String.sub file ext_index_start ext_length in
    match extension with
    | "xes" -> "xes"
    | "xml" -> get_file_format_of_xml file
    | _ ->  invalid_arg ("unknow file extension: " ^ extension)
  with
  | Invalid_argument msg | Failure msg ->
    let msg_1 = "Unrecognized format for file: " ^ file ^ " (" ^ msg ^ ") \n" in
    let msg_2 = "Supported formats are: xes, iccas, ispm2, ispm3, seven" in
    (err_report_and_exit (msg_1 ^ msg_2) "")
  | _ ->
    let msg_1 = "Unrecognized format for file: " ^ file ^ " \n" in
    let msg_2 = "Supported formats are: xes, iccas, ispm2, ispm3, seven" in
    (err_report_and_exit (msg_1 ^ msg_2) "")

let get_file_format_of_some_files files_array =
  if
    (Array.length files_array) > 0   
  then
    let format_array = Array.map get_file_format files_array in
    let format = Array.get format_array 0 in
    if List.for_all (fun x -> x = format) (Array.to_list format_array)
    then format
    else (err_report_and_exit "Files formats are different" "")      
  else
    (err_report_and_exit "No source file for working" "")

let get_file_format_from_dir dir =
  if
    (Sys.is_directory dir)
  then    
    let files_array = Sys.readdir dir in
    let files_array = Array.map (fun x -> dir ^ "/" ^ x) files_array in
    let filter x = not (Sys.is_directory x) in
    let files_array = Array.of_list (List.filter filter (Array.to_list files_array)) in
    get_file_format_of_some_files files_array
  else
    (err_report_and_exit "%s is not a directory" dir)
  

(* function to create the working directory if necessary *)

let create_work_directory_if_necessary config =
  try
    let work_directory = Configuration.get_output_directory config in
    match work_directory with
    | None -> ()
    | Some dir ->
       if not (Sys.is_directory dir)
       then
         (err_report_and_exit "Exiting because '%s' is not a directory \n" dir)
  with
  | _ ->
     try
       let work_directory = Configuration.get_output_directory config in
       match work_directory with
       | None -> ()
       | Some dir -> ignore (Sys.command ("mkdir " ^ dir))
     with _ -> ()
   

  
(*-----------------------------------------------------------------
 * Files adapting functions
 *-----------------------------------------------------------------*)

(* function to copy an input file on working directory, the copied file 
 * becoming the input work file*)

let copy_single_file_to_work_file src_file target_file =
  let command = "cp " ^ src_file ^ " " ^ target_file in
  begin
    ignore (Sys.command command);
  end
  

(* functions to execute the process for group some files *)

let rec define_files files_list result =
  match files_list with
  | f::[] -> (result ^ f)
  | f::fff -> define_files fff (result ^ f ^ "\n")
  | _ -> ""
       
let group_from_files src_type src_files_list target_file =
  let script = Configuration.get_script ("group", src_type, "files") in
  let src_files = stringify 2 (define_files src_files_list "") in
  let command = script ^ " " ^ src_files ^ " " ^ target_file  in
  begin
    ignore (Sys.command command);
  end
       
let group_from_dir src_type src_dir target_file =
  let script = Configuration.get_script ("group", src_type, "dir") in
  let src_dir = match src_dir with Some d -> d | None -> "" in
  let command = script ^ " " ^ src_dir ^ " " ^ target_file  in
  begin
    ignore (Sys.command command);
  end


(* function to remove all xmlns attributes of a xml file *)
  
let remove_xmlns_of_xml_file xml_file =
  let transient_file = "transient_file" in
  let c_copy f1 f2 = "cp " ^ f1 ^ " " ^ transient_file in
  let c_rm_xmlns f1 f2 = ("sed \"s/ xmlns=.*>/>/\" " ^ f1 ^ " > " ^ f2) in
  begin
    ignore (Sys.command (c_copy xml_file transient_file));
    Sys.remove xml_file;
    ignore (Sys.command (c_rm_xmlns transient_file xml_file));
    Sys.remove transient_file;
  end
  

(* function to execute the convert process *)
       
let convert format source target =
  let script = Configuration.get_script ("convert", format, "xspm2") in
  let command = ("xsltproc -o " ^ target ^ " " ^ script ^ " " ^ source) in
  begin
    ignore (Sys.command command)
  end
