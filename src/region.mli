(*
 *
 * Flipflop regions
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2012
 *
 *)

(* Region vector spaces *)

(*
 *
 * Regions sensitive to event i are of the form
 * [ c_0 ... c_n-1  a_i b_i m_0 ] where :
 *
 * c_j is the effect of event j on the region
 * a_i and b_i are the coefficients of the condition
 *   on event i (a_i . m + b_i = 0 mod 2)
 * m_0 is the initial marking
 *
 *)

(*** Region vector spaces ***)

type t

(* [make dim:n event:i] initializes a region space of dimension [n] and sensitive to event [i] *)

val make : dim:int -> event:int -> t

(* [enable s state:q event:j] adds a constraint enabling event [j] in state [q] *)

val enable : t -> state:Vector.t -> event:int -> unit

(* [clone s] returns a clone of [s] *)

val clone : t -> t

(* cooks and optimizes the set of constraints. Should be called after the last call to [enable] *)

val cook : t -> unit

(* add a constraint forcing the sensitive event to test the regions *)

val force_test : t -> unit

(* add a constraint forcing the sensitive event to flip the regions *)

val force_flip : t -> unit

(*** Regions ***)

type r

(* create a null region, orthogonal to every transition an initialized to 0 *)

val null : int -> r

(*
 *
 * Get a (multiplicative condition), b (additive condition), c (effect) and m0 (initial marking) coefficients such that:
 *
 * m |e> m' iff (i) m' = m + c(e) mod 2 and (ii) a(e)*m + b(e) = 0 mod 2
 *
 *)

val get_a : r -> int -> bool
val get_b : r -> int -> bool
val get_c : r -> int -> bool
val get_m0 : r -> bool

val set_a : r -> int -> bool -> unit
val set_b : r -> int -> bool -> unit
val set_c : r -> int -> bool -> unit
val set_m0 : r -> bool -> unit

(* [dimension r] returns the dimension of region [r] *)

val dimension : r -> int

val print : out_channel -> r -> unit

(* [marking r u] computes the value of region [r] after behavior [u] *)

val marking : r -> Vector.t -> bool

(* Solves event-state separation problem. Raises [Separation_failure (q,e)] if no solution exists. *)

exception Separation_failure of Vector.t * int

val solve_essp : t -> Vector.t -> int -> r

(*** Composition operators on regions ***)

(* Exception [Undefined] is raised by the [superimpose] operator *)

exception Undefined

(* Superimpose two regions with equal effects. Raises [Undefined] whenever two regions can not be superimposed *)

val superimpose : r -> r -> r

(* type of TF nets *)

type n = r list

(* [net_dimension n] returns the dimension of net [n]. Raises [Invalid_argument _] if [n] is empty *)

val net_dimension : n -> int

(* superimpose a list of regions onto one another *)

val superimpose_list : n -> n

(* For printing and drawing FF nets *)

type symbol =
    Ortho
    | Exit
    | Enter
    | Compl
    | True
    | False

val symbol_of_event : r -> int -> symbol

val set_symbol : r -> int -> symbol -> unit
