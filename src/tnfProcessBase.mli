(*
 *
 * Base functions for tnf command processes
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, Inria, Rennes, France, 2017
 *
 *)

(* 
 * This module provides the base functions for executing and managing 
 * the main command processes of the tool (named tnf).
 *)

(*-----------------------------------------------------------------
 * Module openning
 *-----------------------------------------------------------------*)

(* None *)



(*-----------------------------------------------------------------
 * Utilities
 *-----------------------------------------------------------------*)

(* functions used for operations about log and trace *)

val css_of_list : string list -> string
val trace_dim : int list -> int
val log_dim : (int list) list -> int
            

(* functions to remove or substitute the extension of a given file name *)

val remove_extension_of_file_name : string -> string
val substitute_extension_of_file_name : string -> string -> string

            
(* functions to obtain a given option type from a string *)

val string_option_of_string : string -> string option
val int_option_of_string : string -> int option

                                    
(* function to obtain a string from a string list *)

val string_list_of_string : char -> string -> string list -> string list


(* function to add the simple or double quotes around a string *)
   
val stringify : int -> string -> string

  

(*-----------------------------------------------------------------
 * Execution and error manager
 *-----------------------------------------------------------------*)

(* function to report an error and exit *)
  
val err_report_and_exit : string -> string -> 'a

  
(* function to execute a step and report informations about the process *)

val execute_step : string -> ('a -> 'b) -> 'a -> unit

  
(* function to execute a step and report informations about the process *)
            
val execute_step_without_end_reporting : string -> ('a -> 'b) -> 'a -> 'b


(* function to execute a step with result and report informations about the process *)

val execute_step_with_result : string -> ('a -> 'b) -> 'a -> 'b
  

                                                          
(*-----------------------------------------------------------------
 * Utilities for working directory and working file
 *-----------------------------------------------------------------*)

(* functions to automatically recognize the format of a file (or several files) *)

val get_file_format : string -> string
val get_file_format_of_some_files : string array -> string
val get_file_format_from_dir : string -> string
  

(* function to create the working directory if necessary *)

val create_work_directory_if_necessary : Configuration.t -> unit
   

  
(*-----------------------------------------------------------------
 * Files adapting functions
 *-----------------------------------------------------------------*)

(* function to copy an input file on working directory, the copied file 
 * becoming the input work file*)
  
val copy_single_file_to_work_file : string -> string -> unit

  
(* functions to execute the process for group some files *)
       
val define_files : string list -> string -> string       
val group_from_files : string -> string list -> string -> unit
val group_from_dir : string -> string option -> string -> unit


(* function to remove all xmlns attributes of a xml file *)
  
val remove_xmlns_of_xml_file : string -> unit

  
(* function to execute the convert process *)
       
val convert : string -> string -> string -> unit
