(*
 *
 * Module to load an ontology basis 
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * The main function of this module is to load an ontology basis from an ontology base file,
 * and complete this basis from other ontology base files.
 *) 

(*-----------------------------------------------------------------
 * Module openning, parameter & utilities
 *-----------------------------------------------------------------*)

(* functions to obtain a given option type from a string *)

let string_option_of_string s = match s with "" -> None | x -> Some x

let int_option_of_string s = match s with "" -> None | x -> Some (int_of_string x)

                                    
(* function to obtain a string list from a string, wherein each substring
 *  are delimited by a separator character (sep) *)

let rec string_list_of_string sep s_source result = 
  let filter s = not (s = "") in 
  try
    match (s_source, (String.contains s_source sep)) with
    | (_, true) ->
       let start_index = (String.rindex s_source sep) + 1 in
       let sub_length = (String.length s_source) - start_index in
       let rest_length = (start_index - 1) in
       let (sub_source, rest_source) =
         (String.sub s_source (start_index) sub_length,
          String.sub s_source 0 rest_length)
       in
       let result = sub_source::result in
       string_list_of_string sep rest_source result
    | _ -> List.filter filter (s_source::result)
  with _ -> List.filter filter result

  
                                                          
(*-----------------------------------------------------------------
 * Generic functions to analyze the elements of a xml document
 *-----------------------------------------------------------------*)

(* function to filter the elements of a XML document for a list of given tags *)
                                                          
let filter_elements target_tags_list xml_doc =
  List.filter
    (function
     | Xml.Element (tag,_,_) -> List.mem tag target_tags_list
     | Xml.PCData _ -> false)
    xml_doc

  
(* function to filter the elements of a sequence for a list of given tags *)
  
let filter_elements_of_sequence target_tags_list sequence =
  let f target_list seq =
    match seq with
    | Xml.Element (_, _, children) ->
       List.rev_append target_list (filter_elements target_tags_list children)
    | _ -> target_list
  in
  List.fold_left f [] sequence
  
  
(* function to obtain a value corresponding to an attribute key *)
  
let rec get_string_attr key s0 att_list =
  match att_list with
  | [] -> s0
  | (h,s) :: l ->
     if h = key
     then s
     else get_string_attr key s0 l
  
  
(* function to obtain a value corresponding to a xml element *)
  
let rec get_string_element elementTag s0 xml_doc =
  try
    let target = List.hd (filter_elements (elementTag::[]) xml_doc) in
    match target with
    | Xml.Element (_, _, children) ->
       (match children with (Xml.PCData x)::[] -> x | _ -> s0)
    | Xml.PCData _ -> s0
  with _ -> s0

    

(* function to map the attributes associated to a list of
 * xml element instance (element appearing several times), 
 * from a root xml element *)
    
let map_attributes_of_xml_element src_xml att_mapping e_name =
  let elements = filter_elements [e_name] src_xml in
  let e_mapping e =
    match e with
    | Xml.Element (_, attributes, _) -> att_mapping attributes
    | _ -> att_mapping []
  in
  List.map e_mapping elements
    

(* function to obtain a values list of an attribute associated to 
 * a xml elements list (element appearing several times), 
 * from a root xml element *)
    
let get_values_of_attributes_from_xml src_xml e_name att_name default_value =
  let att_mapping attr =
    get_string_attr att_name default_value attr
  in
  map_attributes_of_xml_element src_xml att_mapping e_name
  

(* function to obtain a single value of an attribute associated to 
 * a xml element, from a root xml element *)
  
let get_value_of_attribute_from_xml xml e_name att_name default_value =
  List.hd (get_values_of_attributes_from_xml xml e_name att_name default_value) 

  
                  
(*-----------------------------------------------------------------
 * Functions to obtain the ontological data from a xml source
 *-----------------------------------------------------------------*)

(* function to obtain the ontological iri from attributes of xml source *)
  
let get_onto_iri attributes =
  let iri_id = "rdf:about" in
  let default_iri = "" in
  get_string_attr iri_id default_iri attributes 

                            
(* function to obtain the ontological label from a xml source, namely the value
 * corresponding to tag "skos:prefLabel" and "lang = en" *)

let get_onto_label_en xml_source =
  try
    let skos_preflabel_list = filter_elements ["skos:prefLabel"] xml_source in
    let filter xml =
      match xml with
      | Xml.Element (_, attributes, _) -> 
         ((get_string_attr "xml:lang" "" attributes) = "en")
      | _ -> false
    in
    let onto_xml = List.filter filter skos_preflabel_list in
    get_string_element "skos:prefLabel" "" onto_xml
  with _ -> ""

          
(* function to obtain the rdfs ontological label from a xml source, namely the value
 * corresponding to tag "rdfs:label"
 * NB : this value should be used only if the label was not found otherwise *)
          
let get_onto_label_rdfs xml_source =
  try
    let e_name = "rdfs:label" in
    let label_list = filter_elements [e_name] xml_source in
    get_string_element e_name "" label_list
  with _ -> ""
  

(* functions to obtain the virtual reality reference on xml source, namely the value
 * corresponding to tag "UFO_related_match", "UFO_exact_match" or tag "UFR_exact_match" *)

let get_match_value xml_source s_match_ref =
  let match_list = filter_elements [s_match_ref] xml_source in
  get_string_element s_match_ref "" match_list

let get_five_ref xml_source =
  try
    let ufo_match_value = 
      let related_match = get_match_value xml_source "UFO_related_match" in
      let exact_match = get_match_value xml_source "UFO_exact_match" in
      match (exact_match, related_match) with
      | ("", m2) -> m2
      | (m1, _) -> m1
    in
    let ufr_match_value =
      get_match_value xml_source "UFR_exact_match"
    in
    match ufo_match_value with
    | "" -> ufr_match_value
    | v -> ufo_match_value
  with _ -> ""

  
                  
(*-----------------------------------------------------------------
 * Functions to load an ontology basis
 *-----------------------------------------------------------------*)  

(* function to add an element with the informations of the xml source,
 * wherein the added element associate an ontological id (iri), 
 * an ontological label and a virtual reality reference (five reference) *)

let add_element basis xml_source =
  let f_add attribute children =
    let onto_iri = get_onto_iri attribute in
    let onto_label = get_onto_label_en children in
    let five_ref = get_five_ref children in
    match onto_iri with
    | "" -> basis
    | _ -> DataOntologyBasis.add_mapping basis onto_iri onto_label five_ref 
  in
  match xml_source with
  | Xml.Element ("owl:Class", attribute, children) -> f_add attribute children
  | Xml.Element ("rdf:Description", attribute, children) -> f_add attribute children
  | _ -> invalid_arg "DataOntologyBasisIn.add_element: syntax error"
          

(* functions to load data of classes and descriptions from a xml element (xconfig) 
 * to a given ontology basis *)
          
let load_classes_on_basis xml basis =
  let owl_class_list = filter_elements ["owl:Class"] xml in
  List.fold_left add_element basis owl_class_list
          
let load_descriptions_on_basis xml basis =
  let owl_class_list = filter_elements ["rdf:Description"] xml in
  List.fold_left add_element basis owl_class_list

  
(* function to load the initial ontology basis from an ontology basis file *)
  
let load_initial_basis ontology_basis_file =
  let basis = DataOntologyBasis.init in
  let xconfig = Xml.parse_file ontology_basis_file in
  match xconfig with
  | Xml.Element (tag, attributes, children) ->
     begin
       match tag with
       | "rdf:RDF" ->
          begin
            let basis = load_classes_on_basis children basis in
            let basis = load_descriptions_on_basis children basis in
            basis
          end
       | _ -> invalid_arg "DataOntologyBasisInput.load_initial_basis: syntax error"
     end
  | _ -> invalid_arg "DataOntologyBasisInput.load_initial_basis: syntax error"

  
                  
(*-----------------------------------------------------------------
 * Functions to update an ontology basis
 *-----------------------------------------------------------------*)  

(* function to update the ontological label to update, taking into account 
 * different cases *)

let get_onto_label_to_update basis iri xml_source =
  let onto_label_en = get_onto_label_en xml_source in
  let current_label =
    try
      let mapping = (DataOntologyBasis.get_mapping_from_iri basis iri) in
      DataOntologyBasis.get_onto_label mapping
    with _ -> ""
  in
  let onto_label_rdfs = get_onto_label_rdfs xml_source in
  match (onto_label_en, current_label, onto_label_rdfs) with
  | ("", "", rdfs) -> rdfs
  | ("", assigned_label, _) -> assigned_label
  | (new_label, _, _) -> new_label
       

(* function to update an element, identified by its iri (ontological id), 
 * with the informations of the source xml (ontological label, five) *)    

let update_element basis xml_source =
  let f_update attribute children =
    let onto_iri = get_onto_iri attribute in
    let onto_label = get_onto_label_to_update basis onto_iri children in
    let five_ref = get_five_ref children in
    let basis =
      match (onto_iri, onto_label) with
      | ("",_) | (_,"") -> basis
      | _ -> DataOntologyBasis.update_onto_label basis onto_iri onto_label
    in
    let basis =
      match (onto_iri, five_ref) with
      | ("",_) | (_,"") -> basis
      | _ -> DataOntologyBasis.update_five_ref basis onto_iri five_ref
    in
    basis
  in
  match xml_source with
  | Xml.Element ("owl:Class", attribute, children) -> f_update attribute children
  | Xml.Element ("rdf:Description", attribute, children) -> f_update attribute children
  | _ -> invalid_arg "DataOntologyBasisIn.add_element: syntax error"
          

(* functions to update data of classes and descriptions from a xml element (xconfig) 
 * to a given ontology basis *)
          
let load_additional_classes_on_basis xml basis =
  let owl_class_list = filter_elements ["owl:Class"] xml in
  List.fold_left update_element basis owl_class_list
          
let load_additional_descriptions_on_basis xml basis =
  let owl_class_list = filter_elements ["rdf:Description"] xml in
  List.fold_left update_element basis owl_class_list

  
(* function to load additional data on ontology basis, from a global ontology file *)

let load_additional_data_on_basis additional_ontology_file basis = 
  let xconfig = Xml.parse_file additional_ontology_file in
  match xconfig with
  | Xml.Element (tag, attributes, children) ->
     begin
       match tag with
       | "rdf:RDF" ->
          begin
            let basis = load_additional_classes_on_basis children basis in
            let basis = load_additional_descriptions_on_basis children basis in
            basis
          end
       | _ -> invalid_arg "DataOntologyBasisInput.load_additional_data_on_basis: syntax error"
     end
  | _ -> invalid_arg "DataOntologyBasisInput.load_additional_data_on_basis: syntax error"
