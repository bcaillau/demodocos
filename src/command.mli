(***********************************************************************)
(*                                                                     *)
(*                  BDL: a Behaviour Description Language              *)
(*                                                                     *)
(*            Benoit Caillaud, projet Pampa, IRISA/INRIA-Rennes        *)
(*                                                                     *)
(*  Copyright 1999 Institut National de Recherche en Informatique et   *)
(*  Automatique.  Distributed only by permission.                      *)
(*                                                                     *)
(***********************************************************************)

(*
 *                                                                     
 * Command line parsing library
 *
 * $Header: /udd/reutel/bdl/referentiel/bdl/src/mylib/command.mli,v 1.7 2000/08/04 16:08:23 bcaillau Exp $
 * 
 * $Log: command.mli,v $
 * Revision 1.7  2000/08/04 16:08:23  bcaillau
 * ajout des options multiples
 *
 * Revision 1.6  2000/01/10 18:39:56  bcaillau
 * 1) correction Makefile dans mylib et btd
 * 2) correction bug dans btd_command.ml
 *
 * Revision 1.5  2000/01/10 14:03:19  bcaillau
 * Interim (c'est boggue)
 *
 * Revision 1.4  2000/01/06 16:12:00  bcaillau
 * 1) Suite du parser de lignes de commandes
 * 2) Ajout du module proncipal de la commande BTD
 *
 * Revision 1.3  2000/01/04 17:34:00  bcaillau
 * ajout des methodes test_... et models
 *
 * Revision 1.2  2000/01/04 13:02:58  bcaillau
 * Modification dues parametres de la classe generic command
 * Ligne de commande de btd
 *
 * Revision 1.1  1999/12/27 16:22:18  bcaillau
 * Ajout du module mylib::Command
 *
 *
 *)

(* Raised when a syntax error is encountered in a command line *)

exception Syntax_error of string

(* type of command line options *)

class type token_type =
  object
    method add_exclude : token_type -> unit (* add an exclusion *)
    method add_exclude_list : token_type list -> unit (* add exclusions *)
    method add_implies : token_type list -> unit (* add a requirement *)
    method add_implies_list : token_type list list -> unit (* add requirements *)
    method clear : unit (* clears the datastructure for new parsing *)
    method tag : string (* returns the option's tag *)
    method schema : string (* returns the usage of an option *)
    method comment : string (* returns the comment of an option *)
    method select : string Stream.t -> unit (* option parser *)
    method evaluate : unit (* evaluates the option if it has been selected *)
    method excludes : token_type list (* the set of excluded options *)
    method implies : token_type list list (* the set of required options *)
    method is_excluded : bool (* true iff the option is excluded *)
    method is_enabled : bool (* true iff it is enabled *)
    method is_complete : bool (* not selected or not excluded and enabled *)
    method is_selected : bool
    method test_is_excluded : token_type list -> bool (* the same but with  *)
    method test_is_enabled : token_type list -> bool (* a set of options *)
    method test_is_complete : token_type list -> bool
    method test_is_selected : token_type list -> bool
  end

(*
 * the type of command lines
 *
 * Conjunctions are sets of options (tokens).
 * Correct command lines:
 *
 * 1) selected tokens should be both enabled and not excluded
 * 2) all tokens in one the the conjunctions should be selected
 *
 *)

class type command_type =
  object
    method add_token : token_type -> unit (* add a token *)
    method add_token_list : token_type list -> unit (* add a set of tokens *)
    method add_conjunction : token_type list -> unit (* add a conjunction *)
    method add_conjunction_list : token_type list list -> unit
    method clear : unit
    method parse : string Stream.t -> unit
    method evaluate : unit
    method print_usage : out_channel -> unit
    method models : token_type list list
    method is_valid : bool (* every selected token is not excluded and enabled *)
    method is_complete : bool (* a conjunction is satisfied *)
    method is_correct : bool (* both complete and valid *)
    method print_diagnosis : out_channel -> unit
    method test_is_excluded : token_type list -> bool
    method test_is_enabled : token_type list -> bool
    method test_is_complete : token_type list -> bool
    method parameters : string list
  end

class token_generic :
    string (* tag *) -> string (* schema *) ->
      string (* comment *) -> int (* arity *) ->
	(string array -> unit) (* evaluation function *) ->
	  token_type 

class token_generic_multiple :
    string (* tag *) -> string (* schema *) ->
      string (* comment *) -> int (* arity *) ->
	(string array list -> unit) (* evaluation function *) ->
	  token_type 

class command_generic :
    string (* header for usage *) ->
      token_type list (* tokens *) ->
	token_type list list (* conjunction *) ->
	  command_type

val pairwise_exclude : token_type array -> unit
