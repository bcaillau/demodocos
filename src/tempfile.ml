(*
 *
 ***********************************************************************
 *                                                                     *
 *                   Flipflop : A flipflop synthesis tool              *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2013.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Utilities for temporary files
 *
 * $Id$
 *
 *)

(* temp file counter *)

let temp_file_count = ref 0

(* prints in base 16 *)

let base16digit n =
  if (n < 0) || (n >= 16)
  then invalid_arg "Tempfile.base16digit: not a base 16 digit"
  else
    if n < 10
    then Char.chr ((Char.code '0')+n)
    else Char.chr ((Char.code 'A')+n-10)

let rec to_base16 l n =
  if l <= 0
  then
    if n <= 0
    then ""
    else invalid_arg "Tempfile.to_base16: too large"
  else
    let d = n mod 16 in
    let n' = (n-d) / 16 in
      (to_base16 (l-1) n')^(String.make 1 (base16digit d))
      
(* [gen_temp ()] generates a unique temp file name. *)

let make_name () =
  let tfc = 1+(!temp_file_count)
  and pid = Unix.getpid ()
  and tim = Unix.gmtime (Unix.time ())
  and hna = Stringutil.substitute "." "_" (Unix.gethostname ()) in
    begin
      temp_file_count := tfc;
      "/tmp/tempfile_"^hna^"_"^
	(to_base16 8 pid)^"_"^
	(to_base16 4 (1900+tim.Unix.tm_year))^
	(to_base16 1 (1+tim.Unix.tm_mon))^
	(to_base16 2 tim.Unix.tm_mday)^
	(to_base16 2 tim.Unix.tm_hour)^
	(to_base16 2 tim.Unix.tm_min)^
	(to_base16 2 tim.Unix.tm_sec)^"_"^
	(to_base16 4 tfc)
    end
