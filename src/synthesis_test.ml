(*
 *
 * Testing module Synthesis
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA Rennes, France
 *
 *)

let d = 10;;

let s =
  Parikhproc.make_system_of_word_list
    d
    [
      [1;1;2;3;1;7;9;0;8;6;7;6;5;4;4];
      [0;3;1;2;4;6;5;8;7;9];
      [1;1;2;3;7;9;0;0;7;6;5;4;4];
      [1;1;0;3;1;2;9;4;6;5;8;7]
    ];;

Confusion.print_confusion_system stdout s;;

let t = Parikhproc.quotient s;;

Quotient.print stdout t;;

let r = Synthesis.make_region_spaces t;;

let w = Separation.init t;;

Separation.print stdout w;;

Synthesis.synthesize r w;;

Separation.print stdout w;;

Separation.elim_redundent w;;

Separation.print stdout w;;

let n = Separation.get_admissible_regions w;;

let n' = Region.superimpose_list n;;
