(*
 *
 * Module to load an ontology basis 
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * The main function of this module is to load an ontology basis 
 * (from an ontology base file).
 *) 

(*-----------------------------------------------------------------
 * Module openning, parameter & utilities
 *-----------------------------------------------------------------*)

(* functions to obtain a given option type from a string *)

let string_option_of_string s = match s with "" -> None | x -> Some x

let int_option_of_string s = match s with "" -> None | x -> Some (int_of_string x)

                                    
(* function to obtain a string list from a string, wherein each substring
 *  are delimited by a separator character (sep) *)

let rec string_list_of_string sep s_source result = 
  let filter s = not (s = "") in 
  try
    match (s_source, (String.contains s_source sep)) with
    | (_, true) ->
       let start_index = (String.rindex s_source sep) + 1 in
       let sub_length = (String.length s_source) - start_index in
       let rest_length = (start_index - 1) in
       let (sub_source, rest_source) =
         (String.sub s_source (start_index) sub_length,
          String.sub s_source 0 rest_length)
       in
       let result = sub_source::result in
       string_list_of_string sep rest_source result
    | _ -> List.filter filter (s_source::result)
  with _ -> List.filter filter result
             

(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

(* Mapping between ontology (iri and label) and virtual reality reference (five_ref) *)

type onto_vr_mapping =
  {
    onto_iri : string ; (* iri = mapping key *)
    onto_label : string ;
    five_ref : string
  }
  

(* Structure of ontology : mapping onto / vr *)
                     
type t = onto_vr_mapping list

       

(*-----------------------------------------------------------------
 * Constructor(s)
 *-----------------------------------------------------------------*)

(* function to initialize a structure corresonding to ontology basis *)

let init = []

  

(*-----------------------------------------------------------------
 * Accessors of mapping (for a given ontology/five mapping)
 *-----------------------------------------------------------------*) 

(* Base Accessors *)

let get_onto_iri mapping = mapping.onto_iri
                         
let get_onto_iri_segment mapping =
  try
    let iri = get_onto_iri mapping in
    let split_iri_list = (string_list_of_string '#' iri []) in
    let split_iri_list = List.rev split_iri_list in
    match split_iri_list with
    | [] -> ""
    | segment::rest -> segment
  with _ -> ""

let get_onto_label mapping = mapping.onto_label
                           
let get_five_ref mapping = mapping.five_ref

let set_onto_iri mapping new_value = 
  {
    onto_iri = new_value;
    onto_label = mapping.onto_label;
    five_ref = mapping.five_ref
  }
  
let set_onto_label mapping new_value = 
  {
    onto_iri = mapping.onto_iri;
    onto_label = new_value;
    five_ref = mapping.five_ref
  }
  
let set_five_ref mapping new_value = 
  {
    onto_iri = mapping.onto_iri;
    onto_label = mapping.onto_label;
    five_ref = new_value
  }

  

(*-----------------------------------------------------------------
 * Accessors of Ontology Basis Structure
 *-----------------------------------------------------------------*) 

(* Base Accessors *)

let get_head_mapping t = List.hd t (* first mapping of t, failure if t empty*) 
let get_tail_basis t = List.tl t (* t without its first element, failure if t empty *)
                     
let get_mapping_from_iri t o_iri =
  try
    let filter mapping = (o_iri = (get_onto_iri mapping)) in
    get_head_mapping (List.filter filter t)
  with _ -> failwith "DataOntologyBasis.get_mapping_from_iri: no mapping found" 

let get_mapping_from_label t o_label =
  try
    let filter mapping = (o_label = (get_onto_label mapping)) in
    get_head_mapping (List.filter filter t)
  with _ -> failwith "DataOntologyBasis.get_mapping_from_label: no mapping found" 

let get_mapping_from_five_ref t f_ref =
  try
    let filter mapping = (f_ref = (get_five_ref mapping)) in
    get_head_mapping (List.filter filter t)
  with _ -> failwith "DataOntologyBasis.get_mapping_from_five_ref: no mapping found"
          

(* function to add a given mapping *)
          
let add_mapping t o_iri o_label f_ref =
  { onto_iri = o_iri ; onto_label = o_label ; five_ref = f_ref } :: t


(* functions to update a mapping corresponding to a given iri (iri = mapping key) *)
  
let update_onto_label t o_iri o_label_new_value =
  try
    let filter m = (o_iri = (get_onto_iri m)) in
    let (target_mapping_list, other_mappings_list) = (List.partition filter t) in
    let mapping = get_head_mapping target_mapping_list  in
    begin
      add_mapping
        other_mappings_list
        o_iri o_label_new_value (get_five_ref mapping)
    end
  with _ -> t
  
let update_five_ref t o_iri f_ref_new_value =
  try
    let filter m = (o_iri = (get_onto_iri m)) in
    let (target_mapping_list, other_mappings_list) = (List.partition filter t) in
    let mapping = get_head_mapping target_mapping_list  in
    begin
      add_mapping
        other_mappings_list
        o_iri (get_onto_label mapping) f_ref_new_value
    end
  with _ -> t

             
       
(*-----------------------------------------------------------------
 * Utilities
 *-----------------------------------------------------------------*)

(* function to print a given mapping on out channel *)

let output_onto_vr_mapping onto_vr_mapping file =
  let onto_iri = get_onto_iri onto_vr_mapping in
  let onto_iri_segment = get_onto_iri_segment onto_vr_mapping in
  let onto_label = get_onto_label onto_vr_mapping in
  let onto_five_ref = get_five_ref onto_vr_mapping in
  begin
    (Printf.fprintf file " %s:\n" onto_iri);
    (Printf.fprintf file " - ontology iri segment: %s \n" onto_iri_segment);
    (Printf.fprintf file " - ontology label (en): %s \n" onto_label);
    (Printf.fprintf file " - five reference (vr): %s \n" onto_five_ref);
    Printf.fprintf file "\n"
  end

  
(* function to print an ontology basis on out channel *)

let output_ontology_basis t file =
  begin
    Printf.fprintf file "/////////////////////////////////////////////////////////////////////////////////////////////////////// \n";
    Printf.fprintf file "//                                                                                                   // \n";
    Printf.fprintf file "// ONTOLOGY BASIS : Set of mappings between ontology and virtual reality                             // \n";
    Printf.fprintf file "//                                                                                                   // \n";
    Printf.fprintf file "/////////////////////////////////////////////////////////////////////////////////////////////////////// \n";
    Printf.fprintf file "\n";
    Printf.fprintf file " Number of mappings: %i\n" (List.length t);
    Printf.fprintf file "\n";
    List.iter (fun m -> output_onto_vr_mapping m file) (List.rev t);
    Printf.fprintf file "\n"
  end


(* function to print an ontology basis on a given file *)

let print_ontology_basis_to_file t file_name =
  let file = open_out file_name in
    try
      output_ontology_basis t file;
      close_out file
    with
    | y ->
       begin
	 close_out file;
	 raise y
       end
