(*
 *
 * Abstract structure of #SEVEN code
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* This module provides a data structure and functions related
 * to this structure. 
 * The structure groups the data needed to generate #SEVEN code. 
 * It is created from a graph of reachable markings of the TF-net.
 *)

(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)
     
type t


(*-----------------------------------------------------------------
 * Constructor(s)
 *-----------------------------------------------------------------*)

val make : int -> t


(*-----------------------------------------------------------------
 * Accessors
 *-----------------------------------------------------------------*)

val get_urn_mapping : t -> A7UrnMapping.t
val get_place_list : t -> A7Element.t list
val get_sensor_event_list : t -> A7Element.t list
val get_transition_list : t -> A7Element.t list
val get_flow_list : t -> (int * int list) list
val get_init_place_urn_list : t -> int list
val get_final_place_urn_list : t -> int list

val set_place_list : t -> A7Element.t list -> t
val set_sensor_event_list : t -> A7Element.t list -> t
val set_transition_list : t -> A7Element.t list -> t
val set_flow_list : t -> (int * int list) list -> t
val set_init_place_urn_list : t -> int list -> t
val set_final_place_urn_list : t -> int list -> t

val add_place : t -> A7Element.t -> t
val add_sensor_event : t -> A7Element.t -> t
val add_transition : t -> A7Element.t -> t
val add_init_place_urn : t -> int -> t
val add_final_place_urn : t -> int -> t


(*-----------------------------------------------------------------
 * Functions to obtain the list of a given data from a list of elements
 *-----------------------------------------------------------------*)

val get_urn_list_from_elements : A7Element.t list -> int list
val get_event_urn_list_from_elements : A7Element.t list -> int list
val get_down_place_urn_list_from_elements : A7Element.t list -> int list
val get_up_place_urn_list_from_elements : A7Element.t list -> int list
  

(*-----------------------------------------------------------------
 * Functions to obtain lists of elements, filtered by a given data
 *-----------------------------------------------------------------*)

val get_places_filtered_by_urn_list : t -> int list -> A7Element.t list
val get_events_filtered_by_urn_list : t -> int list -> A7Element.t list
val get_transitions_filtered_by_urn_list : t -> int list -> A7Element.t list
  
val get_places_partition_by_urn_list : t -> int list -> (A7Element.t list * A7Element.t list)
val get_events_partition_by_urn_list : t -> int list -> (A7Element.t list * A7Element.t list)
val get_transitions_partition_by_urn_list : t -> int list -> (A7Element.t list * A7Element.t list)

val get_events_filtered_by_origin : t -> A7Element.e_origin -> A7Element.t list

val get_transitions_filtered_by_event_urn_list : t -> int list -> A7Element.t list
val get_transitions_filtered_by_up_place_urn_list : t -> int list -> A7Element.t list
val get_transitions_filtered_by_down_place_urn_list : t -> int list -> A7Element.t list


(*-----------------------------------------------------------------
 * Functions to obtain the children of a given element
 *-----------------------------------------------------------------*)

val get_children_places_of_place : t -> A7Element.t -> A7Element.t list
val get_children_events_of_place : t -> A7Element.t -> A7Element.t list
val get_children_transitions_of_place : t -> A7Element.t -> A7Element.t list
val get_children_places_of_transition : t -> A7Element.t -> A7Element.t list
  
  
(*-----------------------------------------------------------------
 * Functions to replace a place, an event or a transition on a list
 * of places, events or transitions, according by id
 *-----------------------------------------------------------------*)

val replace_element : A7Element.t -> A7Element.t list -> A7Element.t list


(*-----------------------------------------------------------------
 * Element specification as string
 *-----------------------------------------------------------------*)

val get_initial_place_idref_list : t -> string list
val get_final_place_idref_list : t -> string list
