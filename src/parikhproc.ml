(*
 *
 * Processes quotiented modulo parikh image
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA, France, 2012
 *
 *)

(* Type of systems and processes *)

type s =
    {
      dim : int;
      mutable root : t option;
      map : t list Parikh.t
    }

and r =
    {
      par : Vector.t;
      sys : s
    }

and t = r Process.t

(* [make n] initializes a system with [n] events *)

(* val make : int -> s *)

let make d =
  let m0 = Parikh.make d
  and v0 = Vector.make d false in
  let sys =
    {
      dim = d;
      root = None;
      map = m0
    } in
  let root = Process.make d { par=v0; sys=sys } in
    begin
      sys.root <- Some root;
      Parikh.set sys.map v0 [ root ];
      sys
    end

(* [root s] returns the root of system [s] *)

(* val root : s -> t *)

let root =
  function
      { root = Some p } -> p
    | { root = None } -> failwith "Parikhproc.root: impossible case. Please report bug"

(* [dimension s] returns the dimension of [s] *)

(* val dimension : s -> int *)

let dimension s =
  let n = root s in
    Process.dimension n

(* [system p] returns the system of process [p] *)

(* val system : t -> s *)

let system n = (Process.get n).sys

(* Utilities for [add] *)

let inc v e =
  let v' = Vector.clone v in
    begin
      Vector.set v' e (not (Vector.get v' e));
      v'
    end

let get_procs s v =
  try
    Parikh.get s.map v
  with
      Not_found -> []

let add_proc s v n = Parikh.set s.map v (n :: (get_procs s v))

(* [add p e] returns the process reached from process [p] after firing event [e] *)

(* val add : t -> int -> t *)

let add n e =
  let l = Process.get n in
  let s = l.sys
  and v = l.par
  and w = ref false in
  let n' =
    Process.add
      (function () ->
	let v' = inc v e in
	  begin
	    w := true;
	    {
	      sys = s;
	      par = v'
	    }
	  end)
      n e in
  let l' = Process.get n' in
  let v' = l'.par in
    begin
      if !w then add_proc s v' n';
      n'
    end

(* [add_word s u] adds word [u] to system [s] *)

(* val add_word : s -> int list -> unit *)

let add_word s w =
  let rec aw n =
    function
	[] -> ()
      | e :: u ->
	  aw (add n e) u
  in
    aw (root s) w;;

(* [make_system_of_word_list d l] makes a system of dimension [d] from word list [l] *)

(* val make_system_of_word_list : int -> int list list -> s *)

let make_system_of_word_list d l =
  let s = make d in
    begin
      List.iter (add_word s) l;
      s
    end

(* [parikh p] returns the parikh image of process [p] *)

(* val parikh : t -> Vector.t *)

let parikh n = (Process.get n).par

(* [class_iter f s v] evaluates [f p] for all elements [p] of class [v] in system [s] *)

(* val class_iter : (t -> unit) -> s -> Vector.t -> unit *)

let class_iter f s v = List.iter f (get_procs s v)

(* [iter f s] evaluates [f v] for all reachable vector [v] in system [s] *)

(* val iter : (Vector.t -> unit) -> s -> unit *)

let iter f s = Parikh.iter (fun v _ -> f v) s.map

(* May and must vectors of a process at given parikh vector *)

(* val may : s -> Vector.t -> Vector.t *)

let may s v =
  let d = dimension s in
    if (Vector.length v) <> d
    then invalid_arg "Parikhproc.may: invalid parikh vector"
    else
      let x = Vector.make d false in
	begin
	  class_iter
	    (fun n -> Process.ior_ready x n)
	    s v;
	  x
	end

(* val must : s -> Vector.t -> Vector.t *)

let must s v =
  let d = dimension s in
    if (Vector.length v) <> d
    then invalid_arg "Parikhproc.must: invalid parikh vector"
    else
      let x = Vector.make d true in
	begin
	  class_iter
	    (fun n -> Process.iand_ready x n)
	    s v;
	  x
	end

(* Confusion vector : may \ must *)

(* val conf : s -> Vector.t -> Vector.t *)

let conf s v =
  let x = may s v
  and y = must s v in
  let d = Vector.length x in
  let z = Vector.make d true in
    begin 
      Vector.ixor y z; (* y := not y *)
      Vector.iand x y; (* x := x and y *)
      x
    end

(* [quotient s] computes the quotient of process [s] *)

(* val quotient : s -> Quotient.t *)

let quotient s =
  let d = dimension s in
  let q = Parikh.make d in
    begin
      iter
	(fun v ->
	  let r = may s v in
	  Parikh.set q v r)
	s;
      q
    end
