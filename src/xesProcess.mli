(*
 *
 * Process used to execute an operation from XES file
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, Inria, Rennes, France, 2017
 *
 *)

(* 
 * This module provides functions to execute the different stage/step 
 * of an operation from XES file. 
 *)

(*-----------------------------------------------------------------
 * Functions to evaluate the traces using for the synthesis
 * and the traces using for inclusion tests
 *-----------------------------------------------------------------*)
 
val evaluate_synthesis_traces :
  Configuration.t -> (int list list) -> (int list list)

val evaluate_test_traces :
  Configuration.t -> (int list list) -> (int list list)
                                       

(*-----------------------------------------------------------------
 * Function to execute the different stage of synthesis algorithm 
 *-----------------------------------------------------------------*)

(* function to initialize the synthesis *)
            
val initialize_synthesis :
  Configuration.t -> (int list list) -> (Synthesis.t * Separation.t)

(* function to execute the main step of synthesis *)
  
val execute_main_synthesis :
  Configuration.t -> Synthesis.t -> Separation.t -> unit
                                                          
(* function to finalize the synthesis and obtain a T/F net *)
  
val finalize_synthesis_for_tf_net :
  Configuration.t -> Separation.t -> unit


(*-----------------------------------------------------------------
 * Function to produce the marking graph of TnF net
 *-----------------------------------------------------------------*)

(* function to produce the marking graph by performing the necessary operations *)
  
val produce_tnfgraph :
  Configuration.t -> Separation.t -> (Xes.event array) -> (int list list) -> string
  -> unit


(*-----------------------------------------------------------------
 * Function to produce and analyse a #7 code
 *-----------------------------------------------------------------*)
  
(* function to produce #7 code by performing the necessary operations *)

val compute_seven_position : Configuration.t -> A7.t -> A7.t
  
val produce_seven_code :
  Configuration.t -> Separation.t -> (Xes.event array) -> (int list list) -> string
  -> A7.t

(* function to analyse #7 code and to report some informations *)
  
val analyse_seven_code :
  A7.t -> (int list list) -> (int list list) -> unit
