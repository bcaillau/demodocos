(*
 *
 * Weighted processes
 *
 * (c) Benoit Caillaud, INRIA, Rennes, France, 2013, <benoit.caillaud@inria.fr>
 *
 *)

(* process labels = weights *)

type w =
{
  mutable weight : int
}

(* Weighted processes *)

type t = w Process.t

(* creates an empty process of weight 0 *)

(* val make : int -> t *)

let make n = Process.make n { weight=0 }

(* retrieves the label of a process *)

(* val weight : t -> int *)

let weight p =
  let w = Process.get p in
    w.weight

(* adds an edge to a new process if not already there. Weight of a new node is zero *)

(* val add : t -> int -> t *)

let add p e = Process.add (fun () -> { weight=0 }) p e

(* set the weight of a node and propagates weight to parent nodes *)

(* val set : t -> int -> unit *)

let rec increase p d =
  let w = Process.get p in
    begin
      w.weight <- w.weight + d;
      if not (Process.is_root p)
      then increase (Process.parent p) d
    end

let set p n =
  let n' = weight p in
  let d = n - n' in
    increase p d

