(*
 *
 * Parikh images
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2012
 *
 *)

let byte_size = 2

let two_power n = 1 lsl n

(*** Mappings from parikh images ***)

type 'a t =
    {
      dim : int;
      mutable tree : 'a tree
    }

and 'a tree =
    Undef
    | Leaf of 'a
    | Node of 'a tree array
	
(* val make : int -> 'a t *)
	
let make n =
  {
    dim = n;
    tree = Undef
  }
    
(* val dimension : 'a t -> int *)

let dimension { dim = n } = n

(* val set : 'a t -> Vector.t -> 'a -> unit *)

let set ({ dim=n; tree=a0 } as f) u y =
  if (Vector.length u) <> n
  then invalid_arg "Parikh.set: invalid vector"
  else
    let rec update i =
      if i >= n
      then
	fun _ -> Leaf y
      else
	let k = min byte_size (n-i) in
	let j = Vector.get_byte u i k in
	  function
	      (Node b) as w ->
		begin
		  b.(j) <- update (i+k) b.(j);
		  w
		end
	    | Undef as w ->
		let b = Array.make (two_power k) w in
		  begin
		    b.(j) <- update (i+k) b.(j);
		    Node b
		  end
	    | Leaf _ -> failwith "Parikh.set: tree is too shallow"
    in
      f.tree <- update 0 a0

(* val get : 'a t -> Vector.t -> 'a *)

let get { dim=n; tree=a0 } u =
  if (Vector.length u) <> n
  then invalid_arg "Parikh.get: invalid vector"
  else
    let rec search i =
      if i >= n
      then
	function
	    Leaf y -> y
	  | Undef -> raise Not_found
	  | Node _ -> failwith "Parikh.get: tree is too deep. Please report bug"
      else
	let k = min byte_size (n-i) in
	let j = Vector.get_byte u i k in
	  function
	      Undef -> raise Not_found
	    | Leaf _ -> failwith "Parikh.get: tree is too shallow. Please report bug"
	    | Node b ->
		search (i+k) b.(j)
    in
      search 0 a0

(* Tests whether a Parikh mapping is defined for a given vector *)

(* val is_def : 'a t -> Vector.t -> bool *)

let is_def { dim=n; tree=a0 } u =
  if (Vector.length u) <> n
  then invalid_arg "Parikh.is_def: invalid vector"
  else
    let rec search i =
      if i >= n
      then
	function
	    Leaf y -> true
	  | Undef -> false
	  | Node _ -> failwith "Parikh.get: tree is too deep. Please report bug"
      else
	let k = min byte_size (n-i) in
	let j = Vector.get_byte u i k in
	  function
	      Undef -> false
	    | Leaf _ -> failwith "Parikh.get: tree is too shallow. Please report bug"
	    | Node b ->
		search (i+k) b.(j)
    in
      search 0 a0

(* Iterator *)

(* val iter : (Vector.t -> 'a -> unit) -> 'a t -> unit *)

let iter foo { dim=n; tree=a0 } =
  let u = Vector.make n false in
  let rec search i =
    function
	Undef -> ()
      | Leaf y ->
	  if i >= n
	  then foo u y
	  else failwith "Parikh.iter: tree is too shallow"
      | Node b ->
	  let k = min byte_size (n-i) in
	    for j=0 to (two_power k)-1 do
	      Vector.set_byte u ~idx:i ~len:k ~byte:j;
	      search (i+k) b.(j)
	    done
  in
    search 0 a0
    
(* val fold : (Vector.t -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b *)

let fold foo a z0 =
  let z = ref z0 in
    begin
      iter (fun x y -> z := foo x y (!z)) a;
      !z
    end

(* Dump *)

let dump { tree=a0 } =
  let rec search =
    function
	Undef -> print_string "."
      | Leaf _ -> print_string "@"
      | Node b ->
	  begin
	    print_string "<";
	    for i=0 to (Array.length b)-1 do
	      search b.(i)
	    done;
	    print_string ">"
	  end
  in
    search a0
