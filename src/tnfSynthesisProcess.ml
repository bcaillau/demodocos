(*
 *
 * Process used to perform a Tnf net synthesis
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, Inria, Rennes, France, 2017
 *
 *)

(* 
 * This module provides functions to execute the different stage/step 
 * of an operation from XES file. 
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

open Configuration
open TnfBasis

   

(*-----------------------------------------------------------------
 * Functions to evaluate the traces using for the synthesis
 * and the traces using for inclusion tests
 *-----------------------------------------------------------------*)

let rec get_n_traces traces n =
  if (List.length traces < n)
  then traces
  else
    if n > 0
    then (List.hd traces)::(get_n_traces (List.tl traces) (n - 1))
    else []
      
let evaluate_synthesis_traces config traces =
  match (Configuration.get_instances_number config) with
  | None -> traces
  | Some n -> get_n_traces traces n

let evaluate_test_traces config traces =
  match (Configuration.get_instances_number config) with
  | None -> []
  | Some n -> get_n_traces (List.rev traces) ((List.length traces) - n)
       

(*-----------------------------------------------------------------
 * Function to execute the different stage of synthesis algorithm 
 *-----------------------------------------------------------------*)

(* function to initialize the synthesis *)
            
let initialize_synthesis config traces =
  let d = log_dim traces in
  let system_of_word_list = Parikhproc.make_system_of_word_list d traces in
  begin
    if (Configuration.get_verbose config) >= 1
    then Confusion.print_confusion_system stderr system_of_word_list;
    let t = Parikhproc.quotient system_of_word_list in
    begin
      if (Configuration.get_verbose config) >= 1
      then Quotient.print stderr t;
      (Synthesis.make_region_spaces t, Separation.init t)
    end
  end 

  
(* function to execute the main step of synthesis *)
  
let execute_main_synthesis configuration region_spaces ffnet_structure =
  execute_step
    "Main Synthesis"
    (Synthesis.synthesize configuration region_spaces) ffnet_structure
  

(* function to finalize the synthesis and obtain a T/F net *)
  
let merging_regions_with_identical_supports config ffnet_structure =
  let n = Separation.get_admissible_regions ffnet_structure in
  let n' = Region.superimpose_list n in
  begin
    if (Configuration.get_verbose config) >= 1
    then List.iter (Region.print stderr) n';
    flush stdout;
    n'
  end
  
let finalize_synthesis_for_tnfnet config ffnet_structure =
  let default_tnfnet = (Region.null 0)::[] in
  match (is_tfnet_synthesis config) with
  | true ->
     begin
       execute_step
         "Elimination of redundent regions"
         Separation.elim_redundent ffnet_structure;
       Separation.print stderr ffnet_structure;
       flush stderr;
       let tnfnet =
         execute_step_with_result
           "Merge of admissible regions with identical supports"
           (merging_regions_with_identical_supports config) ffnet_structure
       in
       tnfnet
     end
  | _ -> default_tnfnet
  


(*-----------------------------------------------------------------
 * Function to produce the TnF net
 *-----------------------------------------------------------------*)

(* function to produce the tnfnet in the expected format *)
  
let produce_tnfnet config net file =
  Display.net_to_pdf net "net" file
  


(*-----------------------------------------------------------------
 * Function to produce the marking graph of TnF net
 *-----------------------------------------------------------------*)

(* function to update the labels of a given A7 as basis for 
 * the construction of a tnf net marking graph *)

let update_labels_of_a7_to_tnfgraph config a7 ontology =
  match Configuration.get_tnfgraph_label_type config with
  | "letter" -> 
     (A7Work.update_sensors_labels_from_ontology_as_letter
        a7 ontology)
  | "number" -> 
     (A7Work.update_sensors_labels_from_ontology_as_number
        a7 ontology)
  | "label" ->
     (A7Work.update_sensors_labels_from_ontology_as_ontology_text
        a7 ontology)
  | _ -> a7

       
(* function to produce the marking graph by performing the necessary operations *)
  
let produce_tnfgraph config a7 ontology image_file =
  let script = Configuration.get_script ("convert", "seven", "dot") in
  let a7_temp_file_1 = "a7_temp_file_1.xml" in
  let a7_temp_file_2 = "a7_temp_file_2.xml" in
  let dot_file = (Configuration.get_target_file config "dot") in
  let graphviz_command = "dot" (* "neato" *) in
  let image_format = "jpg" in
  let image_file = (Configuration.get_target_file config image_format) in
  let a7_with_updated_label = update_labels_of_a7_to_tnfgraph config a7 ontology in
  begin
    A7Xml.abstract_seven_to_seven_xml_file a7_temp_file_1 a7_with_updated_label;
    ignore (Sys.command ("sed \"s/ xmlns=.*>/>/\" " ^ a7_temp_file_1 ^ " > " ^ a7_temp_file_2));
    ignore (Sys.command ("xsltproc -o " ^ dot_file ^ " " ^ script ^ " " ^ a7_temp_file_2));
    ignore (Sys.command ("sed -i \"s/Place_//g\" " ^ dot_file));
    ignore (Sys.command (graphviz_command ^ " -T" ^ image_format
                         ^ " " ^ dot_file ^ " -o " ^ image_file));
    Sys.remove a7_temp_file_1;
    Sys.remove a7_temp_file_2;    
  end

  
  
(*-----------------------------------------------------------------
 * Function to produce and analyse a #7 code
 *-----------------------------------------------------------------*)
  
(* functions to compute the abstract seven corresponding to a TnF net
 * by performing the necessary operations *)

let add_preliminary_initial_place_if_necessary config a7 =
  let a7 = A7.add_place a7 (A7Element.define_place 0) in
  let a7 = A7Work.add_event_of_edge a7 0 0 in
  let a7 = A7Work.add_transition_of_edge a7 0 0 1 in
  (* let a7 = A7Work.compute_initial_place_list a7 in *)
  a7
            
let compute_a7 config system ontology =
  let a7 = A7Work.abstract_seven_of_system system in
  let a7 = A7Work.update_sensors_labels_from_ontology_as_ontology_text a7 ontology in
  let a7 = A7Work.evaluate_five_code_from_ontology config a7 ontology in
  (* let a7 = add_preliminary_initial_place_if_necessary config a7 in *)
  a7

let compute_a7_if_necessary config ffnet_structure ontology =
  if (Configuration.get_output_tnfgraph config) <> None
     || (Configuration.get_output_seven config) <> None
     || (Configuration.get_output_a7xspm config) <> None
  then
    execute_step_with_result
      "Abstract Seven Construction"      
      (compute_a7 config ffnet_structure)
      ontology
  else (A7.make 1)
  
  
(* function to produce #7 code by performing the necessary operations *)

let evaluate_seven_origin a7 traces =
  try
    let a7 = A7Work.evaluate_events_origin_from_traces_list a7 traces in
    A7Work.evaluate_places_origin_from_events_origin a7
  with _ -> a7

let compute_seven_position config a7 =
  try
    if (Configuration.get_seven_location_computing config)
    then A7Work.compute_and_update_elements_location a7
    else a7
  with _ -> a7
            
let produce_seven_xml_file config a7 traces file =
  let a7 = evaluate_seven_origin a7 traces in
  let a7 = compute_seven_position config a7 in
  begin
    A7Xml.abstract_seven_to_seven_xml_file file a7
  end
  

(* function to analyse #7 code and to report some informations *)

let analyse_test_traces a7 test_traces =
  match test_traces with
  | [] -> (0, 0)
  | _ ->
     let nb_traces_tested = List.length test_traces in
     let nb_traces_inclused =
       A7Analysis.evaluate_inclusion_number a7 test_traces
     in
     (nb_traces_inclused, nb_traces_tested)
  
let analyse_seven_code a7 origin_traces test_traces =
  try
    let (nb_traces_inclused, nb_traces_tested) =
      analyse_test_traces a7 test_traces
    in
    let paths = A7Analysis.get_label_e_paths_from_initial_places a7 in
    begin
      Reporting.length_of_paths origin_traces;
      Reporting.number_of_simple_path a7 origin_traces;
      Reporting.traces_inclusion nb_traces_tested nb_traces_inclused;
      Reporting.list_of_paths paths
    end
  with _ ->
    begin
	Reporting.error_report "\n Error when analysing of #7 code" "";
	exit 2
    end

       

(*-----------------------------------------------------------------
 * Function to produce an A7 xSPM file (iSPM as xes file corresponding
 * to an abstract seven)
 *-----------------------------------------------------------------*)

(* function to produce an A7 xSPM file by performing the necessary operations  *)
  
let produce_a7xspm config a7 file =
  begin
    let a7_event_number = A7Analysis.get_events_number a7 in
    if Configuration.is_a7xspm_generating_possible config a7_event_number
    then
      A7XesOutput.generate_xspm_of_a7 file a7
    else
      Reporting.report_exec_info
        "Generation of A7 xSPM is impossible"
        "too many events"
        (string_of_int a7_event_number)
  end
  
