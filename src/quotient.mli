(*
 *
 * Basic algorithms on quotient of transition systems modulo Parikh vectors
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA, Rennes, France, 2013
 *
 *)

type t = Vector.t Parikh.t

(* Iterates on reachable parikh vectors, enabled and disabled transitions of a quotient system *)

val iter : state:(Vector.t -> unit) -> enabled:(Vector.t -> int -> unit) -> disabled:(Vector.t -> int -> unit) -> t -> unit

(* Iterates on the reachable enabled transitions with a given label of a quotient system *)

val iter_event_trans : (Vector.t -> unit) -> t -> int -> unit

(* Prints a quotient system *)

val print : out_channel -> t -> unit

