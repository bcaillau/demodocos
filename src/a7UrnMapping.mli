(*
 *
 * URN Mapping
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * This module provides any functions to manage the urn of A7 elements.
 *)

(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

val urn_init : int

type urn = Some of int | None
     
type t


(*-----------------------------------------------------------------
 * Constructor(s)
 *-----------------------------------------------------------------*)

val make : int -> t


(*-----------------------------------------------------------------
 * Accessors
 *-----------------------------------------------------------------*)

val get_urn_count : t -> int ref
val get_node_mapping : t -> urn Parikh.t
val get_edge_mapping : t -> ((int * int), urn) Hashtbl.t

val set_node_mapping : t -> Vector.t -> unit
val set_edge_mapping : t -> (int * int) -> unit
  

(*-----------------------------------------------------------------
 * Functions to manage unique reference numbers
 *-----------------------------------------------------------------*)  

(* function to obtain the urn of a node (= state) *)    
val get_urn_of_node : t -> int ref -> Separation.q -> int

(* function to obtain the urn of an edge (= pair node/i) *)  
val get_urn_of_edge : t -> int ref -> Separation.q -> int -> int


