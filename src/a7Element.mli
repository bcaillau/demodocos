(*
 *
 * Abstract #7 element
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* This module provides a data structure and functions related
 * to this structure. 
 *)

(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

(* types to specify the #7 elements *)

type e_type = Place | SensorEvent | Transition  
type e_origin = Ispm | Flipflop | Seven
                     

(* Structure of abstract #7 element *)
   
type t


(*-----------------------------------------------------------------
 * Constructors
 *-----------------------------------------------------------------*)

(* functions to define a place, a sensor/event or a transition 
 * respectively from the place id, from the event id and the classifier reference 
 * and from the event idref, the up place idref and the down place idref*)

val define_place : int -> t
  
val define_sensor_event : int -> int -> t       

val define_transition : int -> int -> int -> t  


(*-----------------------------------------------------------------
 * Accessors
 *-----------------------------------------------------------------*)

(* getters *)
  
val get_type : t -> e_type
val get_urn : t -> int
val get_signature : t -> (e_type * int)
  
val get_classifier_ref : t -> int
val get_origin : t -> e_origin option

val get_event_urn : t -> int
val get_up_place_urn : t -> int
val get_down_place_urn : t -> int
  
val get_five_relation : t -> A7Attributes.f_relation
val get_five_objects_list : t -> A7Attributes.f_objects
  
val get_location : t -> (int * int) (* (x, y) *)


(* setters *)

val set_sensor_label : t -> string -> t
val set_origin : t -> e_origin option -> t
val set_five_relation : t -> A7Attributes.f_relation option -> t
val set_five_objects_list : t -> A7Attributes.f_objects option -> t
val set_location : t -> int * int -> t


(*-----------------------------------------------------------------
 * Element specification as string
 *-----------------------------------------------------------------*)

val get_id_string : e_type -> int -> string
val get_label_string : e_type -> t -> string
  
val get_id : t -> string
val get_label : t -> string
val get_event_idref : t -> string
val get_up_place_idref : t -> string
val get_down_place_idref : t -> string
