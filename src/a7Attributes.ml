(*
 *
 * Attributes of sensor/event for #SEVEN abstract structure
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* 
 * This module provides functions to obtain the attributes of
 * sensor/event from ontology.
 *
 * Ontology is represented in the form of a sorted array of events 
 * with attributes. It is compute from a classifier with module Xes.
 *
 * The functions works from ontology of classifiers to extract
 * events informations and process on them.
 *
 * The results take the form of a sorted array (like ontology) with the 
 * targeted attributes. These lists are used to update the sensors.
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities
 *-----------------------------------------------------------------*)

(* function to remove double quote from a string, if present *)
(* val disstringify : string -> string *)
                                         
let disstringify s =
     let s_length = String.length s in
     let s1_length = min 1 s_length in
     let s2_start = min 1 s_length in
     let s2_length = max (s_length - 2) 0 in
     let s3_start = max ((String.length s) - 1) 0 in
     let s3_length = min 1 s3_start in
     let s1 = String.sub s 0 s1_length
     and s2 = String.sub s s2_start s2_length
     and s3 = String.sub s s3_start s3_length in
     let s1 = if ((String.compare s1 "\"") == 0) then "" else s1 in
     let s3 = if ((String.compare s3 "\"") == 0) then "" else s3 in
     s1 ^ s2 ^ s3
 

     
(*-----------------------------------------------------------------
 * XES Keys and data structures
 *-----------------------------------------------------------------*)

(* Key of "Concept" xes extension *)
let key_concept_name = "concept:name"

(* Keys of "xSPM Action" xes extension *)
let key_action_verb = "action:verb"
let key_action_actuator = "action:actuator"
let key_action_affected i = "action:affected-" ^ (string_of_int i)
let key_action_instrument i = "action:instrument-" ^ (string_of_int i)
let key_action_origin = "action:origin"
let key_action_destination = "action:destination"
                          
(* types for five entities *)
type f_relation = string
type f_objects = (string * string) list

               
                          
(*-----------------------------------------------------------------
 * Functions to manage the attribute values of a given event
 *-----------------------------------------------------------------*)

(* function to obtain a given attribute value of event according to the 
 * configuration *)

let get_value_according_to_config value_type event a_name =
  try
    let result =
      match value_type with
      | "letter" -> Some (DataEvent.get_letter_tag event)
      | "number" -> Some (string_of_int (DataEvent.get_urn event))
      | "ontology_en" | "ontology_label" | "onto_label" | "label" ->
         DataEvent.get_attribute_xes_value event a_name
      | "ref_vr" | "five_ref" -> DataEvent.get_attribute_vr_value event a_name
      | _ -> None
    in
    match result with Some x -> Some (disstringify x) | None -> None
  with _ -> None


(* functions to analyse the attributes of a given event *)
  
let is_target_value key (s, va) = (s = key)

let is_extension key event =
  let a = DataEvent.get_attributes_list event in
  (List.exists (is_target_value key) a)
  
          
(* functions to obtain the attribute value of an event *)

let get_event_value value_type event key =
  match
    get_value_according_to_config value_type event key
  with
  | Some value -> value
  | None -> ""
  
let get_event_verb value_type event =
  get_event_value value_type event key_action_verb
  
let get_event_actuator value_type event =
  get_event_value value_type event key_action_actuator
  
let get_event_origin value_type event =
  get_event_value value_type event key_action_origin
  
let get_event_destination value_type event =
  get_event_value value_type event key_action_destination


(* functions to obtain the list of multiple attribute value of an event *)

let rec get_event_value_list value_type event f_key i result =
  let key = f_key i in
  let value = get_value_according_to_config value_type event key in
  match value with
  | None -> result
  | Some "" -> result
  | Some v -> get_event_value_list value_type event f_key (i + 1) (v::result)

let get_event_affected_list value_type event =
  get_event_value_list value_type event key_action_affected 1 []

let get_event_instrument_list value_type event =
  get_event_value_list value_type event key_action_instrument 1 []
            

  
(*-----------------------------------------------------------------
 * Functions to define and obtain the label of a given event
 *-----------------------------------------------------------------*)
  
let get_event_label_from_xspm_1 value_type event =
  get_event_value value_type event key_concept_name
  
let get_event_label_from_xspm_2 value_type event =
  let str_list = [] @ (get_event_affected_list value_type event) in
  let str_list = str_list @ (get_event_instrument_list value_type event) in
  (get_event_verb value_type event) ^ " : " ^ (String.concat " ; " str_list)


  
(*-----------------------------------------------------------------
 * Functions to define and obtain the five relation of a given event
 *-----------------------------------------------------------------*)
  
let get_event_five_relation_from_xspm_1 value_type event = "NO_RELATION_FROM_XSPM_1"
  
let get_event_five_relation_from_xspm_2 value_type event = (get_event_verb value_type event)
                                                     

                                                         
(*-----------------------------------------------------------------
 * Functions to define and obtain the five objects list of a given 
 * event
 *-----------------------------------------------------------------*)
  
let get_event_five_objects_list_from_xspm_1 value_type event = []

let get_affected_list_from_xspm_2 value_type event =
  let mapping aff = ("affected", aff) in
  let aff_list = (get_event_affected_list value_type event) in
  List.map mapping aff_list

let get_instrument_list_from_xspm_2 value_type event =
  let mapping inst = ("instrument", inst) in
  let inst_list = (get_event_instrument_list value_type event) in
  List.map mapping inst_list

let get_origin_list_from_xspm_2 value_type event =
  let origin = get_event_origin value_type event in
  let origin_list = match origin with "" -> [] | s -> s::[] in
  let mapping loc = ("location", loc) in
  List.map mapping origin_list

let get_destination_list_from_xspm_2 value_type event =
  let destination = get_event_destination value_type event in
  let dest_list = match destination with "" -> [] | s -> s::[] in
  let mapping loc = ("location", loc) in
  List.map mapping dest_list
  
let get_event_five_objects_list_from_xspm_2 value_type event = 
  let objects = [] in
  let objects = objects @ (get_affected_list_from_xspm_2 value_type event) in
  let objects = objects @ (get_instrument_list_from_xspm_2 value_type event) in
  let objects = objects @ (get_origin_list_from_xspm_2 value_type event) in
  let objects = objects @ (get_destination_list_from_xspm_2 value_type event) in
  objects
                          

  
(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)
                                       
(* function to obtain a sorted array of event labels *)

let get_event_label config event =
  let is_format_xspm1 = (is_extension key_concept_name event) in
  let is_format_xspm2 = (is_extension key_action_verb event) in
  let sensor_label_type = Configuration.get_seven_sensor_label_type config in
  let sensor_label =
    match (is_format_xspm1, is_format_xspm2) with
    | (true, _) -> get_event_label_from_xspm_1 sensor_label_type event
    | (_, true) -> get_event_label_from_xspm_2 sensor_label_type event
    | _ -> ""
  in
  sensor_label
     
let get_event_labels_of_ontology config ontology =
  let ontology = List.sort (fun (i1, _) (i2, _) -> i1 - i2) ontology in
  let event_list = List.map (fun (i, e) -> e) ontology in
  let event_array = Array.of_list event_list in
  Array.map (get_event_label config) event_array

  
(* function to obtain a sorted array of event five relation *)

let get_event_five_relation config event =
  let is_format_xspm1 = (is_extension key_concept_name event) in
  let is_format_xspm2 = (is_extension key_action_verb event) in
  let five_value_type = Configuration.get_seven_five_value_type config in
  let five_relation =
    match (is_format_xspm1, is_format_xspm2) with
    | (true, _) -> get_event_five_relation_from_xspm_1 five_value_type event
    | (_, true) -> get_event_five_relation_from_xspm_2 five_value_type event
    | _ -> ""
  in
  five_relation
    
let get_event_five_relations_of_ontology config ontology =
  let ontology = List.sort (fun (i1, _) (i2, _) -> i1 - i2) ontology in
  let event_list = List.map (fun (i, e) -> e) ontology in
  let event_array = Array.of_list event_list in
  Array.map (get_event_five_relation config) event_array

  
(* function to obtain a sorted array of event five objects list *)

let get_event_five_objects_list config event =
  let is_format_xspm1 = (is_extension key_concept_name event) in
  let is_format_xspm2 = (is_extension key_action_verb event) in
  let five_value_type = Configuration.get_seven_five_value_type config in
  let five_objects_list =
    match (is_format_xspm1, is_format_xspm2) with
    | (true, _) -> get_event_five_objects_list_from_xspm_1 five_value_type event
    | (_, true) -> get_event_five_objects_list_from_xspm_2 five_value_type event
    | _ -> []
  in
  five_objects_list
    
let get_event_five_objects_lists_of_ontology config ontology =
  let ontology = List.sort (fun (i1, _) (i2, _) -> i1 - i2) ontology in
  let event_list = List.map (fun (i, e) -> e) ontology in
  let event_array = Array.of_list event_list in
  Array.map (get_event_five_objects_list config) event_array
