(*
 *
 * A7 xSPM (iSPM as xes file)
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* This module generate an A7 xSPM, namely the XES file with some iSPM 
 * corresponding to a #SEVEN code.
 * The main function works from an abstract SEVEN structure to obtain 
 * a dot structure (using module Pp). 
 * Then, finally, the XES file is generated.
 *)

(*-----------------------------------------------------------------
 * Module openning, parameter & utilities
 *-----------------------------------------------------------------*)

(* Modules openning *)

open Pp
open A7XesDocFormat
                


(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

(* types to specify the #7 elements *)

type xspm_format = XSPM1 | XSPM2  
                     
                                     
(*-----------------------------------------------------------------
 * Functions to generates doc containing trace and event parts
 *-----------------------------------------------------------------*)

(* function to generates doc containing event part *)

let doc_of_events_xspm1 label_list =
  let f = fun doc label -> vgrp (doc ^^ (xspm1_event_text label)) in
  List.fold_left f empty label_list
  
                         
(* function to generates doc containing trace part (xSPM-1 format) *)

let rec number_paths paths i result =
  match paths with
  | [] -> result
  | p::other_paths ->
     let i = i + 1 in
     let new_path = (string_of_int i)::p in
     number_paths other_paths i (new_path::result)
  
let rec describe_paths paths result =
  let f_desc p = String.concat " " p in 
  match paths with
  | [] -> result
  | p::other_paths ->
     let desc = (f_desc p) in
     let new_path = desc::p in
     describe_paths other_paths (new_path::result)
                         
let doc_of_traces_xspm1 paths_list =
  let paths_list = (describe_paths paths_list []) in
  let paths_list = (number_paths paths_list 0 []) in  
  let paths_list = (List.rev paths_list) in 
  let f =
    fun doc path
    -> match path with
       | nb::desc::label_list ->
          let events = (doc_of_events_xspm1 label_list) in
          vgrp (doc ^^ (xspm1_trace_text nb desc events))
       | _ -> empty
  in
  List.fold_left f empty paths_list
            

(* function to generates doc containing trace part (xSPM-2 format) *)
  
let doc_of_traces_xspm2 se_list paths_list = empty
                                           

(* function to generates doc containing trace part *)
                                           
let doc_of_traces xspm_format a7 =
  let se_list = A7.get_sensor_event_list a7 in
  let paths_list = A7Analysis.get_e_paths_from_initial_places a7 in
  let label_paths_list = A7Analysis.get_label_e_paths_from_initial_places a7 in
  match xspm_format with
  | XSPM1 -> (doc_of_traces_xspm1 label_paths_list)
  | XSPM2 -> (doc_of_traces_xspm2 se_list paths_list)
           

  
(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)

(* function to abtain th xSPM doc corresponding to a given abstract seven *)
                   
let xspm1_doc_of_a7 a7 =
  vgrp
    (
      xml_prologue_text
      ^^ (log_text
            (xspm1_heading_text
             ^^ (doc_of_traces XSPM1 a7)
            )
         )
    )

  
(* function to generate the xes file with some iSPM corresponding to a
 * given abstract seven. Each iSPM corresponding to a path on the abstract
 * seven graph. It is a trace on the xes file. *)

let generate_xspm_of_a7 file_name a7 =
  let a7_doc = (xspm1_doc_of_a7 a7) in
  let file_out = open_out file_name in
  begin
    ppToFile file_out 80 a7_doc;
    flush file_out;
    close_out file_out
  end
