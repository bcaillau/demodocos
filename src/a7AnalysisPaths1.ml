(*
 *
 * Sub-Module of A7Analysis to compute a list of simple paths on #7 graph (method 1)
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * This module provides functions to compute the list of simple paths starting 
 * from an initial state and ending in a final state
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

open A7Attributes
open A7

(* function to know if a given place is a final place *)
let is_final_place a7 place_urn =
  let fp_urn_list = get_final_place_urn_list a7 in
  (List.mem place_urn fp_urn_list) 

(* function to sum the elements of a given list *)
let rec sum l =
  match l with
  | [] -> 0
  | hd :: tl -> hd + sum tl

(* function to obtain a list without duplicate (result list) 
 * a given list (source list) *)
let rec list_uniq result source =
  match source with
  | [] -> result
  | e::new_source ->
     let new_result = if (List.mem e result) then result else e::result
     in list_uniq new_result new_source 

      
(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

(* structure corresponding to a transition *)
type t_struct = { pe : int ; e : int ; ps : int }

(* constructor of t-struct *)
let cons_t_struct a b c = { pe = a ; e = b ; ps = c }                

(* function to obtain the t-graph corresponding to a given abstract seven *)
let get_t_graph_of_a7 a7 =
  let mapping t = cons_t_struct
                    (A7Element.get_up_place_urn t)
                    (A7Element.get_event_urn t)
                    (A7Element.get_down_place_urn t)
  in
  List.map mapping (A7.get_transition_list a7)  


(*-----------------------------------------------------------------
 * Functions to obtain the list of simple paths starting from the 
 * initial state and ending in a final state
 *-----------------------------------------------------------------*)

(* function to add a given transition (t-struct) to a list of paths,
 * the transition adding only on path that arrive on this transition
 * (ie the arriving place of path is the starting path of the transition) *)   
let add_t_struct_to_paths t_paths t =
  let predicate path =
    match List.hd path with { pe ; e ; ps } -> (ps == t.pe)
  in
  let (paths_1, paths_2) = List.partition predicate t_paths in
  let new_paths_1 = List.map (function path -> t::path) paths_1 in
  List.rev_append new_paths_1 paths_2

(* function filtering a transition list relative to visited transitions *)
let filter_t_visited t_visited t_list =
  let filter t = not (List.mem t t_visited) in
  List.filter filter t_list
  
(* function to compute a list of transition paths from a graph with
 * transitions and a list of starting places *)
let rec compute_t_paths_from_places graph t_paths places t_visited =
  match places with
  | [] -> t_paths
  | _ -> 
     let mapping p =
       (* new t are transitions from p without visited transitions *)
       let t_from_p = List.filter (function t -> p == t.pe) graph in
       let new_t = filter_t_visited t_visited t_from_p in
       (* New paths with new transitions *)
       let new_t_paths = List.concat
                           (List.map (add_t_struct_to_paths t_paths) new_t) in
       (* New places reached with new transitions *)
       let new_places = List.map (function t -> t.ps ) new_t in
       (* Adding new transitions to visited transitions list *)
       let new_t_visited = List.rev_append t_visited new_t in
       compute_t_paths_from_places graph new_t_paths new_places new_t_visited
     in
     List.rev_append t_paths (List.concat (List.map mapping places))

     
(* function to obtain transition paths starting from a list of places,
 * each transition representing by a specific structure (t-struct) *) 
let get_t_paths_from_places a7 graph t_paths places =
  try
    (* step 1 = compute of all t_paths from places *)
    let working_out = compute_t_paths_from_places graph t_paths places [] in
    (* step 2 = filter to obtain only t_paths arriving on a final place *)
    let final_places = A7.get_final_place_urn_list a7 in
    let filter path = List.mem (List.hd path).ps final_places in
    let result = (List.filter filter working_out) in
    (* step 3 = remove duplicates paths *)
    let result = list_uniq [] result in
    (* step 4 = reversal each path to obtain a path starting from first transition
     and arriving on last transition *)
    List.map List.rev result
  with _ -> []

  
(* function to obtain transition paths starting from initial places,
 * each transition representing by a specific structure (t-struct) *)
let get_t_paths_from_initial_places a7 =
  let graph = get_t_graph_of_a7 a7 in
  let places = A7.get_init_place_urn_list a7 in
  let t_from_p p = List.filter (function t -> p == t.pe) graph in
  let t_from_all_p = List.concat (List.map t_from_p places) in 
  let t_paths = List.map (function t -> t::[]) t_from_all_p in
  get_t_paths_from_places a7 graph t_paths places

  
(* function to obtain the event path corresponding of a given transition path,
 * each event representing by his urn and each transition representing by a 
 * specific structure (t-struct) *)
let get_e_path_of_t_path t_path =
  let f t = t.e in
  List.rev (List.rev_map f t_path)

  
(* function to obtain event paths starting from initial places,
 * each event representing by his urn *)
let get_e_paths_from_initial_places a7 =
  let t_paths = get_t_paths_from_initial_places a7 in
  List.rev (List.rev_map get_e_path_of_t_path t_paths)
