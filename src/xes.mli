(*
 *
 * Xes datastructure
 *
 * Compatible with the XES 1.4 Standard Definition
 *
 *)

(*-----------------------------------------------------------------
 * Datastructures
 *-----------------------------------------------------------------*)

(* Datatypes *)

type log =
    {
      log_version : float;
      log_features : string list; (* sorted list *)
      log_content : trace list;
      log_attributes : attributes;
      log_global_trace_attributes : attributes;
      log_global_event_attributes : attributes;
      log_classifiers : classifiers
    }

and classifiers = (string * (string list)) list (* List of (name,key list) *)

and trace =
    {
      trace_content : event list;
      trace_attribute : attributes
    }

and event =
    {
      event_attributes : attributes
    }

and attributes = (string * value) list (* list sorted by ascending key *)

and value =
    {
      value_scalar : scalar;
      value_nested : attributes
    }

and scalar =
      String of string
    | Date of string
    | Int of int
    | Float of float
    | Boolean of bool
    | ID of string

and filter = string list

           

(*-----------------------------------------------------------------
 * Comparison functions
 *-----------------------------------------------------------------*)

(* Comparison functions *)

val compare_scalar : scalar -> scalar -> int

val compare_value : filter -> value -> value -> int

val compare_attributes : filter -> attributes -> attributes -> int


      
(*-----------------------------------------------------------------
 * Accessors (for attributes and values)
 *-----------------------------------------------------------------*)  

(* string of attributes and values *)

val string_of_attributes : attributes -> string

val string_of_value : value -> string

val string_of_scalar : scalar -> string

val string_of_event : event -> string
  

(* accessors for events *)

val get_event_attributes : event -> attributes

val string_formatting_of_event : event -> (string * string) list


(* accessors for attributes *)

val set_attribute : string -> value -> attributes -> attributes

           

(*-----------------------------------------------------------------
 * Parsers for log
 *-----------------------------------------------------------------*)

(* parse a XML document and return a log *)

val log_of_xml : Xml.xml -> log 

(* function to add a list of classifiers into a log structure *)

val add_classifiers_to_log : log -> classifiers -> log
  
(* parse a XES file and return a log *)

val log_of_file : string -> log

           

(*-----------------------------------------------------------------
 * Ontology computing function(s)
 *-----------------------------------------------------------------*)

(* returns the filter associated to a classifier *)

val filter_of_classifier : log -> string -> filter

(* computes an ontology, given a classifier. The ontology is a sorted array of events *)

val ontology_of_classifier : log -> string -> event array

           

(*-----------------------------------------------------------------
 * Log encoding function(s)
 *-----------------------------------------------------------------*)

(* function to encodes a log according to a given classifier 
 * result = (ontology, traces) *)

val encode_log : log -> string -> (event array) * (int list list)
