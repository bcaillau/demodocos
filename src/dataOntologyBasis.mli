(*
 *
 * Module to load an ontology basis 
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * The main function of this module is to load an ontology basis 
 * (from an ontology base file).
 *) 

(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

(* Mapping between ontology (iri and label) and virtual reality reference (five_ref) *)

type onto_vr_mapping
   

(* Structure of ontology : mapping onto / vr *)
                     
type t = onto_vr_mapping list

       

(*-----------------------------------------------------------------
 * Constructor(s)
 *-----------------------------------------------------------------*)

(* function to initialize a structure corresonding to ontology basis *)

val init : onto_vr_mapping list

  

(*-----------------------------------------------------------------
 * Accessors of mapping (for a given ontology/five mapping)
 *-----------------------------------------------------------------*) 

(* Base Accessors *)

val get_onto_iri : onto_vr_mapping -> string
val get_onto_iri_segment : onto_vr_mapping -> string
val get_onto_label : onto_vr_mapping -> string
val get_five_ref : onto_vr_mapping -> string

val set_onto_iri : onto_vr_mapping -> string -> onto_vr_mapping
val set_onto_label : onto_vr_mapping -> string -> onto_vr_mapping
val set_five_ref : onto_vr_mapping -> string -> onto_vr_mapping

  

(*-----------------------------------------------------------------
 * Accessors of Ontology Basis Structure
 *-----------------------------------------------------------------*)                      

(* Base accessors *)
                     
val get_mapping_from_iri : t -> string -> onto_vr_mapping
val get_mapping_from_label : t -> string -> onto_vr_mapping
val get_mapping_from_five_ref : t -> string -> onto_vr_mapping
          
          
(* function to add a given mapping *)
          
val add_mapping : t -> string -> string -> string -> t


(* functions to update a mapping corresponding to a given iri (iri = mapping key) *)
  
val update_onto_label : t -> string -> string -> t 
  
val update_five_ref : t -> string -> string -> t      

                           

(*-----------------------------------------------------------------
 * Utilities
 *-----------------------------------------------------------------*)
  
val print_ontology_basis_to_file : t -> string -> unit
