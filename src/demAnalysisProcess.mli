(*
 *
 * Process used to perform a Tnf net analysis
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, Inria, Rennes, France, 2017
 *
 *)

(* 
 * This module provides functions to execute the different stage of analysis
 * of a proceeding against a generic model
 *)
   
(*-----------------------------------------------------------------
 * Function to execute the different stage of comparison process 
 *-----------------------------------------------------------------*)

(* function to initialize the analysis *)
            
val initialize_comparison :
  Configuration.t -> int list list -> (Synthesis.t * Separation.t) 
