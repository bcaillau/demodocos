(*
 *
 * Module to work on abstract structure of #SEVEN code
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* 
 * This module provides functions to make the abstract structure
 * of #SEVEN code (a7), and work on it. 
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

open A7Attributes
open A7
   
let sort_uniq_of_int_list pl = List.sort_uniq compare pl
  

(*-----------------------------------------------------------------
 * Function to add a place corresponding to a node (state)
 *-----------------------------------------------------------------*)
      
(* val add_place_of_node : Separation.q -> A7.t -> A7.t *)

let add_place_of_node q a7 =
  let u_map = (get_urn_mapping a7) in
  let c = A7UrnMapping.get_urn_count u_map in
  let node_urn = (A7UrnMapping.get_urn_of_node u_map c q) in
  (add_place a7 (A7Element.define_place node_urn))


(*-----------------------------------------------------------------
 * Function to add event and transition corresponding to an edge 
 *-----------------------------------------------------------------*)

(* val add_event_of_edge : A7.t -> int -> int -> A7.t *)
(* val add_transition_of_edge : A7.t -> int -> int -> int -> A7.t *)
(* val add_event_and_transition_of_edge : Separation.q -> int -> Separation.q -> A7.t -> A7.t *)
  
let add_event_of_edge a7 edge_urn i =
  (add_sensor_event a7 (A7Element.define_sensor_event edge_urn i))

let add_transition_of_edge a7 edge_urn up_place_urn down_place_urn =
  (add_transition a7 (A7Element.define_transition edge_urn up_place_urn down_place_urn))

let add_event_and_transition_of_edge q i q' a7 =
  let u_map = (get_urn_mapping a7) in
  let c = A7UrnMapping.get_urn_count u_map in
  let edge_urn = (A7UrnMapping.get_urn_of_edge u_map c q i)
  and up_place_urn = (A7UrnMapping.get_urn_of_node u_map c q)
  and down_place_urn = (A7UrnMapping.get_urn_of_node u_map c q') in
  let a7 = (add_event_of_edge a7 edge_urn i) in
  (add_transition_of_edge a7 edge_urn up_place_urn down_place_urn)


(*-----------------------------------------------------------------
 * Functions to compute the list of flows
 *-----------------------------------------------------------------*)

let urn_of_elements e_list =
  List.map (A7Element.get_urn) e_list

let down_place_urn_of_elements e_list =
  List.map (A7Element.get_down_place_urn) e_list

let compute_successors_of_place a7 p_urn =
  let t_list = get_transition_list a7 in
  let filter e = ((A7Element.get_up_place_urn e) = p_urn) in
  let t_list = List.filter filter t_list in
  let t_list = urn_of_elements t_list in
  (p_urn, t_list)

let compute_successors_of_transition a7 t_urn =
  let t_list = get_transition_list a7 in
  let filter e = ((A7Element.get_urn e) = t_urn) in
  let t_list = List.filter filter t_list in
  let t_list = down_place_urn_of_elements t_list in
  (t_urn, t_list)

let compute_flow_list a7 =
  let p_urn_list = urn_of_elements (get_place_list a7) in
  let mapping p_urn = compute_successors_of_place a7 p_urn in
  let p_list = List.map mapping p_urn_list in
  let t_urn_list = urn_of_elements (get_transition_list a7) in
  let mapping t_urn = compute_successors_of_transition a7 t_urn in
  let t_list = List.map mapping t_urn_list in
  set_flow_list a7 (List.rev_append p_list t_list)


(*-----------------------------------------------------------------
 * Function to compute the lists of initial place and final place
 *-----------------------------------------------------------------*)
  
(* val compute_initial_place_list : abstract_seven -> abstract_seven *)
(* val compute_final_place_list : abstract_seven -> abstract_seven *)
  
let get_up_place_list a7 =
  let t_list = get_transition_list a7 in
  let f = function t -> A7Element.get_up_place_urn t in
  List.map f t_list

let get_down_place_list a7 =
  let t_list = get_transition_list a7 in
  let f = function t -> A7Element.get_down_place_urn t in
  List.map f t_list
  
let make_place_list pl1 pl2 =
  let no_present_in_pl2 = function p -> not (List.mem p pl2) in
  List.filter no_present_in_pl2 pl1
  
let compute_initial_place_list a7 =
  let up = get_up_place_list a7
  and down = get_down_place_list a7
  and f_set = set_init_place_urn_list in
  let place_init = A7UrnMapping.urn_init
  and new_p_list = (make_place_list up down) in
  let new_p_list = sort_uniq_of_int_list (place_init::new_p_list) in
  f_set a7 new_p_list
  
let compute_final_place_list a7 =
  let up = get_up_place_list a7
  and down = get_down_place_list a7
  and f_set = set_final_place_urn_list in
  let new_p_list = sort_uniq_of_int_list (make_place_list down up) in
  f_set a7 new_p_list
  

(*-----------------------------------------------------------------
 * Functions to make the abstract seven corresponding to a system
 *-----------------------------------------------------------------*)
                                               
(* val abstract_seven_of_system : Separation.t -> abstract_seven *)
                                                     
let add_elements_from_system syst a7_init =
  let f1 = add_place_of_node in
  let f2 = add_event_and_transition_of_edge in
  (Separation.fold_graph f1 f2 syst a7_init)

let abstract_seven_of_system syst =
  let d = (Separation.dimension syst) in
  let a7_init = (make d) in
  let a7 = add_elements_from_system syst a7_init in
  let a7 = compute_flow_list a7 in
  let a7 = compute_initial_place_list a7 in
  let a7 = compute_final_place_list a7 in
  a7
  

(*-----------------------------------------------------------------
 * Functions to modify the sensor labels of sensor/event
 *-----------------------------------------------------------------*)

(* function to obtain the label defined for a given classifier reference *) 
(* val new_label_for_sensor : int -> string array -> string *)

let new_label_for_sensor c_ref label_array =
  try
    Array.get label_array c_ref
  with
    _ -> (string_of_int c_ref)


(* functions to update the label of a given sensor/event *)       
(* val sensor_with_new_label : sensor_event -> string array -> sensor_event *)
(* val update_sensors_labels : abstract_seven -> string array -> abstract_seven *)

let sensor_with_new_label se label_array =
  let c_ref = A7Element.get_classifier_ref se in
  let new_label = new_label_for_sensor c_ref label_array in
  A7Element.set_sensor_label se new_label
           
let update_sensors_labels a7 label_array =
  let se_list = get_sensor_event_list a7
  and change_sensor_labels se = sensor_with_new_label se label_array in 
  let new_se_list = List.map change_sensor_labels se_list in
  set_sensor_event_list a7 new_se_list


(* function to update the labels of sensor with informations on ontology,
 * corresponding to the number, letter or ontology text*)
  
let update_sensors_labels_from_ontology_as_number a7 ontology =
  let ontology = List.sort (fun (i1, _) (i2, _) -> i1 - i2) ontology in
  let event_list = List.map (fun (i, e) -> e) ontology in
  let event_array = Array.of_list event_list in
  let label_array = Array.map
                      (fun i -> string_of_int (DataEvent.get_urn i))
                      event_array
  in
  update_sensors_labels a7 label_array
  
let update_sensors_labels_from_ontology_as_letter a7 ontology =
  let ontology = List.sort (fun (i1, _) (i2, _) -> i1 - i2) ontology in
  let event_list = List.map (fun (i, e) -> e) ontology in
  let event_array = Array.of_list event_list in
  let label_array = Array.map (DataEvent.get_letter_tag) event_array in
  update_sensors_labels a7 label_array
  
let update_sensors_labels_from_ontology_as_ontology_text a7 ontology =
  let ontology_text_config = Configuration.default_t in
  let label_array =
    A7Attributes.get_event_labels_of_ontology ontology_text_config ontology
  in
  update_sensors_labels a7 label_array


(*-----------------------------------------------------------------
 * Function to evaluate the #FIVE code associated with a sensor/event
 *-----------------------------------------------------------------*)

(* function to obtain the #FIVE code defined for a given classifier 
 * reference *) 
(* val new_five_relation_for_sensor : int -> string array -> string *)

let new_five_code c_ref array =
  try
    Some (Array.get array c_ref)
  with
    _ -> None


(* functions to update the #FIVE code of a given sensor/event *)

let event_with_new_five_code se array f_set =
  let c_ref = A7Element.get_classifier_ref se in
  let new_code = new_five_code c_ref array in
  f_set se new_code

let update_event_five_code a7 array f_set =
  let se_list = get_sensor_event_list a7
  and change_five_code se = event_with_new_five_code se array f_set in 
  let new_se_list = List.map change_five_code se_list in
  set_sensor_event_list a7 new_se_list

  
(* functions to evaluate the #FIVE code associated with a sensor/event *)
(* val evaluate_five_code_from_ontology : A7.t -> Xes.event array -> A7.t *)
  
let evaluate_five_relation_from_ontology config a7 ontology =
  let relation_array =
    A7Attributes.get_event_five_relations_of_ontology config ontology
  in
  update_event_five_code a7 relation_array A7Element.set_five_relation

let evaluate_five_objects_from_ontology config a7 ontology =
  let objects_array =
    A7Attributes.get_event_five_objects_lists_of_ontology config ontology
  in
  update_event_five_code a7 objects_array A7Element.set_five_objects_list
  
let evaluate_five_code_from_ontology config a7 ontology =
  let a7 = evaluate_five_relation_from_ontology config a7 ontology in
  evaluate_five_objects_from_ontology config a7 ontology


(*-----------------------------------------------------------------
 * Function to evaluate the origin of sensor/event and places
 *-----------------------------------------------------------------*)

(* the sensor/event origin is ISPM if the event originates from the original 
 * procedure, it's FLIPFLOP if the event is induced from the synthesis *)
(* the origin of place is ISPM if a place is reachable from the original procedure, 
 * it's FLIPFLOP if it's induced from the synthesis *)

(* function to obtain the list of transitions with an up place
 * corresponding to a given place *)
  
let transitions_list_with_upplace p_idref t_list =
  let filter t = (p_idref = (A7Element.get_up_place_urn t)) in
  List.filter filter t_list


(* function to obtain event and transition corresponding to a given
 * classifier ref, ie a given event from classifier *)
  
let target_event_and_transition_by_classifier se_list t_list class_ref =
  let mapping t = A7Element.get_event_urn t in 
  let e_urn_list = List.map mapping t_list in (* list with idref of transitions *)
  let filter e = List.mem (A7Element.get_urn e) e_urn_list in
  let e_filter_list = List.filter filter se_list in (* events filtered by idref list *)
  let predicate e = ((A7Element.get_classifier_ref e) = class_ref) in
  let se = List.find predicate e_filter_list in (* event corresponding to classifier ref *)
  let predicate t = ((A7Element.get_event_urn t) = (A7Element.get_urn se)) in
  let t = List.find predicate t_list in (*transition corresponding to classifier ref *)
  (se, t)
  
  
(* function to make the list of sensor/events with iSPM origin marking *)
  
let rec se_with_ispm_origin_marking p se_list t_list trace =
  try
    match trace with
    | e_class_ref::trace' ->
       let tp_list = transitions_list_with_upplace p t_list in
       let (se, t) = target_event_and_transition_by_classifier se_list tp_list e_class_ref in
       let p' = A7Element.get_down_place_urn t in
       let new_se = A7Element.set_origin se (Some A7Element.Ispm) in (* targeted e is from iSPM *)
       let se_list = A7.replace_element new_se se_list in
       (se_with_ispm_origin_marking p' se_list t_list trace')
    | [] -> se_list
  with
  | _ -> se_list


(* function to mark the origin of sensor/event with a given mark,
 * only if the sensor/event origin is unspecified *)
       
let mark_origin_unspecified e origin =
  match (A7Element.get_origin e) with
  | None -> (A7Element.set_origin e origin)
  | _ -> e

       
(* functions to evaluate the origin of sensor/events *)       
(* val evaluate_events_origin_from_trace : A7.t -> int list -> A7.t *)
(* val complete_events_origin_with_flipflop_marking : A7.t -> A7.t *)
(* val evaluate_events_origin_from_traces_list : A7.t -> int list list -> A7.t *)
  
let evaluate_events_origin_from_trace a7 trace =
  let p = A7UrnMapping.urn_init in
  let se_list = A7.get_sensor_event_list a7 in
  let t_list = A7.get_transition_list a7 in
  let new_se_list = (se_with_ispm_origin_marking p se_list t_list trace) in
  set_sensor_event_list a7 new_se_list

let complete_events_origin_with_flipflop_marking a7 =
  let se_list = A7.get_sensor_event_list a7 in
  let mapping se = mark_origin_unspecified se (Some A7Element.Flipflop) in
  let new_se_list = List.map mapping se_list in
  set_sensor_event_list a7 new_se_list
  
let rec evaluate_events_origin_from_traces_list a7 trace_list =
  match trace_list with
  | [] -> (complete_events_origin_with_flipflop_marking a7)
  | trace::trace_list ->
     let a7' = evaluate_events_origin_from_trace a7 trace in
     evaluate_events_origin_from_traces_list a7' trace_list
     

(* functions to evaluate the origin of places from the origin of events 
 * (this function must be called after the events origin evaluation) *)

(* val evaluate_places_origin_from_events_origin : AbtractSeven.t -> A7.t *)
     
let evaluate_places_origin_from_events_origin a7 =
  let se_list = get_events_filtered_by_origin a7 A7Element.Ispm in
  let se_urn_list = get_urn_list_from_elements se_list in
  let t_list = get_transitions_filtered_by_event_urn_list a7 se_urn_list in
  let p_urn_list = get_down_place_urn_list_from_elements t_list in
  let (p_ispm, p_ff) = get_places_partition_by_urn_list a7 p_urn_list in
  let mapping_ispm e = A7Element.set_origin e (Some A7Element.Ispm) in
  let mapping_ff e = A7Element.set_origin e (Some A7Element.Flipflop) in
  let p_ispm = List.rev_map mapping_ispm p_ispm in
  let p_ff = List.rev_map mapping_ff p_ff in
  let new_p_list = List.rev_append p_ispm p_ff in
  A7.set_place_list a7 new_p_list
  

(*-----------------------------------------------------------------
 * Function to order the #7 code for display in an editor
 *-----------------------------------------------------------------*)
     
(* function to obtain the children places, events or transitions 
 * of a list of places *)

let get_children_places_of_places_list a7 work_list =
  let mapping p = (get_children_places_of_place a7 p) in
  let work_list = List.rev_map mapping work_list in
  List.concat work_list

let get_children_events_of_places_list a7 work_list =
  let mapping p = (get_children_events_of_place a7 p) in
  let work_list = List.rev_map mapping  work_list in
  List.concat work_list

let get_children_transitions_of_places_list a7 work_list =
  let mapping p = (get_children_transitions_of_place a7 p) in
  let work_list = List.rev_map mapping  work_list in
  List.concat work_list
  

(* function to filter elements by level *)

let filter_elements_by_level elt_list level =
  let filter e =
    let (x, y) = (A7Element.get_location e) in x == level
  in
  List.filter filter elt_list
  
let filter_elements_with_undefined_level elt_list =
  filter_elements_by_level elt_list 0
  
  
(* function to update the location level of #7 elements *)

let update_level_of_element work_list level =
  let update_level e =
    let (x, y) = (A7Element.get_location e) in
    A7Element.set_location e (x, level)
  in
  List.rev_map update_level work_list

let rec compute_location_of_elements level rank work_list acc =
  let update_location e = A7Element.set_location e (rank, level) in
  match work_list with
  | [] -> acc
  | element::tail ->
     compute_location_of_elements
       level
       (rank + 1)
       tail
       ((update_location element)::acc)

let update_elements_location a7 e_list level =
  let f_compare a b = compare (A7Element.get_urn a) (A7Element.get_urn b) in
  let f_sort l = List.sort_uniq f_compare l in
  let work_list = f_sort e_list in
  let rank_init =
    match ((List.length work_list) mod 2) with
    | 0 -> 2
    | _ -> 1
  in
  compute_location_of_elements level rank_init work_list []
  
let update_location_of_places a7 work_list level =
  let e_updated = update_elements_location a7 work_list level in
  let urn_list = get_urn_list_from_elements e_updated in
  let (_, part_list) = get_places_partition_by_urn_list a7 urn_list in
  let new_list = List.rev_append part_list e_updated in
  A7.set_place_list a7 new_list
  
let update_location_of_events a7 work_list level =
  let work_list = get_children_events_of_places_list a7 work_list in 
  let e_updated = update_elements_location a7 work_list level in
  let urn_list = get_urn_list_from_elements e_updated in
  let (_, part_list) = get_events_partition_by_urn_list a7 urn_list in
  let new_list = List.rev_append part_list e_updated in
  A7.set_sensor_event_list a7 new_list
  
let update_location_of_transitions a7 work_list level =
  let work_list = get_children_transitions_of_places_list a7 work_list in 
  let e_updated = update_elements_location a7 work_list level in
  let urn_list = get_urn_list_from_elements e_updated in
  let (_, part_list) = get_transitions_partition_by_urn_list a7 urn_list in
  let new_list = List.rev_append part_list e_updated in
  A7.set_transition_list a7 new_list

let rec update_location_of_all_elements a7 work_list level =
  let work_list = filter_elements_with_undefined_level work_list in
  match work_list with
  | [] -> a7
  | _ ->
     let a7 = update_location_of_places a7 work_list level in
     let a7 = update_location_of_events a7 work_list (level + 1) in
     let a7 = update_location_of_transitions a7 work_list (level + 1) in
     let new_work_list = get_children_places_of_places_list a7 work_list in
     let next_level = level + 2 in
     update_location_of_all_elements a7 new_work_list next_level

         
(* val compute_and_update_elements_location : A7.t -> A7.t *)

let get_place_list_of_initial_places a7 = 
  let init_p_idref_list = get_init_place_urn_list a7 in
  let p_list = A7.get_place_list a7 in
  let filter p = List.mem (A7Element.get_urn p) init_p_idref_list in
  List.filter filter p_list
  
let compute_and_update_elements_location a7 =
  let work_list_init = get_place_list_of_initial_places a7 in
  let level_init = 1 in
  update_location_of_all_elements a7 work_list_init level_init
  

(*-----------------------------------------------------------------
 * Prints view on an abstract seven
 *-----------------------------------------------------------------*)

(* val print_places : out_channel -> abstract_seven -> unit *)
(* val print_sensor_events : out_channel -> abstract_seven -> unit *)
(* val print_transitions : out_channel -> abstract_seven -> unit *)
(* val print_initial_places : out_channel -> abstract_seven -> unit *)
(* val print_final_places : out_channel -> abstract_seven -> unit *)
(* val print : out_channel -> abstract_seven -> unit *)

let print_places c a7 =
  begin
    Printf.fprintf c "Places : \n";
    List.iter
      (fun pl ->
        let id = A7Element.get_id pl
        and label = A7Element.get_label pl in
	Printf.fprintf c "  (%s, %s)\n" id label)
      (get_place_list a7);
    Printf.fprintf c "\n"
  end

let print_sensor_events c a7 =
  begin
    Printf.fprintf c "Events / sensors : \n";
    List.iter
      (fun e ->
        let e_origin = match (A7Element.get_origin e) with
          | None -> "None"
          | Some A7Element.Ispm -> "iSPM"
          | Some A7Element.Flipflop -> "FlipFlop"
          | Some A7Element.Seven -> "Seven" in
        let e_five_relation = (A7Element.get_five_relation e) in
        let e_five_objects = match (A7Element.get_five_objects_list e) with
          | [] -> "[]"
          | obj_list ->
             "[ " ^
             String.concat
               " + "
               (List.map (function (a, b) -> a ^ "_" ^ b) obj_list)
             ^ " ]" in
	Printf.fprintf c "  (%s, %s, %s, %s, %s, %s)\n"
                       (A7Element.get_id e)
                       (string_of_int (A7Element.get_classifier_ref e))
                       e_origin
                       (A7Element.get_label e)
                       e_five_relation
                       e_five_objects
      )
      (get_sensor_event_list a7);
    Printf.fprintf c "\n"
  end

let print_transitions c a7 =
  begin
    Printf.fprintf c "Transitions : \n";
    List.iter
      (fun t ->
	Printf.fprintf c "  (%s, %s, %s, %s, %s)\n"
                       (A7Element.get_id t)
                       (A7Element.get_label t)
                       (A7Element.get_event_idref t)
                       (A7Element.get_up_place_idref t)
                       (A7Element.get_down_place_idref t))
      (get_transition_list a7);
    Printf.fprintf c "\n"
  end

let print_initial_places c a7 =
  begin
    Printf.fprintf c "Initial places : \n";
    List.iter
      (fun pl ->
	Printf.fprintf c "  %s\n" pl)
      (get_initial_place_idref_list a7);
    Printf.fprintf c "\n"
  end

let print_final_places c a7 =
  begin
    Printf.fprintf c "Final places : \n";
    List.iter
      (fun pl ->
	Printf.fprintf c "  %s\n" pl)
      (get_final_place_idref_list a7);
    Printf.fprintf c "\n"
  end

let print c a7 =
  begin
    print_places c a7;
    print_sensor_events c a7;
    print_transitions c a7;
    print_initial_places c a7;
    print_final_places c a7
  end
