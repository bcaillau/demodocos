(*
 *
 * Abstract structure of #SEVEN code
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* 
 * This module provides a data structure and functions related
 * to this structure. 
 *
 * The structure groups the data needed to generate #SEVEN code. 
 * It is created from a graph of reachable markings of the TF-net.
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities
 *-----------------------------------------------------------------*)

(* None *)
                        

(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)
  
(* type abstract #7 code *)
  
type t =
  {
    urn_mapping : A7UrnMapping.t;
    place_list : A7Element.t list;
    sensor_event_list : A7Element.t list;
    transition_list : A7Element.t list;
    flow_list : (int * int list) list;
    init_urn_list : int list;
    final_urn_list : int list
  }


(*-----------------------------------------------------------------
 * Constructor(s)
 *-----------------------------------------------------------------*)

(* val make : int -> abstract_seven *)
  
let make vector_dim =
  {
    urn_mapping = A7UrnMapping.make vector_dim;
    place_list = [];
    sensor_event_list = [];
    transition_list = [];
    flow_list = [];
    init_urn_list = [];
    final_urn_list = []
  }


(*-----------------------------------------------------------------
 * Accessors
 *-----------------------------------------------------------------*)

(* val get_urn_mapping : t -> A7UrnMapping.t *)
(* val get_place_list : t -> place list *)
(* val get_sensor_event_list : t -> sensor_event list *)
(* val get_transition_list : t -> transition list *)
(* val get_flow_list : t -> (int * int list) list *)
(* val get_init_urn_list : t -> int list *)
(* val get_final_urn_list : t -> int list *)
                        
let get_urn_mapping a7 = a7.urn_mapping
                        
let get_place_list a7 = a7.place_list

let get_sensor_event_list a7 = a7.sensor_event_list                     

let get_transition_list a7 = a7.transition_list                     

let get_flow_list a7 = a7.flow_list

let get_init_place_urn_list a7 = a7.init_urn_list                    

let get_final_place_urn_list a7 = a7.final_urn_list


(* val set_place_list : t -> A7Element.t list -> t *)
(* val set_sensor_event_list : t -> A7Element.t list -> t *)
(* val set_transition_list : t -> A7Element.t list -> t *)
(* val set_flow_list : t -> (int * int list) list -> t *)
(* val set_init_urn_list : t -> int list -> t *)
(* val set_final_urn_list : t -> int list -> t *)

let set_place_list a7 p_list =
  {
    urn_mapping = a7.urn_mapping;
    place_list = p_list;
    sensor_event_list = a7.sensor_event_list;
    transition_list = a7.transition_list;
    flow_list = a7.flow_list;
    init_urn_list = a7.init_urn_list;
    final_urn_list = a7.final_urn_list
  }

let set_sensor_event_list a7 se_list =
  {
    urn_mapping = a7.urn_mapping;
    place_list = a7.place_list;
    sensor_event_list = se_list;
    transition_list = a7.transition_list;
    flow_list = a7.flow_list;
    init_urn_list = a7.init_urn_list;
    final_urn_list = a7.final_urn_list
  }

let set_transition_list a7 t_list =
  {
    urn_mapping = a7.urn_mapping;
    place_list = a7.place_list;
    sensor_event_list = a7.sensor_event_list;
    transition_list = t_list;
    flow_list = a7.flow_list;
    init_urn_list = a7.init_urn_list;
    final_urn_list = a7.final_urn_list
  }

let set_flow_list a7 f_list =
  {
    urn_mapping = a7.urn_mapping;
    place_list = a7.place_list;
    sensor_event_list = a7.sensor_event_list;
    transition_list = a7.transition_list;
    flow_list = f_list;
    init_urn_list = a7.init_urn_list;
    final_urn_list = a7.final_urn_list
  }

let set_init_place_urn_list a7 ip_list =
  {
    urn_mapping = a7.urn_mapping;
    place_list = a7.place_list;
    sensor_event_list = a7.sensor_event_list;
    transition_list = a7.transition_list;
    flow_list = a7.flow_list;
    init_urn_list = ip_list;
    final_urn_list = a7.final_urn_list
  }

let set_final_place_urn_list a7 fp_list =
  {
    urn_mapping = a7.urn_mapping;
    place_list = a7.place_list;
    sensor_event_list = a7.sensor_event_list;
    transition_list = a7.transition_list;
    flow_list = a7.flow_list;
    init_urn_list = a7.init_urn_list;
    final_urn_list = fp_list
  }

  
(* val add_place : abstract_seven -> A7Element.t -> abstract_seven *)
(* val add_sensor_event : abstract_seven -> A7Element.t -> abstract_seven *)
(* val add_transition : abstract_seven -> A7Element.t -> abstract_seven *)
(* val add_initial_place : abstract_seven -> int -> abstract_seven *)
(* val add_final_place : abstract_seven -> int -> abstract_seven *)

let add_place a7 p =
  let p_list = get_place_list a7 in
  set_place_list a7 (p :: p_list)

let add_sensor_event a7 se =
  let se_list = get_sensor_event_list a7 in
  set_sensor_event_list a7 (se :: se_list)

let add_transition a7 t =
  let t_list = get_transition_list a7 in
  set_transition_list a7 (t :: t_list)

let add_init_place_urn a7 ip =
  let init_p_list = get_init_place_urn_list a7 in
  set_init_place_urn_list a7 (ip :: init_p_list)

let add_final_place_urn a7 fp =
  let final_p_list = get_final_place_urn_list a7 in
  set_final_place_urn_list a7 (fp :: final_p_list)


(*-----------------------------------------------------------------
 * Functions to obtain the list of a given data from a list of elements
 *-----------------------------------------------------------------*)

(* val get_urn_list_from_elements : A7Element.t list -> int list *)
(* val get_event_urn_list_from_elements : A7Element.t list -> int list *)
(* val get_up_place_urn_list_from_elements : A7Element.t list -> int list *)
(* val get_down_place_urn_list_from_elements : A7Element.t list -> int list *)

let get_urn_list_from_elements e_list =
  let mapping e = A7Element.get_urn e in
  List.rev_map mapping e_list

let get_event_urn_list_from_elements e_list =
  let mapping e = A7Element.get_event_urn e in
  List.rev_map mapping e_list

let get_up_place_urn_list_from_elements e_list =
  let mapping e = A7Element.get_up_place_urn e in
  List.rev_map mapping e_list

let get_down_place_urn_list_from_elements e_list =
  let mapping e = A7Element.get_down_place_urn e in
  List.rev_map mapping e_list


(*-----------------------------------------------------------------
 * Functions to obtain lists of elements, filtered by a given data
 *-----------------------------------------------------------------*)

(* val get_places_filtered_by_urn_list : t -> int list -> A7Element.t list *)
(* val get_events_filtered_by_urn_list : t -> int list -> A7Element.t list *)
(* val get_transitions_filtered_by_urn_list : t -> int list -> A7Element.t list *)

let get_elements_filterd_by_urn_list e_list urn_list =
  let filter e = List.mem (A7Element.get_urn e) urn_list in
  List.filter filter e_list
  
let get_places_filtered_by_urn_list a7 urn_list =
  let e_list = get_place_list a7 in
  get_elements_filterd_by_urn_list e_list urn_list
  
let get_events_filtered_by_urn_list a7 urn_list =
  let e_list = get_sensor_event_list a7 in
  get_elements_filterd_by_urn_list e_list urn_list
  
let get_transitions_filtered_by_urn_list a7 urn_list =
  let e_list = get_transition_list a7 in
  get_elements_filterd_by_urn_list e_list urn_list


(* val get_places_partition_by_urn_list : t -> int list -> (A7Element.t list * A7Element.t list) *)
(* val get_events_partition_by_urn_list : t -> int list -> (A7Element.t list * A7Element.t list) *)
(* val get_transitions_partition_by_urn_list : t -> int list -> (A7Element.t list * A7Element.t list) *)

let get_elements_partition_by_urn_list e_list urn_list =
  let filter e = List.mem (A7Element.get_urn e) urn_list in
  List.partition filter e_list

let get_places_partition_by_urn_list a7 urn_list =
  let e_list = get_place_list a7 in
  get_elements_partition_by_urn_list e_list urn_list
  
let get_events_partition_by_urn_list a7 urn_list =
  let e_list = get_sensor_event_list a7 in
  get_elements_partition_by_urn_list e_list urn_list
  
let get_transitions_partition_by_urn_list a7 urn_list =
  let e_list = get_transition_list a7 in
  get_elements_partition_by_urn_list e_list urn_list
  

(* val get_events_filtered_by_origin : t -> A7Element.e_origin -> A7Element.t list *)
  
let get_events_filtered_by_origin a7 origin =
  let se_list = get_sensor_event_list a7 in
  let filter se = ((A7Element.get_origin se) = (Some A7Element.Ispm)) in
  List.filter filter se_list
  

(* val get_transitions_filtered_by_event_urn_list : t -> int list -> A7Element.t list *)
(* val get_transitions_filtered_by_up_place_urn_list : t -> int list -> A7Element.t list *)
(* val get_transitions_filtered_by_down_place_urn_list : t -> int list -> A7Element.t list *)

let get_transitions_filtered_by_event_urn_list a7 urn_list =
  let t_list = get_transition_list a7 in
  let filter t = List.mem (A7Element.get_event_urn t) urn_list in
  List.filter filter t_list

let get_transitions_filtered_by_up_place_urn_list a7 urn_list =
  let t_list = get_transition_list a7 in
  let filter t = List.mem (A7Element.get_up_place_urn t) urn_list in
  List.filter filter t_list

let get_transitions_filtered_by_down_place_urn_list a7 urn_list =
  let t_list = get_transition_list a7 in
  let filter t = List.mem (A7Element.get_down_place_urn t) urn_list in
  List.filter filter t_list
  

(*-----------------------------------------------------------------
 * Functions to obtain the children of a given element
 *-----------------------------------------------------------------*)

(* val get_children_places_of_place : t -> A7Element.t -> A7Element.t list *)
(* val get_children_events_of_place : t -> A7Element.t -> A7Element.t list *)
(* val get_children_transitions_of_place : t -> A7Element.t -> A7Element.t list *)
(* val get_children_places_of_transition : t -> A7Element.t -> A7Element.t list *)

let get_children_of_element a7 e =
  let e_type = A7Element.get_type e in
  let e_urn = A7Element.get_urn e in
  let filter (target, s_list) = (target = e_urn) in  
  let (_, s_urn_list) = List.find filter (get_flow_list a7) in
  let filter e = List.mem (A7Element.get_urn e) s_urn_list in
  match e_type with
  | A7Element.Place -> List.filter filter (get_transition_list a7)
  | _ -> List.filter filter (get_place_list a7)

let get_children_places_of_place a7 e =
  let t_list = get_children_of_element a7 e in
  let mapping t = A7Element.get_down_place_urn t in
  let p_urn_list = List.map mapping t_list in
  let filter p = List.mem (A7Element.get_urn p) p_urn_list in
  List.filter filter (get_place_list a7)

let get_children_events_of_place a7 e =
  let t_list = get_children_of_element a7 e in
  let mapping t = A7Element.get_event_urn t in
  let se_urn_list = List.map mapping t_list in
  let filter se = List.mem (A7Element.get_urn se) se_urn_list in
  List.filter filter (get_sensor_event_list a7)

let get_children_transitions_of_place a7 e =
  get_children_of_element a7 e

let get_children_places_of_transition a7 e =
  get_children_of_element a7 e


(*-----------------------------------------------------------------
 * Functions to replace a place, an event or a transition on a list
 * of places, events or transitions, according by urn
 *-----------------------------------------------------------------*)

(* val replace_element : A7Element.t -> A7Element.t list -> A7Element.t list *)

let replace_element new_e e_list =
  let f_get_urn = A7Element.get_urn in
  let filter e = not ((f_get_urn new_e) = (f_get_urn e)) in
  let e_list = List.filter filter e_list in
  new_e::e_list


(*-----------------------------------------------------------------
 * Element specification as string
 *-----------------------------------------------------------------*)

let get_initial_place_idref_list a7 =
  let urn_list = get_init_place_urn_list a7 in
  let mapping = A7Element.get_id_string A7Element.Place in
  List.map mapping urn_list

let get_final_place_idref_list a7 =
  let urn_list = get_final_place_urn_list a7 in
  let mapping = A7Element.get_id_string A7Element.Place in
  List.map mapping urn_list
