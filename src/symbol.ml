(*
 *
 * Symbol table and mapping to integers
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA Rennes, France
 *
 *)

module type SYMBOL_TYPE =
  sig
    type t
    val equal : t -> t -> bool (* test equality *)
    val hash : t -> int (* hashing function *)
    val compare : t -> t -> int (* comparison function *)
    val to_string : t -> string (* generates a string *)
    val print : out_channel -> t -> unit (* prints to an channel *)
    val to_xml : t -> Xml.xml (* generates a XML document *)
    val of_xml : Xml.xml -> t (* parses a XML document *)
  end

module type TABLE_TYPE =
  sig
    type s (* Symbols *)
    type t (* Tables *)
    val make : unit -> t (* Makes an empty open symbol table *)
    val add : t -> s -> int (* add a symbol and assigns an integer unique id *)
    val retrieve : t -> s -> int (* retrieve the unique id of a symbol. Raises [Not_found] if unknown *)
    val resolve : t -> int -> s (* retrieves a symbol from its unique id. Raises [Invalid_argument _] if out of bounds *)
    val length : t -> int (* Length of the table *)
    val freeze : t -> unit (* Optimizes the table so that resolution is done in constant time *)
    val iter : (int -> s -> unit) -> t -> unit (* imperative iterator *)
    val fold : (int -> s -> 'a -> 'a) -> t -> 'a -> 'a (* functional iterator *)
    val to_xml : t -> Xml.xml (* turns the table into an XML document *)
    val of_xml : Xml.xml -> t (* parses an XML document and generates an open symbol table *)
  end

let prime_seed = 257

module Table = functor (S : SYMBOL_TYPE) ->
struct

  module Hash = Hashtbl.Make(S)

  type s = S.t

  type w =
      Open of (int, s) Hashtbl.t
      | Closed of s array

  type t =
      {
	mutable length : int;
	to_int : int Hash.t;
	mutable to_symbol : w
      }

  let make () =
    {
      length = 0;
      to_int = Hash.create prime_seed;
      to_symbol = Open (Hashtbl.create prime_seed)
    }

  let add =
    function
	{ to_symbol = Closed _ } -> invalid_arg "Symbol.Table.add: can not add symbol to a closed table"
      | { length=n; to_int = h0; to_symbol = Open h1 } as w ->
	  fun s ->
	    try
	      Hash.find h0 s
	    with
		Not_found ->
		  begin
		    Hash.add h0 s n;
		    Hashtbl.add h1 n s;
		    w.length <- n+1;
		    n
		  end

  let retrieve { to_int = h } s = Hash.find h s

  let resolve =
    function
	{ length=n; to_symbol=Closed a } ->
	  begin
	    fun i ->
	      if (i < 0) || (i >= n)
	      then raise Not_found
	      else a.(i)
	  end
      | { to_symbol=Open h } ->
	  begin
	    fun i ->
	      Hashtbl.find h i
	  end

  let length { length=n } = n

  let freeze =
    function
	{ to_symbol = Closed _ } -> ()
      | { length = n; to_symbol = Open h } as w ->
	  let a =
	    Array.init
	      n
	      (fun i ->
		try
		  Hashtbl.find h i
		with
		    Not_found ->
		      failwith "Symbol.freeze: internal error. Please report bug")
	  in
	      w.to_symbol <- Closed a

  let iter foo =
    function
	{ length = n; to_symbol = Closed a } ->
	  for i=0 to n-1 do
	    foo i a.(i)
	  done
      | { length = n; to_symbol = Open h } ->
	  for i=0 to n-1 do
	      foo
		i
		(try
		    Hashtbl.find h i
		  with
		      Not_found ->
			failwith "Symbol.iter: internal error. Please report bug")
	  done

  let fold foo w x0 =
    let x = ref x0 in
      begin
	iter
	  (fun i s ->
	    x := foo i s (!x))
	  w;
	!x
      end

  let to_xml w =
    let l =
      fold
	(fun _ s k -> (S.to_xml s) :: k)
	w
	[]
    in
      Xml.Element ("SymbolTable",[],l)

  let of_xml =
    function
	Xml.Element ("SymbolTable",_,l) ->
	  let w = make () in
	    begin
	      List.iter
		(fun d ->
		  let s = S.of_xml d in
		    ignore (add w s))
		l;
	      w
	    end
      | _ -> invalid_arg "Symbol.of_xml: not a valid XML symbol table"
end

