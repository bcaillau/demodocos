(*
 *
 * Processes
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2012
 *
 *)

type 'a t =
    {
      children : 'a s array;
      parent : 'a p;
      mutable label : 'a
    }
      
and 'a s = Void | Def of 'a t

and 'a p = ('a t * int) option

(* creates an empty process *)

(* val make : int -> 'a -> 'a t *)

let make n x =
  {
    children = Array.make n Void;
    parent = None;
    label = x
  }

(* Deep copy of a process *)

(* val clone : ('a -> 'b) -> 'a t -> 'b t *)

let clone f =
  function { children=p0 } as n0 ->
    let d = Array.length p0 in
    let rec clone_down { children=p; label=x } a =
      let q = Array.make d Void in
      let n = { children=q; parent=a; label=f x } in
	begin
	  for i=0 to d-1 do
	    match p.(i) with
		Void -> ()
	      | Def n' ->
		  q.(i) <- Def (clone_down n' (Some (n,i)))
	  done;
	  n
	end
    and clone_up =
      function
	  { parent = None } as n -> clone_down n None
	| { parent = Some (n,i) } ->
	    let n' = clone_up n in
	      match n'.children.(i) with
		  Void -> failwith "Process.clone: impossible case. Please report bug"
		| Def n'' -> n''
    in
      clone_up n0

(* [dimension n] returns the dimension of process [n] *)

(* val dimension : 'a t -> int *)

let dimension { children=p } = Array.length p

(* adds an edge to a new process if not already there *)

(* val add : (unit -> 'a) -> 'a t -> int -> 'a t *)

let add f ({ children=p } as n)  e =
  let d = Array.length p in
    if (0 <= e) && (e < d)
    then
      match p.(e) with
	  Void ->
	    let q = Array.make d Void
	    and x = f () in
	    let n' = { children=q; parent=Some (n,e); label=x } in
	      begin
		p.(e) <- Def n';
		n'
	      end
	| Def n' -> n'
    else
      invalid_arg "Process.add: Invalid event"

(* derivates a process, raises [Not_found] if undefined *)

(* val der : 'a t -> int -> 'a t *)

let der { children=p } e =
  let d = Array.length p in
    if (0 <= e) && (e < d)
    then
      match p.(e) with
	  Void -> raise Not_found
	| Def n -> n
    else
      invalid_arg "Process.der: Invalid event"

(* tests if a process is derivable by a given event *)

(* val is_der : 'a t -> int -> bool *)

let is_der { children=p } e =
  let d = Array.length p in
    if (0 <= e) && (e < d)
    then
      match p.(e) with
	  Void -> false
	| Def _ -> true
    else
      invalid_arg "Process.is_der: Invalid event"

(* tests if a process is a root *)

(* val is_root : 'a t -> bool *)

let is_root =
  function
      { parent = None } -> true
    | { parent = Some _ } -> false

(* returns the parent process. Raises Not_found if the process is a root *)

(* val parent : 'a t -> 'a t *)

let parent =
  function
      { parent = None } -> raise Not_found
    | { parent = Some (n,_) } -> n

(* returns the label of the ingoing edge, raises Not_found if node is root *)

(* val edge : 'a t -> int *)

let edge =
  function
      { parent = None } -> raise Not_found
    | { parent = Some (_,e) } -> e

(* returns the root of a process *)

(* val root : 'a t -> 'a t *)

let rec root =
  function
      { parent = None } as n -> n
    | { parent = Some (n,_) } -> root n

(* retrieves the label of a process *)

(* val get : 'a t -> 'a *)

let get = function { label=x } -> x

(* sets the label of a process *)

(* val set : 'a t -> 'a -> unit *)

let set n x = n.label <- x

(* in place or/and of a vector with the readyset of a process *)

(* val ior_ready : Vector.t -> 'a t -> unit *)

let ior_ready v n =
  let d = Vector.length v in
    for e=0 to d-1 do
      if is_der n e then Vector.set v e true
    done

(* val iand_ready : Vector.t -> 'a t -> unit *)

let iand_ready v n =
  let d = Vector.length v in
    for e=0 to d-1 do
      if not (is_der n e) then Vector.set v e false
    done
