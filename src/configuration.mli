(*
 *
 * Configuration of TnF Tool 
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* 
 * This module provides a type configuration used on tool
 * and some functions directly or indirectly called by a command
 *) 

(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

(* config file definition *)

val main_config_file_name : string
val tag_config_file_name : string -> string
val classifiers_config_file_name : string


(* list of recognized source formats *)

val valid_src_formats_list : string list
  

(* type classifiers_config *)

type classifiers_config
val default_classifiers_config : classifiers_config
  

(* type apx_config *)

type apx_config  
val default_apx_config : apx_config
val quotient_apx_config : apx_config


(* control variables for configuration parameters *)

val default_ontology_global_files : string list (* empty list *)
  
val valid_ontology_label_type : string list (* iri_segment, label *)
val default_ontology_label_type : string (* label *)

val valid_verbose_list : int list
val default_verbose : int

val valid_tnfgraph_label_type : string list (* letter, number, label *)
val default_tnfgraph_label_type : string (* letter *)

val valid_seven_sensor_label_type : string list (* letter, number, onto_label, five_ref *)
val default_seven_sensor_label_type : string (* onto_label *)

val valid_seven_five_value_type : string list (* onto_label, five_ref *)
val default_seven_five_value_type : string (* five_ref *)
  

(* type configuration *)

type t
val default_t : t
  

   
(*-----------------------------------------------------------------
 * Constructor(s)
 *-----------------------------------------------------------------*)

(* function to initialize a configuration with default value *)
  
val init : t -> unit
  

(* functions to initialize the lists of classifier config *)

val init_classifier_prefix_list : t -> unit
val init_classifier_keys_list : t -> unit
val init_classifier_def_list : t -> unit

  
  
(*-----------------------------------------------------------------
 * Accessors
 *-----------------------------------------------------------------*)

(* Accessors of synthesis configuration *)

val get_input_file_name : t -> string
val get_input_working_file_postfix : t -> string
val get_input_adapting_script : t -> string
val get_classifier_choice : t -> string
val get_ontology_base_file : t -> string option
val get_ontology_global_files_list : t -> string list
val get_ontology_label_type : t -> string
val get_instances_number : t -> int option
val get_verbose : t -> int

val set_input_file_name : t -> string -> unit
val set_input_working_file_postfix : t -> string -> unit
val set_input_adapting_script : t -> string -> unit
val set_classifier_choice : t -> string -> unit
val set_ontology_base_file : t -> string option -> unit
val set_ontology_global_files_list : t -> string list -> unit
val set_ontology_label_type : t -> string -> unit
val set_instances_number : t -> int option -> unit
val set_verbose : t -> int -> unit
  
val add_ontology_global_file : t -> string -> unit  
val remove_ontology_global_file : t -> string -> unit
  
  
(* Accessors of approximation configuration *)
  
val is_approximation_requested : t -> bool
val get_approximation_limit : t -> int
val get_approximation_count : t -> int

val set_approximation_requested : t -> bool -> unit
val set_approximation_limit : t -> int -> unit
val set_approximation_count : t -> int -> unit

val set_apx_config : t -> apx_config -> unit
val set_direct_apx_config : t -> bool -> int -> int -> unit


(* Accessors for global classifiers *)

val get_classifier_separator : t -> string
val get_classifier_prefix_list : t -> (string * string) list
val get_classifier_keys_list : t -> (string * string list) list
val get_classifier_def_list : t -> (string * string * string list) list

val set_classifier_separator : t -> string -> unit
val set_classifier_prefix_list : t -> (string * string) list -> unit
val set_classifier_keys_list : t -> (string * string list) list -> unit
val set_classifier_def_list : t -> (string * string * string list) list -> unit

val add_classifier_prefix : t -> string -> string -> unit 
val add_classifier_keys : t -> string -> string list -> unit  
val add_classifier_def : t -> (string * string * string list) -> unit

val remove_classifier_def : t -> string -> unit


(* Accessors of output configuration *)
  
val get_output_directory : t -> string option
val get_output_tnfnet : t -> string option
val get_output_tnfgraph : t -> string option
val get_output_seven : t -> string option
val get_output_a7xspm : t -> string option
val get_output_events_table : t -> string option
(* val get_output_tnfnet_display : t -> bool *)

val set_output_directory : t -> string option -> unit
val set_output_tnfnet : t -> string option -> unit
val set_output_tnfgraph : t -> string option -> unit
val set_output_seven : t -> string option -> unit
val set_output_a7xspm : t -> string option -> unit
val set_output_events_table : t -> string option -> unit
(* val set_output_tnfnet_display : t -> bool -> unit *)

  
(* Accessors for options of output computing *)
  
val get_tnfgraph_label_type : t -> string
val get_seven_sensor_label_type : t -> string
val get_seven_five_value_type : t -> string
val get_seven_location_computing : t -> bool
val get_seven_analysis : t -> bool
val get_seven_event_number_limit : t -> int
  
val set_tnfgraph_label_type : t -> string -> unit
val set_seven_sensor_label_type : t -> string -> unit
val set_seven_five_value_type : t -> string -> unit
val set_seven_location_computing : t -> bool -> unit
val set_seven_analysis : t -> bool -> unit
val set_seven_event_number_limit : t -> int -> unit

  

(*-----------------------------------------------------------------
 * Function to obtain a list of generic classifiers
 *-----------------------------------------------------------------*)

(* function to obtain a list of generic classifiers corresponding to
 * a given group (corresponding to the format of source) *)
                  
val get_classifiers_list_of_group : t -> string -> (string * string list) list

  
(* function to add a list of classifiers def *)
  
(* val add_classifiers_list_of_group : t -> (string * string * string list) list -> unit *)



(*-----------------------------------------------------------------
 * Function to obtain a script
 *-----------------------------------------------------------------*)  

(* function to obtain the script name for a given operation *)
  
val get_script : (string * string * string) -> string



(*-----------------------------------------------------------------
 * Function to obtain a target file
 *-----------------------------------------------------------------*)  

(* function to obtain the target file of a given target type *)
  
val get_target_file : t -> string -> string

  
  
(*-----------------------------------------------------------------
 * Function to analyze the context of synthesis
 *-----------------------------------------------------------------*)  

val is_tfnet_synthesis : t -> bool

val is_graph_synthesis_only : t -> bool
    
val is_region_adding_requested : t -> bool

val is_a7xspm_generating_possible : t -> int -> bool


  
(*-----------------------------------------------------------------
 * Function to manage the configuration of approximation/optimization
 *-----------------------------------------------------------------*)  

(* 
 * The approximation config indicates whether the synthesis has to be optimized 
 * and gives the optimization limit.
 * 
 * The synthesis has to be optimized with an approximation algorithm if the
 * approximation is authorized and if the optimization is requested. The approximation 
 * must not be allowed to generate a T/F net, but it can be allowed to generate a graph.
 *
 * The optimization limit is the maximum computation depth before applying 
 * the optimization. 
 *)

val is_approximation_limit : t -> bool
    
val update_approximation_limit_after_computation : t -> unit
  

  
(*-----------------------------------------------------------------
 * Prints view on configuration
 *-----------------------------------------------------------------*)

val print_approximation_config : out_channel -> t -> unit 
