(* filters messages, depending on the verbosity level *)

let lvl = ref 1

(* val init : unit -> unit *) (* sets verbosity to default value = 1 *)

let init () = lvl := 1

(* val set : int -> unit *)

let set n = lvl := n

(* val get : unit -> int *)

let get () = !lvl

(* val filter : int -> ('a -> 'b) -> 'a -> 'b -> 'b *)

let filter n foo x y =
  if n <= (!lvl) then foo x else y

(* val cond : int -> (unit -> unit) -> unit *)

let cond n foo = filter n foo () ()
