(*
 *
 * Safe_of_ff
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2016
 *
 *
 * Transforms a TF net into a safe net. The safe net is represented as a particular
 * class of TF, with complementary places and no complementation flow arc.
 *
 *)

(*
 *
 * [compl_count_event n i] computes the number of complementation flow arcs for event [i] in TF net [n].
 * [i] must be greater or equal to zero and strictly less than the dimension of [n], otherwise an
 * [Invalid_argument _] exception is raised.
 *
 *)

(* val compl_count_event : Region.n -> int -> int *)

let compl_count_event n e =
  try
    List.fold_right
      (fun r k ->
	match Region.symbol_of_event r e with
	    Region.Compl -> k+1
	  | _ -> k)
      n
      0
  with
      Invalid_argument s -> raise (Invalid_argument ("Safe_of_ff.compl_count_event: "^s))

(*
 *
 * [compl_count_net n] computes the number of complementation flow arcs for each event in TF net [n].
 * Returns an array [x] sur that [x.[e]] is the number of complementation flow arcs for event [e].
 * Raises [Invalid_argument _] if [n] has no places.
 *
 *)

(* val compl_count_net : Region.n -> int array *)

let compl_count_net n =
  let d = Region.net_dimension n in
    Array.init
      d
      (compl_count_event n)

(*
 *
 * Datastructure for mapping events to safe net transitions and vice-versa
 *
 *)

type 'a t =
    {
      events : int;
      transitions : int;
      places : int; (* number of places, not counting the complementary places *)
      labeling : int array; (* maps transitions to events *)
      ontology : 'a array; (* maps events to instances of the ontology *)
      first : int array; (* first transition labeled by an event *)
      count : int array; (* number of complementations for an event *)
      compl : int list array; (* list of regions complemented by each event *)
      rank : int array array; (* rank matrix of complementation events *)
      (* [t.rank.(i).(e)] is the rank of event [e] wrt region [i] *)
      network : Region.r array (* array of regions *)
    }

(*
 *
 * Utilities for initializing the datastructure
 *
 *)

let max_trans = max_int asr 1

let rec log2 x =
  if x <= 0 then 0 else 1+(log2 (x asr 1))

let exp2 x = 1 lsl x

let max_compl_count = log2 max_trans 

let transition_count a =
    Array.fold_right
      (fun c k ->
	if (k > max_trans) || (c > max_compl_count)
	then raise (Failure "Safe_of_ff: too many transitions")
	else k + (exp2 c))
      a
      0

(*
 *
 * Initializes the mapping datastructure.
 *
 *)

(* val init : Region.n -> t *)

let init onto net =
  let d = Region.net_dimension net in
  let cnt = compl_count_net net in
  let m = transition_count cnt
  and n = List.length net in
  let lbl = Array.make m 0
  and frst = Array.make d 0
  and cpl = Array.make d []
  and rnk = Array.make_matrix n d  (-1)
  and ntw = Array.of_list net in
  let j = ref 0 in
    begin
      for i=0 to d-1 do
	let w = exp2 cnt.(i) in
	  begin
	    frst.(i) <- !j;
	    for k=(!j) to (!j)+w-1 do
	      lbl.(k) <- i
	    done;
	    j := (!j) + w
	  end
      done;
      for p=n-1 downto 0 do
	j := 0;
	for e=0 to d-1 do
	  match Region.symbol_of_event ntw.(p) e with
	      Region.Compl ->
		begin
		  cpl.(e) <- p :: cpl.(e);
		  rnk.(p).(e) <- (!j);
		  j := (!j)+1
		end
	    | _ -> ()
	done
      done;
      {
	events = d;
	transitions = m;
	places = n;
	labeling = lbl;
	ontology = onto;
	first = frst;
	count = cnt;
	compl = cpl;
	rank = rnk;
	network = ntw
      }
    end

(* Utilities for safe_place_of_tf *)

(* complement_flow *)

let complement_flow =
  function
      Region.Ortho -> Region.Ortho
    | Region.Exit -> Region.Enter
    | Region.Enter -> Region.Exit
    | Region.Compl -> invalid_arg "Safe_of_ff.complement_flow: complementation flow arcs can not be complemented"
    | Region.True -> Region.False
    | Region.False -> Region.True

(*
 *
 * Safe (or its complementary) place of TF region
 *
 *)

(* val safe_place_of_tf : 'a t -> int (* region index *) -> bool (* false = positive, true = complementary *) -> Region.r *)

let safe_place_of_tf t x p (* false for place and true for complementary place *) =
  let r = t.network.(x)
  and s = Region.null t.transitions in
    begin
      (* same initial marking *)
      Region.set_m0
	s
	(let b = Region.get_m0 r in
	  if p then not b else b);
      (* copy flow arcs, except for complementation flow arcs, *)
      (* where things are more involved *)
      for e=0 to t.events-1 do
	match Region.symbol_of_event r e with
	    Region.Compl ->
	      let w = t.rank.(x).(e) in
	      for i=0 to (exp2 t.count.(e))-1 do
		let q = (((i lsr w) land 1) = 1) in
		Region.set_symbol
		  s
		  (t.first.(e)+i)
		  (if p
		  then if q then Region.Enter else Region.Exit
		    else if q then Region.Exit else Region.Enter)
	      done
	  | w ->
	      for f=t.first.(e) to t.first.(e)+(exp2 t.count.(e))-1 do
		Region.set_symbol s f (if p then complement_flow w else w)
	      done
      done;
      s
    end

(* Safe net of a TF net *)

(* val safe_net_of_tf : 'a array -> Region.n -> ('a array) * Region.n *)

let safe_net_of_tf onto net =
  let t = init onto net in
  let net' = ref []
  and onto' =
    Array.init
      t.transitions
      (fun e ->
	let f = t.labeling.(e) in 
	  t.ontology.(f)) in
    begin
      for i=0 to t.places-1 do
	net' := (safe_place_of_tf t i true) :: (safe_place_of_tf t i false) :: (!net')
      done;
      (onto' , (!net'))
    end
