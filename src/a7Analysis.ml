(*
 *
 * Module to analyse an abstract structure of #SEVEN code
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* 
 * This module provides functions to analyse an abstract structure
 * of #SEVEN code (a7), and to obtain information about the code. 
 *)

(*-----------------------------------------------------------------
 * Basic Analysis
 *-----------------------------------------------------------------*)

let get_places_number a7 = List.length (A7.get_place_list a7)
let get_events_number a7 = List.length (A7.get_sensor_event_list a7)
let get_transitions_number a7 = List.length (A7.get_transition_list a7)

            
(*-----------------------------------------------------------------
 * Function to compute the number of realizations of a given scenario, 
 * ie the number of simple paths starting from the initial state 
 * and ending in a final state
 *-----------------------------------------------------------------*)
      
(* function to obtain the number of simple paths on an abstract seven,
 * starting from a given place *) 
let rec get_number_of_simple_paths_from_place a7 fp_urn_list places_of_path place =
  A7AnalysisPathsNumber.get_number_from_given_place a7 fp_urn_list places_of_path place

           
(* function to obtain the number of simple path on an abstract seven,
 * starting from initial places *)       
let get_number_of_simple_paths_from_initial_places a7 =
  A7AnalysisPathsNumber.get_number_from_initial_places a7


(*-----------------------------------------------------------------
 * Functions to obtain the list of simple paths starting from the 
 * initial state and ending in a final state
 *-----------------------------------------------------------------*)
  
(* function to obtain event paths starting from initial places,
 * each event representing by his urn *)
  
let get_e_paths_from_initial_places a7 =
  (* A7AnalysisPaths1.get_e_paths_from_initial_places a7 *)
  A7AnalysisPaths2.get_e_paths_from_initial_places a7

  
(* function to obtain event paths starting from initial places,
 * each event representing by his urn *)
  
let get_label_e_paths_from_initial_places a7 =
  A7AnalysisPaths2.get_label_e_paths_from_initial_places a7
  
       
(*-----------------------------------------------------------------
 * Analysis of inclused traces
 *-----------------------------------------------------------------*)       

(* function to compute the number of inclusion starting 
 * from a list of traces *)
  
let evaluate_inclusion_number a7 test_traces =
  A7AnalysisInclusedTraces.evaluate a7 test_traces
