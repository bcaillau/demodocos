(*
 *
 *  Doc Format of Configuration in xml format
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* This module provides standard text corresponding to DOC
 * format in xml format.
 *)

(*-----------------------------------------------------------------
 *  Parameter name (string)
 *-----------------------------------------------------------------*)

val mainconfig_root_param : string                          
val classifiers_root_param :  string
val classifiers_src_format_param : string -> string



(*-----------------------------------------------------------------
 * Functions to generates doc containing mutual config text
 *-----------------------------------------------------------------*)

(* Xml prologue *)

val xml_prologue_text : Pp.doc

  

(*-----------------------------------------------------------------
 * Functions to generates doc containing specific main config text 
 *-----------------------------------------------------------------*)

(* Text of root tag *)

val mainconfig_root_text : Pp.doc -> Pp.doc
  

(* Synthesis configuration *)

val input_working_file_postfix_text : string -> Pp.doc
val input_adapting_script_text : string -> Pp.doc
val classifier_choice_text : string -> Pp.doc
val ontology_base_file_text : string -> Pp.doc
val ontology_global_files_text : string -> Pp.doc
val ontology_label_type_text : string -> Pp.doc
val instances_number_text : string -> Pp.doc
val verbose_text : string -> Pp.doc
  

(* Approximation configuration *)
  
val approximation_requested_text : string -> Pp.doc
val approximation_limit_text : string -> Pp.doc


(* Output configuration *)

val output_directory_text : string -> Pp.doc
val output_tnfnet_text : string -> Pp.doc
val output_tnfgraph_text : string -> Pp.doc
val output_seven_text : string -> Pp.doc
val output_a7xspm_text : string -> Pp.doc
val output_events_table_text : string -> Pp.doc
(* val output_tnfnet_display_text : string -> Pp.doc *)


(* Options for output computing (and analysis) *)

val tnfgraph_label_type_text : string -> Pp.doc
val seven_sensor_label_type_text : string -> Pp.doc
val seven_five_value_type_text : string -> Pp.doc
val seven_location_computing_text : string -> Pp.doc
val seven_analysis_text : string -> Pp.doc
val seven_event_number_limit_text : string -> Pp.doc
  
  
  
(*-----------------------------------------------------------------
 * Functions to generates doc containing specific classifier config
 * text 
 *-----------------------------------------------------------------*)
                      
(* Text of root tag *)

val classifiers_root_text : Pp.doc -> Pp.doc
  

(* Text of classifier configuration data *)

val separator_text : string -> Pp.doc
val prefix_text : string -> string -> Pp.doc
val keys_text : string -> string list -> string -> Pp.doc
val classifier_text : string -> string -> string list -> string -> Pp.doc 
