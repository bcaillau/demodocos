(*
 *
 * Module to load an ontology basis 
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * The main function of this module is to load an ontology basis from an ontology base file.
 *)        
                  
(*-----------------------------------------------------------------
 * Function to load an ontology basis
 *-----------------------------------------------------------------*)

(* function to load the ontology basis from an ontology basis file *)
  
val load_initial_basis : string -> DataOntologyBasis.t

  
                  
(*-----------------------------------------------------------------
 * Function to update an ontology basis
 *-----------------------------------------------------------------*)
  
(* function to load additional data on ontology basis, from a global ontology file *)

val load_additional_data_on_basis : string -> DataOntologyBasis.t -> DataOntologyBasis.t
