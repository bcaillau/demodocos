(*
 *
 * Process used to execute an operation from XES file
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, Inria, Rennes, France, 2017
 *
 *)

(* 
 * This module provides functions to execute the different stage/step 
 * of an operation from XES file. 
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

open Configuration

let rec css_of_list =
  function
      [] -> ""
    | [s] -> s
    | s::l -> s ^ ", " ^ (css_of_list l)

let rec log_dim =
  function
      [] -> 0
    | t::l -> max (trace_dim t) (log_dim l)

and trace_dim =
  function
      [] -> 0
    | i::l -> max (i+1) (trace_dim l)
            
let err_report_and_exit err_msg arg_msg =
  let default_error_code = 10 in
  begin
    Reporting.error_report err_msg arg_msg;
    exit default_error_code
  end

let execute_step s_name s_function s_param =
  try
    begin
      Reporting.init_step s_name;
      s_function s_param;
      Reporting.end_step s_name
    end
  with _ ->
    (err_report_and_exit "Problem during the execution of the following step: " s_name)

let execute_step_with_result s_name s_function s_param =
  begin
    Reporting.init_step s_name;
    let result = s_function s_param in
    begin
      Reporting.end_step s_name;
      result
    end
  end

(*-----------------------------------------------------------------
 * Functions to evaluate the traces using for the synthesis
 * and the traces using for inclusion tests
 *-----------------------------------------------------------------*)

let rec get_n_traces traces n =
  if (List.length traces < n)
  then traces
  else
    if n > 0
    then (List.hd traces)::(get_n_traces (List.tl traces) (n - 1))
    else []
      
let evaluate_synthesis_traces config traces =
  match (Configuration.get_synthesis_traces_number config) with
  | None -> traces
  | Some n -> get_n_traces traces n

let evaluate_test_traces config traces =
  match (Configuration.get_synthesis_traces_number config) with
  | None -> []
  | Some n -> get_n_traces (List.rev traces) ((List.length traces) - n)
       

(*-----------------------------------------------------------------
 * Function to execute the different stage of synthesis algorithm 
 *-----------------------------------------------------------------*)

(* function to initialize the synthesis *)
            
let initialize_synthesis config traces =
  let d = log_dim traces in
  let s = Parikhproc.make_system_of_word_list d traces in
  begin
    if (Configuration.get_verbose config) >= 1 then Confusion.print_confusion_system stderr s;
    let t = Parikhproc.quotient s in
    begin
      if (Configuration.get_verbose config) >= 1 then Quotient.print stderr t;
      (Synthesis.make_region_spaces t, Separation.init t)
    end
  end 

  
(* function to execute the main step of synthesis *)
  
let execute_main_synthesis configuration region_spaces ffnet_structure =
  execute_step
    "Main Synthesis"
    (Synthesis.synthesize configuration region_spaces) ffnet_structure
  

(* function to finalize the synthesis and obtain a T/F net *)
  
let merging_regions_with_identical_supports config ffnet_structure =
  let n = Separation.get_admissible_regions ffnet_structure in
  let n' = Region.superimpose_list n in
  begin
    if (Configuration.get_verbose config) >= 1 then List.iter (Region.print stderr) n';
    flush stdout;
    n'
  end

let display_and_export_tf_net config net =
  begin
    begin
      match (Configuration.get_tnfnet_output config) with
      | None -> ()
      | Some f -> Display.net_to_pdf net "net" f
    end;
    begin
      if (Configuration.get_display_net config)
      then Display.net_display net "net"
    end;
  end
  
let finalize_synthesis_for_tf_net config ffnet_structure =
  if (is_tfnet_synthesis config)
  then
    begin
      execute_step
        "Elimination of redundent regions"
        Separation.elim_redundent ffnet_structure;
      Separation.print stderr ffnet_structure;
      flush stderr;
      let net =
        execute_step_with_result
          "Merge of admissible regions with identical supports"
          (merging_regions_with_identical_supports config) ffnet_structure
      in
      (execute_step
         "Display the T/F Net (and generate a pdf file)"
         (display_and_export_tf_net config) net)
    end


(*-----------------------------------------------------------------
 * Function to produce the marking graph of TnF net
 *-----------------------------------------------------------------*)

(* function to produce the marking graph by performing the necessary operations *)
  
let produce_tnfgraph config system ontology traces image_file =
  let a7 = A7Work.abstract_seven_of_system system in
  let a7 = A7Work.update_sensors_labels_from_ontology a7 ontology in
  let dir = Filename.dirname (Sys.executable_name) in
  let script_name = "seven2dot.xsl" in
  let script = Filename.concat dir script_name in
  let a7_temp_file_1 = "a7_temp_file_1.xml" in
  let a7_temp_file_2 = "a7_temp_file_2.xml" in
  let dot_file = (Configuration.get_target_file config "dot") in
  let image_file = (Configuration.get_target_file config "ps") in
  begin
    XmlSeven.abstract_seven_to_seven_xml_file a7_temp_file_1 a7;
    ignore (Sys.command ("sed \"s/ xmlns=.*>/>/\" " ^ a7_temp_file_1 ^ " > " ^ a7_temp_file_2));
    ignore (Sys.command ("xsltproc -o " ^ dot_file ^ " " ^ script ^ " " ^ a7_temp_file_2));
    ignore (Sys.command ("sed -i \"s/Place_//g\" " ^ dot_file));
    ignore (Sys.command ("dot -Tps " ^ dot_file ^ " -o" ^ image_file));
    Sys.remove a7_temp_file_1;
    Sys.remove a7_temp_file_2;    
  end
  
  
(*-----------------------------------------------------------------
 * Function to produce and analyse a #7 code
 *-----------------------------------------------------------------*)
  
(* function to produce #7 code by performing the necessary operations *)

let evaluate_seven_origin a7 traces =
  try
    let a7 = A7Work.evaluate_events_origin_from_traces_list a7 traces in
    A7Work.evaluate_places_origin_from_events_origin a7
  with _ -> a7

let compute_seven_position config a7 =
  try
    if (Configuration.get_a7work_location config)
    then A7Work.compute_and_update_elements_location a7
    else a7
  with _ -> a7
            
let produce_seven_code config system ontology traces file =
  let a7 = A7Work.abstract_seven_of_system system in
  let a7 = A7Work.update_sensors_labels_from_ontology a7 ontology in
  let a7 = A7Work.evaluate_five_code_from_ontology a7 ontology in
  let a7 = evaluate_seven_origin a7 traces in
  let a7 = compute_seven_position config a7 in
  begin
    XmlSeven.abstract_seven_to_seven_xml_file file a7 ;
    a7
  end

(* function to analyse #7 code and to report some informations *)

let analyse_test_traces a7 test_traces =
  match test_traces with
  | [] -> (Reporting.traces_inclusion 0 0)
  | _ ->
     let n_trace_tests = List.length test_traces in
     let n_trace_inclusion = A7Analysis.evaluate_inclusion_number a7 test_traces in
     (Reporting.traces_inclusion n_trace_tests n_trace_inclusion)
  
let analyse_seven_code a7 origin_traces test_traces =
  try
      begin
        Reporting.length_of_paths origin_traces;
        Reporting.number_of_simple_path a7 origin_traces;
        analyse_test_traces a7 test_traces;
        let paths = A7Analysis.get_label_e_paths_from_initial_places a7 in
        Reporting.list_of_paths paths
      end
  with _ ->
    begin
	Reporting.error_report "Error when analysing of #7 code" "";
	exit 2
    end
