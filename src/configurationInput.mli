(*
 *
 * Module to load a configuration 
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * The main function of this module is to load a configuration 
 * (from a config file).
 *)

(*-----------------------------------------------------------------
 * Load File
 *-----------------------------------------------------------------*)

(* function to load the configuration from a config file *)

val load_config : Configuration.t -> string -> unit
  

(* function to load the global classifiers from a classifiers file *)

val load_classifiers : Configuration.t -> string -> unit
