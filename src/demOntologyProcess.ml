(*
 *
 * Ontology Management Processes
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

open DemBasis
   


(*-----------------------------------------------------------------
 * Ontology structure computing
 *-----------------------------------------------------------------*)

(* function to compute the ontology basis *)

let rec update_ontology_basis_from_global_files files_list basis =
  try
    match files_list with
    | [] -> basis
    | file::rest_list ->
       begin
         let basis =
           DataOntologyBasisInput.load_additional_data_on_basis file basis
         in
         update_ontology_basis_from_global_files rest_list basis
       end
  with _ -> basis

let compute_ontology_basis config =
  try
    let base_file = Configuration.get_ontology_base_file config in
    let global_files = Configuration.get_ontology_global_files_list config in
    let basis =
      match base_file with
      | Some f -> DataOntologyBasisInput.load_initial_basis f
      | None -> DataOntologyBasis.init
    in
    let basis = update_ontology_basis_from_global_files global_files basis in
    basis
  with _ -> DataOntologyBasis.init

(* function to update the ontology structure with ontology basis (mapping
 * between xes value and vr value) *)

let update_with_ontology_basis config ontology =
  try
    let basis = compute_ontology_basis config in
    DataOntology.update_with_ontology_basis ontology basis config
  with _ -> ()


(* function to compute the ontology structure from log and classifier,
 * and update this struture with ontology basis *)

let compute_ontology_from_log config log classifier_name =
  let (xes_ontology, _) = Xes.encode_log log classifier_name in
  let ontology = DataOntology.make_from_event_array xes_ontology in
  begin
    update_with_ontology_basis config ontology;
    ontology
  end

  
   
(*-----------------------------------------------------------------
 * Ontology Management: value
 *-----------------------------------------------------------------*)
  
(* function to value the ontology basis from the ontology base file 
 * (the ontology base file is defined on configuration) *)

let value_ontology_basis config =
  let f_value target_file =
    let basis = compute_ontology_basis config in
    DataOntologyBasis.print_ontology_basis_to_file basis target_file
  in
  begin
    execute_step
      "Value Ontology Basis"
      (f_value)
      (Configuration.get_target_file config "ontology_basis")
  end
