(*
 *
 * #SEVEN pretty-printer
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2016
 *
 *)

(* generates a document containing #SEVEN code representing a safe net *)

val safe_to_doc :
  string array (* event labels *) ->
  int array (* transition labels *) ->
  Region.n (* safe net *) ->
  Pp.doc

