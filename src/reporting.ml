(*
 *
 * Module to manage the reporting of program execution
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * This module provides functions to initialize an execution report,
 * and updating it during execution.
 *)

(*-----------------------------------------------------------------
 * Module openning, parameter & utilities
 *-----------------------------------------------------------------*)

open Configuration
   

(* function to obtain a string from an option (None replacing by default_value) *)

let string_of_string_option opt_str default_value =
  match opt_str with None -> default_value | Some x -> x 

let string_of_int_option opt_int default_value =
  match opt_int with None -> default_value | Some x -> (string_of_int x)
                                    

(* Reporting channel and basic print functions *)
                                    
let report_channel = stdout

let fpm msg = Printf.fprintf report_channel msg
let fpmd msg data = Printf.fprintf report_channel msg data
let fpmd2 msg data1 data2 = Printf.fprintf report_channel msg data1 data2
let fpmd3 msg data1 data2 data3 = Printf.fprintf report_channel msg data1 data2 data3
let fpmoi msg i = fpmd msg (string_of_int_option i "-")
let fpmos msg str = fpmd msg (string_of_string_option str "-")
let fpmf msg file = fpmd msg (Filename.basename file)
let fpmof msg file = fpmf msg (string_of_string_option file "-")
                   

(* function to print a step *)
let print_a_step s =
  begin
    Printf.fprintf report_channel
                   "\n--------------------------------------------------------------\n";
    Printf.fprintf report_channel
                   "   %s" s;
    Printf.fprintf report_channel
                   "\n--------------------------------------------------------------\n"
  end

  
(* function to manage the execution time *)
  
let global_time_ref = ref 0.0  
let time_ref = ref 0.0
  
let print_chrono i =
  let this_time = Sys.time() in
  let chrono = this_time -. !time_ref in
  Printf.fprintf report_channel
                 "\n Process time = %f\n" chrono
       

       
(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)

(* function to obtain the list of output files as a string, and to print this list *)
  
let get_list_of_output_files conf =
  let is_output_conf output_conf =
    match output_conf with Some f -> true | None -> false
  in
  let f_add_output_file result cond file_name =
    if cond then (Filename.basename file_name)::result else result
  in
  let result = [] in
  let result =
    f_add_output_file
      result
      (is_output_conf (Configuration.get_output_tnfnet conf))
      (Configuration.get_target_file conf "tnfnet")
  in
  let result =
    f_add_output_file
      result
      (is_output_conf (Configuration.get_output_tnfgraph conf))
      (Configuration.get_target_file conf "tnfgraph")
  in
  let result =
    f_add_output_file
      result
      (is_output_conf (Configuration.get_output_seven conf))
      (Configuration.get_target_file conf "seven")
  in
  let result =
    f_add_output_file
      result
      (is_output_conf (Configuration.get_output_a7xspm conf))
      (Configuration.get_target_file conf "a7xspm")
  in
  let result =
    f_add_output_file
      result
      (is_output_conf (Configuration.get_output_events_table conf))
      (Configuration.get_target_file conf "table")
  in
  List.rev result

let print_list_of_output_files files_list =
  let print_a_file f =
    Printf.fprintf report_channel "\n   (x) %s " f
  in
  begin
    Printf.fprintf report_channel " Output files: %s" "" ;
    List.iter print_a_file files_list;
    Printf.fprintf report_channel "%s\n" "" ;
  end
  

(* val init_report : Configuration.t -> unit *)

let init_report conf =
  begin
    fpm "=============================================================================\n";
    fpm "=                     FLIPFLOP EXECUTION REPORTING                          =\n";
    fpm "=                TF Net Synthesis & #7 Code Generating                      =\n";
    fpm "=============================================================================\n";
    global_time_ref := Sys.time();
    print_a_step "Configuration";
    fpmf "\n Input file: %s\n" (Configuration.get_input_file_name conf);
    fpmd " Classifier choice: %s\n" (Configuration.get_classifier_choice conf);
    fpmf " Input work file: %s\n" (Configuration.get_target_file conf "input_work");
    fpmof " Directory output: %s\n" (Configuration.get_output_directory conf);
    print_list_of_output_files (get_list_of_output_files conf);
    (* fpmf " iSPM file: %s\n" (Configuration.get_target_file conf "ispm");
    fpmf " TnF Net file: %s\n" (Configuration.get_target_file conf "tnfnet");
    fpmf " TnF Graph file: %s\n" (Configuration.get_target_file conf "tnfgraph");
    fpmf " #7 file: %s\n" (Configuration.get_target_file conf "seven");
    fpmof " Ontology Table file: %s\n" (Configuration.get_output_events_table conf); *)
    Configuration.print_approximation_config report_channel conf;
    fpm "\n"
  end
  
  
(* val init_step : string -> unit *)
  
let init_step step =
  begin
    print_a_step step;
    time_ref := Sys.time()
  end

(* val end_step : string -> unit *)
  
let end_step step =
  let fpm msg = Printf.fprintf report_channel msg in
  let fpmd msg data = Printf.fprintf report_channel msg data in
  begin
    fpmd "\n Process ending : %s" step;
    print_chrono 1;
    fpm "\n \n"
  end


(* val report_msg : string -> string -> unit *)
  
let report_exec_info msg1 msg2 data =
  fpmd3 "\n %s (%s: %s)\n" msg1 msg2 data 
                   
  
  
(*-----------------------------------------------------------------
 * Functions to report informations about the configuration
 *-----------------------------------------------------------------*)

(* functions to report the configuration data *)

let report_configuration config =
  let string_of_string_list sl = try String.concat " " sl with _ -> "" in
  begin
    fpmf " input_working_file_postfix: %s" (get_input_working_file_postfix config);
    fpmf "\n input_adapting_script (starting_action, russiandolls or none): %s"
         (get_input_adapting_script config);
    fpmd "\n classifier_choice: %s" (get_classifier_choice config);
    fpmos "\n ontology_base_file: %s" (get_ontology_base_file config);
    fpmd "\n ontology_global_files: %s"
         (string_of_string_list (get_ontology_global_files_list config));
    fpmd "\n ontology_label_type (iri_segment or label): %s" (get_ontology_label_type config);
    fpmoi "\n instances_number: %s" (get_instances_number config);
    fpmd "\n verbose: %i" (get_verbose config);
    fpmd "\n approximation_requested (apx_config): %b" (is_approximation_requested config);
    fpmd "\n approximation_limit (apx_config): %i" (get_approximation_limit config);
    fpmof "\n output_directory: %s" (get_output_directory config);
    fpmof "\n output_tnfnet: %s" (get_output_tnfnet config);
    fpmof "\n output_tnfgraph: %s" (get_output_tnfgraph config);
    fpmof "\n output_seven: %s" (get_output_seven config);
    fpmof "\n output_a7xspm: %s" (get_output_a7xspm config);
    fpmof "\n output_events_table: %s" (get_output_events_table config);
    fpmd "\n tnfgraph_label_type (letter, number or label): %s"
         (get_tnfgraph_label_type config);
    fpmd "\n seven_sensor_label_type (letter, number, onto_label or five_ref): %s"
         (get_seven_sensor_label_type config);
    fpmd "\n seven_five_value_type (onto_label or five_ref): %s"
         (get_seven_five_value_type config);
    fpmd "\n seven_location_computing: %b" (get_seven_location_computing config);
    fpmd "\n seven_analysis: %b" (get_seven_analysis config);
    fpmd "\n seven_event_number_limit: %i" (get_seven_event_number_limit config);
    fpm "\n";
  end

  
  
(*-----------------------------------------------------------------
 * Functions to report informations from #7 code analysis
 *-----------------------------------------------------------------*)

(* function to report information about analysis configuration*)

let report_analysis_configuration config =
  begin
    Printf.fprintf report_channel "\n #7 result analysis: %b \n"
                   (Configuration.get_seven_analysis config)
  end


(* functions to report information number of simple paths and traces inclused *)

let number_of_simple_path t traces =
  let n_paths = A7Analysis.get_number_of_simple_paths_from_initial_places t in
  let n_origin_paths = List.length traces in
  let n_new_paths = n_paths - n_origin_paths in
  begin
    Printf.fprintf report_channel "\n Simple paths number: %i" n_paths;
    Printf.fprintf report_channel "\n Origin paths number: %i" n_origin_paths;
    Printf.fprintf report_channel "\n New paths number: %i" n_new_paths
  end

let traces_inclusion n_tests n_inclusion =
  begin
    Printf.fprintf
      report_channel
      "\n Number of inclused traces / tested traces : %i / %i" n_inclusion n_tests;
  end
  

(* functions to report information about length of paths *)
  
let rec compute_max i_list max =
  let f_max a b = if a > b then a else b in
  match i_list with
  | [] -> max
  | i::i_list' -> compute_max i_list' (f_max i max)

let rec compute_sum i_list sum =
    match i_list with
  | [] -> sum
  | i::i_list' -> compute_max i_list' (i + sum)

let length_of_paths traces =
  let length_list = List.map List.length traces in
  let max_length = compute_max length_list 0 in
  begin
    Printf.fprintf report_channel "\n Lenght max of path : %i" max_length
  end
  

(* functions to report the list of event paths, each event being  
 * represented by a string *)
  
let list_of_paths paths_list =
  let print_an_element e = Printf.fprintf report_channel "%s " e in
  let print_a_path p =
    begin
      Printf.fprintf report_channel "\n %s" "" ;
      List.iter print_an_element p
    end
  in
  begin
    Printf.fprintf report_channel "\n Event paths : %s" "";
    List.iter print_a_path paths_list
  end


  
(*-----------------------------------------------------------------
 * Error reporting
 *-----------------------------------------------------------------*)

(* val error_report : string -> string -> unit *)
  
let error_report err_msg arg_msg =
  begin
    let print_error channel =
      if (String.length arg_msg) = 0
      then Printf.fprintf report_channel "%s\n" err_msg
      else Printf.fprintf report_channel "%s: %s\n" err_msg arg_msg
    in
    begin
      print_error stderr
    end
  end
