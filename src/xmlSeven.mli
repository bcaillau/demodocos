(*
 *
 * Switch between XML xml #7 file and abstract #7 data.
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* 
 * This module provides functions for switching from an xml file to abstract data, 
 * or vice versa. Namely, the proposed functions allow to generate an xml #7 file 
 * from abstract #7 data  and to get abstract #7 data from an xml #7 file.
 *)

(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)

val abstract_seven_to_seven_xml_file : string -> A7.t -> unit

val seven_xml_file_to_abstract_seven : string -> A7.t
