(*
 *
 * XML of #SEVEN code
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* 
 * This module generate the XML file of #SEVEN code.
 *
 * The main function works from an abstract SEVEN structure to obtain 
 * a dot structure (using module Pp). Then, finally, the XML file is 
 * generated.
 *)

(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)

val abstract_seven_to_seven_xml_doc : A7.t -> Pp.doc

val abstract_seven_to_seven_xml_file : string -> A7.t -> unit
