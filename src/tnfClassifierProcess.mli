(*
 *
 * Classifier Management Processes
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

open TnfBasis
   


(*-----------------------------------------------------------------
 * Classifiers management :  : view classifiers, add/remove, modify
 *-----------------------------------------------------------------*)  

(* functions to view informations about classifiers*)

val view_classifier_sep : Configuration.t -> unit
val view_classifier_prefix : Configuration.t -> unit
val view_classifier_keys : Configuration.t -> unit
val view_classifier_def : Configuration.t -> unit
val view_classifiers : Configuration.t -> unit


(* function to add a classifier *)

val add_classifier_def :
  string -> Configuration.t -> string -> string -> string list -> unit


(* function to remove a classifier *)

val remove_classifier_def : string -> Configuration.t -> string -> unit


(* function to modify a classifier *)

val edit_classifier_def :
  string -> Configuration.t -> string -> string -> string list -> unit
