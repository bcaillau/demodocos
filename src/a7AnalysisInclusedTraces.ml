(*
 *
 * Sub-Module of A7Analysis to evaluate inclused traces on #7 code
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * This module provides functions to evaluate the number of inclused traces
 * on a #7 code.
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

open A7Attributes
open A7

(* function to know if a given place is a final place *)
let is_final_place a7 place_urn =
  let fp_urn_list = get_final_place_urn_list a7 in
  (List.mem place_urn fp_urn_list) 

(* function to sum the elements of a given list *)
let rec sum l =
  match l with
  | [] -> 0
  | hd :: tl -> hd + sum tl

(* function to obtain a list without duplicate (result list) 
 * a given list (source list) *)
let rec list_uniq result source =
  match source with
  | [] -> result
  | e::new_source ->
     let new_result = if (List.mem e result) then result else e::result
     in list_uniq new_result new_source 

      
       
(*-----------------------------------------------------------------
 * Function to evaluate the number of inclused traces
 *-----------------------------------------------------------------*)

(* function to obtain the list of transitions with an up place
 * corresponding to a given place *)
let transitions_list_with_upplace p_idref t_list =
  let filter t = (p_idref = (A7Element.get_up_place_urn t)) in
  List.filter filter t_list

  
(* function to obtain event and transition corresponding to a given
 * classifier ref, ie a given event from classifier *)
let target_event_and_transition_by_classifier se_list t_list class_ref =
  let mapping t = A7Element.get_event_urn t in 
  let e_urn_list = List.map mapping t_list in (* list with idref of transitions *)
  let filter e = List.mem (A7Element.get_urn e) e_urn_list in
  let e_filter_list = List.filter filter se_list in (* events filtered by idref list *)
  let predicate e = ((A7Element.get_classifier_ref e) = class_ref) in
  let se = List.find predicate e_filter_list in (* event corresponding to classifier ref *)
  let predicate t = ((A7Element.get_event_urn t) = (A7Element.get_urn se)) in
  let t = List.find predicate t_list in (*transition corresponding to classifier ref *)
  (se, t)
  

(* function to evaluate the inclusion of a given path (= trace) *)
let rec evaluate_inclusion_of_path a7 p se_list t_list trace =
  try
    if (is_final_place a7 p)
    then true
    else match trace with
         | e_class_ref::trace' ->
            let tp_list = transitions_list_with_upplace p t_list in
            let (_, t) = target_event_and_transition_by_classifier se_list tp_list e_class_ref in
            let p' = A7Element.get_down_place_urn t in
            (evaluate_inclusion_of_path a7 p' se_list t_list trace')
         | [] -> true
  with
  | _ -> false

(* function to evaluate the inclusion of a given trace *)
let evaluate_inclusion_of_trace a7 trace =
  let p = A7UrnMapping.urn_init in
  let se_list = A7.get_sensor_event_list a7 in
  let t_list = A7.get_transition_list a7 in
  evaluate_inclusion_of_path a7 p se_list t_list trace


  
(*-----------------------------------------------------------------
 * Main function
 *-----------------------------------------------------------------*)

(* function to compute the number of inclusion starting 
 * from a list of traces *)
let evaluate a7 test_traces =
  let test_values = List.map (evaluate_inclusion_of_trace a7) test_traces in
  let filter v = (v = true) in
  List.length (List.filter filter test_values)
