(*
 *
 * Module to save a configuration 
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * The main function of this module is to save a configuration 
 * (on a config file).
 *) 

(*-----------------------------------------------------------------
 * Module openning, parameter & utilities
 *-----------------------------------------------------------------*)

open Pp

open Configuration

open ConfigurationDocFormat
   

(* function to obtain a string from a string option (None replacing by "") *)

let string_of_string_option s =
  match s with None -> "" | Some x -> x 

                                    
(* function to obtain a string from a int option (None replacing by "") *)

let string_of_int_option i =
  match i with None -> "" | Some x -> (string_of_int x) 

                                    
(* function to obtain a string from a string list *)

let string_of_string_list sl =
  try
    String.concat " " sl
  with _ -> ""
   

(*-----------------------------------------------------------------
 * Functions to generates doc containing the main configuration data
 *-----------------------------------------------------------------*)

let doc_of_config_data config =

  (* Synonyms *)
  let sb = string_of_bool
  and si = string_of_int
  and sio = string_of_int_option
  and sso = string_of_string_option
  and ssl = string_of_string_list
  in
    
  (* Synthesis configuration *)
  let input_working_file_postfix_value = (get_input_working_file_postfix config) in
  let classifier_choice_value = (get_classifier_choice config) in
  let ontology_base_file_value = sso (get_ontology_base_file config) in
  let ontology_global_files_value = ssl (get_ontology_global_files_list config) in
  let ontology_label_type_value = (get_ontology_label_type config) in
  let instances_number_value = (sio (get_instances_number config)) in
  let verbose_value = (si (get_verbose config)) in
  
  (* approximation configuration *)
  let approximation_requested_value = (sb (is_approximation_requested config)) in
  let approximation_limit_value = (si (get_approximation_limit config)) in
  
  (* Output configuration *)
  let output_directory_value = (sso (get_output_directory config)) in
  let output_tnfnet_value = (sso (get_output_tnfnet config)) in
  let output_tnfgraph_value = (sso (get_output_tnfgraph config)) in
  let output_seven_value = (sso (get_output_seven config)) in
  let output_a7xspm_value = (sso (get_output_a7xspm config)) in
  let output_events_table_value = (sso (get_output_events_table config)) in
  (* let output_tnfnet_display_value = (sb (get_output_tnfnet_display config)) in *)
  
  (* Options for output computing *)
  let tnfgraph_label_type_value = (get_tnfgraph_label_type config) in
  let seven_sensor_label_type_value = (get_seven_sensor_label_type config) in
  let seven_five_value_type_value = (get_seven_five_value_type config) in
  let seven_location_computing_value = (sb (get_seven_location_computing config)) in
  let seven_analysis_value = (sb (get_seven_analysis config)) in
  let seven_event_number_limit_value = (si (get_seven_event_number_limit config)) in
  
  vgrp (
      
      (* Synthesis configuration *)
      (input_working_file_postfix_text input_working_file_postfix_value)
      ^^ (classifier_choice_text classifier_choice_value)
      ^^ (ontology_base_file_text ontology_base_file_value)
      ^^ (ontology_global_files_text ontology_global_files_value)
      ^^ (ontology_label_type_text ontology_label_type_value)
      ^^ (verbose_text verbose_value)
      ^^ (instances_number_text instances_number_value)
    
      (* approximation configuration *)
      ^^ (approximation_requested_text approximation_requested_value)
      ^^ (approximation_limit_text approximation_limit_value)
    
      (* Output configuration *)
      ^^ (output_directory_text output_directory_value)
      ^^ (output_tnfnet_text output_tnfnet_value)
      ^^ (output_tnfgraph_text output_tnfgraph_value)
      ^^ (output_seven_text output_seven_value)
      ^^ (output_a7xspm_text output_a7xspm_value)
      ^^ (output_events_table_text output_events_table_value)
      (* ^^ (output_tnfnet_display_text output_tnfnet_display_value) *)
    
      (* Options for output computing *)
      ^^ (tnfgraph_label_type_text tnfgraph_label_type_value)
      ^^ (seven_sensor_label_type_text seven_sensor_label_type_value)
      ^^ (seven_five_value_type_text seven_five_value_type_value)
      ^^ (seven_location_computing_text seven_location_computing_value)
      ^^ (seven_analysis_text seven_analysis_value)
      ^^ (seven_event_number_limit_text seven_event_number_limit_value)
    
    )
  

(* functions to obtain the config doc of a given configuration *)

let main_config_doc_of_configuration config =
  vgrp
    (
      xml_prologue_text
      ^^ (mainconfig_root_text
            (doc_of_config_data config)
         )
    )


  
(*-----------------------------------------------------------------
 * Functions to generates doc containing the global classifiers data
 *-----------------------------------------------------------------*)

(* function to obtain the doc corresponding to a prefix list (prefix by group) *)

let doc_of_prefix prefix_list =
  let f_text doc (group, prefix) = vgrp (doc ^^ (prefix_text group prefix)) in
  List.fold_left f_text empty prefix_list


(* function to obtain the doc corresponding to a keys list (keys by group) *)

let doc_of_keys keys_list sep =
  let f_text doc (group, keys) = vgrp (doc ^^ (keys_text group keys sep)) in
  List.fold_left f_text empty keys_list


(* function to obtain the doc corresponding to a classifiers list *)
                          
let rec doc_of_classifiers classifiers_list sep result =
  match classifiers_list with
  | (c_name, c_group, c_keys)::r_list ->
     let classifier = classifier_text c_name c_group c_keys sep in  
     let result = vgrp (result ^^ classifier) in
     doc_of_classifiers r_list sep result
  | _ -> result
       

(* let doc_of_classifiers_group config src_format =
  let prefix = "" in
  let sep = ";" in
  let keys = "" in
  (* let classifiers_list = 
     Configuration.get_global_classifiers config 
     in *)
  let classifiers_list =
    Configuration.get_classifiers_list_of_group config src_format
  in
  (classifiers_group_text
     src_format
     (vgrp (
          (prefix_text prefix)
          ^^ (separator_text sep)
          ^^ (keys_text keys)
          ^^ (doc_of_classifiers classifiers_list empty)
        )
     )
  ) *)

(* let doc_of_all_classifiers_group config =
  let groups_list = Configuration.valid_src_formats_list in
  let f_classifiers_doc result group =
       vgrp 
         (
           result
           ^^ (doc_of_classifiers_group config group)
         )
  in
  List.fold_left f_classifiers_doc empty groups_list *)
  

(* functions to obtain the classifiers doc of a given configuration *)

let classifiers_content_doc_of_configuration config =
  let sep = Configuration.get_classifier_separator config in
  let prefix_list = Configuration.get_classifier_prefix_list config in
  let keys_list = Configuration.get_classifier_keys_list config in
  let classifier_def_list = Configuration.get_classifier_def_list config in
  vgrp
  ((separator_text sep)
   ^^ (doc_of_prefix prefix_list)
   ^^ (doc_of_keys keys_list sep)
   ^^ (doc_of_classifiers classifier_def_list sep empty)
  )
  
let classifiers_doc_of_configuration config =
  let root_text = classifiers_root_text in
  let content_text = classifiers_content_doc_of_configuration config in 
  vgrp
    (
      xml_prologue_text
      ^^ (root_text content_text)
    )


  
(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)
  

(* function to save a given configuration on config file *)

let save_doc doc file = 
  let file_out = open_out file in
  begin
    ppToFile file_out 80 doc;
    flush file_out;
    close_out file_out
  end
  

(* function to save a given configuration on config file *)

let save_config config config_file = 
  let config_doc = (main_config_doc_of_configuration config) in
  save_doc config_doc config_file
  

(* function to save the classifiers (from configuration) on a 
 * global classifiers config file *)

let save_classifiers config classifiers_config_file = 
  let config_doc = (classifiers_doc_of_configuration config) in
  save_doc config_doc classifiers_config_file
