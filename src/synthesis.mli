(*
 *
 * Flipflop net synthesis algorithm
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA Rennes, France
 *
 *)

(* Region spaces *)

type t

(* Compute region spaces *)

val make_region_spaces : Quotient.t -> t

(* Solves all ESSPs until saturation of the graph. The graph is expected to be virgin. Traversal starts at initial state *)

val synthesize : Configuration.t -> t -> Separation.t -> unit


