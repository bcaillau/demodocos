(*
 *
 * URN Mapping
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * This module provides any functions to manage the urn of A7 elements.
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities
 *-----------------------------------------------------------------*)

(* None *)
                        

(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

(* Unique reference number to identify the elements of #SEVEN code *)

let urn_init = 1

type urn = Some of int | None


(* Mapping *)

type t =
  {
    urn_count : int ref;
    node_map : urn Parikh.t;
    mutable edge_map : ((int * int), urn) Hashtbl.t;
  }


(*-----------------------------------------------------------------
 * Constructor(s)
 *-----------------------------------------------------------------*)

(* val make : int -> t *)
  
let make vector_dim =
  {
    urn_count = ref urn_init;
    node_map = Parikh.make vector_dim;
    edge_map = Hashtbl.create 100
  }
  

(*-----------------------------------------------------------------
 * Accessors
 *-----------------------------------------------------------------*)

(* val get_urn_count : t -> int ref *)
(* val get_node_mapping : t -> urn Parikh.t *)
(* val get_edge_mapping : t -> ((int * int), urn) Hashtbl.t *)
  
let get_urn_count u_map = u_map.urn_count
  
let get_node_mapping u_map = u_map.node_map
                           
let get_edge_mapping u_map = u_map.edge_map

                           
(* val set_node_mapping : t -> Vector.t -> unit *)
(* val set_edge_mapping : t -> (int * int) -> unit *)
                           
let set_node_mapping { node_map = m } v =
  if not (Parikh.is_def m v) then (Parikh.set m v None)
  
let set_edge_mapping { edge_map = m } e =
  if not (Hashtbl.mem m e) then (Hashtbl.add m e None)
                     
  
(*-----------------------------------------------------------------
 * Functions to manage unique reference numbers
 *-----------------------------------------------------------------*)

(* functions to obtain the urn of a vector or a pair *)
  
(* val get_urn_of_vector : t -> int ref -> Vector.t -> int *)
(* val get_urn_of_pair : t -> int ref -> IntPair.t -> int *)
  
let get_urn_of_vector { node_map = m } c v =
  match (Parikh.get m v)
  with
    Some num -> num
  | None ->
     let num = !c in  
     begin
       c := num + 1;
       Parikh.set m v (Some num);
       num
     end

let get_urn_of_pair { edge_map = m } c p =
  match (Hashtbl.find m p)
  with
    Some num -> num
  | None ->
     let num = !c in  
     begin
       c := num + 1;
       Hashtbl.add m p (Some num);
       num
     end  

     
(* function to obtain the urn of a node (= state) *)
     
(* val get_urn_of_node : t -> int ref -> Separation.q -> int *)
  
let get_urn_of_node u_map c q =
 let v = (Separation.parikh_of_state q) in
 begin
   set_node_mapping u_map v;
   get_urn_of_vector u_map c v
 end

 
(* function to obtain the urn of an edge (= pair node/i) *)
 
(* val get_urn_of_edge : t -> int ref -> Separation.q -> int -> int *)
 
let get_urn_of_edge u_map c q i =
  let v = (Separation.parikh_of_state q) in
  let uv = (get_urn_of_vector u_map c v) in
  let p = (uv, i) in
  begin
    set_edge_mapping u_map p;
    get_urn_of_pair u_map c p
  end
