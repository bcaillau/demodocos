/*
 *
 * $Header$
 *
 * Z/2Z vector space algebra
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2012
 *
 */

#include <string.h>
#include <limits.h>
#include <stdio.h>

#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/custom.h>
#include <caml/fail.h>

#include "vector_ml.h"

#ifdef DARWIN
#define BITS_PER_WORD LONG_BIT
#else
#ifdef LINUX
#define BITS_PER_WORD __WORDSIZE
#endif
#endif

#define WORD_FALSE 0UL
#define WORD_TRUE (~ WORD_FALSE)
#define WORD_ONE 1UL
#define MAX_BYTE_SIZE 24

typedef unsigned long word;

typedef struct
{
  int len; // Number of bits
  int cnt; // Number of words
  word vec[];
} vector_block;

// Custom block handler

CAMLprim int vector_ml_compare_custom(value,value);

static struct custom_operations vector_block_ops = {
  "fr.inria.flipflop.vector.t.1.00", // Version 1.00
  custom_finalize_default,
  vector_ml_compare_custom,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};

CAMLprim value vector_ml_make(value length, value init)
{
  // This function allocates in the heap. We have to use CAMLparam and CAMLlocal.

  CAMLparam2(length,init);
  CAMLlocal1(v); // Custom block

  int n = Int_val(length); // Number of bits
  int b = Bool_val(init); // Initial value
  int m; // Number of words
  word c; // Initial word value
  vector_block * a;; // Vector block within the custom block
  int i; // Iteration variable

  // Checks whether parameters are valid

  if (n < 0)
    {
      caml_invalid_argument("Vector.make: invalid length");
    };

  // Number of words

  m = n/BITS_PER_WORD;

  if (m*BITS_PER_WORD < n)
    {
      m++;
    };

  // Initial word

  if (b)
    {
      c = WORD_TRUE;
    }
  else
    {
      c = WORD_FALSE;
    };

  // Custom block allocation
  
  v = caml_alloc_custom(&vector_block_ops,
			sizeof(vector_block)+m*sizeof(word), 0, 1);

  // Vector block initialization

  a = (vector_block *)Data_custom_val(v);

  a->len = n;
  a->cnt = m;

  // Initialization of the vector

  for (i=0;i<m;i++)
    {
      a->vec[i] = c;
    };
  
  // Return the custom block

  CAMLreturn(v);
}

// clone(u) returns a clone of u

CAMLprim value vector_ml_clone(value u)
{
  CAMLparam1(u);
  CAMLlocal1(v);

  vector_block * a = (vector_block *)Data_custom_val(u); // Vector block
  vector_block * b;
  int n = a->len;
  int m = a->cnt;

  // Allocates the new custom block

  v = caml_alloc_custom(&vector_block_ops,
			sizeof(vector_block)+m*sizeof(word), 0, 1);

  b = (vector_block *)Data_custom_val(v);

  // Copies vector block

  memcpy(b,a,sizeof(vector_block)+m*sizeof(word));

  // Return new vector

  CAMLreturn(v);
}

// vector_ml_extend(v,n,b) creates a clone of v with greater dimension n and initial value b for all new bits

CAMLprim value vector_ml_extend(value u1,value k2,value e)
{
  CAMLparam3(u1,k2,e);
  CAMLlocal1(u2);

  vector_block * a1 = (vector_block *)Data_custom_val(u1); // Vector block
  vector_block * a2;
  int f = Bool_val(e);
  word g;
  int n1 = a1->len;
  int m1 = a1->cnt;
  int q1 = n1 % BITS_PER_WORD;
  int n2 = Int_val(k2);
  int m2 = n2 / BITS_PER_WORD;
  int q2 = n2 % BITS_PER_WORD;
  int i;

  // Sanity checks

  if (n2 < n1)
    {
      caml_invalid_argument("Vector.extend: invalid new length");      
    };

  // Adjust new word count

  if (q2 > 0)
    {
      m2++;
    };

  // Allocates the new custom block

  u2 = caml_alloc_custom(&vector_block_ops,
			 sizeof(vector_block)+m2*sizeof(word), 0, 1);

  a2 = (vector_block *)Data_custom_val(u2);

  // Copies vec array

  memcpy(a2->vec,a1->vec,m1*sizeof(word));

  // Initializes len and cnt fields

  a2->len = n2;
  a2->cnt = m2;

  // Intializes the new bits in the last old word

  if (q1 > 0)
    {
      for (i=q1;i<BITS_PER_WORD;i++)
	{
	  if (f)
	    {
	      a2->vec[m1-1] |= WORD_ONE << i;
	    }
	  else
	    {
	      a2->vec[m1-1] &= ~(WORD_ONE << i);
	    };
	};
    };

  // Initial word
  
  if (f)
    {
      g = WORD_TRUE;
    }
  else
    {
      g = WORD_FALSE;
    };

  // Initializes the new words

  for (i=m1;i<m2;i++)
    {
      a2->vec[i] = g;
    }

  // Return new vector

  CAMLreturn(u2);
}

// vector_ml_restrict(v,n) creates a clone of v with only the first n bits of v

CAMLprim value vector_ml_restrict(value u1,value k2)
{
  CAMLparam2(u1,k2);
  CAMLlocal1(u2);

  vector_block * a1 = (vector_block *)Data_custom_val(u1); // Vector block
  vector_block * a2;
  int n1 = a1->len;
  int n2 = Int_val(k2);
  int p2 = n2 / BITS_PER_WORD;
  int q2 = n2 % BITS_PER_WORD;
  int m2 = p2;

  // Sanity checks

  if ((n2 > n1) || (n2 <= 0))
    {
      caml_invalid_argument("Vector.extend: invalid new length");      
    };

  // Adjust new word count

  if (q2 > 0)
    {
      m2++;
    };

  // Allocates the new custom block

  u2 = caml_alloc_custom(&vector_block_ops,
			 sizeof(vector_block)+m2*sizeof(word), 0, 1);

  a2 = (vector_block *)Data_custom_val(u2);

  // Copies vec array

  memcpy(a2->vec,a1->vec,m2*sizeof(word));

  // Initializes len and cnt fields

  a2->len = n2;
  a2->cnt = m2;

  // Return new vector

  CAMLreturn(u2);
}

// Set bit at index

CAMLprim value vector_ml_set(value v, value x, value y)
{
  CAMLparam3(v,x,y);
  vector_block * a = (vector_block *)Data_custom_val(v); // Vector block
  int i = Int_val(x); // index
  int b = Bool_val(y); // value

  // Check whether index is within range

  if ((i < 0) || (i >= a->len))
    {
      caml_invalid_argument("Vector.set: index out of range");
    };

  // Compute offset and shift

  int j = i / BITS_PER_WORD;
  int k = i % BITS_PER_WORD;

  // Apply mask

  if (b)
    {
      a->vec[j] |= WORD_ONE << k;
    }
  else
    {
      a->vec[j] &= ~(WORD_ONE << k);
    };

  // Return ()

  CAMLreturn(Val_unit);
}

// Get bit at index

CAMLprim value vector_ml_get(value v, value x)
{
  CAMLparam2(v,x);
  vector_block * a = (vector_block *)Data_custom_val(v); // Vector block
  int i = Int_val(x); // index

  // Check whether index is within range

  if ((i < 0) || (i >= a->len))
    {
      caml_invalid_argument("Vector.get: index out of range");
    };

  // Compute offset and shift

  int j = i / BITS_PER_WORD;
  int k = i % BITS_PER_WORD;

  // Apply mask

  if (a->vec[j] & (WORD_ONE << k))
    {
      CAMLreturn(Val_true);
    }
  else
    {
      CAMLreturn(Val_false);
    };
}

// vector_ml_length(v) returns the length of v

CAMLprim value vector_ml_length(value v)
{
  CAMLparam1(v);
  vector_block * a = (vector_block *)Data_custom_val(v); // Vector block
  
  CAMLreturn(Val_int(a->len));
}

// vector_ml_is_zero(v) checks whether vector v is constant and equal to false

CAMLprim value vector_ml_is_zero(value v)
{
  CAMLparam1(v);
  vector_block * a = (vector_block *)Data_custom_val(v); // Vector block
  int p = a->len / BITS_PER_WORD; // number of completely packed words
  int q = a->len % BITS_PER_WORD; // remaining bits
  int i;

  // check that complete chunks are zero

  for (i=0;i<p;i++)
    {
      if (a->vec[i])
	{
	  CAMLreturn(Val_false);
	};
    };

  // check the last remaining bits

  for (i=0;i<q;i++)
    {
      if (a->vec[a->cnt-1] & (WORD_ONE << i))
	{
	  CAMLreturn(Val_false);
	};
    };

  // All bits are zero

  CAMLreturn(Val_true);
}

// Number of leading zeroes

CAMLprim value vector_ml_leading_zeroes(value v)
{
  CAMLparam1(v);
  vector_block * a = (vector_block *)Data_custom_val(v); // Vector block
  int p = a->len / BITS_PER_WORD; // number of completely packed words
  int q = a->len % BITS_PER_WORD; // remaining bits
  int i,j;
  int n=0;
  
  // look for a non-zero word
  
  for (i=0;i<p;i++)
    {
      if (a->vec[i] == WORD_FALSE)
	{
	  n += BITS_PER_WORD;
	}
      else
	{
	  break;
	};
    };
 
  // is there another word to count?
  if (i < a->cnt)
    {
      // count the remaining zero bits
      if (i < a->cnt-1)
	{
	  // word is fully packed
	  for (j=0;j<BITS_PER_WORD;j++)
	    {
	      if (a->vec[i] & (WORD_ONE << j))
		{
		  break;
		}
	      else
		{
		  n++;
		};
	    };
	}
      else
	{
	  // word is the last word
	  for (j=0;j<q;j++)
	    {
	      if (a->vec[a->cnt-1] & (WORD_ONE << j))
		{
		  break;
		}
	      else
		{
		  n++;
		};
	    };
	};
    };

  CAMLreturn(Val_int(n));
}

// Arity

CAMLprim value vector_ml_arity(value v)
{
  CAMLparam1(v);
  vector_block * a = (vector_block *)Data_custom_val(v); // Vector block
  int p = a->len / BITS_PER_WORD; // number of completely packed words
  int q = a->len % BITS_PER_WORD; // remaining bits
  int i,j;
  int n=0;
  
  //Count set bits in fully packed words
  
  for (i=0;i<p;i++)
    {
      for (j=0;j<BITS_PER_WORD;j++)
	{
	  if (a->vec[i] & (WORD_ONE << j))
	    {
	      n++;
	    };
	};
    };

  // Count set bits in the last word

  for (j=0;j<q;j++)
    {
      if (a->vec[a->cnt-1] & (WORD_ONE << j))
	{
	  n++;
	};
    };
  
  CAMLreturn(Val_int(n));
}

// In place xor

CAMLprim value vector_ml_ixor(value u,value v)
{
  CAMLparam2(u,v);
  vector_block * a = (vector_block *)Data_custom_val(u);
  vector_block * b = (vector_block *)Data_custom_val(v);
  int i;

  if (a->len != b->len)
    {
      caml_invalid_argument("Vector.ixor: lengths differ");
    };

  for (i=0;i<a->cnt;i++)
    {
      a->vec[i] ^= b->vec[i];
    };

  CAMLreturn(Val_unit);
}

// In place and

CAMLprim value vector_ml_iand(value u,value v)
{
  CAMLparam2(u,v);
  vector_block * a = (vector_block *)Data_custom_val(u);
  vector_block * b = (vector_block *)Data_custom_val(v);
  int i;

  if (a->len != b->len)
    {
      caml_invalid_argument("Vector.iand: lengths differ");
    };

  for (i=0;i<a->cnt;i++)
    {
      a->vec[i] &= b->vec[i];
    };

  CAMLreturn(Val_unit);
}

// In place or

CAMLprim value vector_ml_ior(value u,value v)
{
  CAMLparam2(u,v);
  vector_block * a = (vector_block *)Data_custom_val(u);
  vector_block * b = (vector_block *)Data_custom_val(v);
  int i;

  if (a->len != b->len)
    {
      caml_invalid_argument("Vector.ior: lengths differ");
    };

  for (i=0;i<a->cnt;i++)
    {
      a->vec[i] |= b->vec[i];
    };

  CAMLreturn(Val_unit);
}

// Scalar product

CAMLprim value vector_ml_prod(value u,value v)
{
  CAMLparam2(u,v);
  vector_block * a = (vector_block *)Data_custom_val(u);
  vector_block * b = (vector_block *)Data_custom_val(v);
  int p = a->len / BITS_PER_WORD; // number of completely packed words
  int q = a->len % BITS_PER_WORD; // remaining bits
  int i,j;
  word w;
  word r = WORD_FALSE;

  // Check whether vectors have equal lengths

  if (a->len != b->len)
    {
      caml_invalid_argument("Vector.prod: lengths differ");
    };

  // Compute scalar product on fully packed words

  for (i=0;i<p;i++)
    {
      w = a->vec[i] & b->vec[i];
      for (j=0;j<BITS_PER_WORD;j++)
	{
	  r ^= (w & WORD_ONE);
	  w = w >> 1;
	};
    };

  // Finish with the last word, if necessary

  if (q > 0)
    {
      w = a->vec[a->cnt-1] & b->vec[b->cnt-1];
      for (j=0;j<q;j++)
	{
	  r ^= (w & WORD_ONE);
	  w = w >> 1;
	};
    };
  
  // Return true iff r is not zero

  if (r)
    {
      CAMLreturn(Val_true);
    }
  else
    {
      CAMLreturn(Val_false);
    };
}

CAMLprim value vector_ml_partial_prod(value u, value v, value vfrom, value vto)
{
  CAMLparam4(u,v,vfrom,vto);
  int from = Int_val(vfrom);
  int to = Int_val(vto);
  vector_block * a = (vector_block *)Data_custom_val(u);
  vector_block * b = (vector_block *)Data_custom_val(v);
  int p_from = from / BITS_PER_WORD; // index of first word
  int q_from = from % BITS_PER_WORD; // index of first bit in first word
  int p_to = to / BITS_PER_WORD; // index of the last completely packed word
  int q_to = to % BITS_PER_WORD; // number of bits in the last, partially packed word
  int i,j;
  word w;
  word r = WORD_FALSE;

  // Debug

#ifdef DEBUG
  
  printf("%p * %p : [%d(%d,%d);%d(%d,%d)]\n",(void *)a,(void *)b,from,p_from,q_from,to,p_to,q_to);
  fflush(stdout);

#endif

  // Check whether vectors have equal lengths

  if (a->len != b->len)
    {
      caml_invalid_argument("Vector.partial_prod: lengths differ");
    };

  // Check that indexes are within range

  if ((from < 0) || (from >= a->len) || (to < 0) || (to >= a->len))
    {
      caml_invalid_argument("Vector.partial_prod: index out of range");
    };

  // Are we scanning any bit?

  if (from <= to)
    {

      // Debug

#ifdef DEBUG

      printf("<1>\n");
      fflush(stdout);

#endif

      // Are we scanning a single word?

      if (p_from == p_to)
	{
	  // Debug

#ifdef DEBUG

	  printf("<2>\n");
	  fflush(stdout);

#endif

	  w = (a->vec[p_from] & b->vec[p_from]) >> q_from;
	  
	  for (i=q_from;i<=q_to;i++)
	    {
	      r ^= (w & WORD_ONE);
	      w = w >> 1;      
	    };
	}
      else // p_from < p_to
	{
	  
	  // Debug

#ifdef DEBUG

	  printf("<3>\n");
	  fflush(stdout);

#endif

	  // Compute scalar product on the first word.
	  
	  w = (a->vec[p_from] & b->vec[p_from]) >> q_from;
	  
	  for (i=q_from;i<BITS_PER_WORD;i++)
	    {
	      r ^= (w & WORD_ONE);
	      w = w >> 1;      
	    };
      
	  // Debug
	  
#ifdef DEBUG

      printf("<4>\n");
      fflush(stdout);
      
#endif

	  // Compute scalar product on fully packed words p_from+1 to p_to-1
	  
	  for (i=p_from+1;i<p_to;i++)
	    {
	      w = a->vec[i] & b->vec[i];
	      for (j=0;j<BITS_PER_WORD;j++)
		{
		  r ^= (w & WORD_ONE);
		  w = w >> 1;
		};
	    };
	  
	  // Debug
	  
#ifdef DEBUG
	  
	  printf("<5>\n");
	  fflush(stdout);
	  
#endif
	  
	  // Finish with the last word
	  
	  w = a->vec[p_to] & b->vec[p_to];
	  for (i=0;i<=q_to;i++)
	    {
	      r ^= (w & WORD_ONE);
	      w = w >> 1;
	    };
	};
    };
      
  // Return true iff r is not zero
  
  // Debug
  
#ifdef DEBUG
  
  printf("<6>\n");
  fflush(stdout);
  
#endif
  
  if (r)
    {
      CAMLreturn(Val_true);
    }
  else
    {
      CAMLreturn(Val_false);
    };
}

// Comparison functions

CAMLprim int vector_ml_compare_custom(value u1, value u2)
{
  CAMLparam2(u1,u2);
  vector_block * a1 = (vector_block *)Data_custom_val(u1);
  vector_block * a2 = (vector_block *)Data_custom_val(u2);

  int n1 = a1->len;
  int n2 = a2->len;
  int p = n1 / BITS_PER_WORD;
  int q = n1 % BITS_PER_WORD;
  int i,j;
  word m1,m2;
  word m = WORD_FALSE;

  if (n1 < n2)
    {
      CAMLreturn(-1);
    }
  else if (n1 > n2)
    {
      CAMLreturn(1);
    }
  else
    {
      // Find least index where words differ
      for (i=0;i<p;i++)
	{
	  if (a1->vec[i] != a2->vec[i])
	    {
	      for (j=0;j<BITS_PER_WORD;j++)
		{
		  m1 = a1->vec[i] & (WORD_ONE << j);
		  m2 = a2->vec[i] & (WORD_ONE << j);

		  if (m1 ^ m2)
		    {
		      if (m1)
			{
			  CAMLreturn(-1);
			};

		      if (m2)
			{
			  CAMLreturn(1);
			};
		    };
		};
	    };
	};

      // Finish with the last word

      for (j=0;j<q;j++)
	{
	  m1 = a1->vec[a1->cnt-1] & (WORD_ONE << j);
	  m2 = a2->vec[a2->cnt-1] & (WORD_ONE << j);

	  if (m1 ^ m2)
	    {
	      if (m1)
		{
		  CAMLreturn(-1);
		};
	      
	      if (m2)
		{
		  CAMLreturn(1);
		};
	    };
	};
      
      CAMLreturn(0);
    };
}


CAMLprim value vector_ml_compare(value u1, value u2)
{
  CAMLparam2(u1,u2);
  vector_block * a1 = (vector_block *)Data_custom_val(u1);
  vector_block * a2 = (vector_block *)Data_custom_val(u2);

  int n1 = a1->len;
  int n2 = a2->len;
  int p = n1 / BITS_PER_WORD;
  int q = n1 % BITS_PER_WORD;
  int i,j;
  word m1,m2;

  if (n1 < n2)
    {
      CAMLreturn(Val_int(-1));
    }
  else if (n1 > n2)
    {
      CAMLreturn(Val_int(1));
    }
  else
    {
      // Find least index where words differ
      for (i=0;i<p;i++)
	{
	  if (a1->vec[i] != a2->vec[i])
	    {
	      for (j=0;j<BITS_PER_WORD;j++)
		{
		  m1 = a1->vec[i] & (WORD_ONE << j);
		  m2 = a2->vec[i] & (WORD_ONE << j);

		  if (m1 ^ m2)
		    {
		      if (m1)
			{
			  CAMLreturn(Val_int(-1));
			};

		      if (m2)
			{
			  CAMLreturn(Val_int(1));
			};
		    };
		};
	    };
	};

      // Finish with the last word

      for (j=0;j<q;j++)
	{
	  m1 = a1->vec[a1->cnt-1] & (WORD_ONE << j);
	  m2 = a2->vec[a2->cnt-1] & (WORD_ONE << j);

	  if (m1 ^ m2)
	    {
	      if (m1)
		{
		  CAMLreturn(Val_int(-1));
		};
	      
	      if (m2)
		{
		  CAMLreturn(Val_int(1));
		};
	    };
	};
      
      CAMLreturn(Val_int(0));
    };
}

// Xor

CAMLprim value vector_ml_xor(value u,value v)
{
  CAMLparam2(u,v);
  CAMLlocal1(w);
  vector_block * a = (vector_block *)Data_custom_val(u);
  vector_block * b = (vector_block *)Data_custom_val(v);
  vector_block * c;
  int i;

  if (a->len != b->len)
    {
      caml_invalid_argument("Vector.vxor: lengths differ");
    };

  // Custom block allocation
  
  w = caml_alloc_custom(&vector_block_ops,
			sizeof(vector_block)+a->cnt*sizeof(word), 0, 1);

  c = (vector_block *)Data_custom_val(w);

  // Initialize new vector

  c->len = a->len;
  c->cnt = a->cnt;

  // Compute xor

  for (i=0;i<a->cnt;i++)
    {
      c->vec[i] = a->vec[i] ^ b->vec[i];
    };

  // Return new vector

  CAMLreturn(w);
}

// And

CAMLprim value vector_ml_and(value u,value v)
{
  CAMLparam2(u,v);
  CAMLlocal1(w);
  vector_block * a = (vector_block *)Data_custom_val(u);
  vector_block * b = (vector_block *)Data_custom_val(v);
  vector_block * c;
  int i;

  if (a->len != b->len)
    {
      caml_invalid_argument("Vector.vand: lengths differ");
    };

  
  // Custom block allocation
  
  w = caml_alloc_custom(&vector_block_ops,
			sizeof(vector_block)+a->cnt*sizeof(word), 0, 1);

  c = (vector_block *)Data_custom_val(w);

  // Initialize new vector

  c->len = a->len;
  c->cnt = a->cnt;

  // Compute and

  for (i=0;i<a->cnt;i++)
    {
      c->vec[i] = a->vec[i] & b->vec[i];
    };

  // Return new vector

  CAMLreturn(w);
}

// Blit

CAMLprim value vector_ml_blit(value vsrc, value vsrc_idx, value vdst, value vdst_idx, value vlen)
{
  CAMLparam5(vsrc,vsrc_idx,vdst,vdst_idx,vlen);
  vector_block * src = (vector_block *)Data_custom_val(vsrc);
  vector_block * dst = (vector_block *)Data_custom_val(vdst);
  int src_idx = Int_val(vsrc_idx);
  int dst_idx = Int_val(vdst_idx);
  int len = Int_val(vlen);
  int src_p, src_q, src_r, dst_p, dst_q, dst_r;
  int count;
  word w,m;
  int i,r;

  // Sanity checks

  if ((src_idx < 0) || (src_idx + len > src->len) ||
      (dst_idx < 0) || (dst_idx + len > dst->len) ||
      (len <= 0))
    {
      caml_invalid_argument("Vector.blit: invalid index or length");
    };

  if ((src == dst) &&
      ! ((src_idx + len <= dst_idx) ||
	 (dst_idx + len <= src_idx)))
    {
      caml_invalid_argument("Vector.blit: subvectors overlap");
    };
    
  // loop until length is zero

  while (len > 0)
    {
      src_p = src_idx / BITS_PER_WORD;
      src_q = src_idx % BITS_PER_WORD;
      src_r = BITS_PER_WORD - src_q;
      dst_p = dst_idx / BITS_PER_WORD;
      dst_q = dst_idx % BITS_PER_WORD;
      dst_r = BITS_PER_WORD - dst_q;

      if ((src_q == 0) && (dst_q == 0))
	{
	  // Particular case where both subvectors are aligned on a word

	  if (len >= BITS_PER_WORD)
	    {
	      // At least one full word to copy

	      count = len / BITS_PER_WORD;
	  
	      // Copies subvector 

	      memcpy(dst->vec+dst_p, src->vec+src_p, count*sizeof(word));

	      // Increments indexes and decrements length

	      src_idx += BITS_PER_WORD*count;
	      dst_idx += BITS_PER_WORD*count;
	      len -= BITS_PER_WORD*count;
	    }
	  else
	    {
	      // Strictly less than one full word to copy

	      // Compute mask

	      m = WORD_FALSE;

	      for (i=0;i<len;i++)
		{
		  m |= WORD_ONE << i;
		}

	      // Copy bits within the mask, preserving bits outside the mask

	      dst->vec[dst_p] = (~m & dst->vec[dst_p]) | (m & src->vec[src_p]);

	      // Adjust indexes and length

	      src_idx += len;
	      dst_idx += len;
	      len = 0;
	    };
	}
      else
	{
	  // One of the two subvectors is not aligned to a word

	  // compute the number of bits to move
	  
	  if (dst_r < src_r) { r = dst_r; } else { r = src_r; };
	  if (len < r) { r = len; };

	  // Extract bits from source and shift to align with destination

	  w = src->vec[src_p];
	  i = dst_q - src_q;

	  if (i > 0) { w <<= i; };
	  if (i < 0) { w >>= -i; };

	  // compute mask

	  m = WORD_FALSE;

	  for (i=0;i<r;i++)
	    {
	      m |= WORD_ONE << i;
	    };

	  m <<= dst_q;

	  // store in destination using mask

	  dst->vec[dst_p] = (~m & dst->vec[dst_p]) | (m & w);

	  // Adjust length and indexes

	  src_idx += r;
	  dst_idx += r;
	  len -= r;
	};
    };

  // Exit

  CAMLreturn(Val_unit);
}

// Byte read and write

CAMLprim value vector_ml_get_byte(value vu, value vidx, value vlen)
{
  CAMLparam3(vu,vidx,vlen);
  vector_block * u = (vector_block *)Data_custom_val(vu);
  int idx = Int_val(vidx);
  int len = Int_val(vlen);
  int idx_p = idx / BITS_PER_WORD;
  int idx_q = idx % BITS_PER_WORD;
  int idx_r = BITS_PER_WORD - idx_q;
  word w,m,n;
  int i,l;

  // Sanity checks

  if ((idx < 0) || (len < 0) || (len > MAX_BYTE_SIZE) || (idx + len > u->len))
    {
      caml_invalid_argument("Vector.get_byte: invalid index or length");
    };

  // Byte spans other one or two word?

  if (len <= idx_r)
    {
      // One word
      
      // Compute mask

      m = WORD_FALSE;
      
      for (i=0;i<len;i++)
	{
	  m |= WORD_ONE << i;
	};
      
      // Extract byte and apply mask

      w = (u->vec[idx_p] >> idx_q) & m;
    }
  else
    {
      // Two words

      // Compute masks

      m = WORD_FALSE;
      
      for (i=0;i<idx_r;i++)
	{
	  m |= WORD_ONE << i;
	};
      
      n = WORD_FALSE;
      
      for (i=idx_r;i<len;i++)
	{
	  n |= WORD_ONE << i;
	};
      
      w =
	((u->vec[idx_p] >> idx_q) & m) |
	((u->vec[idx_p+1] << idx_r) & n);

    };

  // Return byte
  
  CAMLreturn(Val_int((int)w));
}

CAMLprim value vector_ml_set_byte(value vu, value vidx, value vlen, value vbte)
{
  CAMLparam4(vu,vidx,vlen,vbte);
  vector_block * u = (vector_block *)Data_custom_val(vu);
  int idx = Int_val(vidx);
  int len = Int_val(vlen);
  int bte = Int_val(vbte);
  int idx_p = idx / BITS_PER_WORD;
  int idx_q = idx % BITS_PER_WORD;
  int idx_r = BITS_PER_WORD - idx_q;
  word w,m,n;
  int i,l;

  // Sanity checks

  if ((idx < 0) || (len < 0) || (len > MAX_BYTE_SIZE) || (idx + len > u->len))
    {
      caml_invalid_argument("Vector.set_byte: invalid index or length");
    };

  // Byte spans other one or two word?

  if (len <= idx_r)
    {
      // One word
      
      // Compute mask

      m = WORD_FALSE;
      
      for (i=idx_q;i<idx_q+len;i++)
	{
	  m |= WORD_ONE << i;
	};
      
      // Shift and set byte using mask

      u->vec[idx_p] = (~m & u->vec[idx_p]) | (m & ((word)bte << idx_q));
    }
  else
    {
      // Two words
      
      // Compute masks

      m = WORD_FALSE;
      
      for (i=idx_q;i<BITS_PER_WORD;i++)
	{
	  m |= WORD_ONE << i;
	};
      
      n = WORD_FALSE;
      
      for (i=0;i<len-idx_r;i++)
	{
	  n |= WORD_ONE << i;
	};
      
      // set words using masks

      u->vec[idx_p] = (~m & u->vec[idx_p]) | (m & ((word)bte << idx_q));
      u->vec[idx_p+1] = (~n & u->vec[idx_p+1]) | (n & ((word)bte >> idx_r));
    };

  // Return byte
  
  CAMLreturn(Val_unit);
}

