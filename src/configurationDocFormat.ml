(*
 *
 *  Doc Format of Configuration (for XML file generation)
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* This module provides standard text corresponding to doc format
 * of Configuration module.
 *)

(*-----------------------------------------------------------------
 * Module openning, parameter & utilities
 *-----------------------------------------------------------------*)

open Pp
   
(* function to add the double quotes around a string *)
let stringify s = "\""^(String.escaped s)^"\""


                
(*-----------------------------------------------------------------
 *  Parameter name (string)
 *-----------------------------------------------------------------*)

let mainconfig_root_param = "main_config"
let classifiers_root_param = "global_classifiers"
let classifiers_src_format_param src_format = src_format ^ "_classifiers"
  
   
(*-----------------------------------------------------------------
 * Standard string in xml format
 *-----------------------------------------------------------------*)

(* XML prologue *)
                
let xml_prologue_string =
  "<?xml "
  ^ " version=" ^ (stringify "1.0")
  ^ " encoding=" ^ (stringify "UTF-8")
  ^ " standalone=" ^ (stringify "yes")
  ^ "?>"


(* Root tags *)
  
let mainconfig_root_start_string = "<" ^ mainconfig_root_param ^ ">"
let mainconfig_root_end_string = "</" ^ mainconfig_root_param ^ ">"

let classifiers_root_start_string = "<" ^ (classifiers_root_param) ^ ">"
let classifiers_root_end_string = "</" ^ (classifiers_root_param) ^ ">"

let classifiers_src_format_start_string src_format =
  "<" ^ (classifiers_src_format_param src_format) ^ ">"
let classifiers_src_format_end_string src_format = 
  "</" ^ (classifiers_src_format_param src_format) ^ ">"
                                           

(* Configuration data *)

let cdv_string config_data value =
  "<" ^ config_data ^ " "
  ^ "value=" ^ (stringify value)
  ^ "/>"

let cdgv_string config_data group value =
  "<" ^ config_data ^ " "
  ^ "group=" ^ (stringify group)
  ^ " value=" ^ (stringify value)
  ^ "/>"

let cdngk_string config_data name group keys =
  "<" ^ config_data ^ " "
  ^ "name=" ^ (stringify name)
  ^ " group=" ^ (stringify group)
  ^ " keys=" ^ (stringify keys)
  ^ "/>"
                                           

(* Classifier definition *)

let classifier_definition_string name value =
  "<def "
  ^ "name=" ^ (stringify name)
  ^ " keys=" ^ (stringify value)
  ^ "/>"


  
(*-----------------------------------------------------------------
 * Functions to generates doc containing mutual config text
 *-----------------------------------------------------------------*)

(* Text of prologue *)

let xml_prologue_text = text xml_prologue_string  
  

(* Text of configuration data *)
  
let cdv_text config_data value =
  break ^^ (text (cdv_string config_data value))
  
let cdgv_text config_data group value =
  break ^^ (text (cdgv_string config_data group value))
  
let cdngk_text config_data name group keys =
  break ^^ (text (cdngk_string config_data name group keys))
  
  
  
(*-----------------------------------------------------------------
 * Functions to generates doc containing specific main config text 
 *-----------------------------------------------------------------*)

(* Text of root tag *)

let mainconfig_root_text s_content =
  break ^^ (text mainconfig_root_start_string)
  ^^ (nest 4 s_content)
  ^^ break ^^ (text mainconfig_root_end_string)
  

(* Text of main config data *)
  
let input_working_file_postfix_text value = cdv_text "input_working_file_postfix" value
let input_adapting_script_text value = cdv_text "input_adapting_script" value
let classifier_choice_text value = cdv_text "classifier_choice" value
let ontology_base_file_text value = cdv_text "ontology_base_file" value
let ontology_global_files_text value = cdv_text "ontology_global_files" value
let ontology_label_type_text value = cdv_text "ontology_label_type" value 
let instances_number_text value = cdv_text "instances_number" value
let verbose_text value = cdv_text "verbose" value
let approximation_requested_text value = cdv_text "approximation_requested" value
let approximation_limit_text value = cdv_text "approximation_limit" value
let output_directory_text value = cdv_text "output_directory" value
let output_tnfnet_text value = cdv_text "output_tnfnet" value
let output_tnfgraph_text value = cdv_text "output_tnfgraph" value
let output_seven_text value = cdv_text "output_seven" value
let output_a7xspm_text value = cdv_text "output_a7xspm" value
let output_events_table_text value = cdv_text "output_events_table" value
let tnfgraph_label_type_text value = cdv_text "tnfgraph_label_type" value
let seven_sensor_label_type_text value = cdv_text "seven_sensor_label_type" value
let seven_five_value_type_text value = cdv_text "seven_five_value_type" value
let seven_location_computing_text value = cdv_text "seven_location_computing" value
let seven_analysis_text value = cdv_text "seven_analysis" value
let seven_event_number_limit_text value = cdv_text "seven_event_number_limit" value
  
  
  
(*-----------------------------------------------------------------
 * Functions to generates doc containing specific classifier config
 * text 
 *-----------------------------------------------------------------*)
                      
(* Text of classifiers root tag *)

let classifiers_root_text s_content =
  break ^^ (text (classifiers_root_start_string))
  ^^ (nest 4 s_content)
  ^^ break ^^ (text (classifiers_root_end_string))


(* Text of classifiers config data *)

let separator_text value = cdv_text "separator" value
                         
let prefix_text group value = cdgv_text "prefix" group value
  
let keys_text group keys_list sep =
  let value = String.concat sep keys_list in
  cdgv_text "keys" group value

let classifier_text name group keys_list sep =
  let keys = String.concat sep keys_list in
  cdngk_text "def" name group keys
