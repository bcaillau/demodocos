(*
 *
 * Module to load an ontology basis 
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * The main function of this module is to load an ontology basis 
 * (from an ontology base file).
 *) 

(*-----------------------------------------------------------------
 * Module openning, parameter & utilities
 *-----------------------------------------------------------------*)

(* functions to obtain a given option type from a string *)

let string_option_of_string s = match s with "" -> None | x -> Some x

let int_option_of_string s = match s with "" -> None | x -> Some (int_of_string x)


(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

(* Structure of ontology : mapping onto / vr *)
                     
type t = (string * string) list


                                                          
(*-----------------------------------------------------------------
 * Generic functions to analyze the elements of a xml document
 *-----------------------------------------------------------------*)

(* function to filter the elements of a XML document for a list of given tags *)
                                                          
let filter_elements target_tags_list xml_doc =
  List.filter
    (function
     | Xml.Element (tag,_,_) -> List.mem tag target_tags_list
     | Xml.PCData _ -> false)
    xml_doc

  
(* function to filter the elements of a sequence for a list of given tags *)
  
let filter_elements_of_sequence target_tags_list sequence =
  let f target_list seq =
    match seq with
    | Xml.Element (_, _, children) ->
       List.rev_append target_list (filter_elements target_tags_list children)
    | _ -> target_list
  in
  List.fold_left f [] sequence
  
  
(* function to obtain a value corresponding to an attribute key *)
  
let rec get_string_attr key s0 att_list =
  match att_list with
  | [] -> s0
  | (h,s) :: l ->
     if h = key
     then s
     else get_string_attr key s0 l
  
  
(* function to obtain a value corresponding to a xml element *)
  
let rec get_string_element elementTag s0 xml_doc =
  try
    let target = List.hd (filter_elements (elementTag::[]) xml_doc) in
    match target with
    | Xml.Element (_, _, children) ->
       (match children with (Xml.PCData x)::[] -> x | _ -> s0)
    | Xml.PCData _ -> s0
  with _ -> s0

    

(* function to map the attributes associated to a list of
 * xml element instance (element appearing several times), 
 * from a root xml element *)
    
let map_attributes_of_xml_element src_xml att_mapping e_name =
  let elements = filter_elements [e_name] src_xml in
  let e_mapping e =
    match e with
    | Xml.Element (_, attributes, _) -> att_mapping attributes
    | _ -> att_mapping []
  in
  List.map e_mapping elements
    

(* function to obtain a values list of an attribute associated to 
 * a xml elements list (element appearing several times), 
 * from a root xml element *)
    
let get_values_of_attributes_from_xml src_xml e_name att_name default_value =
  let att_mapping attr =
    get_string_attr att_name default_value attr
  in
  map_attributes_of_xml_element src_xml att_mapping e_name
  

(* function to obtain a single value of an attribute associated to 
 * a xml element, from a root xml element *)
  
let get_value_of_attribute_from_xml xml e_name att_name default_value =
  List.hd (get_values_of_attributes_from_xml xml e_name att_name default_value) 

  
                  
(*-----------------------------------------------------------------
 * Functions to load an ontology basis
 *-----------------------------------------------------------------*)

(* function to obtain the ontology value on xml source, namely the value
 * corresponding to tag "skos:prefLabel" and "lang = en" *)

let get_onto_value xml_source =
  try
    let skos_preflabel_list = filter_elements ["skos:prefLabel"] xml_source in
    let filter xml =
      match xml with
      | Xml.Element (_, attributes, _) -> 
         ((get_string_attr "xml:lang" "" attributes) = "en")
      | _ -> false
    in
    let onto_xml = List.filter filter skos_preflabel_list in
    get_string_element "skos:prefLabel" "" onto_xml
  with _ -> ""
          

(* functions to obtain the VR value on xml source, namely the value
 * corresponding to tag "UFO_exact_match" or tag "UFR_exact_match" *)

let get_exact_match_value xml_source s_match_ref =
  let exact_match_list = filter_elements [s_match_ref] xml_source in
  get_string_element s_match_ref "" exact_match_list

let get_vr_value xml_source =
  try
    let ufo_exact_match_value =
      get_exact_match_value xml_source "UFO_exact_match"
    in
    let ufr_exact_match_value =
      get_exact_match_value xml_source "UFR_exact_match"
    in
    match ufo_exact_match_value with
    | "" -> ufr_exact_match_value
    | v -> ufo_exact_match_value
  with _ -> ""
  

(* function to add an element with the informations of the xml source,
 * wherein an element associate an ontology value and a vr value from the xml source *)

let add_element ob_list xml_source =
  let f_add children =
    let onto_value = get_onto_value children in
    let vr_value = get_vr_value children in
    match (onto_value, vr_value) with
    | ("", _) | (_, "") -> ob_list
    | _ -> (onto_value, vr_value)::ob_list 
  in
  match xml_source with
  | Xml.Element ("owl:Class", _, children) -> f_add children
  | _ -> invalid_arg "DataOntologyBasisIn.add_element: syntax error"
          

(* function to load all values from a xml element (xconfig) *)
          
let load_all_values xml =
  (* let owl_class_tag = "owl:Class" in *)
  (* let vr_ufo_exact_match_tag = "UFO_exact_match" in *)
  (* let onto_skos_prefLabel_tag = "skos:prefLabel" in *)
  (* let onto_attribute_xml_lang_tag = "xml:lang" in *)
  (* let onto_attribute_xml_lang_value = "en" in *)
  let owl_class_list = filter_elements ["owl:Class"] xml in
  List.fold_left add_element [] owl_class_list

  
(* function to load the ontology basis from an ontology basis file *)
  
let load ontology_basis_file =
  let xconfig = Xml.parse_file ontology_basis_file in
  match xconfig with
  | Xml.Element (tag, attributes, children) ->
     if tag = "rdf:RDF"
     then load_all_values children
     else invalid_arg "DataOntologyBasisIn.load: syntax error"
  | _ -> invalid_arg "DataOntologyBasisIn.load: syntax error"


       
(*-----------------------------------------------------------------
 * Utilities
 *-----------------------------------------------------------------*)

(* val output_ontology : DataOntologyBasis.t -> out_channel -> unit *)

let output_ontology_basis t file =
  let
    f_print (onto, vr) = (Printf.fprintf file " ( %s ; %s ) \n" onto vr)
  in
  begin
    Printf.fprintf file "//////////////////////////////////////////////////////////// \n";
    Printf.fprintf file "// ONTOLOGY BASIS \n";
    Printf.fprintf file "//////////////////////////////////////////////////////////// \n\n";
    List.iter f_print (List.rev t);
    Printf.fprintf file "\n"
  end

(* val print_ontology_basis_to_file : DataOntologyBasis.t -> string -> unit *)

let print_ontology_basis_to_file t file_name =
  let file = open_out file_name in
    try
      output_ontology_basis t file;
      close_out file
    with
    | y ->
       begin
	 close_out file;
	 raise y
       end
