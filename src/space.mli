(*
 *
 * Region spaces
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA, Rennes, France
 *
 *)

type pair =
    {
      test : Region.t;
      flip : Region.t
    }

(* Generates the set of regions of the graph for a given event *)

val reachability : Vector.t Parikh.t -> int -> Region.t

(* Generates the pair of region spaces of the graph for a given event *)

val pair :  Vector.t Parikh.t -> int -> pair 

(* Generates the array of pairs region spaces of the graph *)

val pair_array : Vector.t Parikh.t -> pair array
