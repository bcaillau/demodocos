(*
 *
 * Xorsat: satisfiability of systems of XOR formulas
 *
 * (C) Benoit Caillaud <benoit.caillaud@inria.fr>, INRIA Rennes, France, 2012
 *
 *)

(* Xor constraints *)

type c = Vector.t

let cmake n = Vector.make (n+1) false

let cclone c = Vector.clone c

let dimension c = (Vector.length c)-1

let get_polarity c = Vector.get c ((Vector.length c)-1)

let get_coeff c i = Vector.get c i

let set_polarity c b = Vector.set c ((Vector.length c)-1) b

let set_coeff c i a = Vector.set c i a

let ixor c1 c2=
  Vector.ixor c1 c2

let cxor c1 c2 =
  Vector.vxor c1 c2

let leading_zeroes c =
  let n = dimension c
  and m = Vector.leading_zeroes c in
    min n m

let arity c =
  if get_polarity c
  then (Vector.arity c)-1
  else Vector.arity c

let csort l = List.sort compare l
  
(* eliminates double and trivial constraints *)

let trace s =
  begin
    Printf.printf "<%s>" s;
    flush stdout
  end

let trace_int n = trace (string_of_int n)

let celim a =
  let a' = Array.of_list a in
  let n = Array.length a'
  and q = ref 0
  and l = ref [] in
    begin
      while (!q) < n do
	begin
	  if (((arity a'.(!q)) > 0) || (get_polarity (a'.(!q)))) && (((!q) <= 0) || (a'.((!q)-1) <> a'.(!q)))
	  then
	    begin
	      l := (!q)::(!l) (* ; *)
	      (* trace_int (!q) *)
	    end
	end;
	q := (!q)+1 (* ; *)
	(* trace "++" *)
      done;
      (* trace "done"; *)
      List.map (fun i -> a'.(i)) (List.rev !l)
    end
      
let csort_elim a =
  begin
    (* trace "sorting"; *)
    let b = csort a in
      begin
	(* trace "elim"; *)
	celim b
      end
  end
  
(* is constraint not satisfiable? *)
  
let cunsat c = ((arity c) = 0) && (get_polarity c)
  
(* insert a constraint in a sorted list of constraints, if not already there *)
  
let cinsert_elim c l0 =
  let rec xcie =
    function
	[] -> [c]
      | (d::k) as l ->
	  let g = compare c d in
	    if g < 0
	    then c::l
	    else
	      if g > 0
	      then d::(xcie k)
  	      else l
  in
    if ((leading_zeroes c) < (dimension c)) || (get_polarity c)
    then xcie l0
    else l0
      
(* eliminate one variable on a sorted list of constraints *)

let celiminate c l0 =
  let m = leading_zeroes c in
  let rec elm =
    function
	[] -> []
      | (d::k) as l ->
	  if (get_coeff d m)
	  then
	    begin
	      ixor d c;
	      cinsert_elim d (elm k)
	    end
	  else
	    l
  in
    elm l0

(* Gauss pivoting: puts a system in upper triangular form *)

let cgauss a0 d =
  let a = Array.map (List.map (fun e -> cclone e)) a0
  and b = Array.make (d+1) None in
    begin
      for i=0 to d-1 do
	match a.(i) with
	    c::l ->
	      begin
		b.(i) <- Some c;
		List.iter
		  (fun e ->
		    begin
		      ixor e c;
		      let j = leading_zeroes e in
			if j < d
			then a.(j) <- e :: a.(j)
			else
			  if get_polarity e
			  then a.(d) <- [e]
		    end)
		  l
	      end
	  | _ -> ()
      done;
      begin
	match a.(d) with
	    [] -> ()
	  | [e] -> b.(d) <- Some e
	  | _ -> failwith "Xorsat.cgauss: impossible case. Please report bug."
      end;
      b
    end

exception Inconsistent

(* Compute a solution. System should be in upper triangular form *)

let csolve a d x0 =
  let n = Vector.length x0 in
  let x = Vector.extend x0 (n+1) true in
  begin
    assert (n=d);
    match a.(d) with
	Some _ -> raise Inconsistent
      | None ->
	  begin
	    for i = n-1 downto 0 do
	      match a.(i) with
		  None -> ()
		| Some c ->
		    begin
		      let z = Vector.partial_prod c x (i+1) n in
			Vector.set x i z
		    end
	    done;
	    Vector.restrict x n
	  end
  end

(* val cprint : c -> unit *)

let cto_string c =
  let n = dimension c 
  and l = ref [] in
  let rec expr =
    function
	[] -> "0"
      | [i] -> "x"^(string_of_int i)
      | i::l -> "x"^(string_of_int i)^"+"^(expr l)
  in
    begin
      for i=n-1 downto 0 do
	if get_coeff c i
	then l := i :: (!l)
      done;
      (expr (!l))^"="^(if get_polarity c then "1" else "0")
    end

let cprint c =
  let s = cto_string c in
    print_endline s

(* Xor constraint systems *)

type m = Unsorted | Sorted | Triangular

type n =
    Uc of c list
    | Sc of c list array
    | Tc of c option array

type t =
    {
      dimension : int;
      mutable constr : n;
    }

let make d =
  {
    dimension = d;
    constr = Uc []
  }

let clone =
  function
      { dimension = d; constr = Uc l } -> { dimension = d; constr = Uc l }
    | { dimension = d; constr = Sc a } -> { dimension = d; constr = Sc (Array.copy a) }
    | { dimension = d; constr = Tc a } -> { dimension = d; constr = Tc (Array.copy a) }

let print =
  let prl l =
    begin
      print_endline "[";
      List.iter
	(fun c ->
	  begin
	    print_string "  ";
	    cprint c
	  end)
	l;
      print_endline "]";
    end in
    function
	{ constr = Uc l } -> prl l
      | { dimension = d; constr = Sc a } ->
	  let l = ref [] in
	    begin
	      for i=d downto 0 do
		l := a.(i) @ (!l)
	      done;
	      prl (!l)
	    end
      | { dimension = d; constr = Tc a } ->
	  let l = ref [] in
	    begin
	      for i=d downto 0 do
		match a.(i) with
		    None -> ()
		  | Some c -> l:= c :: (!l)
	      done;
	      prl (!l)
	    end

let sort_constr_list l d =
  let a = Array.make (d+1) [] in
    begin
      List.iter
	(fun c ->
	  let i = leading_zeroes c in
	    a.(i) <- c :: a.(i))
	l;
      for i=0 to d do
	a.(i) <- csort_elim a.(i)
      done;
      a
    end

let rec set_mode ({ dimension = d; constr = m } as s) m' =
  match (m,m') with
      ((Uc _),Unsorted) -> ()
    | ((Sc a),Unsorted) ->
	let l = ref [] in
	  begin
	    for i=d downto 0 do
	      l := a.(i) @ (!l)
	    done;
	    s.constr <- Uc (!l)
	  end
    | ((Tc a),Unsorted) ->
	let l = ref [] in
	  begin
	    for i=d downto 0 do
	      match a.(i) with
		  None -> ()
		| Some c -> l := c :: (!l)
	    done;
	    s.constr <- Uc (!l)
	  end
    | ((Uc l),Sorted) ->
	s.constr <- Sc (sort_constr_list l d)
    | ((Sc _),Sorted) -> ()
    | ((Tc a),Sorted) ->
	let b = Array.map (function None -> [] | Some c -> [c]) a in
	s.constr <- Sc b
    | ((Uc l),Triangular) ->
	begin
	  set_mode s Sorted;
	  set_mode s Triangular
	end
    | ((Sc a),Triangular) ->
	  s.constr <- Tc (cgauss a d)
    | ((Tc _),Triangular) -> ()

let rec add ({ dimension=d; constr=m } as s) c =
  begin
    assert ((dimension c) = d);
    match m with
	Uc l -> s.constr <- Uc (c :: l)
      | Sc a ->
	  let i = leading_zeroes c in
	    a.(i) <- cinsert_elim c a.(i)
      | Tc a ->
	  let e = cclone c
	  and x = ref true in
	    while !x do
	      let i = leading_zeroes e in
		if (i < d) || (get_polarity e)
		then
		  match a.(i) with
		      None ->
			begin
			  a.(i) <- Some e;
			  x := false
			end
		    | Some f ->
			if e<>f
			then ixor e f
			else x := false
		else x := false
	    done
  end
	      
let solve { dimension=d; constr=m } x0 =
  begin
    assert ((Vector.length x0) = d);
    match m with
	Uc l ->
	  let a = sort_constr_list l d in
	  let b = cgauss a d in
	    csolve b d x0
      | Sc a ->
	  let b = cgauss a d in
	    csolve b d x0
      | Tc b ->
	  csolve b d x0
  end

(* val vc_blit : src:Vector.t -> src_idx:int -> dst:c -> dst_idx:int -> len:int -> unit *)

let vc_blit ~src:src ~src_idx:src_idx ~dst:dst ~dst_idx:dst_idx ~len:len =
  if (dst_idx + len) > (dimension dst)
  then invalid_arg "Xorsat.vc_blit: invalid destination index or length"
  else Vector.blit ~src:src ~src_idx:src_idx ~dst:dst ~dst_idx:dst_idx ~len:len

