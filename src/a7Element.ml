(*
 *
 * Abstract #7 element
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* This module provides a data structure and functions related
 * to this structure. 
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities
 *-----------------------------------------------------------------*)

open A7Attributes
   

(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

(* types to specify the #7 elements *)

type e_type = Place | SensorEvent | Transition  
                                
type e_origin = Ispm | Flipflop | Seven

type transition_def =
  {
    event_urn : int ;
    up_place_urn : int ;
    down_place_urn : int
  }

type five_data =
  {
    relation : f_relation ;
    objects_list : f_objects
  }

type location = { x : int ; y : int }


(* defaults values to specify the #7 elements *)

let default_urn = 0
                  
let default_classifier_ref = 0
                  
let default_transition_def =
  {
    event_urn = default_urn ;
    up_place_urn = default_urn ;
    down_place_urn = default_urn
  }

let default_five_relation = "NO_RELATION"
let default_five_objects_list = []
let default_five_data =
  {
    relation = default_five_relation ;
    objects_list = default_five_objects_list
  }
  
let default_location = { x = 0 ; y = 0 }


(* Structure of abstract #7 element *)
                     
type t = 
  {
    e_type : e_type ;
    urn : int ;
    sensor_label : string ;
    classifier_ref : int option ;
    origin : e_origin option ;
    transition_def : transition_def option ;
    five_data : five_data option ;
    location : location
  }


(*-----------------------------------------------------------------
 * Constructeur(s)
 *-----------------------------------------------------------------*)

(* val define_place : int -> place *)

let define_place i =
  {    
    e_type = Place ;
    urn = i ;
    sensor_label = "" ;
    classifier_ref = None ;
    origin = None ;
    transition_def = None ;
    five_data = None ;
    location = default_location
  }
                      

(* val define_sensor_event : int -> int -> sensor_event *)
  
let define_sensor_label_init c_ref = string_of_int c_ref

let define_sensor_event i c_ref =
  {
    e_type = SensorEvent ;
    urn = i ;
    sensor_label = (define_sensor_label_init c_ref) ;
    classifier_ref = Some c_ref ;
    origin = None ;
    transition_def = None ;
    five_data = None ;
    location = default_location
  }
  
     
(* val define_transition : int -> int -> int -> transition *)
  
let define_transition_def edge_urn up_place_urn down_place_urn =
  {
    event_urn = edge_urn ;
    up_place_urn = up_place_urn ;
    down_place_urn = down_place_urn
  }

let define_transition edge_urn up_place_urn down_place_urn =
  {
    e_type = Transition ;
    urn = edge_urn ;
    sensor_label = "" ;
    classifier_ref = None ;
    origin = None ;
    transition_def = Some (define_transition_def edge_urn up_place_urn down_place_urn) ;
    five_data = None ;
    location = default_location
  }


(*-----------------------------------------------------------------
 * Accesseurs
 *-----------------------------------------------------------------*)    

(* val get_type : t -> e_type *)
(* val get_urn : t -> int *)
(* val get_signature : t -> (e_type * int) *)

let get_type t = t.e_type
               
let get_urn t = t.urn
              
let get_signature t = (t.e_type, t.urn)

               
(* val get_classifier_ref : t -> int option *)
(* val get_origin : t -> e_origin option *)
                
let get_classifier_ref t =
  match t.classifier_ref with
  | Some c_ref -> c_ref
  | _ -> default_classifier_ref
                         
let get_origin t = t.origin

                 
(* val get_event_urn : t -> int *)
(* val get_up_place_urn : t -> int *)
(* val get_down_place_urn : t -> int *)

let get_transition_def t =
  if ((get_type t) = Transition)
  then match (t.transition_def) with
       | Some def -> def
       | _ -> default_transition_def
  else default_transition_def

let get_event_urn t =
  let def = (get_transition_def t) in def.event_urn

let get_up_place_urn t =
  let def = (get_transition_def t) in def.up_place_urn

let get_down_place_urn t =
  let def = (get_transition_def t) in def.down_place_urn
                                  
  
(* val get_five_relation : t -> f_relation *)
(* val get_five_objects_list : t -> f_objects *)

let get_five_data t =
  if ((get_type t) = SensorEvent)
  then match (t.five_data) with
       | Some data -> data
       | _ -> default_five_data
  else default_five_data
  
let get_five_relation t =
  let data = (get_five_data t) in data.relation
                                
let get_five_objects_list t =
  let data = (get_five_data t) in data.objects_list
                             
  
(* val get_location : t -> (int * int) *)
                        
let get_location t = (t.location.x, t.location.y)


(* val set_sensor_label : t -> string -> t *)
(* val set_origin : t -> event_origin option -> t *)
(* val set_five_relation : t -> string option -> t *)
(* val set_five_objects_list : t -> (string * string) list -> t *)

let set_sensor_label t new_label =
  {
    e_type = t.e_type ;
    urn = t.urn ;
    sensor_label = new_label ;
    classifier_ref = t.classifier_ref ;
    origin = t.origin ;
    transition_def = t.transition_def ;
    five_data = t.five_data ;
    location = t.location
  }

let set_origin t new_origin =
  {
    e_type = t.e_type ; 
    urn = t.urn ;
    sensor_label = t.sensor_label ;
    classifier_ref = t.classifier_ref ;
    origin = new_origin ;
    transition_def = t.transition_def ;
    five_data = t.five_data ;
    location = t.location
  }

let set_five_data t new_five_data =
  {
    e_type = t.e_type ; 
    urn = t.urn ;
    sensor_label = t.sensor_label ;
    classifier_ref = t.classifier_ref ;
    origin = t.origin ;
    transition_def = t.transition_def ;
    five_data = new_five_data ;
    location = t.location
  }

let set_five_relation t new_relation =
  match (t.five_data, new_relation) with
  | (Some f_data, Some f_relation) ->
     let new_f_data =
       Some { relation = f_relation ; objects_list = f_data.objects_list }
     in
     set_five_data t new_f_data
  | (None, Some f_relation) -> 
     let new_f_data =
       Some { relation = f_relation ; objects_list = default_five_objects_list }
     in
     set_five_data t new_f_data
  | _ -> t

let set_five_objects_list t new_objects_list =
  match (t.five_data, new_objects_list) with
  | (Some f_data, Some f_objects_list) ->
     let new_f_data =
       Some { relation = f_data.relation ; objects_list = f_objects_list }
     in
     set_five_data t new_f_data
  | (None, Some f_objects_list) -> 
     let new_f_data =
       Some { relation = default_five_relation ; objects_list = f_objects_list }
     in
     set_five_data t new_f_data
  | _ -> t


(* val set_location : t -> (int * int) -> t *)

let set_location t new_location =
  let (x, y) = new_location in
  {
    e_type = t.e_type ; 
    urn = t.urn ;
    sensor_label = t.sensor_label ;
    classifier_ref = t.classifier_ref ;
    origin = t.origin ;
    transition_def = t.transition_def ;
    five_data = t.five_data ;
    location = { x = x ; y = y }
  }


(*-----------------------------------------------------------------
 * Element specification as string
 *-----------------------------------------------------------------*)

let get_id_string e_type urn =
  match e_type with
  | Place -> "Place_" ^ (string_of_int urn)
  | SensorEvent -> "Event_" ^ (string_of_int urn)
  | Transition -> "Transition_" ^ (string_of_int urn)

let get_label_string e_type t =
  match e_type with
  | Place -> "P" ^ (string_of_int t.urn)
  | SensorEvent -> t.sensor_label
  | Transition -> "T" ^ (string_of_int t.urn)

let get_id t = (get_id_string t.e_type t.urn)

let get_label t = (get_label_string t.e_type t)

let get_event_idref t =
  let urn = get_event_urn t in (get_id_string SensorEvent urn)

let get_up_place_idref t =
  let urn = get_up_place_urn t in (get_id_string Place urn)

let get_down_place_idref t =
  let urn = get_down_place_urn t in (get_id_string Place urn)
