(*
 *
 * Abstract #7 data (A7) corresponding to xml #7 file (x7f)
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * This module offers functions to generate the abstract #7 data 
 * corresponding to a xml #7 file.
 *)

(*-----------------------------------------------------------------
 * Module openning, parameter & utilities
 *-----------------------------------------------------------------*)

open A7XmlDocFormat

(* function to obtain the urn corresponding to an A7 element,
 * from the name of the given element and the type *)
   
let get_urn_of_a7element e_id e_type =
  let n1 = String.length e_type in
  let n2 = (String.length e_id) - n1 in
  int_of_string (String.sub e_id n1 n2)
  
                

(*-----------------------------------------------------------------
 * Generic functions to analyze the elements of a xml document
 *-----------------------------------------------------------------*)

(* function to filter the elements of a XML document for a list of given tags *)
  
let filter_elements target_tags_list xml_doc =
  List.filter
    (function
     | Xml.Element (tag,_,_) -> List.mem tag target_tags_list
     | Xml.PCData _ -> false)
    xml_doc
  

(* function to filter the elements of a sequence for a list of given tags *)
  
let filter_elements_of_sequence target_tags_list sequence =
  let f target_list seq =
    match seq with
    | Xml.Element (_, _, children) ->
       List.rev_append target_list (filter_elements target_tags_list children)
    | _ -> target_list
  in
  List.fold_left f [] sequence
  
  
(* function to obtain a value corresponding to an attribute key *)
  
let rec get_string_attr key s0 att_list =
  match att_list with
  | [] -> s0
  | (h,s) :: l ->
     if h = key
     then s
     else get_string_attr key s0 l
  
  
(* function to obtain a value corresponding to a xml element *)
  
let rec get_string_element elementTag s0 xml_doc =
  try
    let target = List.hd (filter_elements (elementTag::[]) xml_doc) in
    match target with
    | Xml.Element (_, _, children) ->
       (match children with (Xml.PCData x)::[] -> x | _ -> s0)
    | Xml.PCData _ -> s0
  with _ -> s0

    
  
(*-----------------------------------------------------------------
 * Generic functions to analyse the elements of a xml #7 document
 *-----------------------------------------------------------------*)

let get_element_id attributes = List.hd (filter_elements ["id"] attributes)
                              

let get_element_idref attributes = List.hd (filter_elements ["idref"] attributes)
                                 

let get_element_idref_of_transition target_tags_list e_type children =
  let default_idref = "" in
  let idref_str =
    match List.hd (filter_elements target_tags_list children) with
    | Xml.Element (_, attributes, _) -> 
       get_string_attr "idref" default_idref attributes
    | _ -> default_idref
  in
  get_urn_of_a7element idref_str e_type

  
       
(*-----------------------------------------------------------------
 * Generic functions to add the elements to an abstract #7
 *-----------------------------------------------------------------*)

(* function to add a sensor/event to A7 from attributes/children of a x7 event *)
  
(* format of sensorEvent: 
<event id="Event_XXX"> 
 [...]
 <sensorCheck classname="RelationSensor, SEVEN.FIVE">
  <param name="relation" value="xxx"/>
  <param name="object" value="xxx"/>
 </sensorCheck>
 [...]
 <sensorLabel>xxx</sensorLabel>
</event>
 *)
  
let add_sensor_event_to_a7 attributes children a7 =
  let default_id = "" in
  let default_label = "default_label" in
  let e_id = get_string_attr "id" default_id attributes in
  let e_urn = get_urn_of_a7element e_id "Event_" in 
  let e_sensor_label = get_string_element "sensorLabel" default_label children in
  let se = (A7Element.define_sensor_event e_urn e_urn) in
  let se = A7Element.set_sensor_label se e_sensor_label in
  (* TODO : let se = ... in *)
  (A7.add_sensor_event a7 se)
  

(* function to add a place to A7 from attributes/children of a x7 place *)
  
(*
<place xsi:type="Place" id="Place_XXX" label="xxx">
 [...]
</place>
 *)
  
let add_place_to_a7  attributes children a7 =
  let default_id = "" in
  let e_id = get_string_attr "id" default_id attributes in
  let e_urn = get_urn_of_a7element e_id "Place_" in 
  let p = (A7Element.define_place e_urn) in
  (* TODO : let p = ... in *)
  (A7.add_place a7 p)
  

(* function to add a transition to A7 from attributes/children of a x7 transition*)
  
(* format of transition: 
<transition id="Transition_XXX" label="xxx">
 <event idref="Event_xxx"/>
 <upstreamPlace idref="Place_xxx"/>
 <downstreamPlace idref="Place_xxx"/>
 [...]
</transition>
 *)
  
let add_transition_to_a7  attributes children a7 =
  let edge_urn = get_element_idref_of_transition ["event"] "Event_" children in
  let up_place_urn = get_element_idref_of_transition ["upstreamPlace"] "Place_" children in
  let down_place_urn = get_element_idref_of_transition ["downstreamPlace"] "Place_" children in
  let t = (A7Element.define_transition edge_urn up_place_urn down_place_urn) in
  (* TODO : let t = ... in *)
  (A7.add_transition a7 t)
  

(* function to add a sensor/event to A7 from attributes/children of a x7 event *)
  
let add_init_or_final_place_to_a7 attributes children a7 f_ifp =
  let default_id = "" in
  let p_id = get_string_attr "idref" default_id attributes in
  let p_urn = get_urn_of_a7element p_id "Place_" in 
  (f_ifp a7 p_urn)
  
let add_initplace_to_a7 attributes children a7 =
  (add_init_or_final_place_to_a7 attributes children a7 A7.add_init_place_urn)
  
let add_finalplace_to_a7 attributes children a7 =
  (add_init_or_final_place_to_a7 attributes children a7 A7.add_final_place_urn)
  
  
  
(*-----------------------------------------------------------------
 * Functions to add the elements of a xml #7 document to an abstract #7
 *-----------------------------------------------------------------*)

let add_x7event_to_a7 a7 x7_event =
  match x7_event with
  | Xml.Element ("event", attributes, children)->
     (add_sensor_event_to_a7 attributes children a7)
  | _ -> a7

let add_x7place_to_a7 a7 x7_place =
  match x7_place with
  | Xml.Element ("place", attributes, children)->
     (add_place_to_a7 attributes children a7)
  | _ -> a7


let add_x7transition_to_a7 a7 x7_transition =
  match x7_transition with
  | Xml.Element ("transition", attributes, children)->
     (add_transition_to_a7 attributes children a7)
  | _ -> a7

let add_x7initplace_to_a7 a7 x7_initplace =
  match x7_initplace with
  | Xml.Element ("initialPlace", attributes, children)->
     (add_initplace_to_a7 attributes children a7)
  | _ -> a7

let add_x7finalplace_to_a7 a7 x7_finalplace =
  match x7_finalplace with
  | Xml.Element ("finalPlace", attributes, children)->
     (add_finalplace_to_a7 attributes children a7)
  | _ -> a7

                                         

(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)

(* function to parse a #7 scenario of xml document and return an abstract
 * #7 data *)
let a7_of_x7scenario x7_scenario =
  let x7_events_list = filter_elements ["event"] x7_scenario in
  let sequence = filter_elements ["sequence"] x7_scenario in
  let x7_places_list = filter_elements_of_sequence ["place"] sequence in
  let x7_transitions_list = filter_elements_of_sequence ["transition"] sequence in
  let x7_init_places_list = filter_elements_of_sequence ["initialPlace"] x7_scenario in
  let x7_final_places_list = filter_elements_of_sequence ["finalPlace"] x7_scenario in
  let d = (List.length x7_events_list)
          + (List.length x7_places_list)
          + (List.length x7_transitions_list) in
  let a7 = A7.make d in
  let a7 = List.fold_left add_x7event_to_a7 a7 x7_events_list in
  let a7 = List.fold_left add_x7place_to_a7 a7 x7_places_list in
  let a7 = List.fold_left add_x7transition_to_a7 a7 x7_transitions_list in
  let a7 = List.fold_left add_x7initplace_to_a7 a7 x7_init_places_list in
  let a7 = List.fold_left add_x7finalplace_to_a7 a7 x7_final_places_list in
  a7

(* function to parse a xml #7 document and return an abstract #7 data *)
(* val a7_of_x7: Xml.xml -> A7.t *)   
let a7_of_x7 = 
  function
  | Xml.Element ("scenario", attributes, children) -> a7_of_x7scenario children
  | _ -> invalid_arg "XmlSevenIn.a7_of_xml: syntax error"
                          

(* function to parse a xml #7 file and return an abstract #7 data *)
(* val a7_of_x7_file : string -> A7.t *)

let a7_of_x7_file x7f_name =
  let x7 = Xml.parse_file x7f_name in
  a7_of_x7 x7
