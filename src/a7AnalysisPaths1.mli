(*
 *
 * Sub-Module of A7Analysis to compute a list of simple paths on #7 graph (method 1)
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * This module provides functions to compute the list of simple paths starting 
 * from an initial state and ending in a final state
 *)


(*-----------------------------------------------------------------
 * Main function
 *-----------------------------------------------------------------*)

(* function to obtain event paths starting from initial places,
 * each event representing by his urn *)
val get_e_paths_from_initial_places : A7.t -> (int list) list  
  
      

