(*
 *
 * Attributes of sensor/event for #SEVEN abstract structure
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* 
 * This module provides functions to obtain the attributes of
 * sensor/event from ontology.
 *
 * Ontology is represented in the form of a sorted array of events 
 * with attributes. It is compute from a classifier with module Xes.
 *
 * The functions works from ontology of classifiers to extract
 * events informations and process on them.
 *
 * The results take the form of a sorted array (like ontology) with the 
 * targeted attributes. These lists are used to update the sensors.
 *)

(*-----------------------------------------------------------------
 * Data structures
 *-----------------------------------------------------------------*)

type f_relation = string
type f_objects = (string * string) list
               

(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)

(* function to obtain a sorted array of event labels *)
     
val get_event_label : Configuration.t -> DataEvent.t -> string
val get_event_labels_of_ontology : Configuration.t -> DataOntology.t -> string array

  
(* function to obtain a sorted array of event five relation *)

val get_event_five_relation : Configuration.t -> DataEvent.t -> f_relation
val get_event_five_relations_of_ontology : Configuration.t -> DataOntology.t -> f_relation array

  
(* function to obtain a sorted array of event five objects list *)

val get_event_five_objects_list : Configuration.t -> DataEvent.t -> f_objects
val get_event_five_objects_lists_of_ontology : Configuration.t -> DataOntology.t -> f_objects array
  
