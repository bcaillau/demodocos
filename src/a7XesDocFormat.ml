(*
 *
 *  Doc Format of A7 xSPM (iSPM as xes file)
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* This module provides standard text corresponding to doc format
 * of A7 xSPM. It's use to generate an iSPM as xes file corresponding
 * to an abstract seven.
 *)

(*-----------------------------------------------------------------
 * Module openning, parameter & utilities
 *-----------------------------------------------------------------*)

open Pp
   
(* function to add the double quotes around a string *)
   
let stringify s = "\"" ^ (String.escaped s) ^ "\""
                
   
(*-----------------------------------------------------------------
 * Standard string of XES format
 *-----------------------------------------------------------------*)

(* XML prologue *)
                
let xml_prologue_string =
  "<?xml "
  ^ " version=" ^ (stringify "1.0")
  ^ " encoding=" ^ (stringify "ISO-8859-1")
  ^ "?>"


(* Log *)
  
let log_start_string = "<log>"

let log_end_string = "</log>"
                     

(* Trace *)
  
let trace_start_string = "<trace>"

let trace_end_string = "</trace>"
  

(* Event *)

let event_start_string = "<event>"

let event_end_string = "</event>"


(* Attributes*)

let attribute_string type_attr key value =
  "<" ^ type_attr ^ " "
  ^ "key=" ^ (stringify key)
  ^ " value=" ^ (stringify value)
  ^ "/>" 
                     
let string_attribute_string key value = attribute_string "string" key value
let int_attribute_string key value = attribute_string "int" key value
                                   

(* Global *)
  
let global_start_string value =
  "<global "
  ^ "scope=" ^ (stringify value)
  ^ ">" 

let global_end_string = "</global>"
                              
  
(* Event classifier *)
                       
let event_classifier_string name keys =
  "<classifier "
  ^ "name=" ^ (stringify name)
  ^ " keys=" ^ (stringify keys)
  ^ "/>"


(* Extension *)

let extension_string name prefix uri =
  "<extension "
  ^ "name=" ^ (stringify name)
  ^ " prefix=" ^ (stringify prefix)
  ^ " uri=" ^ (stringify uri)
  ^ "/>"


(*-----------------------------------------------------------------
 * Mutaul string of xSPM-1 and xSPM-2 format (iSPM as xes file)
 *-----------------------------------------------------------------*)

(* Heading data: global trace *)

let global_trace_start_string = global_start_string "trace"

let global_trace_concept_name_string =
  let key = "concept:name" and value = "__INVALID__"
  in string_attribute_string key value


(* Heading data: global event *)

let global_event_start_string = global_start_string "event"

let global_event_concept_name_string =
  let key = "concept:name" and value = "__INVALID__"
  in string_attribute_string key value


(* Heading data: description *)
                        
let heading_description_string value =
  let key = "description" in (string_attribute_string key value)


(*-----------------------------------------------------------------
 * Specific string of xSPM-1 format (iSPM as xes file)
 *-----------------------------------------------------------------*)   
  
(* Heading data: usual classifier *)
  
let ispm_event_name_classifier_string =
  (event_classifier_string "Event Name" "concept:name")


(* Heading data: description *)
                        
let xspm1_description_string =
  let value = "xSPM-1 as Xes file generating with TnF Net Synthesis Tool" in
  (string_attribute_string "description" value)
                              

(* Trace definition *)

let trace_concept_name_string value = string_attribute_string "concept:name" value

let trace_description_string value = string_attribute_string "description" value
                                   
  
(* Event (action) definition *)
                                
let event_concept_name_string value = string_attribute_string "concept:name" value

                           

(*-----------------------------------------------------------------
 * Specific string of xSPM-2 format (iSPM as xes file)
 *-----------------------------------------------------------------*)

(* Heading data *)

let xspm_action_extension_string name prefix uri =
  let name = "xSPM Action" and prefix = "action" and uri = "xspm-action.xesext"
  in (extension_string name prefix uri)
                              
  
(* Heading data: usual classifier *)
  
let ispm_v_classifier_string =
  let name = "V Classifier" and keys = "action:verb"
  in (event_classifier_string name keys)
                             
let ispm_vat_classifier_string =
  let name = "VAT Classifier"
  and keys = "action:verb action:affected action:treatedStructure"
  in (event_classifier_string name keys)
                               
let ispm_avat_classifier_string =
  let name = "AVAT Classifier"
  and keys = "action:actuator action:verb action:affected action:treatedStructure"
  in (event_classifier_string name keys)


(* Heading data: description *)
                        
let xspm2_description_string =
  let value = "xSPM-2 as Xes file generating with TnF Net Synthesis Tool" in
  (string_attribute_string "description" value)
                                
  
(* Event (action) definition *)
                                
let event_action_id_string value = int_attribute_string "action:id" value
                                
let event_action_type_string value = string_attribute_string "action:type" value
                                
let event_action_state_string value = int_attribute_string "action:state" value
                                
let event_action_actuator_string value = string_attribute_string "action:actuator" value
                                
let event_action_description_string value = string_attribute_string "action:description" value
                                
let event_action_verb_string value = string_attribute_string "action:verb" value
                                
let event_action_affected_string value = string_attribute_string "action:affected" value
                                
let event_action_affected_number_string number =
  string_attribute_string "action:affected-number" number
                                
let event_action_numbered_affected_string number value =
  let s_number = string_of_int number in
  string_attribute_string ("action:affected-" ^ s_number) value
                                
let event_action_instrument_string value = string_attribute_string "action:instrument" value
                                
let event_action_instrument_number_string number =
  string_attribute_string "action:instrument-number" number
                                
let event_action_numbered_instrument_string number value =
  let s_number = string_of_int number in
  string_attribute_string ("action:instrument-" ^ s_number) value
                                
let event_action_recipient_string value = string_attribute_string "action:recipient" value
                                
let event_action_origin_string value = string_attribute_string "action:origin" value
                                
let event_action_destination_string value = string_attribute_string "action:destination" value
                                
let event_action_treatedStructure_string value =
  string_attribute_string "action:treatedStructure" value
  
  
  
(*-----------------------------------------------------------------
 * Functions to generates doc containing mutual xSPM-1 and xSPM-2 text
 * (iSPM as xes file)
 *-----------------------------------------------------------------*)

(* val xml_prologue_text : Pp.doc *)

let xml_prologue_text = text xml_prologue_string

                      
(* val log_text : Pp.doc -> Pp.doc *)

let log_text s_content =
  break ^^ (text log_start_string)
  ^^ (nest 4 s_content)
  ^^ break ^^ (text log_end_string)
  

(* val usual_heading_text : Pp.doc *)

let usual_heading_text =
  break ^^ (text global_trace_start_string)
  ^^ break ^^ (text global_event_concept_name_string)
  ^^ break ^^ (text global_end_string)
  ^^ break ^^ (text global_event_start_string)
  ^^ break ^^ (text global_event_concept_name_string)
  ^^ break ^^ (text global_end_string)
  

(* val trace : string -> Pp.doc *)

let trace_text s_content =
  break ^^ (text trace_start_string)
  ^^ (nest 4 s_content)
  ^^ break ^^ (text trace_end_string)
  

(* val event_text : string -> Pp.doc *)

let event_text s_content =
  break ^^ (text event_start_string)
  ^^ (nest 4 s_content)
  ^^ break ^^ (text event_end_string)
  
  
  
(*-----------------------------------------------------------------
 * Functions to generates doc containing specific xSPM-1 text
 * (iSPM as xes file)
 *-----------------------------------------------------------------*)

(* val xspm1_heading_text : Pp.doc *)

let xspm1_heading_text =
  usual_heading_text
  ^^ break ^^ (text ispm_event_name_classifier_string)
  ^^ break ^^ (text xspm1_description_string)
  

(* val xspm1_trace : string -> string -> string -> Pp.doc *)

let xspm1_trace_text s_concept s_description s_content =
  let s_content =
    break ^^ (text (trace_concept_name_string s_concept))
    ^^ break ^^ (text (trace_description_string s_description))
    ^^ s_content
  in trace_text s_content
   
  
(* val xspm1_event : string -> Pp.doc *)

let xspm1_event_text cn_value =
  let s_content =
    break ^^ (text (event_concept_name_string cn_value))
  in (event_text s_content)
  

   
(*-----------------------------------------------------------------
 * Functions to generates doc containing specific xSPM-2 text
 * (iSPM as xes file)
 *-----------------------------------------------------------------*)

(* val xspm2_heading_text : Pp.doc *)

let xspm2_heading_text =
  usual_heading_text
  ^^ break ^^ (text ispm_v_classifier_string)
  ^^ break ^^ (text ispm_vat_classifier_string)
  ^^ break ^^ (text ispm_avat_classifier_string)
  ^^ break ^^ (text xspm2_description_string)
  

(* val xspm2_trace : string -> string -> string -> Pp.doc *)

let xspm2_trace_text s_content = trace_text s_content
   

(* val xspm2_event : string array -> Pp.doc *)

let rec enum_text f_string value_list n doc =
  if (List.length value_list) > 0
  then
    let n = n + 1 in
    let (value, new_value_list) = (List.hd value_list, List.tl value_list) in
    let doc = doc ^^ break ^^ (text (f_string n value)) in
    enum_text f_string new_value_list n doc
  else doc

let affected_enum_text aff_list =
  let f_string = event_action_numbered_affected_string in
  enum_text f_string aff_list 0 empty
  
let instrument_enum_text inst_list = 
  let f_string = event_action_numbered_instrument_string in
  enum_text f_string inst_list 0 empty
                                      
let xspm2_event_text data_array affected_list instrument_list =
  let affected_number = string_of_int (List.length affected_list) in
  let instrument_number = string_of_int (List.length instrument_list) in
  let data n = data_array.(n) in
  let s_content =
    break ^^ (text (event_action_id_string (data 0)))
    ^^ break ^^ (text (event_action_type_string (data 1)))
    ^^ break ^^ (text (event_action_state_string (data 2)))
    ^^ break ^^ (text (event_action_actuator_string (data 3)))
    ^^ break ^^ (text (event_action_description_string (data 4)))
    ^^ break ^^ (text (event_action_verb_string (data 5)))
    ^^ break ^^ (text (event_action_affected_string (data 6)))
    ^^ break ^^ (text (event_action_affected_number_string affected_number))
    ^^ break ^^ (affected_enum_text affected_list)
    ^^ break ^^ (text (event_action_instrument_string (data 7)))
    ^^ break ^^ (text (event_action_instrument_number_string instrument_number))
    ^^ break ^^ (instrument_enum_text instrument_list)
    ^^ break ^^ (text (event_action_recipient_string (data 8)))
    ^^ break ^^ (text (event_action_origin_string (data 9)))
    ^^ break ^^ (text (event_action_destination_string (data 10)))
    ^^ break ^^ (text (event_action_treatedStructure_string (data 11)))
  in (event_text s_content)
