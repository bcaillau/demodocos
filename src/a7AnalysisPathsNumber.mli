(*
 *
 * Sub-Module of A7Analysis to compute an simple paths number on #7 code
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * This module provides functions to compute the number of realizations 
 * of a given scenario, ie the number of simple paths starting from 
 * the initial state and ending in a final state
 *)
(*-----------------------------------------------------------------
 * Functions to compute the number of realizations of a given scenario, 
 * ie the number of simple paths starting from the initial state 
 * and ending in a final state
 *-----------------------------------------------------------------*)

(* function to obtain the number of simple paths on an abstract seven,
 * starting from a given place *) 
val get_number_from_given_place :
  A7.t -> int list -> int list -> A7Element.t -> int

(* function to obtain the number of simple path on an abstract seven,
 * starting from initial places *) 
val get_number_from_initial_places : A7.t -> int
  
      

