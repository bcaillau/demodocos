(* filters messages, depending on the verbosity level *)

val init : unit -> unit (* sets verbosity to default value = 1 *)

val set : int -> unit

val get : unit -> int

val filter : int -> ('a -> 'b) -> 'a -> 'b -> 'b

val cond : int -> (unit -> unit) -> unit
