(*
 *
 * Sub-Module of A7Analysis to compute a list of simple paths on #7 graph (method 2)
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * This module provides functions to compute the list of simple paths starting 
 * from an initial state and ending in a final state
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

open A7Attributes
open A7

(* function to know if a given place is a final place *)
let is_final_place a7 place_urn =
  let fp_urn_list = get_final_place_urn_list a7 in
  (List.mem place_urn fp_urn_list) 

(* function to sum the elements of a given list *)
let rec sum l =
  match l with
  | [] -> 0
  | hd :: tl -> hd + sum tl

(* function to obtain a list without duplicate (result list) 
 * a given list (source list) *)
let rec list_uniq result source =
  match source with
  | [] -> result
  | e::new_source ->
     let new_result = if (List.mem e result) then result else e::result
     in list_uniq new_result new_source 

      
      
(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

(* structure corresponding to a transition *)
type t_struct = { pe : int ; e : int ; ps : int }

(* constructor of t-struct *)
let cons_t_struct a b c = { pe = a ; e = b ; ps = c }                

(* function to obtain the t-graph corresponding to a given abstract seven *)
let get_t_graph_of_a7 a7 =
  let mapping t = cons_t_struct
                    (A7Element.get_up_place_urn t)
                    (A7Element.get_event_urn t)
                    (A7Element.get_down_place_urn t)
  in
  List.map mapping (A7.get_transition_list a7)  

  
(* structure corresponding to a multi-choice place, 
 * with saving of the unvisited post transitions *)
type multichoice_place = { urn : int ; unvisited_post_transition_list : t_struct list }

let make_mcp p upt_list =
  { urn = p ; unvisited_post_transition_list = upt_list }
  
  

(*-----------------------------------------------------------------
 * Functions used to computing the simple paths
 *-----------------------------------------------------------------*)
  
(* function to obtain a path stopping until a given place *)
  
let rec  get_path_stop_until_a_given_place t_path p =
  match t_path with
  | [] -> []
  | t::new_path ->
     if t.pe = p
     then new_path
     else get_path_stop_until_a_given_place new_path p


(* function filtering a transition list relative to visited transitions *)
    
let filter_t_visited t_visited t_list =
  let filter t = not (List.mem t t_visited) in
  List.filter filter t_list


(* function to compute of list possible transitions relative to
 * a given place, knoledge of visited transition and specific 
 * unvisited post transition of a mcp place *)
    
let compute_t_possible graph ip t_visited upt_list =
  let t_from_p = List.filter (function t -> ip = t.pe) graph in
  match upt_list with
  | [] -> filter_t_visited t_visited t_from_p
  | _ -> upt_list

  
(* function to discard several given places from a list of places *)
    
let discard_places p_list p_discarded =
  let filter p = not (List.mem p p_discarded) in
  List.filter filter p_list
  
    
(* function filtering a transition list relative to transition
 * which ending on an excluded place *)
  
let filter_p_excluded t_list p_excluded =
  let filter t = not (List.mem t.ps p_excluded) in
  List.filter filter t_list


(* function to update a list of multi-choice place for a given
 * place *)

let update_mcp_list ip mcp_list upt_list t_possible t =
  let get_upt_list t_list =
    List.filter (function x -> x.e <> t.e) t_list
  in
  let upt_list = get_upt_list
                   (match upt_list with [] -> t_possible | _ -> upt_list)
  in
  (make_mcp ip upt_list) :: mcp_list
     

(*-----------------------------------------------------------------
 * Computing of simple paths
 *-----------------------------------------------------------------*)
  
(* function to compute a list of transition paths from a graph
 * of transitions, starting from a given init place (ip) 
 * and ending to a given final place (fp) *)

let rec computing_of_paths
          graph ip fp mcp_list upt_list work_path result =

  (* function to continue from the last multi-choice place *)
  let f_continue_from_last_mcp last_mcp new_mcp_list new_result =
    let new_ip = last_mcp.urn in
    let new_upt_list = last_mcp.unvisited_post_transition_list in
    let new_work_path = get_path_stop_until_a_given_place work_path new_ip in
    (computing_of_paths
       graph new_ip fp new_mcp_list new_upt_list new_work_path new_result)
  in

  (* function to continue with a given transition *)
  let f_continue_with_transition t new_mcp_list =
    let new_ip = t.ps in
    let new_work_path = t::work_path in
    (* let f_result r mcp = (mcp.unvisited_post_transition_list)::r in (* DEBUG !!! *)
    let result = List.fold_left f_result result mcp_list in (* DEBUG !!! *) *)
    (computing_of_paths
       graph new_ip fp new_mcp_list [] new_work_path result)
  in

  (* test of arrival *)
  match (ip = fp) with
    
  (* end of work path *)
  | true ->

     (* result update *)
     let new_result = work_path(*::[]*)::result in
    (* let f_result r mcp = (mcp.unvisited_post_transition_list)::r in (* DEBUG !!! *)
     let new_result = List.fold_left f_result new_result mcp_list in (* DEBUG !!! *) *)
     begin match mcp_list with

     (* end of computing because there is no more multichoice place *)
     | [] -> new_result

     (* continue from the last multi-choice place *)
     | last_mcp::new_mcp_list ->
        (f_continue_from_last_mcp last_mcp new_mcp_list new_result)
        
     end

  (* continue of the computing for path working *)
  | false ->
     
     (* t possible are transitions from p without visited transitions *)
     let t_possible = (compute_t_possible graph ip work_path upt_list) in
     match (t_possible) with
       
     | [] ->

        begin match mcp_list with

        (* end of computing because there is no more multi-choice place *)
        | [] -> result

        (* continue from the last multi-choice place *)
        | last_mcp::new_mcp_list ->
           (* let f_result r mcp = (mcp.unvisited_post_transition_list)::[]::r in (* DEBUG !!! *)
           let result = List.fold_left f_result result mcp_list in (* DEBUG !!! *) *)
           (f_continue_from_last_mcp last_mcp new_mcp_list result)
          
        end
       
     | t::[] -> (f_continue_with_transition t mcp_list)

     | t::xxx ->
        let new_mcp_list = (update_mcp_list ip mcp_list upt_list t_possible t) in
        (f_continue_with_transition t new_mcp_list)
        

(* TEST PLUG ! *)
let rec computing_of_paths_test
          graph ip fp p_visited p_multichoice
          t_path_working t_paths_resulting = ((cons_t_struct ip fp fp)::[])::t_paths_resulting


(* functions to compute a list of transition paths from several
 * init places (ip) to several final places (fp) *)
                                           
let rec computing_of_paths_from_ip graph ip fpl result =
  match fpl with
  | [] -> []
  | fp::[] -> (computing_of_paths graph ip fp [] [] [] result)
  | fp::new_fpl ->
     let result = (computing_of_paths graph ip fp [] [] [] result) in
     computing_of_paths_from_ip graph ip new_fpl result
        
let rec computing_of_paths_fully graph ipl fpl result =
  match ipl with
  | [] -> []
  | ip::[] -> computing_of_paths_from_ip graph ip fpl result
  | ip::new_ipl ->
     let result = (computing_of_paths_from_ip graph ip fpl result) in
     computing_of_paths_fully graph new_ipl fpl result

     

(*-----------------------------------------------------------------
 * Main function
 *-----------------------------------------------------------------*)
     
(* function to obtain transition paths starting from a list of places,
 * each transition representing by a specific structure (t-struct) *)
     
let get_t_paths_from_initial_places a7 =
  
  try
    
    (* step 1 = evaluation of graph, initial places and final places *)
    let graph = get_t_graph_of_a7 a7 in
    let init_places = A7.get_init_place_urn_list a7 in
    let final_places = A7.get_final_place_urn_list a7 in
    
    (* step 2 = compute of all t_paths from places *)
    let result = computing_of_paths_fully graph init_places final_places [] in
    
    (* step 3 = reversal each path to obtain a path starting from first transition
     and arriving on last transition *)
    List.map List.rev result
    
  with _ -> []


(* TEST PLUG ! *)
let get_t_paths_from_initial_places_test a7 =
  let graph = get_t_graph_of_a7 a7 in
  let init_places = A7.get_init_place_urn_list a7 in
  let final_places = A7.get_final_place_urn_list a7 in
  let ip = match init_places with [] -> 777 | p::xxx -> p in
  let fp = match final_places with [] -> 779 | p::xxx -> p in
  let ep = int_of_string (string_of_int ip ^ "000" ^ string_of_int fp) in
  let result = computing_of_paths_fully graph init_places final_places [] in
  let result = List.map List.rev result in
  match result with
  | [] -> ((cons_t_struct ip ep fp)::[])::[]
  | x -> ((cons_t_struct ip ep fp)::[])::x
  
  
(* function to obtain the event path corresponding of a given transition path,
 * each event representing by his urn and each transition representing by a 
 * specific structure (t-struct) *)
  
let get_e_path_of_t_path t_path =
  let f t = t.e in
  List.rev (List.rev_map f t_path)
 
  
(* function to obtain event paths starting from initial places,
 * each event representing by his urn *)
  
let get_e_paths_from_initial_places a7 =
  (* let t_paths = get_t_paths_from_initial_places_test a7 in (* DEBUG *) *)
  let t_paths = get_t_paths_from_initial_places a7 in
  List.rev (List.rev_map get_e_path_of_t_path t_paths)


(* function to obtain event paths starting from initial places,
 * each event representing by his label *)

let get_label_e_paths_from_initial_places a7 =

  (* function to obtain the label of a given event representing by urn *)
  let se_list = get_sensor_event_list a7 in
  let f_find e x = (A7Element.get_urn x) == e in
  let find_event_by_urn e = (List.find (f_find e) se_list) in
  let get_label e =
    try A7Element.get_label (find_event_by_urn e)
    with _ -> string_of_int e
  in
  
  (* step 1 : compute of event path with event representing by urn *)
  let e_paths = get_e_paths_from_initial_places a7 in

  (* step 2 : convert to obtain event path with event representing by label *)
  let f_path p = List.map (function e -> get_label e) p in
  List.map f_path e_paths
