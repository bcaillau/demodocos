Random.self_init ();;

let n = 200
and p = 8;;

let rbit p =
  let n = truncate (ceil (1. /. p)) in
    (Random.int n)=0;;

let rvect n p =
  let c = Vector.make n false in
    begin
      for i=1 to p do
	let j = ref (Random.int n) in
	  begin
	    while (Vector.get c (!j)) do
	      j := Random.int n
	    done;
	    Vector.set c (!j) true
	  end
      done;
      c
    end;;

let m = 100000;;

let z = 10007;; (* prime *)

exception Exit

let rparikh m n p =
  let f = Parikh.make n in
    begin
      for i=0 to m-1 do
	try
	  while true do
	    let u = rvect n p in
	      try
		ignore (Parikh.get f u)
	      with
		  Not_found ->
		    begin
		      Parikh.set f u i;
		      raise Exit
		    end
	  done
	with
	    Exit -> ()
      done;
      f
    end;;

let sparikh f =
  let n = ref 0 in
    begin
      Parikh.iter
	(fun u i ->
	  begin
	    (* Vector.print u; *)
	    n := ((!n)+i) mod z
	  end)
	f;
      !n
    end

(* Timing *)

let usage f x =
  let t1 = Unix.times () in
    begin
      let r = f x in
      let t2 = Unix.times () in
      let t =
	(t2.Unix.tms_utime -. t1.Unix.tms_utime) +.
	(t2.Unix.tms_stime -. t1.Unix.tms_stime) +.
	(t2.Unix.tms_cutime -. t1.Unix.tms_cutime) +.
	(t2.Unix.tms_cstime -. t1.Unix.tms_cstime) in
	begin
	  Printf.printf "usage=%8.4fs\n" t;
	  r
	end
    end;;

let sum =
  usage
    (fun () ->
      let f = rparikh m n p in
	(* Parikh.dump f; *)
	sparikh f)
    ();;

assert (((2 * sum) mod z) = (((m mod z) * ((m - 1) mod z)) mod z));;

