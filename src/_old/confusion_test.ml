(*
 *
 * Confusion_test: Test suite for module Confusion
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA Rennes, France
 *
 *)

(* run with fliflop *)

open Parikhproc;;

let s = make 10;;

let n0 = root s;;

let add_word s w =
  let rec aw n =
    function
	[] -> ()
      | e :: u ->
	  aw (add n e) u
  in
    aw (root s) w;;
	   
add_word s [1;1;2;3;1;7;9;0;8;6;7;6;5;4;4];;    
add_word s [0;3;1;2;4;6;5;8;7;9];;
add_word s [1;1;2;3;7;9;0;0;7;6;5;4;4];;
add_word s [1;1;0;3;1;2;9;4;6;5;8;7];;

Confusion.print_confusion_system stdout s;;

let s' = quotient s;;

Quotient.print stdout s';;
