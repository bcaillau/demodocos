(*
 *
 * #SEVEN pretty-printer
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2016
 *
 *)

open Pp

(* utilities *)

let string_of_event i = "e"^(string_of_int i)
and string_of_transition i = "t"^(string_of_int i)
and string_of_place i = "p"^(string_of_int i)

let stringify s = "\""^(String.escaped s)^"\""

let doc_of_events a =
  let n = Array.length a
  and d = ref empty in
    begin
      for i=0 to n-1 do
	d := (!d) ^^
	  (text ("<event id="^(stringify (string_of_event i))^">")) ^^
	  (nest 2 (agrp ((text "<sensorLabel>") ^^
			    (text (String.escaped a.(i))) ^^
			    (text "</sensorLabel>")))) ^^
	  (text "</event>")
      done;
      vgrp (!d)
    end

let doc_of_transitions a (* array mapping transitions to events *) =
  let n = Array.length a
  and d = ref empty in
    begin
      for i=0 to n-1 do
	let j = a.(i) in
	let idi = string_of_transition i
	and idj = string_of_event j in
	  d := (!d) ^^
	  (text ("<transition id="^(stringify idi)^" label="^(stringify idi)^">")) ^^
	  (nest 2 (agrp ((text ("<event idref="^(stringify idj)^"/>"))))) ^^
	  (text "</transition>")
      done;
      vgrp (!d)
    end

(* generates a document containing #SEVEN code representing a safe net *)

(* val safe_to_doc : string array (* event labels *) -> int array (* transition labels *) -> Region.n (* safe net *) -> Pp.doc *)

let safe_to_doc ont lbl net =
  let pla = Array.of_list net in
  let k = Array.length ont (* number of events *)
  and n = Array.length lbl (* number of transitions *)
  and m = Array.length pla (* number of places *) in
    vgrp
      (
	(text "<scenario>") ^^
	  (nest 2
	      (vgrp
		  (
		    (doc_of_events ont) ^^
		      (text "<place xsi:type=\"SafePetriNet\" id=\"Net\" label=\"Net\">") ^^
		      (nest 2 (vgrp ((doc_of_transitions lbl) ^^
					empty (* ... *)))) ^^
		      (text "</place>")))) ^^
	  (text "</scenario>"))
