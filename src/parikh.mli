(*
 *
 * Parikh images
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2012
 *
 *)

(*** Mappings from parikh images ***)

type 'a t

val make : int -> 'a t

val dimension : 'a t -> int

val set : 'a t -> Vector.t -> 'a -> unit

val get : 'a t -> Vector.t -> 'a

(* Tests whether a Parikh mapping is defined for a given vector *)

val is_def : 'a t -> Vector.t -> bool

(* Iterator *)

val iter : (Vector.t -> 'a -> unit) -> 'a t -> unit

val fold : (Vector.t -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b

(* For debugging *)

val dump : 'a t -> unit



