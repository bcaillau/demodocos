(*
 *
 ***********************************************************************
 *                                                                     *
 *                   Flipflop : A flipflop synthesis tool              *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2013.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Pretty-printer library
 *
 *   Copyright (c) 1999 Christian Lindig <lindig@ips.cs.tu-bs.de>. All
 *   rights reserved. See COPYING for details.
 *
 * $Id$
 *
 *)

(* Auxilliary functions *)
let strlen  = String.length

(* definitions *)
let nl      = "\n"

(* A type for the different kind of (user visible) groups *)
type gmode =
    | GFlat             (* hgrp *)
    | GBreak            (* vgrp *)
    | GFill             (* fgrp *)
    | GAuto             (* agrp *)

(* users build (complex) documents of this type *)    
type doc =
    | DocNil
    | DocCons           of doc * doc
    | DocText           of string
    | DocNest           of int * doc
    | DocBreak          of string
    | DocGroup          of gmode * doc

(*  constructor functions for documents *)

let (^^) x y            = DocCons(x,y)
let empty               = DocNil
let text s              = DocText(s)
let nest i x            = DocNest(i,x)
let break               = DocBreak(" ")
let breakWith s         = DocBreak(s)

let hgrp d              = DocGroup(GFlat, d)
let vgrp d              = DocGroup(GBreak,d)
let agrp d              = DocGroup(GAuto, d)
let fgrp d              = DocGroup(GFill, d)
                          
(* Formatting turns a complex [doc] document into a simpler
   [sdoc] document which *)

type sdoc =
    | SNil
    | SText             of string * sdoc
    | SLine             of int    * sdoc    (* newline + spaces *)

(* [sdocToString] formats a simple document into a string: [SLIne]
   line breaks are expanded into a newline followed by spaces *)

let rec sdocToString = function
    | SNil              -> ""
    | SText(s,d)        -> s ^ sdocToString d
    | SLine(i,d)        -> let prefix = String.make i ' ' 
                           in  nl ^ prefix ^ sdocToString d

(* [sdocToFile oc doc] formats [doc] into output channel [oc] *)
                           
let sdocToFile oc doc = 
    let pstr = output_string oc in
    let rec loop = function
        | SNil          -> () 
        | SText(s,d)    -> pstr s; loop d
        | SLine(i,d)    -> let prefix = String.make i ' ' 
                           in  pstr nl;
                               pstr prefix;
                               loop d
    in
        loop doc

(* [agrp]s are turned into [Flat] or [Break] groups - so their are
   only 3 different modes internally. *)

type mode =
    | Flat
    | Break
    | Fill

(* [fits] checks whether a documents up to the next [break] fits into w
   characters. All kind of groups are considered flat: their breaks count
   as spaces. This means the [break] this function looks for must not be
   inside sub-groups *)

let rec fits w = function
    | _ when w < 0                   -> false
    | []                             -> true
    | (i,m,DocNil)              :: z -> fits w z
    | (i,m,DocCons(x,y))        :: z -> fits w ((i,m,x)::(i,m,y)::z)
    | (i,m,DocNest(j,x))        :: z -> fits w ((i+j,m,x)::z)
    | (i,m,DocText(s))          :: z -> fits (w - strlen s) z
    | (i,Flat, DocBreak(s))     :: z -> fits (w - strlen s) z
    | (i,Fill, DocBreak(_))     :: z -> true 
    | (i,Break,DocBreak(_))     :: z -> true
    | (i,m,DocGroup(_,x))       :: z -> fits w ((i,Flat,x)::z)


(* [pretty_format] does the actual pretty printing. It turns a [doc] document
   into a simple [sdoc] document.
   This function can be used only to obtain a string of small document. *)

let rec pretty_format w k = function
  | []                             -> SNil
  | (i,m,DocNil)              :: z -> pretty_format w k z
  | (i,m,DocCons(x,y))        :: z -> pretty_format w k ((i,m,x)::(i,m,y)::z)
  | (i,m,DocNest(j,x))        :: z -> pretty_format w k ((i+j,m,x)::z)
  | (i,m,DocText(s))          :: z -> SText(s ,pretty_format w (k + strlen s) z)
  | (i,Flat, DocBreak(s))     :: z -> SText(s ,pretty_format w (k + strlen s) z)
  | (i,Fill, DocBreak(s))     :: z -> let l = strlen s in
	                              if   fits (w - k - l) z 
	                              then SText(s, pretty_format w (k+l) z)
	                              else SLine(i, pretty_format w  i    z)
  | (i,Break,DocBreak(s))     :: z -> SLine(i,pretty_format w i z)
  | (i,m,DocGroup(GFlat ,x))  :: z -> pretty_format w k ((i,Flat ,x)::z)
  | (i,m,DocGroup(GFill ,x))  :: z -> pretty_format w k ((i,Fill ,x)::z)
  | (i,m,DocGroup(GBreak,x))  :: z -> pretty_format w k ((i,Break,x)::z)
  | (i,m,DocGroup(GAuto, x))  :: z -> if fits (w-k) ((i,Flat,x)::z)
	                              then pretty_format w k ((i,Flat ,x)::z)
	                              else pretty_format w k ((i,Break,x)::z)

                                    
(* functions to manage recursive depth (rd), used for large document *)
let depth_init = 0
let incr_depth rd = rd + 1
let is_depth_limit rd = (rd >= 100)

                                    
(* [format] does the actual pretty printing. It turns a [doc] document
   into a simple [sdoc] documen. 
   This function can be used for large documents. *)
                                    
let rec format w k = fun z sdoc rd ->
  if (is_depth_limit rd) (* if recursive depth is too deep *)
  then (z, sdoc, rd) (* stop the process *)
  else match z with
    | []                             -> (z, SNil, rd)
    | (i,m,DocNil)              :: z -> format w k z sdoc rd
    | (i,m,DocCons(x,y))        :: z -> format w k ((i,m,x)::(i,m,y)::z) sdoc rd
    | (i,m,DocNest(j,x))        :: z -> format w k ((i+j,m,x)::z) sdoc rd
    | (i,m,DocText(s))          :: z -> let rd = incr_depth rd in
                                        let (zr, sr, rd) = format w (k + strlen s) z sdoc rd in
                                        (zr, SText(s, sr), rd)
    | (i,Flat, DocBreak(s))     :: z -> let rd = incr_depth rd in
                                        let (zr, sr, rd) = format w (k + strlen s) z sdoc rd in
                                        (zr, SText(s, sr), rd)
    | (i,Fill, DocBreak(s))     :: z -> let rd = incr_depth rd in
                                        let l = strlen s in
                                            if   fits (w - k - l) z 
                                            then let (zr, sr, rd) = format w (k+l) z sdoc rd in
                                                 (zr, SText(s, sr), rd)
                                            else let (zr, sr, rd) = format w i z sdoc rd in
                                                 (zr, SLine(i, sr), rd)
    | (i,Break,DocBreak(s))     :: z -> let rd = incr_depth rd in
                                        let (zr, sr, rd) = format w i z sdoc rd in
                                        (zr, SLine(i, sr), rd)
    | (i,m,DocGroup(GFlat ,x))  :: z -> format w k ((i,Flat ,x)::z) sdoc rd
    | (i,m,DocGroup(GFill ,x))  :: z -> format w k ((i,Fill ,x)::z) sdoc rd
    | (i,m,DocGroup(GBreak,x))  :: z -> format w k ((i,Break,x)::z) sdoc rd
    | (i,m,DocGroup(GAuto, x))  :: z -> if fits (w-k) ((i,Flat,x)::z)
                                        then format w k ((i,Flat ,x)::z) sdoc rd
                                        else format w k ((i,Break,x)::z) sdoc rd


(* val ppToString : int -> doc -> string *)
(* [ppToString w doc] formats [doc] for line length [w] (> 0) and
   returns it as string. This is not efficient and should be avoided
   for large documents.  The document is considered to be surrounded
   by a virtual [agrp] - if this is not desried it should have itself
   a different group at its outermost level to protect itself from
   the [agrp]. *)                                      
                                      
let ppToString w doc =
  sdocToString  (pretty_format w 0 [0,Flat,agrp(doc)])

  
(* val ppToFile : out_channel -> int -> doc -> unit *)
(* [ppToFile oc w doc] pretty prints [doc] for line length [w] into an
   output channel [oc]. The document is considered to be surrounded
   by a virtual [agrp] - if this is not desried it should have itself
   a different group at its outermost level to protect itself from
   this [agrp]. *)
  
let rec ppToFileProcess oc w z =
  let (z, s, rd) = (format w 0 z SNil 0) in
  if (is_depth_limit rd) (* if the recursive depth limit is reached *)
  then (* print and continue the process on the rest of doc *)
    begin
      sdocToFile oc s;
      ppToFileProcess oc w z
    end
  else sdocToFile oc s

let ppToFile oc w doc = ppToFileProcess oc w [0,Flat,agrp(doc)]

  
