/*
 *
 * $Header$
 *
 * Z/2Z vector space algebra
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2012
 *
 */

#ifndef __VECTOR_ML_H

#define __VECTOR_ML_H

#include <caml/mlvalues.h>

// vector_ml_make(n,b) where n is the length and b is the default value

CAMLprim value vector_ml_make(value, value);

// vector_ml_set(v,i,b) set bit of index i to b in v

CAMLprim value vector_ml_set(value, value, value);

// vector_ml_get(v,i,b) get bit of index i in v

CAMLprim value vector_ml_get(value, value);

// clone(u) returns a clone of u

CAMLprim value vector_ml_clone(value);

// vector_ml_extend(v,n,b) creates a clone of v with greater dimension n and initial value b for all new bits

CAMLprim value vector_ml_extend(value,value,value);

// vector_ml_restrict(v,n) creates a clone of v with only the first n bits of v

CAMLprim value vector_ml_restrict(value,value);

// vector_ml_length(v) returns the length of v

CAMLprim value vector_ml_length(value);

// vector_ml_is_zero(v) checks whether vector v is constant and equal to false

CAMLprim value vector_ml_is_zero(value);

// Number of leading zeroes

CAMLprim value vector_ml_leading_zeroes(value);

// Arity

CAMLprim value vector_ml_arity(value);

// In place xor

CAMLprim value vector_ml_ixor(value,value);

// In place and

CAMLprim value vector_ml_iand(value,value);

// In place or

CAMLprim value vector_ml_ior(value,value);

// Scalar product

CAMLprim value vector_ml_prod(value,value);

CAMLprim value vector_ml_partial_prod(value,value,value,value);

// Comparison function

CAMLprim value vector_ml_compare(value,value);

// Xor

CAMLprim value vector_ml_xor(value,value);

// And

CAMLprim value vector_ml_and(value,value);

// Blit

CAMLprim value vector_ml_blit(value,value,value,value,value); // (src, src_idx, dst, dst_idx, length)

// Byte read and write

CAMLprim value vector_ml_get_byte(value,value,value); // (vector,idx,len)
CAMLprim value vector_ml_set_byte(value,value,value,value); // (vector,idx,len,byte)

#endif
