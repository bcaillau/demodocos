(*
 *
 * Process used to perform a Tnf net analysis
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, Inria, Rennes, France, 2017
 *
 *)

(* 
 * This module provides functions to execute the different stage of analysis
 * of a proceeding against a generic model
 *)

(*-----------------------------------------------------------------
 * Module openning & utilities (including definition of exceptions)
 *-----------------------------------------------------------------*)

open Configuration
open TnfBasis


   
(*-----------------------------------------------------------------
 * Function to execute the different stage of comparison process 
 *-----------------------------------------------------------------*)

(* function to initialize the analysis *)
            
let initialize_comparison config traces = 
  let d = log_dim traces in
  let system_of_word_list = Parikhproc.make_system_of_word_list d traces in
  begin
    if (Configuration.get_verbose config) >= 1
    then Confusion.print_confusion_system stderr system_of_word_list;
    let t = Parikhproc.quotient system_of_word_list in
    begin
      if (Configuration.get_verbose config) >= 1
      then Quotient.print stderr t;
      (Synthesis.make_region_spaces t, Separation.init t)
    end
  end 
  
  
  
