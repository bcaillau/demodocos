(*
 *
 * Module to work on abstract structure of #SEVEN code
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* 
 * This module provides functions to make the abstract structure
 * of #SEVEN code (a7), and work on it. 
 *)

(*-----------------------------------------------------------------
 * Function to add a place corresponding to a node (state)
 *-----------------------------------------------------------------*)
  
val add_place_of_node : Separation.q -> A7.t -> A7.t 


(*-----------------------------------------------------------------
 * Function to add event and transition corresponding to an edge 
 *-----------------------------------------------------------------*)

val add_event_of_edge : A7.t -> int -> int -> A7.t  
val add_transition_of_edge : A7.t -> int -> int -> int -> A7.t
  
val add_event_and_transition_of_edge : Separation.q -> int -> Separation.q -> A7.t -> A7.t


(*-----------------------------------------------------------------
 * Function to compute the lists of initial place and final place
 *-----------------------------------------------------------------*)

val compute_initial_place_list : A7.t -> A7.t
val compute_final_place_list : A7.t -> A7.t


(*-----------------------------------------------------------------
 * Functions to make the abstract seven corresponding to a system
 *-----------------------------------------------------------------*)
  
val abstract_seven_of_system : Separation.t -> A7.t
  

(*-----------------------------------------------------------------
 * Functions to modify the sensors labels
 *-----------------------------------------------------------------*)
  
val update_sensors_labels : A7.t -> string array -> A7.t
  
val update_sensors_labels_from_ontology_as_number :
  A7.t -> DataOntology.t -> A7.t
  
val update_sensors_labels_from_ontology_as_letter :
  A7.t -> DataOntology.t -> A7.t
  
val update_sensors_labels_from_ontology_as_ontology_text :
  A7.t -> DataOntology.t -> A7.t


(*-----------------------------------------------------------------
 * Function to evaluate the #FIVE code associated with a sensor/event
 *-----------------------------------------------------------------*)
  
val evaluate_five_code_from_ontology :
  Configuration.t -> A7.t -> DataOntology.t -> A7.t


(*-----------------------------------------------------------------
 * Function to evaluate the origin of sensor/event and places
 *-----------------------------------------------------------------*)

(* the sensor/event origin is ISPM if the event originates from the original 
 * procedure, it's FLIPFLOP if the event is induced from the synthesis *)

val evaluate_events_origin_from_traces_list : A7.t -> int list list -> A7.t

  
(* the origin of place is ISPM if a place is reachable from the original procedure, 
 * it's FLIPFLOP if it's induced from the synthesis *)

val evaluate_places_origin_from_events_origin : A7.t -> A7.t


(*-----------------------------------------------------------------
 * Function to order the #7 code for display in an editor
 *-----------------------------------------------------------------*)

val compute_and_update_elements_location : A7.t -> A7.t

  
(*-----------------------------------------------------------------
 * Prints view on an abstract seven
 *-----------------------------------------------------------------*)

val print_places : out_channel -> A7.t -> unit  
val print_sensor_events : out_channel -> A7.t -> unit
val print_transitions : out_channel -> A7.t -> unit
val print_initial_places : out_channel -> A7.t -> unit
val print_final_places : out_channel -> A7.t -> unit
  
val print : out_channel -> A7.t -> unit

      

