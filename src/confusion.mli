(*
 *
 * Confusion: iterators on the state confusions leading to an event-state separation problem
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA Rennes, France
 *
 *)

(* [print_confusion_parikh f s v] prints to [f] confusions in process [s] for parikh vector [v], if any *)

val print_confusion_parikh : out_channel -> Parikhproc.s -> Vector.t -> unit

(* [print_confusion_system f s] prints to [f] confusions in process [s], if any *)

val print_confusion_system : out_channel -> Parikhproc.s -> unit

