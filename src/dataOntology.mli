(*
 *
 * Ontology from data
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* This module provides a data structure to define an ontology from data,
 * namely a list of data events defined from the data sources.
 * Functions related to this structure are also proposed. 
 *)

(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)


(* Structure of ontology *)
                     
type t = (int * DataEvent.t) list

       

(*-----------------------------------------------------------------
 * Constructor(s)
 *-----------------------------------------------------------------*)

(* function to make an ontology (data events list) from an xes event array *)        

val make_from_event_array : Xes.event array -> t

                           

(*-----------------------------------------------------------------
 * Accessors
 *-----------------------------------------------------------------*)    

(* Base accessors *)
(* function to obtain an event corresponding to an urn  *)
(* function to obtain a given attribute of a given event *)
  
val get_event : t -> int -> DataEvent.t
val get_attribute_of_event : t -> int -> string -> DataEvent.attribute
                          

(* function to obtain the letter tag of an event *)
                          
val get_letter_tag_of_event : t -> int -> string
                          

(* functions to obtain a specific value associated to a given attribute 
 * of a given event (xes value or vr value) *)
  
val get_xes_value_of_event_attribute : t -> int -> string -> string option  
val get_vr_value_of_event_attribute : t -> int -> string -> string option
                          

(* functions to set a specific value associated to a given attribute 
 * of a given event (xes value or vr value) *)
  
val set_xes_value_of_event_attribute : t -> int -> string -> string -> unit  
val set_vr_value_of_event_attribute : t -> int -> string -> string -> unit

                           

(*-----------------------------------------------------------------
 * Updating with ontology basis
 *-----------------------------------------------------------------*)  

(* function to update the ontology structure with the mapping 
 * corresponding to the ontology basis structure *)
  
val update_with_ontology_basis :
  t -> DataOntologyBasis.t -> Configuration.t -> unit

                           

(*-----------------------------------------------------------------
 * Utilities
 *-----------------------------------------------------------------*)
  
val print_ontology_to_file : t -> string -> unit
