(*
 *
 * Module to manage the reporting of program execution
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * This module provides functions to initialize an execution report,
 * and updating it during execution.
 *)

(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)

val init_report : Configuration.t -> unit

val init_step : string -> unit

val end_step : string -> unit


(* function to report information about execution 
 * format of message: "msg1 (msg2: data)" *)

val report_exec_info : string -> string -> string -> unit


  
(*-----------------------------------------------------------------
 * Functions to report informations about the configuration
 *-----------------------------------------------------------------*)

(* functions to report the configuration data *)

val report_configuration : Configuration.t -> unit

  

(*-----------------------------------------------------------------
 * Functions to report informations from #7 code analysis
 *-----------------------------------------------------------------*)

(* functions to report information number of simple paths and traces inclused *)
  
val number_of_simple_path : A7.t -> int list list -> unit
val traces_inclusion : int -> int -> unit

  
(* functions to report information about length of paths *)
  
val length_of_paths : int list list -> unit
  

(* functions to report the list of event paths, each event being  
 * represented by a string *)
  
val list_of_paths : string list list -> unit

  
(*-----------------------------------------------------------------
 * Error reporting
 *-----------------------------------------------------------------*)

val error_report : string -> string -> unit
  
