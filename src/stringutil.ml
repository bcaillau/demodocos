(*
 *
 ***********************************************************************
 *                                                                     *
 *                   Flipflop : A flipflop synthesis tool              *
 *                                                                     *
 *                      Benoit Caillaud, INRIA-Rennes                  *
 *                       <Benoit.Caillaud@inria.fr>                    *
 *                                                                     *
 * Copyright Benoit Caillaud, Institut National de Recherche en        *
 * Informatique et  Automatique, September 2013.                       *
 *                                                                     *
 * Distributed under the CeCILL-C Free Software Licence Agreement:     *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html         * 
 *                                                                     *
 ***********************************************************************
 *
 * Utilities on strings
 *
 * $Id$
 *
 *)

(* [find_string s s'] returns the index in [s] of the first occurrence of [s'] *)

let find_string s s' =
  let l = String.length s
  and l' = String.length s'
  and i = ref 0
  and f = ref false in
    if l' <= 0
    then Some 0
    else
      begin
	while (not (!f)) && ((!i) < l) do
	  try
	    let j = String.index_from s (!i) s'.[0] in
	    let s'' = String.sub s j (min l' (l-j)) in
	      if s'' = s'
	      then
		begin
		  f := true;
		  i := j
		end
	      else i := j+1
	  with
	      Not_found -> i := l
	done;
	if !f
	then Some (!i)
	else None
      end

(* [substitute u v s] iteratively replaces the left-most occurrence of [u] by [v] in [s] *)

let rec substitute u v s =
  match find_string s u with
      None -> s
    | Some i ->
	let l = String.length s
	and k = String.length u in
	let s1 = String.sub s 0 i
	and s2 = String.sub s (i+k) (l-i-k) in
	  if k <= 0
	  then invalid_arg "Ppwrap.substitute: zero length string"
	  else
	    let s' = s1 ^ v ^ s2 in
	      substitute u v s'

(* [silex s s'] compares [s] and [s'] according to the size-lexicographic order *)

(* val silex : string -> string -> int *)

let silex r s =
  let p = String.length r
  and q = String.length s in
    if p < q
    then -1
    else
      if p > q
      then +1
      else
	compare r s
