Random.self_init ();;

let d = 30
and n = 50
and p = 3;;

let rbit p =
  let n = truncate (ceil (1. /. p)) in
    (Random.int n)=0;;

let rconst n p =
  let c = Xorsat.cmake n in
    begin
      Xorsat.set_polarity c (rbit 0.5);
      for i=1 to p do
	let j = ref (Random.int n) in
	  begin
	    while (Xorsat.get_coeff c (!j)) do
	      j := Random.int n
	    done;
	    Xorsat.set_coeff c (!j) true
	  end
      done;
      c
    end;;

Xorsat.cprint (rconst n p);;
Xorsat.cprint (rconst n p);;
Xorsat.cprint (rconst n p);;
Xorsat.cprint (rconst n p);;

let c1 = rconst n p
and c2 = rconst n p;;

compare c1 c2;;

c1 = c2;;
c1 <= c2;;
c1 == c2;;
c1 >= c2;;

Xorsat.cprint c1;
Xorsat.cprint c2;;

let l =  List.sort compare [ c1 ; c2 ];;

List.iter Xorsat.cprint l;;

let d1 = Xorsat.cclone c1
and d2 = Xorsat.cclone c2;;

c1 == d1;;
c2 == d2;;
c1 = d1;;
c2 = d2;;

Xorsat.cprint d1;
Xorsat.cprint d2;;

Xorsat.ixor d1 d2;
Xorsat.ixor d2 d1;
Xorsat.ixor d1 d2;;

Xorsat.cprint d1;
Xorsat.cprint d2;;

c1 = d2;;
c2 = d1;;

Xorsat.cprint (Xorsat.cxor d1 d2);;

let chk_compare m n p =
  for i=1 to m do
    let c1 = rconst n p
    and c2 = rconst n p
    and c3 = rconst n p in
      begin
	assert (not ((c1 <= c2) && (c2 <= c1) && (c1 <> c2)));
	assert (not ((c1 <= c2) && (c2 <= c3) && (not (c1 <= c3))));
	assert (c1 <= c1)
      end
  done;;

chk_compare 10000 n p;;

let rsys m n p =
  let s = Xorsat.make n in
    begin
      for i=1 to m do
	Xorsat.add s (rconst n p)
      done;
      s
    end;;

let m = 30
and n = 50
and p = 3;;

let s = rsys m n p;;

Xorsat.print s;;

Xorsat.set_mode s Xorsat.Sorted;;

Xorsat.print s;;

Xorsat.set_mode s Xorsat.Triangular;;

Xorsat.print s;;

let y = Xorsat.solve s (Vector.make n false);;

Vector.print y;;

(* Timing *)

let usage f x =
  let t1 = Unix.times () in
    begin
      let r = f x in
      let t2 = Unix.times () in
      let t =
	(t2.Unix.tms_utime -. t1.Unix.tms_utime) +.
	(t2.Unix.tms_stime -. t1.Unix.tms_stime) +.
	(t2.Unix.tms_cutime -. t1.Unix.tms_cutime) +.
	(t2.Unix.tms_cstime -. t1.Unix.tms_cstime) in
	begin
	  Printf.printf "usage=%8.4fs\n" t;
	  r
	end
    end;;

(* Large system *)

let m = 10000
and n = 15000
and p = 4;;

let s = usage (rsys m n) p;;

usage (Xorsat.set_mode s) Xorsat.Sorted;;

usage (Xorsat.set_mode s) Xorsat.Triangular;;

let y = usage (Xorsat.solve s) (Vector.make n false);;

(* Vector.print y;; *)

