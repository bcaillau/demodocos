(*
 *
 * Xes_utilities: utility functions related to the XES file format
 *
 * (c) Benoit Caillaud <benoit.caillaud@inria.fr>, Inria, Rennes, France, 2016
 *
 *)

val print_ontology : Xes.event array -> unit

val print_ontology_to_file : Xes.event array -> string -> unit

