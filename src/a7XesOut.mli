(*
 *
 * A7 xSPM (iSPM as xes file)
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* This module generate an A7 xSPM, namely the XES file with some iSPM 
 * corresponding to a #SEVEN code.
 * The main function works from an abstract SEVEN structure to obtain 
 * a dot structure (using module Pp). 
 * Then, finally, the XES file is generated.
 *)

(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

(* types to specify the #7 elements *)

type xspm_format = XSPM1 | XSPM2

                         

(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)

(* function to abtain th xSPM doc corresponding to a given abstract seven *)

val xspm1_doc_of_a7 : A7.t -> Pp.doc
  

(* function to generate the xes file with some iSPM corresponding to a
 * given abstract seven. Each iSPM corresponding to a path on the abstract
 * seven graph. It is a trace on the xes file. *)
  
val generate_xspm_of_a7 : string -> A7.t -> unit
