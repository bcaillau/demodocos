<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" encoding="ISO-8859-1" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template >

  <xsl:template match="iccas" >
    <log>
      <extension name="xSPM Action" prefix="action" uri="xspm-action.xesext"/>
      <global scope="trace">
      	<string key="name" value="__INVALID__"/>
      </global>
      <global scope="event">
        <string key="name" value="__INVALID__"/>
      </global>
      <classifier
	name="V Classifier"
	keys="action:verb"/>
      <classifier
	name="VAT Classifier"
	keys="action:verb action:affected action:treatedStructure"/>
      <classifier
	name="AVAT Classifier"
	keys="action:actuator action:verb action:affected action:treatedStructure"/>
      <string 
        key="description" 
	value="NA"/>
      <xsl:apply-templates select="rec_workflow" />
    </log>
  </xsl:template >
  
  <xsl:template match="rec_workflow" >
    <trace>
      <xsl:apply-templates select="activity" />
    </trace>
  </xsl:template >

  <xsl:template match="discipline" />
  <xsl:template match="patient" />
  <xsl:template match="rec_location" />
  <xsl:template match="rec_date" />

  <xsl:template match="activity" >
    <event>
      <int key="action:id" value="{@id}"/>
      <string key="action:type" value="{@type}"/>
      <int key="action:state" value="1"/>
      <xsl:apply-templates select="actuator" />
      <string key="action:description" value=""/>
      <xsl:apply-templates select="action" />
      <xsl:apply-templates select="usedInstruments" mode="concat" />
      <xsl:apply-templates select="usedInstruments" mode="count" />
      <xsl:apply-templates select="usedInstruments" mode="decomposition" />
      <string key="action:instrument" value=""/>
      <string key="action:instrument-number" value="0"/>
      <string key="action:recipient" value=""/>
      <string key="action:origin" value=""/>
      <string key="action:destination" value=""/>
      <xsl:apply-templates select="treatedStructure" />
    </event>
  </xsl:template >

  <xsl:template match="actuator" >
      <string key="action:actuator" value="{position}_{usedbodypart}"/>
  </xsl:template >

  <xsl:template match="action" >
      <string key="action:verb" value="{action}"/>
  </xsl:template >

  <xsl:template match="usedInstruments" mode="concat" >
      <xsl:variable name="concat_affected">
        <xsl:for-each select="instrument">
          <xsl:sort select="current()"/>
            <xsl:text>#</xsl:text>
            <xsl:value-of select="current()"/>
        </xsl:for-each>
      </xsl:variable>
      <string key="action:affected" value="{$concat_affected}"/>
  </xsl:template >

  <xsl:template match="usedInstruments" mode="count" >
      <string key="action:affected-number" value="{count(instrument)}"/>
  </xsl:template >

  <xsl:template match="usedInstruments" mode="decomposition" >
      <xsl:for-each select="instrument">
        <xsl:variable name="num_affected">
          <xsl:number format="1"/>
        </xsl:variable>
        <string key="action:affected-{$num_affected}" value="{current()}"/>
      </xsl:for-each>
  </xsl:template >

  <xsl:template match="treatedStructure" >
      <string key="action:treatedStructure" value="{anatomicStructure}"/>
  </xsl:template > 

  <xsl:template match="state" />
  <xsl:template match="rec_by" />
  
  <xsl:template match="sw-logs" />
  <xsl:template match="flood-events" />

</xsl:stylesheet>
