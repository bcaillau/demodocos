<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" encoding="ISO-8859-1" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template >

  <xsl:template match="iSPM" >
    <log>
      <extension name="xSPM Action" prefix="action" uri="xspm-action.xesext"/>
      <xsl:apply-templates select="header" />
      <xsl:apply-templates select="data" />
    </log>
  </xsl:template >
  

  <xsl:template match="header" >
    <global scope="trace">
      <string key="name" value="__INVALID__"/>
    </global>
    <global scope="event">
      <string key="name" value="__INVALID__"/>
    </global>
    <classifier
	name="V Classifier"
	keys="action:verb"/>
    <classifier
	name="VAI Classifier"
	keys="action:verb action:affected action:instrument"/>
    <classifier
	name="VAIROD Classifier"
	keys="action:verb action:affected action:instrument action:recipient action:origin action:destination"/>
    <classifier
	name="AVAIROD Classifier"
	keys="action:actuator action:verb action:affected action:instrument action:recipient action:origin action:destination"/>
    <xsl:apply-templates select="info" />
  </xsl:template >

  <xsl:template match="info" >
    <string key="description" value="{study}">
    </string>
  </xsl:template >

  
  <xsl:template match="data" >
    <trace>
      <xsl:apply-templates select="action_list" />
    </trace>
  </xsl:template >

  <xsl:template match="action_list" >
      <xsl:apply-templates select="action" />
  </xsl:template >

  <xsl:template match="action" >
    <event>
      <int key="action:id" value="{id}"/>
      <string key="action:type" value="{action_type}"/>
      <int key="action:state" value="{state}"/>
      <string key="action:actuator" value="{actuator}"/>
      <string key="action:description" value="{description}"/>
      <string key="action:verb" value="{verb}"/>
      <xsl:apply-templates select="affected_list" mode="concat" />
      <xsl:apply-templates select="affected_list" mode="count" />
      <xsl:apply-templates select="affected_list" mode="decomposition" />
      <xsl:apply-templates select="instrument_list" mode="concat" />
      <xsl:apply-templates select="instrument_list" mode="count" />
      <xsl:apply-templates select="instrument_list" mode="decomposition" />
      <string key="action:recipient" value="{recipient}"/>
      <string key="action:origin" value="{origin}"/>
      <string key="action:destination" value="{destination}"/>
    </event>
  </xsl:template >

  <xsl:template match="affected_list" mode="concat" >
      <xsl:variable name="concat_affected">
        <xsl:for-each select="affected">
          <xsl:sort select="current()"/>
            <xsl:text>#</xsl:text>
            <xsl:value-of select="current()"/>
        </xsl:for-each>
      </xsl:variable>
      <string key="action:affected" value="{$concat_affected}"/>
  </xsl:template >

  <xsl:template match="affected_list" mode="count" >
      <string key="action:affected-number" value="{count(affected)}"/>
  </xsl:template >

  <xsl:template match="affected_list" mode="decomposition" >
      <xsl:for-each select="affected">
        <xsl:variable name="num_affected">
          <xsl:number format="1"/>
        </xsl:variable>
        <string key="action:affected-{$num_affected}" value="{current()}"/>
      </xsl:for-each>
  </xsl:template >

  <xsl:template match="instrument_list" mode="concat" >
      <xsl:variable name="concat_instrument">
        <xsl:for-each select="instrument">
          <xsl:sort select="current()"/>
            <xsl:text>#</xsl:text>
            <xsl:value-of select="current()"/>
        </xsl:for-each>
      </xsl:variable>
      <string key="action:instrument" value="{$concat_instrument}"/>
  </xsl:template >

  <xsl:template match="instrument_list" mode="count" >
      <string key="action:instrument-number" value="{count(instrument)}"/>
  </xsl:template >

  <xsl:template match="instrument_list" mode="decomposition" >
      <xsl:for-each select="instrument">
        <xsl:variable name="num_instrument">
          <xsl:number format="1"/>
        </xsl:variable>
        <string key="action:instrument-{$num_instrument}" value="{current()}"/>
      </xsl:for-each>
  </xsl:template >

</xsl:stylesheet>
