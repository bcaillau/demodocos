(*
 *
 * Attributes of sensor/event for #SEVEN abstract structure
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016
 *
 *) 
 
(* 
 * This module provides functions to obtain the attributes of
 * sensor/event from ontology.
 *
 * Ontology is represented in the form of a sorted array of events 
 * with attributes. It is compute from a classifier with module Xes.
 *
 * The functions works from ontology of classifiers to extract
 * events informations and process on them.
 *
 * The results take the form of a sorted array (like ontology) with the 
 * targeted attributes. These lists are used to update the sensors.
 *)


(*-----------------------------------------------------------------
 * Module openning & utilities
 *-----------------------------------------------------------------*)

(* function to remove double quote from a string, if present *)
(* val disstringify : string -> string *)
                                         
let disstringify s =
     let s_length = String.length s in
     let s1_length = min 1 s_length in
     let s2_start = min 1 s_length in
     let s2_length = max (s_length - 2) 0 in
     let s3_start = max ((String.length s) - 1) 0 in
     let s3_length = min 1 s3_start in
     let s1 = String.sub s 0 s1_length
     and s2 = String.sub s s2_start s2_length
     and s3 = String.sub s s3_start s3_length in
     let s1 = if ((String.compare s1 "\"") == 0) then "" else s1 in
     let s3 = if ((String.compare s3 "\"") == 0) then "" else s3 in
     s1 ^ s2 ^ s3
 
  
(*-----------------------------------------------------------------
 * XES Keys and data structures
 *-----------------------------------------------------------------*)

(* Key of "Concept" xes extension *)
let key_concept_name = "concept:name"

(* Keys of "xSPM Action" xes extension *)
let key_action_verb = "action:verb"
let key_action_affected i = "action:affected-" ^ (string_of_int i)
let key_action_instrument i = "action:instrument-" ^ (string_of_int i)
                          
(* types for five entities *)
type f_relation = string
type f_objects = (string * string) list

                          
(*-----------------------------------------------------------------
 * Functions to manage the attribute values of a given event
 *-----------------------------------------------------------------*)

(* functions to analyse the attributes of a given event *)
  
let is_target_value key (s, va) = (s = key)

let is_extension key event =
  let a = event.Xes.event_attributes in
  (List.exists (is_target_value key) a)


(* functions to check if an attribute value exists for an event *)
  
let event_has_value key event =
  try
    let a = event.Xes.event_attributes in
    let (s, va) = List.find (is_target_value key) a in
    let value_scalar = va.Xes.value_scalar in
    not ((disstringify (Xes.string_of_scalar value_scalar)) = "")
  with
  | _ -> false
    
let event_has_verb e = event_has_value key_action_verb e
                     
let event_has_affected e i = event_has_value (key_action_affected i) e
                         
let event_has_instrument e i = event_has_value (key_action_instrument i) e


(* functions to obtain the attribute values of a given event *)
  
let get_event_value key event =
  let a = event.Xes.event_attributes in
  let (s, va) = List.find (is_target_value key) a in
  let value_scalar = va.Xes.value_scalar in
  disstringify (Xes.string_of_scalar value_scalar)
  
let get_event_verb e = get_event_value key_action_verb e
                     
let get_event_affected e i =
  try
    get_event_value (key_action_affected i) e
  with
  | _ -> ""
  
let get_event_instrument e i =
  try
    get_event_value (key_action_instrument i) e
  with
  | _ -> ""
  
let get_event_affected_list e =
    let i = ref 1
    and target_list = ref [] in
    begin
      while (event_has_affected e !i)
      do
        target_list := (get_event_affected e !i)::!target_list;
        i := !i + 1
      done;
      !target_list
    end

let get_event_instrument_list e =
    let i = ref 1
    and target_list = ref [] in
    begin
      while (event_has_instrument e !i)
      do
        target_list := (get_event_instrument e !i)::!target_list;
        i := !i + 1
      done;
      !target_list
    end
    

(*-----------------------------------------------------------------
 * Functions to define and obtain the label of a given event
 *-----------------------------------------------------------------*)
  
let get_event_label_from_xspm_1 e =
  get_event_value key_concept_name e

(* let rec get_aff_str_list e i str_list =
    if (event_has_affected e i) then
    let str_list = (get_event_affected e i) :: str_list in
    (get_aff_str_list e (i+1) str_list)
  else str_list

let rec get_inst_str_list e i str_list =
    if (event_has_instrument e i) then
    let str_list = (get_event_instrument e i) :: str_list in
    (get_inst_str_list e (i+1) str_list)
  else str_list *)
  
let get_event_label_from_xspm_2 e =
  let str_list = [] @ (get_event_affected_list e) in
  let str_list = str_list @ (get_event_instrument_list e) in
  (get_event_verb e) ^ " : " ^ (String.concat " ; " str_list)


(*-----------------------------------------------------------------
 * Functions to define and obtain the five relation of a given event
 *-----------------------------------------------------------------*)
  
let get_event_five_relation_from_xspm_1 e = "NO_RELATION_FROM_XSPM_1"
  
let get_event_five_relation_from_xspm_2 e = (get_event_verb e)


(*-----------------------------------------------------------------
 * Functions to define and obtain the five objects list of a given 
 * event
 *-----------------------------------------------------------------*)
  
let get_event_five_objects_list_from_xspm_1 e = []

(* let rec get_affected_list_from_xspm_2 e i a_list =
  if (event_has_affected e i) then
    let a_list = ("affected", (get_event_affected e i)) :: a_list in
    (get_affected_list_from_xspm_2 e (i+1) a_list)
  else a_list

let rec get_instrument_list_from_xspm_2 e i i_list =
  if (event_has_instrument e i) then
    let i_list = ("instrument", (get_event_instrument e i)) :: i_list in
    (get_instrument_list_from_xspm_2 e (i+1) i_list)
  else i_list *)

let get_affected_list_from_xspm_2 e =
  let mapping aff = ("affected", aff) in
  let aff_list = (get_event_affected_list e) in
  List.map mapping aff_list

let get_instrument_list_from_xspm_2 e =
  let mapping inst = ("instrument", inst) in
  let inst_list = (get_event_instrument_list e) in
  List.map mapping inst_list
  
let get_event_five_objects_list_from_xspm_2 e = 
  let objects = [] in
  let objects = objects @ (get_affected_list_from_xspm_2 e) in
  let objects = objects @ (get_instrument_list_from_xspm_2 e) in
  objects
                          
  
(*-----------------------------------------------------------------
 * Main functions
 *-----------------------------------------------------------------*)
                                       
(* function to obtain a sorted array of event labels *)
                                       
(* val get_event_label : Xes.event -> string *)
(* val get_event_labels_of_ontology : Xes.event array -> string array *)

let get_event_label event =
  if (is_extension key_concept_name event) then
    get_event_label_from_xspm_1 event
  else if (is_extension key_action_verb event) then
    get_event_label_from_xspm_2 event
  else ""
     
let get_event_labels_of_ontology event_array =
  Array.map get_event_label event_array

  
(* function to obtain a sorted array of event five relation *)

(* val get_event_five_relation : Xes.event -> f_relation *)
(* val get_event_five_relations_of_ontology : Xes.event array -> f_relation array *)

let get_event_five_relation event =
  if (is_extension key_concept_name event) then
    get_event_five_relation_from_xspm_1 event
  else if (is_extension key_action_verb event) then
    get_event_five_relation_from_xspm_2 event
  else ""
    
let get_event_five_relations_of_ontology event_array =
  Array.map get_event_five_relation event_array

  
(* function to obtain a sorted array of event five objects list *)

(* val get_get_event_five_objects_list_label : Xes.event -> f_objects *)
(* val get_event_five_objects_lists_of_ontology : Xes.event array -> f_objects array *)

let get_event_five_objects_list event =
  if (is_extension key_concept_name event) then
    get_event_five_objects_list_from_xspm_1 event
  else if (is_extension key_action_verb event) then
    get_event_five_objects_list_from_xspm_2 event
  else []
    
let get_event_five_objects_lists_of_ontology event_array =
  Array.map get_event_five_objects_list event_array
