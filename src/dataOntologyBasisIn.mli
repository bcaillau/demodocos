(*
 *
 * Module to load an ontology basis 
 *
 * (c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2017
 *
 *) 
 
(* 
 * The main function of this module is to load an ontology basis 
 * (from an ontology base file).
 *) 


(*-----------------------------------------------------------------
 * Data structure
 *-----------------------------------------------------------------*)

(* Structure of ontology : mapping ontology_ref / vr_ref *)
                     
type t = (string * string) list

       
                  
(*-----------------------------------------------------------------
 * Functions to load an ontology basis
 *-----------------------------------------------------------------*)

(* function to load the ontology basis from an ontology basis file *)
  
val load : string -> t

                           

(*-----------------------------------------------------------------
 * Utilities
 *-----------------------------------------------------------------*)
  
val print_ontology_basis_to_file : t -> string -> unit
