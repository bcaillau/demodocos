

# Demodocos : A Test 'n Flip Synthesis Tool

*Version : 0.7.6 (November 2017)*

(c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA Rennes, France

(c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France

## Introduction

This application is a synthesis tool to construct generic process models from some examples. The generic model is produce in the form of a Test 'n Flip Net, its marking graph and some other interesting forms. The goal of the tool is to produce a model that integrates the input examples while adding new states and events, generalizing the variability of the input data.

This tool implements a linear algebraic algorithm to produce a Test 'n Flip Net, with polynomial time complexity. Computations are done in the Z/2Z ring. Test and Flip nets extend Elementary Net Systems by allowing test to zero, test to one and flip arcs. The effect of flip arcs is to complement the marking of the place. While the net synthesis problem has been proved to be NP hard for Elementary Net Systems, thanks to flip arcs, the synthesis of Test and Flip nets can be done in polynomial time. Test and flip nets have the required expressivity to give concise and accurate representations of surgical processes  
(models of types of surgical operations). Test and Flip nets can express causality and 
conflict relations. The tool takes as input either standard XES log files (a standard XML file format for process mining tools) or a specific XML file format for surgical applications. The output is a Test and Flip net, solution of the following synthesis problem: given a finite input language (log file), compute a net, which language is the least language in the class of Test and Flip net languages, containing the input language.

This version is stable. It supports examples of process in various formats. The basic format is xes. 
Surgical process models defined as iccas, iSPM 2 and iSPM3 are also accepted. Moreover, it is possible 
to apply the synthesis on #7 scenario (scenario for integration into a virtual reality environment).
 
On output, the tool produces several files depending on the configuration. The generic model is given 
in the form of a graph (corresponding to the marking graph of the TnF network), in dot and JPG format. 
The tool also produces a scenario for integration into a virtual reality environment, in format #7. 
A synthesis report is also produced.
 
The interest of the tool is not perceptible in an isolated use, without exploiting the result for a 
given objective. We must therefore consider its use in the context of an application requiring the use 
of a generic model. 

## Installation

### Simple installation

Copy the sources into any directory. 
If necessary, specify the local configuration by modifying the file 'makefile'. By default,
the install dir is "$(HOME)/.tnf" (it's possible to modify this on the makefile).
Open a terminal, and type the command "make install".

The program requires xsltproc and sed.

### Optimal installation (with adjustment)

CCopy the sources into any directory. Go to directory 'src'.
If necessary, specify the local configuration by modifying the "Makefile-config" file.
Open a terminal, and type the command "make" and then "make install".

The program requires the ocaml package "Xml-Light" (which can be retrieved with OPAM),
and the commands xsltproc and sed.

### Uninstall

Go to the sources directory. Open a terminal, and type the command "make uninstall". It's also
possible to go on directory 'src', and then use the command "make cleanup".

## Usage

The main command, "tnf",  is associated with several operations:

   - classifiers definition

`tnf classifiers [ def ] [ view | add | remove | edit ] < c_name > < c_group > < keys >`

   - configuration

`tnf config [ view | edit ] < config-var > < config-data >`

   - ontology

`tnf ontology value`

   - synthesis

`tnf synthesis < source file name > { < other files > }`

   - conversion to xes format from a given source file

`tnf convert < source file type > < source file name >`
 
   - compute of the quotient graph

`tnf quotient { < source file type > } < source file name > { < target file name > }`

The conventional use of the tool involves the following steps:
1. updating of the classifiers (if necessary)
2. checking and updating the configuration
3. defined ontology evaluation (optional)
4. execution of the synthesis

A classifier regroups the attributes that allow to classify the events of the model, namely the attributes identifying an event. 
The following command is to update the definition of a given classifier:

`tnf classifiers def edit < c_name > < c_group > < c_keys >`

with:

  `< c_name >` = classifier name
  
  `< c_group >` = group of the classifier (format of the file to be processed)
  
  `< c_keys >` = key attributes identifying an event (each attribute is separated by a space).
        
The configuration allows to set the synthesis, specify the form of output and manage the work on generating A7
(computation of location, analysis, computation of paths). To configure the tool, it is enough to use the command :

`tnf config edit`

specifying the variable to be configured and the new value of this variable. 
It is possible to view the configuration with the command:

 `tnf config view`

The synthesis of a TnF net can be carried out from a xes file regrouping the instances to be synthesized. 
Synthesis is also possible with the file formats "iccas", "ispm2" and "ispm3". #7 scenario files 
are also managed.

Some additional options without synthesis are also proposed, including conversion of source files 
and calculation of the quotient graph.


## Configuration

A default configuration is proposed. 

It is possible to view the configuration with the command:

`tnf config view`

It's also possible to configure the tool before execution of synthesis, by using the 
following command:

`tnf config edit < config-var > < config-data >`

It's propose many variables to configure:

* Synthesis option: default_imput, classifier choice, ontology base file, instances number, verbose, approximation ;
* Output choices: directory of output, tnfnet, tnfgraph, seven, a7xspm, table ;
* Output options: computation of location, analysis, event number limit.
* A7 Work: computation of location, analysis, event number limit.

To outputs, a first variable is proposed to define the directory of output. The others variables
are used to define the postfix output (None or "" = no output).

The event number limit is used to define a limit to computation of paths. The paths on A7 is not compute if the limit 
is depassed. It's also used to limited the generation of A7 xSPM.

## Synthesis

It is possible to apply the synthesis to a single file, to several files or to a directory. 
This point must be specified in the command to launch the synthesis, namely:

`tnf synth < file, files or directory >`

The synthesis produces different outputs depending on the configuration.
  
## Development Comments

The program requires the ocaml package "Xml-Light" (which can be retrieved with OPAM).

The tool xslt is used to convert and adapt some files.


## Licence and credits

Flipflop is LGPL / CeCill-C

(c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA, France, 2012-2017
(c) Aurélien Lamercerie <aurelien.lamercerie@inria.fr>, INRIA Rennes, France, 2016-2017

The module "Pp" is an adaptation of the Pretty-printer library proposed by Christian Lindig (copyleft).
The ocaml package "Xml-Light" developped by Nicolas Cannasse (LGPL) is used.
