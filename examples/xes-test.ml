(* run with flipflop *)

let x = Xes.log_of_file "simplerepairexample.xes";;

x.Xes.log_classifiers;;

let c = "MXML Legacy Classifier";;

let v = Xes.ontology_of_classifier x c;;

let (a,y) = Xes.encode_log x c;;

let d = Array.length v;;

let s = Parikhproc.make_system_of_word_list d y;;

Confusion.print_confusion_system stdout s;;

let t = Parikhproc.quotient s;;

Quotient.print stdout t;;

let r = Synthesis.make_region_spaces t;;

let w = Separation.init t;;

Synthesis.synthesize r w;;

Separation.elim_redundent w;;

Separation.print stdout w;;

let n = Separation.get_admissible_regions w;;

List.iter (Region.print stdout) n;;

let n' = Region.superimpose_list n;;

List.iter (Region.print stdout) n';;

