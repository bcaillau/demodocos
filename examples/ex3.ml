(*
 *
 * Example 3: cylinder head studs torquing process
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA Rennes, France
 *
 *)

let d = 4;;

let s =
  Parikhproc.make_system_of_word_list
    d
    [
      [0;2;3;1];
      [0;2;1;3];
      [2;0;3;1];
      [2;0;1;3];
      [1;3;2;0];
      [3;1;0;2]
    ];;

Confusion.print_confusion_system stdout s;;

let t = Parikhproc.quotient s;;

Quotient.print stdout t;;

let r = Synthesis.make_region_spaces t;;

let w = Separation.init t;;

Synthesis.synthesize r w;;

Separation.elim_redundent w;;

Separation.print stdout w;;

let n = Separation.get_admissible_regions w;;

List.iter (Region.print stdout) n;;

let n' = Region.superimpose_list n;;

List.iter (Region.print stdout) n';;

Display.net_to_pdf n' "ex3" "ex3.pdf";;
