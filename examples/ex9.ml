(*
 *
 * Example 9: Random recordings obtained by swapping, duplicating and discarding events
 *
 * (c) Benoit Caillaud <Benoit.Caillaud@inria.fr>, INRIA Rennes, France
 *
 *)

let dim = 40;;

let nb = 20;;

let swap a i j =
  let x = a.(i) in
    begin
      a.(i) <- a.(j);
      a.(j) <- x
    end;;

let discard a i =
  a.(i) <- None;;

let dup a i j =
  a.(j) <- a.(i);;

let to_list a =
  let l = ref []
  and n = Array.length a in
    begin
      for i=n-1 downto 0 do
	match a.(i) with
	    None -> ()
	  | Some x -> l := x :: (!l)
      done;
      !l
    end;;

let init n =
  let a = Array.make n None in
    begin
      for i=0 to n-1 do
	a.(i) <- Some i
      done;
      a
    end;;

let op a =
  let n = Array.length a in
    match Random.int 5 with
	0 | 1 | 2 -> swap a (Random.int n) (Random.int n)
      | 3 -> dup a (Random.int n) (Random.int n)
      | 4 -> discard a (Random.int n)
      | _ -> failwith "op: impossible case";;

let gen n =
  let a = init n in
    begin
      while (Random.int (10*n)) <> 0 do
	op a
      done;
      to_list a
    end;;

let gen_list m n =
  let l = ref [] in
    begin
      for i=0 to m-1 do
	l := (gen n) :: (!l)
      done;
      !l
    end;;

let s =
  Parikhproc.make_system_of_word_list
    dim
    (gen_list nb dim);;

Confusion.print_confusion_system stdout s;;

let t = Parikhproc.quotient s;;

Quotient.print stdout t;;

let r = Synthesis.make_region_spaces t;;

let w = Separation.init t;;

Synthesis.synthesize r w;;

Separation.elim_redundent w;;

Separation.print stdout w;;

let n = Separation.get_admissible_regions w;;

List.iter (Region.print stdout) n;;

let n' = Region.superimpose_list n;;

List.iter (Region.print stdout) n';;

Display.net_to_pdf n' "ex9" "ex9.pdf";;
