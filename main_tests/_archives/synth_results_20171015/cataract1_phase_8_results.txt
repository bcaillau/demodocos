=============================================================================
=                     FLIPFLOP EXECUTION REPORTING                          =
=                TF Net Synthesis & #7 Code Generating                      =
=============================================================================

--------------------------------------------------------------
   Configuration
--------------------------------------------------------------

 Input file: cataract1_phase_8.xes
 Classifier choice: default_xes
 Input work file: cataract1_phase_8_input.xes
 Directory output: cataract
 Output files: 
   (x) cataract1_phase_8_graph.dot 
   (x) cataract1_phase_8_seven.xml 
   (x) cataract1_phase_8_events_table.txt 
 Approximation requested : NO 


--------------------------------------------------------------
   Initialisation of synthesis
--------------------------------------------------------------

 Process ending : Initialisation of synthesis
 Process time = 0.000002

 

--------------------------------------------------------------
   Main Synthesis
--------------------------------------------------------------

 Process ending : Main Synthesis
 Process time = 0.002611

 

--------------------------------------------------------------
   Abstract Seven Construction
--------------------------------------------------------------

 Process ending : Abstract Seven Construction
 Process time = 0.008099

 

--------------------------------------------------------------
   TnF Net Marking Graph Generating
--------------------------------------------------------------

 Process ending : TnF Net Marking Graph Generating
 Process time = 0.009625

 

--------------------------------------------------------------
   #7 Code Generating
--------------------------------------------------------------

 Process ending : #7 Code Generating
 Process time = 0.256195

 

--------------------------------------------------------------
   Events Table Generating
--------------------------------------------------------------

 Process ending : Events Table Generating
 Process time = 0.000124

 
