/////////////////////////////////////////////////////////////////////////////////////////////////////// 
//                                                                                                   // 
// ONTOLOGY : Set of events with associated data from ontology                                       // 
//            (including mapping with five references)                                               // 
//                                                                                                   // 
/////////////////////////////////////////////////////////////////////////////////////////////////////// 

 Number of events: 5

  Event 0 (a) : 
     { 
     concept:name : ontology label = "incise 1.1mm knife cornea 3"  |  five reference = -
     lifecycle:transition : ontology label = "complete"  |  five reference = -
     org:ressource : ontology label = "surgeon left hand"  |  five reference = -
     time:timestamp : ontology label = [00:00:16]  |  five reference = - 
     } 

  Event 1 (b) : 
     { 
     concept:name : ontology label = "hold colibri tweezers bulbus oculi 3"  |  five reference = -
     lifecycle:transition : ontology label = "complete"  |  five reference = -
     org:ressource : ontology label = "surgeon right hand"  |  five reference = -
     time:timestamp : ontology label = [00:00:16]  |  five reference = - 
     } 

  Event 2 (c) : 
     { 
     concept:name : ontology label = "incise 1.4mm knife cornea 3"  |  five reference = -
     lifecycle:transition : ontology label = "complete"  |  five reference = -
     org:ressource : ontology label = "surgeon right hand"  |  five reference = -
     time:timestamp : ontology label = [00:01:01]  |  five reference = - 
     } 

  Event 3 (d) : 
     { 
     concept:name : ontology label = "desinfect betaisodona cornea and conjunctiva 2"  |  five reference = -
     lifecycle:transition : ontology label = "complete"  |  five reference = -
     org:ressource : ontology label = "surgeon right hand"  |  five reference = -
     time:timestamp : ontology label = [00:00:23]  |  five reference = - 
     } 

  Event 4 (e) : 
     { 
     concept:name : ontology label = "wash irrigation cannula cornea and conjunctiva 3"  |  five reference = -
     lifecycle:transition : ontology label = "complete"  |  five reference = -
     org:ressource : ontology label = "surgeon left hand"  |  five reference = -
     time:timestamp : ontology label = [00:00:47]  |  five reference = - 
     } 


