=============================================================================
=                     FLIPFLOP EXECUTION REPORTING                          =
=                TF Net Synthesis & #7 Code Generating                      =
=============================================================================

--------------------------------------------------------------
   Configuration
--------------------------------------------------------------

 Input file: exp5.xml
 Classifier choice: default_ispm3
 Input work file: exp5_input.xml
 Directory output: russiandolls
 Output files: 
   (x) exp5_graph.dot 
   (x) exp5_seven.xml 
   (x) exp5_events_table.txt 
 Approximation requested : NO 


--------------------------------------------------------------
   Initialisation of synthesis
--------------------------------------------------------------

 Process ending : Initialisation of synthesis
 Process time = 0.000002

 

--------------------------------------------------------------
   Main Synthesis
--------------------------------------------------------------

 Process ending : Main Synthesis
 Process time = 0.004963

 

--------------------------------------------------------------
   Abstract Seven Construction
--------------------------------------------------------------

 Process ending : Abstract Seven Construction
 Process time = 0.002286

 

--------------------------------------------------------------
   TnF Net Marking Graph Generating
--------------------------------------------------------------

 Process ending : TnF Net Marking Graph Generating
 Process time = 0.002524

 

--------------------------------------------------------------
   #7 Code Generating
--------------------------------------------------------------

 Process ending : #7 Code Generating
 Process time = 0.007907

 

--------------------------------------------------------------
   Events Table Generating
--------------------------------------------------------------

 Process ending : Events Table Generating
 Process time = 0.000631

 

--------------------------------------------------------------
   #7 Code Analysis
--------------------------------------------------------------

 Lenght max of path : 41
 Simple paths number: 6
 Origin paths number: 4
 New paths number: 2
 Number of inclused traces / tested traces : 0 / 0
 Event paths : 
 starting_action : starting_system grabbing_an_object : size_1_russian_doll disassemble_an_object : top_of_size_1_russian_doll positioning_an_object_in_some_place : top_of_size_1_russian_doll grabbing_an_object : smallest_russian_doll fixing_an_object : smallest_russian_doll grabbing_an_object : top_of_size_1_russian_doll fixing_an_object : top_of_size_1_russian_doll positioning_an_object_in_some_place : size_1_russian_doll grabbing_an_object : size_2_russian_doll disassemble_an_object : top_of_size_2_russian_doll positioning_an_object_in_some_place : top_of_size_2_russian_doll grabbing_an_object : size_1_russian_doll fixing_an_object : size_1_russian_doll grabbing_an_object : top_of_size_2_russian_doll fixing_an_object : top_of_size_2_russian_doll positioning_an_object_in_some_place : size_2_russian_doll grabbing_an_object : size_3_russian_doll disassemble_an_object : top_of_size_3_russian_doll positioning_an_object_in_some_place : top_of_size_3_russian_doll grabbing_an_object : size_2_russian_doll fixing_an_object : size_2_russian_doll grabbing_an_object : top_of_size_3_russian_doll fixing_an_object : top_of_size_3_russian_doll positioning_an_object_in_some_place : size_3_russian_doll grabbing_an_object : sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_1 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_1 ; sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_2 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_2 ; sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_3 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_3 ; sugar_tong positioning_an_object_in_some_place : sugar_tong folding_a_drape_to_cover_its_content : towel grabbing_an_object : scissors grabbing_an_object : paper_card cutting_an_object : paper_card ; scissors positioning_an_object_in_some_place : paper_card positioning_an_object_in_some_place : scissors grabbing_an_object_with_both_hands : tray positioning_an_object_in_some_place_with_both_hands : tray 
 starting_action : starting_system grabbing_an_object : size_1_russian_doll disassemble_an_object : top_of_size_1_russian_doll positioning_an_object_in_some_place : top_of_size_1_russian_doll grabbing_an_object : smallest_russian_doll fixing_an_object : smallest_russian_doll grabbing_an_object : top_of_size_1_russian_doll fixing_an_object : top_of_size_1_russian_doll positioning_an_object_in_some_place : size_1_russian_doll grabbing_an_object : size_2_russian_doll disassemble_an_object : top_of_size_2_russian_doll positioning_an_object_in_some_place : top_of_size_2_russian_doll grabbing_an_object : size_1_russian_doll fixing_an_object : size_1_russian_doll grabbing_an_object : top_of_size_2_russian_doll fixing_an_object : top_of_size_2_russian_doll positioning_an_object_in_some_place : size_2_russian_doll grabbing_an_object : size_3_russian_doll disassemble_an_object : top_of_size_3_russian_doll positioning_an_object_in_some_place : top_of_size_3_russian_doll grabbing_an_object : size_2_russian_doll fixing_an_object : size_2_russian_doll grabbing_an_object : top_of_size_3_russian_doll fixing_an_object : top_of_size_3_russian_doll positioning_an_object_in_some_place : size_3_russian_doll grabbing_an_object : sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_1 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_1 ; sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_3 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_3 ; sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_2 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_2 ; sugar_tong positioning_an_object_in_some_place : sugar_tong folding_a_drape_to_cover_its_content : towel grabbing_an_object : scissors grabbing_an_object : paper_card cutting_an_object : paper_card ; scissors positioning_an_object_in_some_place : paper_card positioning_an_object_in_some_place : scissors grabbing_an_object_with_both_hands : tray positioning_an_object_in_some_place_with_both_hands : tray 
 starting_action : starting_system grabbing_an_object : size_1_russian_doll disassemble_an_object : top_of_size_1_russian_doll positioning_an_object_in_some_place : top_of_size_1_russian_doll grabbing_an_object : smallest_russian_doll fixing_an_object : smallest_russian_doll grabbing_an_object : top_of_size_1_russian_doll fixing_an_object : top_of_size_1_russian_doll positioning_an_object_in_some_place : size_1_russian_doll grabbing_an_object : size_2_russian_doll disassemble_an_object : top_of_size_2_russian_doll positioning_an_object_in_some_place : top_of_size_2_russian_doll grabbing_an_object : size_1_russian_doll fixing_an_object : size_1_russian_doll grabbing_an_object : top_of_size_2_russian_doll fixing_an_object : top_of_size_2_russian_doll positioning_an_object_in_some_place : size_2_russian_doll grabbing_an_object : size_3_russian_doll disassemble_an_object : top_of_size_3_russian_doll positioning_an_object_in_some_place : top_of_size_3_russian_doll grabbing_an_object : size_2_russian_doll fixing_an_object : size_2_russian_doll grabbing_an_object : top_of_size_3_russian_doll fixing_an_object : top_of_size_3_russian_doll positioning_an_object_in_some_place : size_3_russian_doll grabbing_an_object : sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_2 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_2 ; sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_1 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_1 ; sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_3 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_3 ; sugar_tong positioning_an_object_in_some_place : sugar_tong folding_a_drape_to_cover_its_content : towel grabbing_an_object : scissors grabbing_an_object : paper_card cutting_an_object : paper_card ; scissors positioning_an_object_in_some_place : paper_card positioning_an_object_in_some_place : scissors grabbing_an_object_with_both_hands : tray positioning_an_object_in_some_place_with_both_hands : tray 
 starting_action : starting_system grabbing_an_object : size_1_russian_doll disassemble_an_object : top_of_size_1_russian_doll positioning_an_object_in_some_place : top_of_size_1_russian_doll grabbing_an_object : smallest_russian_doll fixing_an_object : smallest_russian_doll grabbing_an_object : top_of_size_1_russian_doll fixing_an_object : top_of_size_1_russian_doll positioning_an_object_in_some_place : size_1_russian_doll grabbing_an_object : size_2_russian_doll disassemble_an_object : top_of_size_2_russian_doll positioning_an_object_in_some_place : top_of_size_2_russian_doll grabbing_an_object : size_1_russian_doll fixing_an_object : size_1_russian_doll grabbing_an_object : top_of_size_2_russian_doll fixing_an_object : top_of_size_2_russian_doll positioning_an_object_in_some_place : size_2_russian_doll grabbing_an_object : size_3_russian_doll disassemble_an_object : top_of_size_3_russian_doll positioning_an_object_in_some_place : top_of_size_3_russian_doll grabbing_an_object : size_2_russian_doll fixing_an_object : size_2_russian_doll grabbing_an_object : top_of_size_3_russian_doll fixing_an_object : top_of_size_3_russian_doll positioning_an_object_in_some_place : size_3_russian_doll grabbing_an_object : sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_2 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_2 ; sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_3 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_3 ; sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_1 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_1 ; sugar_tong positioning_an_object_in_some_place : sugar_tong folding_a_drape_to_cover_its_content : towel grabbing_an_object : scissors grabbing_an_object : paper_card cutting_an_object : paper_card ; scissors positioning_an_object_in_some_place : paper_card positioning_an_object_in_some_place : scissors grabbing_an_object_with_both_hands : tray positioning_an_object_in_some_place_with_both_hands : tray 
 starting_action : starting_system grabbing_an_object : size_1_russian_doll disassemble_an_object : top_of_size_1_russian_doll positioning_an_object_in_some_place : top_of_size_1_russian_doll grabbing_an_object : smallest_russian_doll fixing_an_object : smallest_russian_doll grabbing_an_object : top_of_size_1_russian_doll fixing_an_object : top_of_size_1_russian_doll positioning_an_object_in_some_place : size_1_russian_doll grabbing_an_object : size_2_russian_doll disassemble_an_object : top_of_size_2_russian_doll positioning_an_object_in_some_place : top_of_size_2_russian_doll grabbing_an_object : size_1_russian_doll fixing_an_object : size_1_russian_doll grabbing_an_object : top_of_size_2_russian_doll fixing_an_object : top_of_size_2_russian_doll positioning_an_object_in_some_place : size_2_russian_doll grabbing_an_object : size_3_russian_doll disassemble_an_object : top_of_size_3_russian_doll positioning_an_object_in_some_place : top_of_size_3_russian_doll grabbing_an_object : size_2_russian_doll fixing_an_object : size_2_russian_doll grabbing_an_object : top_of_size_3_russian_doll fixing_an_object : top_of_size_3_russian_doll positioning_an_object_in_some_place : size_3_russian_doll grabbing_an_object : sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_3 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_3 ; sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_1 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_1 ; sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_2 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_2 ; sugar_tong positioning_an_object_in_some_place : sugar_tong folding_a_drape_to_cover_its_content : towel grabbing_an_object : scissors grabbing_an_object : paper_card cutting_an_object : paper_card ; scissors positioning_an_object_in_some_place : paper_card positioning_an_object_in_some_place : scissors grabbing_an_object_with_both_hands : tray positioning_an_object_in_some_place_with_both_hands : tray 
 starting_action : starting_system grabbing_an_object : size_1_russian_doll disassemble_an_object : top_of_size_1_russian_doll positioning_an_object_in_some_place : top_of_size_1_russian_doll grabbing_an_object : smallest_russian_doll fixing_an_object : smallest_russian_doll grabbing_an_object : top_of_size_1_russian_doll fixing_an_object : top_of_size_1_russian_doll positioning_an_object_in_some_place : size_1_russian_doll grabbing_an_object : size_2_russian_doll disassemble_an_object : top_of_size_2_russian_doll positioning_an_object_in_some_place : top_of_size_2_russian_doll grabbing_an_object : size_1_russian_doll fixing_an_object : size_1_russian_doll grabbing_an_object : top_of_size_2_russian_doll fixing_an_object : top_of_size_2_russian_doll positioning_an_object_in_some_place : size_2_russian_doll grabbing_an_object : size_3_russian_doll disassemble_an_object : top_of_size_3_russian_doll positioning_an_object_in_some_place : top_of_size_3_russian_doll grabbing_an_object : size_2_russian_doll fixing_an_object : size_2_russian_doll grabbing_an_object : top_of_size_3_russian_doll fixing_an_object : top_of_size_3_russian_doll positioning_an_object_in_some_place : size_3_russian_doll grabbing_an_object : sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_3 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_3 ; sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_2 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_2 ; sugar_tong grabbing_an_object_with_an_instrument : sugar_cube_1 ; sugar_tong positioning_an_object_in_some_place_with_an_instrument : sugar_cube_1 ; sugar_tong positioning_an_object_in_some_place : sugar_tong folding_a_drape_to_cover_its_content : towel grabbing_an_object : scissors grabbing_an_object : paper_card cutting_an_object : paper_card ; scissors positioning_an_object_in_some_place : paper_card positioning_an_object_in_some_place : scissors grabbing_an_object_with_both_hands : tray positioning_an_object_in_some_place_with_both_hands : tray 
 Process ending : #7 Code Analysis
 Process time = 0.001541

 
